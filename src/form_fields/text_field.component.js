import React, { useEffect, useRef} from 'react';
import FormField from '../form-fields/elements/form_field.component'



// Text field template: 
// <TextField 
//   id='description'
//   label = 'Description'
//   maxLength = '310'
//   screen_res = {screen_res}
//   value = {props.state.description ? props.state.description : ''}
//   onChange = {props.onChange}
//   errorText = {getErrorText(errors, step, 'description')} 
//   placeholder = 'Quick summary of your product'
// /> 




// const cl = console.log


export default function TextField (props) {


  // Need to track focus state. Focus in, blur out.
  const [focus_id, setFocus_id] = React.useState('');

  const setIsBlurred = ()  => {
      setFocus_id(null)
  }

  const setIsFocused =  ({ currentTarget })  => {
      setFocus_id(currentTarget.id)
  }


  // Need to track hover state. Mouseover, mouseout.
  const [hover_id, setHover_id] = React.useState('');

  const setMouseout = ()  => {
    setHover_id(null)
  }

  const setMouseover =  ({ currentTarget })  => {
    setHover_id(currentTarget.id)
  }


// props are not extensible so I need to create my own object and add focus_id, setIsFocused, and setIsBlurred to know focus status
  let my_props = {...props}
  my_props["focus_id"] = focus_id
  my_props['setIsFocused'] = setIsFocused
  my_props['setIsBlurred'] = setIsBlurred
  my_props['field_hasFocus'] = my_props.id === focus_id ? true : false 
  my_props['field_hasValue'] = !!my_props.value
  my_props['field_hasError'] = !!my_props.errorText
  my_props['setMouseout'] = setMouseout
  my_props['setMouseover'] = setMouseover
  my_props['field_hasHover'] = my_props.id === hover_id ? true : false 

  // cl("textfield")
  // cl(my_props)

  return <FormField config={my_props} component={content(my_props).props.children} /> 
 
}


const content = (my_props) => {

  let {id} = my_props

  return (
    <div>
      <label 
          htmlFor={id}
          style={{
            minHeight: '82px',   
            width: "100%",
          }}>

          <TextArea {...my_props} />

      </label>
    </div>
  )
}





const TextArea = ({
    id, maxLength, value, screen_res, onChange, 
    setIsBlurred, setIsFocused, field_hasFocus, errorText, 
    placeholder, height, setMouseover, setMouseout, field_hasHover
  }) => {

  const style = () => {

    if (screen_res === 'mobile-portrait') {
      return ({
        minHeight: height ? height : '100px',
        width: '100%',
        fontSize: '18px',
        color: 'black',
        border: 'none',
        borderWidth: '0px',
        textAlign: 'center',
        marginTop: '26px',
        paddingLeft: '29px',
        paddingRight: '29px',
        display: "block",
        overflow: "hidden",
        resize: "none",
        backgroundColor: 'inherit',
        outline: 'none',
      })


    } else {
  // mobile-landscape or laptop
      return ({
        minHeight: height ? height : '60px',
        width: '100%',
        maxWidth: '550px',

        fontSize: '17px',
        color: 'var(--black)',
        backgroundColor: field_hasFocus ? 'white' : field_hasHover ? 'var(--grey100)' : !field_hasFocus && !!errorText ? 'var(--blue500)' : 'inherit',

        borderStyle: 'solid',
        borderColor: field_hasFocus ? '#cfcdc8' : !field_hasFocus && !!errorText ? 'var(--blue900)' : '#cfcdc8',
        borderWidth: '2px',
        borderRadius: '12px',

        paddingLeft: '10px',
        paddingRight: '10px',
        outline: 'none',

      })
    }
  }



  const textareaRef = useRef(null);

  useEffect(() => {
    textareaRef.current.style.height = "0px";
    const scrollHeight = textareaRef.current.scrollHeight;
    textareaRef.current.style.height = scrollHeight + "px";
}, [value]);


  if (typeof value === 'undefined') {value = ''}

  if (screen_res === 'mobile-portrait') {

  return (

    <>
    <div 
      id="counter" 
      style={{
        textAlign: 'right', 
        fontSize: '13px', 
        marginTop: '4px', 
        position: 'absolute', 
        right: '24px', 
        color: 'var(--grey600)'
      }}>
        {maxLength - value.length}
      </div>

    <div style={{
        width: '100%', 
        marginBottom: !!errorText ? '20px' : '0px'
        }}>

      {/* character counter */}


      <textarea 
          id={id}
          maxLength = {maxLength}
          ref ={textareaRef}
          style = {style()}
          value = {value}
          onChange={onChange}
          onFocus = {setIsFocused}
          onBlur = {setIsBlurred}
          />
    </div> 
    </>
  )

  } else {
  // mobile-landscape and laptop
  
    return (

      <div>

        {/* character counter */}
        <div style ={{
            textAlign: 'right', 
            fontSize: '13px', 
            marginTop: '-21px', 
            paddingBottom: '6px', 
            color: 'var(--grey600)',
            maxWidth: '550px',
          }}>
            Max: {maxLength - value.length} chars
        </div>

        <textarea 
            id={id}
            // className = 'form-field-string'
            maxLength = {maxLength}
            ref={textareaRef}
            style={style()}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            onBlur = {setIsBlurred}
            onFocus = {setIsFocused}
            onMouseOver = {setMouseover}
            onMouseOut = {setMouseout}
            />

        </div> 
    )
  }
}









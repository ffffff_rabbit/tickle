import React from 'react';


function expandOptions(selectOptions) {

        return selectOptions.map(selectOptions => {
        return <option key={selectOptions.value} value={selectOptions.value}>{selectOptions.text}</option> ;
    })

}


export default function SelectField({ selectField: {id, label, value, onChange, required, name, selectOptions}  }) {

  return (

            <div style={{maxWidth: '500px'}}>
            <label className="form-field-label" >{label}</label>
            <select
                id={id}
                required={required}
                className = "form-control"
                value={value} 
                onChange = {onChange}
                name = {name}
            >

                option = {expandOptions(selectOptions)}

            </select>

            </div>


  );
}


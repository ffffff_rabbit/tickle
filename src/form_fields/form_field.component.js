import React from 'react';


// About FormField
    // FormField is the wrapper for all form fields. It controls stying and behaviour. 
    // To use it, you construct an input component and then pass it to FormField.
    // Like this:   
    //     return (  <FormField config={my_props} component={<Input {...my_props} /> } /> )
 


// Pass these props!!
    // config = id, label, screen_res, field_hasFocus, field_hasValue, errorText, placeholder


// const cl = console.log

    const FormField = React.forwardRef((props, ref) => {

    let {config, component} = props

    let {id, label, screen_res, field_hasFocus, field_hasValue, errorText, placeholder} = config

    let field_hasPlaceholder = !!placeholder
    let field_hasError = !!errorText

    // cl("component")
    // cl(component)
    // cl("config")
    // cl(config)


    if (screen_res === 'mobile-portrait') { 

        return (

            <label 
                htmlFor={id}
                style={{
                    width: '100%',
                    // backgroundColor: 'red',
                    backgroundColor: !field_hasFocus && !!errorText ? 'var(--blue500)' : 'var(--grey400)',
                    borderStyle: 'solid',
                    borderColor: field_hasFocus? 'var(--grey550)' : !field_hasFocus && !!errorText ? 'var(--blue900)' : 'transparent',
                    borderWidth: '2px',
                    borderRadius: '8px',
                    marginTop: '10px',
                    display: 'inline-block',
                    // maxHeight: '80px',
                    minHeight: '80px',
                }}
            >
    
                <FieldLabel
                    id= {id} 
                    label = {label}
                    screen_res = {screen_res}
                    field_hasFocus = {field_hasFocus}
                    field_hasValue = {field_hasValue}
                />
    
                {field_hasPlaceholder && !field_hasValue && !field_hasError ? <PlaceholderText ref={ref} {...config} /> : null}
    
                {component}
    
                <ErrorText {...config} />
    
    
            </label>
    
    
        )
    } else {

    // mobile-landscape and laptop
        return (

            <label 
                htmlFor={id}
                style={{

                }}
            >
                <FieldLabel
                    id= {id} 
                    label = {label}
                    screen_res = {screen_res}
                    field_hasFocus = {field_hasFocus}
                    field_hasValue = {field_hasValue}
                />
        
                {component}
    
                <ErrorText {...config} ref = {ref}/>

            </label>
        )
    }
})


export default FormField




// const PlaceholderText = ({placeholder, id, focus_id, errorText, value, screen_res}) => {
const PlaceholderText = React.forwardRef(({placeholder, id, focus_id, errorText, value, screen_res}, ref) => {

if (screen_res === 'mobile-portrait') {

    return (

        focus_id !== id && !value
    
          ? <div 
              id="placeholder"
              style={{
                fontSize: '13px', 
                maxWidth: 'inherit', 
                textAlign: 'center', 
                marginTop: '46px',
                position: 'absolute',
                left: '0',
                right: '0',
              }}>
                <label htmlFor={id}>
                  {!!placeholder && focus_id !== id && !errorText && !value ? placeholder : null }
                </label>
            </div> 
          : null
      )
    
} else { return null }
// Placeholder text only used for mobile-portrait
})




// const ErrorText = ({id, field_hasFocus, field_hasValue, errorText, screen_res, alignError}) => {
const ErrorText = React.forwardRef(({id, field_hasFocus, field_hasValue, errorText, screen_res, alignError}, ref) => {


    let justifyContent = ''

    switch(alignError) {
        case 'left' : justifyContent = 'flex-start'; break;
        case 'right' : justifyContent = 'flex-end'; break;
        default: justifyContent = 'center'; }

    if (screen_res === 'mobile-portrait') {
        return (
  
            !!errorText && !field_hasFocus
              ?  <div 
                    id = "error text"
                    style={{
                        maxWidth: 'inherit', 
                        textAlign: 'center', 
                        marginTop: field_hasValue ? '-21px' : '-33px',
                        marginLeft: '20px',
                        marginRight: '20px',
                        marginBottom: '5px',
                    }}>
                    <label 
                        style={{
                        fontSize: '13px', 
                        fontWeight: 'bold',
                        display: 'flex',
                        justifyContent: justifyContent
                        }}
                        htmlFor={id}>
        
                        <div 
                            id = "alert dot"
                            style={{
                            height: '9px', 
                            width: '9px', 
                            minWidth: '9px',
                            borderRadius: '20px', 
                            backgroundColor: 'var(--blue900)',
                            marginRight: '5px',
                            alignSelf: 'center',
        
                            //   position: 'absolute',
                            //   marginTop: '30px',
                            //   marginLeft: '5px',
                        }}/>
        
        
                        {!!errorText && !field_hasFocus 
                        ? <div>{errorText}</div>
                        : <div style={{height: '50px', width: '100%'}}/>}
                    </label>
                </div>
                  
              : null
          )
      
    } else {
    // mobile landscape and laptop

        return (

            !!errorText && !field_hasFocus
            ?  <div 
                  id = "error text"
                  style={{
                      maxWidth: '550px', 
                      height: '15px',
                    //   marginBottom: '5px',

                  }}>
                  <label 
                      style={{
                        // position: 'absolute',
                      fontSize: '13px', 
                      fontWeight: 'bold',
                      display: 'flex',
                      justifyContent: justifyContent,
                      }}
                      htmlFor={id}>
      
                      <div 
                          id = "alert dot"
                          style={{
                          height: '9px', 
                          width: '9px', 
                          minWidth: '9px',
                          borderRadius: '20px', 
                          backgroundColor: 'var(--blue900)',
                          marginRight: '5px',
                          alignSelf: 'center',
                      }}/>
      
      
                      {!!errorText && !field_hasFocus 
                      ? <div>{errorText}</div>
                      : <div style={{height: '50px', width: '100%'}}/>}
                  </label>
              </div>
                
            : <div style={{height: '15px'}} />
        )

    }
  })
  

  // Pass these props!!
    // id= {id} 
    // label = {label}
    // screen_res = {screen_res}
    // field_hasFocus = {true}
    // field_hasValue = {true}

// const cl = console.log

const FieldLabel = ({id, label, screen_res, field_hasFocus, field_hasValue}) => {

    // cl("FieldLabel")
    // cl(field_hasFocus)
    // cl(field_hasValue)
    // cl(screen_res)
    // cl(label)


    const labelStyle = () => {
    // If the field has a value or is focused

      if (screen_res === 'mobile-portrait') {

        if (field_hasValue || field_hasFocus) {

            return ({
                fontSize: '13px',
                lineHeight: '1em',
                fontWeight: 'bold',
                color: 'var(--grey600)',
                position: 'absolute',
                paddingTop: '5px',
                textAlign: 'center',
                width: '100%',
                marginLeft: '-15px',
              })

        // If the field has no value or is not focused
      } else {
        //   cl("else block")
        return ({
          fontSize: '20px',
          fontWeight: 'bold',
          color: 'black',
          position: 'absolute',
          paddingTop: '13px',
          textAlign: 'center',
          width: '100%',
          marginLeft: '-15px',
        //   zIndex: '1'
        })
      }




    } else {
    // mobile-landscape and laptop

    return ({
        display: 'block',
        fontSize: '13px',
        fontWeight: 'bold',
        marginTop: '13px',
        width: '100%',
        marginLeft: '12px',
        paddingBottom: '5px',
        color: field_hasValue? 'var(--grey600)' : 'var(--grey900)',
      })
    } 
}







    if (label) {
        // cl("return " + label )
        return (

            <label 
                htmlFor={id}
                style={labelStyle()}>
                {label}
            </label>
    
        )

    } else {
        return <div />
    }

}
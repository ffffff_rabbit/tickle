import React, { Component } from 'react';
import DragAndDrop from '../elements/drag_drop.component'
import DeleteIcon from '../assets/delete_icon.svg'


// https://codeburst.io/react-image-upload-with-kittens-cc96430eaece
// https://medium.com/@650egor/simple-drag-and-drop-file-upload-in-react-2cb409d88929
// https://www.newline.co/@dmitryrogozhny/how-to-drag-and-drop-files-in-react-applications-with-react-dropzone--c6732c93




// Pass these props!!
//  value = {props.post.image_url}
//  onChange = {props.onChangeImageURL}
// label = 'Image'


// const cl = console.log



export default class ImageField extends Component {

  constructor() {
    super();

    this.state = {
      images: [],
      image_url: '',
      errs: '',
      image_load: false
    }
  }

  componentDidMount() {
    // cl("ImageField")
    if (this.state.image_url === '' && typeof this.props.value !== 'undefined' && this.props.value !== '' && !this.state.image_load) {
      this.setState({
        image_url: this.props.value,
        image_load: true
      })
    }
  }


  onChange = async (e, filelist) => {

    // cl("ImageField onChange")

    this.setState({ errs: '' })

    const errs = []

    let files = filelist

    if (e.type === 'drop') {
      files = Array.from(files)

    } else {
      files = Array.from(e.target.files)
    }

    if (files.length > this.props.max_file_num) {
      const msg = this.props.max_file_num === 1 ? 'Only 1 file can be uploaded at a time' : 'Only' + this.props.max_file_num + ' files can be uploaded at a time'
      errs.push(msg)
      this.setState({ errs: errs })
      // console.log(msg)  
    }

    const formData = new FormData()
    const types = ['image/png', 'image/jpeg', 'image/gif']

    files.forEach((file, i) => {

      if (types.every(type => file.type !== type)) {
        errs.push(`'${file.type}' is not a supported format`)
        this.setState({ errs: errs })
      }

      if (file.size > this.props.max_file_size) {
        let size = ""
        if (1000 < this.props.max_file_size && this.props.max_file_size >= 100000) {
          size = (this.props.max_file_size / 1000) + " KB"
        }

        if (this.props.max_file_size >= 1000000) {
          size = (this.props.max_file_size / 1000000) + " MB"
        }

        errs.push(`'${file.name}' is larger than ` + size)
        this.setState({ errs: errs })
      }

      formData.append(i, file)
    })


    if (errs.length) {
      // return errs.forEach(err => this.toast(err, 'custom', 2000, toastColor))
      errs.forEach(err => console.log(err))
    } else {

      await this.setState({
        ...this.props,
        images: files
      })

      this.props.onChange(this.state)
    }

    //   console.log("formData")
    //   console.log(formData)



  }

  filter = name => {
    return this.state.images.filter(image => image.name !== name)
  }

  removeImage = async id => {
    await this.setState({
      images: this.filter(id),
      image_url: ''
    })
    this.props.onChange(this.state)
  }

  onError = id => {
    console.log("Oops, something went wrong")
    this.setState({ images: this.filter(id) })
  }

  handleDrop = (files) => {
    let fileList = this.state.images

    for (var i = 0; i < files.length; i++) {
      if (!files[i].name) return
      fileList.push(files[i].name)
    }
    this.setState({ images: fileList })
  }



  render() {
    // cl(this.state)

    const { images } = this.state

    const content = () => {
      switch (true) {

        case images.length > 0:
          return <Images
            images={images}
            removeImage={this.removeImage}
            onError={this.onError}
            icon_style={this.props.icon_style}
          />

        case this.state.image_url !== '': 
          return <Image
            image_url={this.state.image_url} 
            removeImage={this.removeImage} 
            onError={this.onError}
            icon_style={this.props.icon_style}
         />

        default:
          return (

            <DragAndDrop handleDrop={this.onChange}>

              <div
                style={this.props.style}>

                <div style={{
                  width: 'inherit',
                  height: 'inherit',
                  maxWidth: 'inherit',
                  backgroundColor: '#F5F5F5',
                  borderColor: this.state.errs.length ? 'red' : '#CFCDCB',
                  borderStyle: 'dashed',
                  borderRadius: '12px',
                }}>
                  <div
                    style={{
                      position: 'absolute',
                      display: 'flex',
                      height: 'inherit',
                      width: 'inherit',
                      maxWidth: 'inherit',
                    }}>
                    <div style={{
                      textAlign: 'center',
                      margin: 'auto',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                      color: this.state.errs.length ? 'red' : '#6E6E73',
                      fontWeight: this.state.errs.length ? '500' : '',
                      fontSize: '13px',
                    }}>
                      {this.state.errs.length ? this.state.errs[0] : 'Click here to select an image'}
                    </div>
                  </div>

                  <input
                    type='file'
                    id='single'
                    onChange={this.onChange}
                    style={{
                      width: 'inherit',
                      height: 'inherit',
                      opacity: '0',
                    }}
                  />
                </div>
              </div>
            </DragAndDrop>


          )
      }
    }

    return (
      <div> {content()} </div>
    )
  }
}




const Images = props => (
  props.images.map((image, i) =>

    <div key={i}
      style={{
        borderColor: 'rgb(207, 205, 203)',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderRadius: '12px',
        width: 'inherit',
        maxWidth: 'inherit',
        boxSizing: 'border-box',
        position: 'relative',
      }}>
      <div
        style={{
          width: '20px',
          height: '20px',
          marginLeft: 'auto',
          position: 'absolute',
        }}
        onClick={() => props.removeImage(image.name)}
      >
        <DeleteIcon fill='red' />
      </div>
      <img
        style={props.icon_style}
        src={URL.createObjectURL(image)}
        alt=''
      />

    </div>
  )
)

const Image = props => (
  // console.log(props),

  <div key="image"
    style={{
      borderColor: 'rgb(207, 205, 203)',
      borderWidth: '1px',
      borderStyle: 'solid',
      borderRadius: '12px',
      width: 'inherit',
      maxWidth: 'inherit',
      boxSizing: 'border-box',
      position: 'relative',
    }}>
    <div 
      style={{
        width: '20px',
        height: '20px',
        marginLeft: 'auto',
        position: 'absolute',
      }}
      onClick={() => props.removeImage("image")} 
    >
        <DeleteIcon  fill='red'/>
    </div>
    <img
      style={props.icon_style}
      src={props.image_url} 
      alt='' />

  </div>
)
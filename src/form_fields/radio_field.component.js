import React from 'react';

import RadioSelected from '../../assets/icons/radio_selected.svg'
import RadioUnselected from '../../assets/icons/radio_unselected.svg'
import CheckIcon from '../../assets/icons/check_icon.svg'


// <RadioButton 
//     label = {['Product is ', <b key={'private'} >Private</b>, ' and only visible to people invited by a Product Admin.']}
//     id = 'visibility'
//     onChange = {onChange_wizard}
//     value = 'private'
//     isSelected = {state_wizard.visibility === 'private' ? true : false}
// /> 






export default function RadioButton ({label, id, isSelected, mobile_title, mobile_label, onChange,  screen_res, value,}) {

    // cl("radio button")
    // cl(props.id)
    // cl(props.value)


if (screen_res === 'mobile-portrait') {
    return (

        <>


        <div 
            style={{
                display: 'flex', 
                justifyContent: 'space-between',
                marginTop: '15px', 
                paddingTop: '5px', 
                paddingBottom: '5px'
            }} 
            id={id} 
            value={value} 
            onClick={onChange}>




            <div 
                id="radio label" 
                style={{
                    fontSize: '15px',
                }}
            >
                <div 
                    id="radio label" 
                    style={{
                        color: 'black',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        paddingBottom: '6px',
                    }}
                >
                    {mobile_title}   
                </div> 


                {mobile_label}   
            </div> 

            <div 
                id='check' 
                style={{
                    width: '20px', 
                    minWidth: '20px', 
                    marginLeft: '10px',
                    alignSelf: 'center',
                }}>

                {isSelected
                    ? <CheckIcon fill={'#E97314'}/>
                    : <CheckIcon fill={'transparent'}/>
                }           

            </div >



        </div>

        <div style={{
            backgroundColor: 'var(--grey550)',
            height: '1px',
            width: '100%',
            marginTop: '15px'
        }}/>
        </>

    )


} else {
        return (
            <div 
                style={{display: 'flex', marginTop: '15px', paddingTop: '5px', paddingBottom: '5px'}} 
                id={id} 
                value={value} 
                onClick={onChange}>

                <div id='radio circles' style={{height: '15px', width: '15px', minWidth: '15px', marginRight: '8px'}}>

                    {isSelected
                        ? <RadioSelected fill={'#767676'}/>
                        : <RadioUnselected fill={'#767676'}/>
                    }           

                </div >

                <div id="radio label" >
                    {label}   
                </div> 
            </div>
        )

    }
}

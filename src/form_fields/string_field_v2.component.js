import React from 'react';
import FormField from '../form-fields/elements/form_field.component'



// String field template: 
// <StringField 
//     id = "owner_name"
//     label = "Owner Name"
//     type = "text"
//     // ref: props.textInput, 
//     screen_res = {screen_res} // mobile-portrait, mobile-landscape, or laptop
//     value = {props.state.owner_name ? props.state.owner_name : ''}
//     onChange = {props.onChange}
//     errorText = {getErrorText(errors, step, 'owner_name')}
//     placeholder = "Required - for display on your product card"
// /> 



// const cl = console.log 


const StringField = React.forwardRef((props, ref) => {

  // Need to track focus state. Focus in, blur out.
    const [focus_id, setFocus_id] = React.useState('');

    const setIsBlurred = ()  => {
        setFocus_id(null)
    }

    const setIsFocused =  (event)  => {
      // cl("setIsFocused")
      //   // cl(event.relatedTarget)
      //   // cl(target)
      //   let e = {...event}
      //   cl(e)
        setFocus_id(event.currentTarget.id)

    }


  // Need to track hover state. Mouseover, mouseout.
  const [hover_id, setHover_id] = React.useState('');

  const setMouseout = ()  => {
    setHover_id(null)
  }

  const setMouseover =  ({ currentTarget })  => {
    setHover_id(currentTarget.id)
  }


// props are not extensible so I need to create my own object and add focus_id, setIsFocused, and setIsBlurred to know focus status
    let field_hasFocus = props.id === focus_id ? true : false
    let field_hasHover = props.id === hover_id ? true : false 

    let my_props = {...props}
      // let {id, label, value, onChange, ref, required, name, autoComplete, type, placeholder, className, errorText, screen_res, prepopulate } = props

      my_props["focus_id"] = focus_id
      my_props['setIsFocused'] = setIsFocused
      my_props['setIsBlurred'] = setIsBlurred
      my_props['setMouseout'] = setMouseout
      my_props['setMouseover'] = setMouseover
      my_props['field_hasFocus'] = field_hasFocus 
      my_props['field_hasValue'] = !!props.value
      my_props['field_hasHover'] = field_hasHover
      my_props['ref'] = ref

    // cl("String Field")
    // cl(my_props)

// prepopulate the value if it exists and onfocus
    if (field_hasFocus && !!my_props.prepopulate && my_props.value === '') {
        my_props.value = my_props.prepopulate 
    }

// clear prepopulated values on blur, if the user didn't add anything
    if (!field_hasFocus && my_props.value === my_props.prepopulate) {
        my_props.value = ''
        my_props.onChange({
            target: {
                id: my_props.id,
                value: ''
            }
        })
    }
    



  return (  
    <FormField 
      config={my_props} 
      component={<Input {...my_props}
      ref = {ref} /> } 
    /> )
    
})



const Input = React.forwardRef((my_props, ref) => {

  // cl(my_props)
  let {screen_res, field_hasFocus, field_hasHover, placeholder, errorText} = my_props


    const onClick = (e) => {

      if (e.currentTarget) {
        e.currentTarget.selectionStart = e.currentTarget.value.length;
        e.currentTarget.selectionEnd = e.currentTarget.value.length;
      }

}
  


    const inputStyle = () => {

    if (screen_res === 'mobile-portrait') {
      return ({
        width: '100%',
        fontSize: '18px',
        color: 'black',
        borderWidth: '0px',
        textAlign: 'center',
        marginTop: '28px',
        outline: 'none',
        backgroundColor: 'inherit',
        paddingLeft: '29px',
        paddingRight: '29px',
        })

      
    } else {
    // mobile-portrait and landscape

      return({

        width: '100%',
        // backgroundColor: 'red',
        backgroundColor: field_hasFocus ? 'white' : field_hasHover ? 'var(--grey100)' : !field_hasFocus && !!errorText ? 'var(--blue500)' : 'inherit',
        borderStyle: 'solid',
        borderColor: field_hasFocus ? '#cfcdc8' : !field_hasFocus && !!errorText ? 'var(--blue900)' : '#cfcdc8',
        borderWidth: '2px',
        borderRadius: '12px',
        // marginTop: '10px',
        color: 'var(--black)',
        height: '44px',
        // backgroundColor: field_hasFocus ? 'white' : '#f5f5f5',
        // borderColor: '#cfcdc8',
        maxWidth: '550px',
        paddingLeft: '10px',
        paddingRight: '10px',
        outline: 'none',
        fontSize: '17px',

      })
    }

    }




    return (
        <div style = {{
          minHeight: screen_res === 'mobile-portrait' ? '76px' : null,
          WebkitAutoFill: '-webkit-text-fill-color: yellow !important',
          // width: '100%',
        }}>
            <input 
                style={inputStyle()}
                id={my_props.id}
                ref={ref}
                value={my_props.value}
                onChange = {my_props.onChange}
                autoComplete ={my_props.autoComplete}
                type = {my_props.type}
                onBlur = {my_props.setIsBlurred}
                onFocus = {my_props.setIsFocused}
                onClick = {onClick}
                placeholder = {screen_res === 'mobile-portrait' ? null : placeholder}
                onMouseOver = {my_props.setMouseover}
                onMouseOut = {my_props.setMouseout}
            />
          </div> 
    )


  })


  





export default StringField






import React from 'react';
import ReactDOM from 'react-dom';
import { GetSharedData, BroadcastSharedData} from '../helpers/shared_data'
import { Views } from '../App'


// let cl = console.log


        // import {toggleModal} from '../elements/modal.component'


        // <button 
        //     id="sign_in" 
        //     variant="link" 
        //     // style={auth_style} 
        //     onClick = {() => toggleModal(<div>Passthrough</div>)}
        // > 
        // Pop the Modal! 
        // </button>

let modal_content = ""
let modal_data = ""
        
export function toggleModal (component, data) {

    const isModalOpen = () => {
        switch (GetSharedData()?.isModalOpen){
            case 'true': return 'true'
            default: return 'false'
	    }
    }

    if (isModalOpen() !== 'true') {

        modal_content = component
        if (data !== undefined) {
            modal_data = data
        }

        BroadcastSharedData('isModalOpen', 'true')

        const modalRoot = document.getElementById('root')
    
        const el = document.createElement('modal')
    
        if (modalRoot) {modalRoot.appendChild(el)}

    } else {
        BroadcastSharedData('isModalOpen', 'false')

        const modalRoot = document.getElementById('root')
        const modal_node = document.getElementsByTagName('modal')

        modalRoot.removeChild(modal_node[0])

    }
    
}



export const Modal = () => {

    if (GetSharedData()?.isModalOpen === 'true') {

        const el = document.getElementsByTagName('modal')[0]

        if (el !== null && el !== undefined){

            return ReactDOM.createPortal( Content(), el)
    
        } else { return null }
    
    } else { return null }
    
}



const Content = () => {

    const content = () => {
        switch (modal_content){
        case 'Views.ManageProfile': return <Views.ManageProfile />
        case 'Views.ConnectionsNew': return <Views.ConnectionsNew />
        case 'Views.VideoPlayer': return <Views.VideoPlayer path={modal_data} />
        case 'Views.ImageViewer': return <Views.ImageViewer path={modal_data} />
        case 'Views.EmbeddableViewer': return <Views.EmbeddableViewer path={modal_data} />
        default: return <div/>
        }
    }

    return (
			
        <div 
            id = "modal_shell"
            style={{
                display: 'flex',
                justifyContent: 'center',
            }}
        >

            {/* background opacity */}
            <div 
                id = "modal_opacity"
                style={{
                    height: '100%', 
                    width: '100%',
                    backgroundColor: 'black',
                    opacity: '.35',
                    position: 'fixed',
                    top: '0',
                    zIndex: '10',

                }} 
                onClick = {toggleModal}
                role= "none"
            /> 

        {/* Main modal area */}

           
            <div 
                id = "modal_content"
                style={{
                    height: '100%',
                    width: '100%',
                    position: 'fixed',
                    top: '0',
                    display: 'table',
                    zIndex: '11',
                }}
            >
                <div 
                id = "modal_container"
                style={{
                    display: 'table-cell',
                    verticalAlign: 'middle',
                }}
                onClick = {(e) => {if (e.target.id === 'modal_container'){toggleModal()}}}
                >
                {content()}
                </div>

            </div>
        </div>
    )

}




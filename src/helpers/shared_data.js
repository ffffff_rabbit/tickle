// import { window } from '../helpers/browser_stub'


// Shared Data - Subscribe and Broadcast
    // A simple Redux replacement. Deep child components can use this to broadcast data to other components, 
    // regardless of where the broadcasters and listeners are in the stack. To implement: 
    // (1) put this in the constructor:  this.setSharedData = (value) => {this.setState({'shared_data' : value})}
    // (2) put this in componentDidMount:  Helpers.subscribeSharedData(this.setSharedData)
    // (3) put this in componentWillUnmount:  Helpers.unsubscribeSharedData(this.setSharedData)
    // (4) shared data can be accessed from this.state.shared_data
    // (5) any component can broadcast new values (keys must be unique):  Helpers.broadcastSharedData('screen_res', this.screen_res)


// takes an array and returns it without a specified element. 
function arrayRemove(arr, value) { return arr.filter(function(ele){ return ele !== value; });}


let cl = console.log

let subscribers = []

export const SubscribeSharedData = (callback) => {
  subscribers.push(callback)
  callback(shared_data)
}

export const UnsubscribeSharedData = (callback) => {
  // cl(subscribers)
  subscribers = arrayRemove(subscribers, callback)
  // cl(subscribers)

}


let shared_data = {}
export const BroadcastSharedData = async (key, value) => {
  cl("Broadcasting: " + key + ": " + value)
  shared_data[key] = value

  for (let subscriber of subscribers) {

      // await subscriber(shared_data)
      subscriber(shared_data)
   
  }

}

export const DeleteSharedData = async (key) => {
  delete shared_data[key]
}

export const GetSharedData = () => {
    return shared_data
}


export const UpdatePage = (page) => {

  if (shared_data.page !== window.location.href){
    // cl("SD Page Change")
    BroadcastSharedData('page', page)
    // BroadcastSharedData('active_headings', [])
    // cl(page)
  } 

  // this.BroadcastSharedData ("active_headings", [])

}
import { useEffect, useRef } from 'react';
import { BroadcastSharedData } from '../helpers/shared_data';
// import { window } from './browser_stub';

const cl = console.log 

// ===================================================================================================


// Formats a date string to something human readable
export const formatDate = (string) => {

    const utc = new Date(string).toUTCString();

    function changeDate(str) {
        var months = {
            Jan: 'January',
            Feb: 'February',
            Mar: 'March',
            Apr: 'April',
            May: 'May',
            Jun: 'June',
            Jul: 'July',
            Aug: 'August',
            Sep: 'September',
            Oct: 'October',
            Nov: 'November',
            Dec: 'December',
    }
  
        var str2 = new Date(str).toLocaleString()
        str2 = str2.split(/,/)[0].split(/\//g)
        str2 = months[str.split(/,/)[1].trim().split(/ /g)[1]] + ' ' + str.split(/,/)[1].trim().split(/ /)[0] + ', ' + str.split(/ /)[3]
        
        return str2
    }

    return changeDate(utc)

}





// ===================================================================================================

// This manages screen width to handle device layouts.

export const getScreenWidth = () => {

    // cl(window.innerWidth)

    if (window.innerWidth > 1024){ return 'laptop' } 
    if (window.innerWidth <= 1024 && window.innerWidth >= 500){ return 'mobile-landscape' } 
    if (window.innerWidth < 500 ){ return 'mobile-portrait' } 
    
    }

let timeout = false
let screen_res = ''

export const throttledHandleWindowResize = () => {

        if (!timeout) {

            timeout = true;

            // cl(this.getScreenSize())
            // cl(screen_res)

            screen_res = getScreenWidth()
            cl(screen_res)
            // cl("broadcasting")
            BroadcastSharedData('screen_res', screen_res)
            setTimeout( () => { timeout = false }, 50)
        }

    return screen_res

    }





// ===================================================================================================
// window.removeEventListener('scroll', this.throttledHandleScroll);
// window.addEventListener('scroll', this.throttledHandleScroll);

//   throttledHandleScroll = () => {

//     if (!timeout) {

//         timeout = true;
        
//         let y_offset = window.pageYOffset
//         this.setState({ y_offset : y_offset })
        
//         // cl("y_offset")
//         // cl(y_offset)

//         setTimeout( () => { timeout = false }, 50)
//     }

//   }





// Create a sleep / poll to do something at a particular interval

    // useInterval(() => {
    //   if (props.page === 'settings'){
    //     BroadcastSharedData('new_events', Math.random())
    //   }
    // }, 1000 * 1);

export const useInterval = (callback, delay) => {

  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);


  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}
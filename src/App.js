import React, { Component, useState, useEffect } from 'react'
import { invoke } from '@tauri-apps/api/tauri'
import { open } from '@tauri-apps/api/dialog';
// import RightChevron from '../src/assets/right_chevron.svg'
import { toggleModal, Modal } from '../src/elements/modal'
import { SubscribeSharedData, GetSharedData, UnsubscribeSharedData, BroadcastSharedData, DeleteSharedData } from '../src/helpers/shared_data'
import { appWindow } from '@tauri-apps/api/window'
// import { emit, listen } from '@tauri-apps/api/event';

import ImageField from './form_fields/image_field.component'

import CloseIcon from './assets/X_icon.svg'
import DeleteIcon from './assets/delete_icon.svg'
import PersonIcon from '../src/assets/person.svg'
import PaperclipIcon from './assets/paperclip_icon.svg'
import PlayIcon from './assets/play_icon.svg'
import FileIcon from './assets/file_icon.svg'
// import ErrorIcon from './assets/error_icon.svg'



const cl = console.log


export default class App extends Component {

  constructor() {
    super();
    this.state = {
    };
    this.setSharedData = (value) => {
      this.setState({ 'shared_data': value })
    }
  }

  dropRef = React.createRef()

  componentDidMount() {
    SubscribeSharedData(this.setSharedData)
    appWindow.listen('new_sysevent', Actions.updateEvents)
    appWindow.listen('new_person', Actions.updatePeople)
    appWindow.listen('new_message', Actions.updateMessages)
    appWindow.listen('send_msg_to_ui', Actions.sendMsgToUI)

    // These suppress errant drag / drop events
    let div = this.dropRef.current
    div.addEventListener("drop", this.handleDrop)
    div.addEventListener('dragover', this.handleDrop)
    // document.getElementsByTagName('modal_content').addEventListener('dragover', cl("modal listener"))
  }

  componentWillUnmount() {
    let div = this.dropRef.current
    div.removeEventListener('drop', this.handleDrop)
    div.removeEventListener('dragover', this.handleDrop)
    UnsubscribeSharedData(this.setSharedData)
  }

  // Capture and suppress any errant drag/drop events
  handleDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }

  render() {
    cl("render")

    Actions.getConfig()
    Actions.onAppStart()

    return (
      <x-page ref={this.dropRef} >
        <Modal />
        <Views.Menu />
        <Views.Content />

        {/* we use this to suppress errant drag / drop events */}
        <div style={{ height: '100vh' }} />
      </x-page>
    )
  }
};


// Global data
const image_types = ['.png', '.jpeg', '.gif', '.jpg']
const video_types = ['.mov']
const embeddable_types = ['.pdf']
let all_events = []
let people_not_followed = []
let connections = []
let people_followed = []
let client_messages = []
let messages = []
let config = {} //my_peer_id, my_person_id, my_name, listeners[]
let on_start = true
let server_config = {}
let connection_count = 0
let assets = []
let box_height = 147
let downloads = new Map()
let locals = new Map()
let isProcessing = false


const Actions = {

  sendMsgToUI: function (msg) {
    let json = JSON.parse(msg.payload)

    switch (json.event) {

      case "Refresh": {
        BroadcastSharedData('refresh', json.msg);
        break;
      }

      case "SaveProfileSuccess": {

        if (GetSharedData().isModalOpen === 'true') {
          cl("SaveProfileSuccess")
          BroadcastSharedData('SaveProfileSuccess', json.msg);
        }
        break;
      }

      case "NewConnectionSuccess": {
        BroadcastSharedData('NewConnectionSuccess', json.msg);
        break;
      }

      case "NewConnectionCount": {
        if (json.msg !== connection_count) {
          connection_count = json.msg
          BroadcastSharedData('NewConnectionCount', json.msg);
        }
        break;
      }

      case "NewInternalStubStatus": {
        // cl("NewLocalFileStatus")
        // cl(json.msg)
        let files = JSON.parse(json.msg)
        // cl(files)

        locals.clear()

        files.forEach(file => {
          locals.set(file.path, file)
        })
        BroadcastSharedData('NewLocalFileStatus', locals.size);
        break;
      }

      case "NewDownloadFileStatus": {
        cl("NewDownloadFileStatus")
        if (JSON.stringify(GetSharedData().NewLocalFileStatus) !== json.msg) {
          let files = JSON.parse(json.msg)
          cl(files)
          files.forEach(file => {
            downloads.set(file.cid, file)
          })

          cl(downloads)
          BroadcastSharedData('NewDownloadFileStatus', files);
        }
        break;
      }

      case "NewAppStatus": {
        cl("NewAppStatus")
        if (JSON.stringify(GetSharedData().app_status) !== json.msg) {
          BroadcastSharedData('app_status', json.msg);
        }
        break;
      }

      default: cl("ERROR!! | sendMsgToUI | unknown event: " + json.event);

    }
  },

  addMessage: function (new_message) {
    messages.push(new_message)
    // if (messages.length > 500) {
    //   messages.shift()
    // }
    if (GetSharedData().page === 'home') {
      BroadcastSharedData('last_message', new_message)
    }
  },


  updateEvents: function (e) {
    if (JSON.stringify(all_events[0]) !== e.payload) {
      // cl(JSON.stringify(all_events[0]))
      // cl(e.payload)
      const new_event = JSON.parse(e.payload)
      all_events.push(new_event)

      switch (new_event.id) {
        case 1: config.my_peer_id = new_event.message; break;
        case 2: config.my_person_id = new_event.message; break;
        default: break;
      }

      if (all_events.length > 100) {
        all_events.shift()
      }
      if (GetSharedData().page === 'settings') {
        BroadcastSharedData('last_event', e.payload)
      }
    }
  },


  updateMessages: function (e) {
    if (JSON.stringify(messages[0]) !== e.payload) {

      Actions.addMessage(JSON.parse(e.payload))

    }
  },


  updatePeople: function (e) {
    if (JSON.stringify(people_not_followed[0]) !== e.payload) {

      // cl(JSON.stringify(people_not_followed))
      // cl(e.payload)

      // const new_person = JSON.parse(e.payload)
      // people.push(new_person)
      // if (people.length > 500){
      //   people.shift()
      // }
      if (GetSharedData().page === 'people') {
        BroadcastSharedData('last_person', e.payload)
      }
    }
  },


  openFileDialog: async function (e) {

    const selected_file_paths = await open({
      multiple: true,
    });

    if (Array.isArray(selected_file_paths)) {
      // TODO: We need to check here that we're not adding dups

      const payload = {
        paths: selected_file_paths
      }

      appWindow.emit('ui', { command: "add_files", payload: JSON.stringify(payload) })

      BroadcastSharedData("isLocalFileProcessing", true)

    } else if (selected_file_paths === null) {
      // user cancelled the selection
    } else {
      // user selected a single file
      console.error("single file selection isn't handled")
    }
  },


  publishMessage: function (msg, file_cids) {

    let timestamp = new Date(Date.now()).toISOString()

    if (msg !== "") {
      const payload = {
        person_id: config.my_person_id,
        msg: msg,
        file_cids: file_cids,
      }

      appWindow.emit('ui', { command: "publish_msg", payload: JSON.stringify(payload) })

      Actions.addMessage({
        "content": msg,
        "person_id": config.my_person_id ? config.my_person_id : "2489h249t828th98e",
        "timestamp": timestamp,
        "file_cids": file_cids,
      })
    }
  },


  getConfig: function () {
    invoke('get_config')
      .then((res) => {
        server_config = JSON.parse(res)
        if (JSON.stringify(server_config) !== JSON.stringify(config)) {
          config = server_config
          BroadcastSharedData("config", server_config)
        }
      })
      .catch((e) => console.error(e));
  },


  updatePage: function (page) {
    localStorage.setItem("page", page)
    BroadcastSharedData('page', page)
  },

  onAppStart: function () {

    if (GetSharedData().isModalOpen === 'true') {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'auto'
    }

    invoke("get_app_status")
      .then((res) => {
        let status = JSON.parse(res)

        if (GetSharedData().app_status !== status){
          BroadcastSharedData('app_status', status)
        }
      })

    invoke('get_connection_count')
      .then((res) => {
        const count = JSON.parse(res)

        if (connection_count !== count) {
          connection_count = count
          BroadcastSharedData("connection_count", connection_count)
        }

        // Open the New Connections page if there are no connections
        if (count === 0 && on_start === true) {
          toggleModal("Views.ConnectionsNew")
          BroadcastSharedData('page', 'home')
          on_start = false
        }

        if (count > 0 && on_start === true) {
          let page = localStorage.getItem("page") ? localStorage.getItem("page") : 'home'
          Actions.updatePage(page)
          on_start = false
        }

      })
      .catch((e) => console.error(e));
  },



  getIcon: function (cid) {

    const path = Actions.getPathForCid(cid)

    if (path === "") {
      return (
        <div style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          maxHeight: 'inherit',
          maxWidth: 'inherit',
          width: 'inherit',
          height: 'inherit',
          borderStyle: 'solid',
          borderWidth: '1px',
          borderColor: '#a3a3a3',
          borderRadius: '100%',
          backgroundColor: 'white',
        }}>
          <PersonIcon height='26px' fill='#a3a3a3' />
        </div>
      )
    } else {
      return (
        <div style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          maxHeight: 'inherit',
          maxWidth: 'inherit',
          width: 'inherit',
          height: 'inherit',
          borderStyle: 'solid',
          borderWidth: '1px',
          borderColor: '#a3a3a3',
          borderRadius: '100%',
          // backgroundColor: 'white',
        }}>
          <img
            style={{
              zIndex: '-1',
              borderRadius: '50%',
              objectFit: 'cover',
              width: 'inherit',
              height: 'inherit',
              maxHeight: 'inherit',
              maxWidth: 'inherit',
            }}
            alt="icon"
            src={path}
          />
        </div>

      )
    }
  },

  getPathForCid: function (cid) {
    let path = ""
    // cl(cid)
    if (cid !== null && cid !== undefined && assets.length > 0) {
      assets.forEach((asset) => {
        if (asset[0] === cid) {
          path = asset[1]
        }
      })
    }

    // cl(assets)

    if (cid !== null && cid !== undefined && path === "" && !assets.some(asset => { return asset[0] === cid })) {
      // cl("TODO: unify local files with msgbox")
      invoke('get_internal_url_for_file', { cidStr: cid })
        .then((path) => {
          if (path !== "" && !assets.some(asset => { return asset[0] === cid })) {
            assets.push([cid, path])
            BroadcastSharedData('refresh', "getIcon")
          }
        })
        .catch((e) => console.error(e));
    }
    return path
  },

  removeLocalFile: function (old) {
    cl(locals)
    locals.delete(old.path)
    cl(locals)

    let cid = old.cid
    cl(cid)

    const payload = {
      cid: cid,
    }
    appWindow.emit('ui', { command: "delete_file_by_cid", payload: JSON.stringify(payload) })

  },

  truncateFileName: function (str) {

    if (str.length > 16) {
      return str.substr(0, 8) + '...' + str.substr(str.length - 8, str.length);
    }
    return str;

  },

}


export const Views = {

  Menu: function () {

    let page = GetSharedData().page;

    const truncate = (name, limit) => {

      if (name !== undefined) {
        if (name.length <= limit) {
          return name
        } else {
          return name.slice(0, limit) + "..."
        }
      } else {
        return "My Name"
      }


    }

    // cl(config)
    const cid = config.my_icon_cid
    const icon = Actions.getIcon(cid);
    const my_name = truncate(config.my_name, 14)


    return (
      <x-menu>

        <x-menu-top>

          <x-logo>
            tickle
          </x-logo>

          <x-menu-top-link
            style={{
              color: page === 'home' ? 'black' : '',
              backgroundColor: page === 'home' ? '#ECE0F2' : '',
              borderColor: page === 'home' ? '#630090' : '#f3f3f3',
            }}
            onClick={() => Actions.updatePage('home')}
          >
            HOME
          </x-menu-top-link>

          <x-menu-top-link
            style={{
              color: page === 'people' ? 'black' : '',
              backgroundColor: page === 'people' ? '#ECE0F2' : '',
              borderColor: page === 'people' ? '#630090' : '#f3f3f3',
            }}
            onClick={() => Actions.updatePage('people')}
          >
            PEOPLE
          </x-menu-top-link>

          <x-menu-top-hr />

          <x-menu-top-link

            style={{
              color: page === 'invite' ? 'black' : '',
              backgroundColor: page === 'invite' ? '#ECE0F2' : '',
              borderColor: page === 'invite' ? '#630090' : '#f3f3f3',
            }}
            onClick={() => Actions.updatePage('invite')}
          >
            INVITE TO TICKLE
          </x-menu-top-link>

        </x-menu-top>

        <x-menu-btm>

          <Views.OnlineLed
            app_status={GetSharedData().app_status}
            connection_count={connection_count}
          />

          <x-menu-btm-hr />

          <x-user
            style={{
              display: 'flex',
              paddingLeft: '15px',
              paddingTop: '20px',
              paddingBottom: '20px'
              ,
            }}>
            <div style={{
              height: '44px',
              width: '44px',
              maxWidth: '44px',
              maxHeight: '44px',
            }}>

              {icon}

            </div>

            <x-user-profile-links
              style={{
                fontSize: '13px',
                paddingLeft: '15px',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}>
              <div style={{
                fontWeight: 'bold',
                paddingBottom: '5px',
              }}>
                {my_name}
              </div>
              <div style={{
                color: 'var(--grey900)',
                fontWeight: '500',
                cursor: 'pointer'
              }}
                id="manage_profile"
                onClick={(e) => { if (e.target.id === 'manage_profile') { toggleModal("Views.ManageProfile") } }}
              >
                Manage Profile

              </div>

            </x-user-profile-links>

          </x-user>
        </x-menu-btm>
      </x-menu>
    )
  },

  OnlineLed: function (props) {

    const flashAnimation = `@keyframes flash {
      0% { opacity: 0.8; }
      50% { opacity: 0.2; }
      100% { opacity: 0.8; }
    }`;

    const styleSheet = document.createElement("style");
    styleSheet.type = "text/css";
    styleSheet.innerText = flashAnimation;
    document.head.appendChild(styleSheet);

    let is_connected = false;
    if (props.app_status === "Online") {
      is_connected = true;
    }
  
    return (
      <div style={{
        display: 'flex',
        alignItems: 'center',
        paddingLeft: '10px',
        marginLeft: '6px',
        lineHeight: '31px',
        cursor: 'pointer',
      }}>
        <div style={{
          height: '11px',
          width: '11px',
          borderRadius: '100%',
          borderStyle: 'solid',
          borderWidth: '1px',
  
          backgroundImage: is_connected
            ? 'radial-gradient(rgb(0, 255, 35), rgb(11, 203, 0))'
            : 'radial-gradient(rgb(0, 255, 35), rgb(11, 203, 0))',
  
          borderColor: is_connected
            ? 'rgb(12, 214, 0)'
            : 'rgb(12, 214, 0)',
  
          boxShadow: is_connected
            ? '0 0 6px 1px #19ff00'
            : '0 0 6px 1px #19ff00',
  
          animation: !is_connected && 'flash 2s infinite',
        }} />
  
        <div
          style={{
            fontWeight: 'bold',
            color: 'var(--grey600)',
            fontSize: '13px',
            paddingLeft: '5px',
            marginTop: '5px',
            marginBottom: '5px',
            marginLeft: '6px',
            marginRight: '10px',
            lineHeight: '31px',
            cursor: 'pointer',
          }}
          onClick={() => Actions.updatePage('connections')}
        >
          {!is_connected ? 'CONNECTING' : 'ONLINE (' + props.connection_count + ')'}
        </div>
      </div>
    );
  },


  Content: function () {

    let page = GetSharedData().page

    return (
      <x-content>
        <Views.Home page={page} />
        <Views.People page={page} />
        <Views.Invite page={page} />
        <Views.Connections page={page} />
        <Views.Settings page={page} />
      </x-content>
    )
  },

  PageHeader: function (props) {
    return (
      <>
        <x-page-title>
          {props.page_title}
        </x-page-title>
      </>
    )
  },

  Tabs: function () {

    let page = GetSharedData().page

    let tabs = [
      ["settings", ["Config", "System Log"]],
      ["people", ["Not Followed", "Following"]]
    ]

    let display = []

    tabs.forEach(row => {
      if (row[0] === page) {
        row[1].forEach(tab => {
          display.push(
            <x-tab
              key={tab}
              onClick={() => BroadcastSharedData("tab", tab)}
              style={{
                color: GetSharedData().tab === tab ? 'var(--grey900)' : 'var(--grey600)',
                borderBottomColor: GetSharedData().tab === tab ? "var(--grey900)" : "",
                borderBottomWidth: GetSharedData().tab === tab ? '2px' : "",
                borderBottomStyle: GetSharedData().tab === tab ? 'solid' : "",
              }}
            >
              {tab}
            </x-tab>
          )
        });
        return display
      } else {
        return null
      }
    })

    return (
      <x-tabs>{display}</x-tabs>
    )
  },


  FileIcon: function (file) {
    cl(file)
    const pct_processed = file && file.pct_processed ? file.pct_processed : 0

    const path = file ? Actions.getPathForCid(file.cid) : ""
    const file_ext = file && file.file_ext !== undefined ? file.file_ext : ""

    cl(path)
// cl(file_ext)

    if (pct_processed < 100) {
      isProcessing = true
      return (


        <div style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%', 
          height: '100%',
          backgroundColor: 'white',

          borderColor: 'rgb(207, 205, 203)',
          borderWidth: '1px',
          borderStyle: 'solid',
          borderRadius: '12px',
        }}>
          <Views.ProgressWidget pct_processed={pct_processed} />
        </div>
      )
    } else {
      // file.pct_processed === 100
      if (image_types.includes(file_ext)) {
        return (
          <img
            style={{
              borderColor: 'rgb(207, 205, 203)',
              borderWidth: '1px',
              borderStyle: 'solid',
              borderRadius: '12px',
              maxHeight: 'inherit',
              height: '100%',
              width: '100%',
              objectFit: 'cover',
            }}
            src={path}
            alt=''
            onClick={() => { toggleModal("Views.ImageViewer", path) }}
          />
        )
      } else if (video_types.includes(file_ext)) {
        // cl(base + file.cid + file.file_ext);
        return (
          <div
            id='video_player'
            style={{
              position: 'relative',
              borderColor: 'rgb(207, 205, 203)',
              borderWidth: '1px',
              borderStyle: 'solid',
              borderRadius: '12px',
              // maxHeight: 'inherit',
              height: '98%',
              // width: '100%',
              objectFit: 'cover',
              cursor: 'pointer',
            }}
            onClick={() => { toggleModal("Views.VideoPlayer", path) }}
          >
            <div style={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%,-50%)',
              zIndex: '1',
              width: '50%',
              maxWidth: "60px",
            }}>
              <PlayIcon
                fill='black'
                fillOpacity='.45'
              />
            </div>
            <video
              src={path}
              style={{
                borderRadius: '12px',
                maxHeight: 'inherit',
                maxWidth: 'inherit',
                height: '100%',
                width: '100%',
                objectFit: 'cover',
              }}
            />

          </div>
        )
      } else if (embeddable_types.includes(file_ext)) {
        return (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%', 
              height: '100%',
              backgroundColor: 'white',
    
              borderColor: 'rgb(207, 205, 203)',
              borderWidth: '1px',
              borderStyle: 'solid',
              borderRadius: '12px',
            }}>

              <div
                style={{
                  margin: 'auto',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: '70%',
                  height: '70%',
                }}
                onClick={() => { toggleModal("Views.EmbeddableViewer", path) }}

              >
                 <FileIcon
                  fill='var(--grey600)'
                />

                <div style={{
                  fontSize: '13px',
                  color: 'var(--black)',
                  fontWeight: '500',
                  paddingTop: '5px',
                  textAlign: 'center',
                  wordBreak: 'break-all',
                }}>
                  {/* {fileName()} */}
                  {Actions.truncateFileName(file.file_name)}
                </div>
              </div>
          </div>
        )
      } else {
        return (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%', 
              height: '100%',
              backgroundColor: 'white',
    
              borderColor: 'rgb(207, 205, 203)',
              borderWidth: '1px',
              borderStyle: 'solid',
              borderRadius: '12px',
            }}>

              <div
                style={{
                  margin: 'auto',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: '70%',
                  height: '70%',
                }}

              >
                 <FileIcon
                  fill='var(--grey600)'
                />

                <div style={{
                  fontSize: '13px',
                  color: 'var(--black)',
                  fontWeight: '500',
                  paddingTop: '5px',
                  textAlign: 'center',
                  wordBreak: 'break-all',
                }}>
                  {/* {fileName()} */}
                  {Actions.truncateFileName(file.file_name)}
                </div>
              </div>


          </div>
        )
      }
    }
  },


  MsgBox: function () {
    const [msg, setMsg] = useState("")
    const my_icon_cid = config.my_icon_cid;
    const icon = Actions.getIcon(my_icon_cid);


    let files = []
    if (locals.size !== 0) {
      locals.forEach((file) => { files.push(file) } )
    }

    let file_cids = []
    if (locals.size !== 0) {
      locals.forEach(file => {
        file_cids.push(file.cid)
      })
    }

    // cl(file_cids)

    // load local file status when page loads
    invoke('get_internal_stubs')
      .then((res) => {
        locals.clear()

        let files = JSON.parse(res)

        files.forEach(file => {
          locals.set(file.path, file)
        })
      })
      .catch((e) => console.error(e));

      // cl(files)


    // Resize the text area as the user types
    useEffect(() => {
      let el = document.getElementById(`msgBox`);
      if (el !== null) {
        let height = el.scrollHeight + 20
        el.style.height = height + "px"
      }
    })

    // Resize the padding between the box and the message rows as
    // the box is resized.
    useEffect(() => {
      let box = document.getElementById('box')
      let row_padding = document.getElementById('row_padding')
      if (box && row_padding) {
        function outputsize() {
          if (box_height !== box.offsetHeight) {
            box_height = box.offsetHeight
            row_padding.style.paddingTop = box.offsetHeight + "px"
          }
        }
        outputsize()
        new ResizeObserver(outputsize).observe(box)
      }
    })


    const displayFiles = () => {

      if (files !== undefined && files.length !== 0) {
        isProcessing = false
        cl(files)
        return (
          files.map((file, i) =>

            <div
              key={i}
              style={{
                position: 'relative',
                maxHeight: '150px',
                maxWidth: '150px',
                flex: '1 1 150px', 
                height: '150px',
                marginLeft: i === 0 ? '' : '1.2%',
                borderRadius: '12px',
              }}>
              <div
                style={{
                  width: '20px',
                  position: 'absolute',
                  zIndex: '1',
                  top: '0',
                  left: '0',

                }}
                onClick={() => Actions.removeLocalFile(file)}
              >
                <DeleteIcon fill='red' />
              </div>

              {Views.FileIcon(file)}

            </div>
          )
        )
      }
    }


    return (
      <x-msg-box
        id="box"
        style={{
          display: 'flex',
          backgroundColor: 'var(--grey400)',
          padding: '20px',
          width: 'calc(100% - 240px)',
          position: 'fixed',
          zIndex: '10',
        }}
      >
        <div style={{
          height: '44px',
          width: '44px',
          maxWidth: '44px',
          maxHeight: '44px',
        }}>

          {icon}

        </div>

        <div style={{
          marginLeft: '15px',
          width: '100%',
        }}>
          <input
            id="msgBox"
            style={{
              marginLeft: '0px',
              marginRight: '0px',
              // width: 'min-intrinsic',
              width: 'calc(100% - 24px)',
              // width: '-webkit-fill-available',
              // width: '-moz-available',
              // width: 'fill-available',
              borderWidth: '2px',
              borderStyle: 'solid',
              borderColor: 'var(--grey575)',
              borderRadius: '5px',
              fontSize: '14px',
              textAlign: 'left',
              paddingLeft: '10px',
              paddingRight: '10px',
              wordBreak: 'break-word',
              overflow: 'hidden',
              resize: 'none',
            }}
            type="textarea"
            placeholder="What's on your mind?"
            value={msg}
            onChange={(e) => setMsg(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                Actions.publishMessage(msg, file_cids)
                setMsg("")
              }
            }}
          />

          <div
            style={{
              marginTop: files && files.length > 0 ? '10px' : '',
              display: 'flex',
              flexWrap: 'nowrap',
              overflowX: 'auto',
            }}>
            {displayFiles()}
          </div>

          <div style={{
            marginTop: '15px',
            borderTop: '1px solid var(--grey550)',
          }} />
          <div style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingTop: '10px',
          }}>
            <div style={{
              display: 'flex',
            }}>

              <div
                style={{
                  width: '25px',
                  height: '25px',
                  cursor: 'pointer',
                }}
                onClick={Actions.openFileDialog}

              >

                <PaperclipIcon
                  height='25px'
                  width='25px'
                  fill='var(--purple500)'
                />

              </div>
            </div>


            <x-button-primary
              style={{
                backgroundColor: isProcessing ? "var(--grey600)" : ""
              }}
              type="button"
              onClick={() => { Actions.publishMessage(msg, file_cids); setMsg("") }}
            >
              Send
            </x-button-primary>


          </div>
        </div>

      </x-msg-box>
    )

  },

  Home: function (props) {

    if (props.page === 'home') {

      invoke('get_all_messages')
        .then((res) => {
          let server_messages = JSON.parse(res)
          if (JSON.stringify(server_messages) !== JSON.stringify(client_messages)) {
            client_messages = server_messages
            BroadcastSharedData("client_messages", client_messages)
          }
        })
        .catch((e) => console.error(e));

      return (
        <>
          <Views.MsgBox />
          <div id='row_padding' style={{ paddingTop: box_height + 'px' }} />
          <Views.MessageRows messages={client_messages} />
          <Views.Test />
        </>
      )
    } else {
      return null
    }

  },

  MessageRows: function (props) {
    // cl(props)
    const messages = props.messages ? props.messages : []

    if (JSON.stringify(messages) !== "[]" && messages !== undefined) {
      cl(messages)

      // load file status when page loads
      invoke('get_download_file_status')
        .then((res) => {
          cl(res)

          if (JSON.stringify(GetSharedData().NewDownloadFileStatus) !== res) {
            let files = JSON.parse(res)
            files.forEach(file => {
              downloads.set(file.cid, file)
            })
            BroadcastSharedData("NewDownloadFileStatus", JSON.parse(res))
          }
        })
        .catch((e) => console.error(e));


      let arr = [...messages];
      let rev = arr.reverse();

      const displayFiles = (file_ids) => {
        cl(downloads)
        cl(file_ids)

        if (file_ids && file_ids.length !== 0) {
          return (
            file_ids.map((file_id, i) => {
              cl(file_id.File.cid)
              const file = downloads ? downloads.get(file_id.File.cid) : null;
              return (
                <div
                  key={i}
                  style={{
                    position: 'relative',
                    maxHeight: '150px',
                    maxWidth: '150px',
                    flex: '1 1 150px', 
                    height: '150px',
                    marginLeft: i === 0 ? '' : '1.2%',
    
                    borderRadius: '12px',
                  }}>

                  {Views.FileIcon(file)}

                </div>
              )
            })
          )
        }
      }


      const message_row = rev.map(row => {
        cl(row)
        const icon = Actions.getIcon(row.icon_cid);
        const file_ids = row.file_ids

        let timestamp = ''
        let time = (Date.now() - Date.parse(row.timestamp)) / 1000
        if (time < 1) {
          timestamp = 'now'
        } else if (time < 60) {
          timestamp = Math.floor(time) + "s"
        } else if (time < 600) {
          timestamp = Math.floor(time / 60) + "m"
        } else if (time < 86400) {
          timestamp = Math.floor(time / 3600) + 'h'
        } else {
          timestamp = Math.floor(time / 86400) + 'd'
        }


        return (
          <div
            key={row.person_id+row.sequence_number}
            style={{
              display: 'flex',
              borderBottomStyle: 'solid',
              borderBottomWidth: '1px',
              borderBottomColor: 'var(--grey400)',
              marginLeft: '20px',
              marginRight: '20px',

            }}
          >

            <div style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',

              borderStyle: 'solid',
              borderWidth: '1px',
              borderColor: '#a3a3a3',
              borderRadius: '25px',

              minWidth: '44px',
              width: '44px',
              height: '44px',

              marginTop: '15px',
            }}>
              {icon}
            </div>

            <div
              style={{
                marginLeft: '15px',
                marginTop: '20px',
                width: '100%',
              }}
            >

              <div style={{
                display: 'flex',
                alignItems: 'center',
              }}>

                <div style={{
                  fontWeight: 'bold',
                  fontSize: '14px',
                }}>
                  {row.name}
                </div>

                <div style={{
                  height: '4px',
                  width: '4px',
                  borderRadius: '10px',
                  backgroundColor: 'var(--grey600)',
                  marginLeft: '5px',
                  marginRight: '5px',
                }} />

                <div style={{
                  fontSize: '14px',
                }}>
                  {timestamp}
                </div>

              </div>

              <div style={{
                fontSize: '14px',
                fontWeight: 'medium',
                wordWrap: 'breakword',
                paddingTop: '5px',

              }}>
                {row.content}
              </div>
              <div
                style={{
                  marginTop: file_ids && file_ids.length > 0 ? '10px' : '',
                  display: 'flex',
                  paddingBottom: '20px',

                }}>
                {displayFiles(file_ids)}
              </div>

            </div>

          </div>
        )
      })
      return message_row
    } else {
      return null
    }

  },

  People: function (props) {

    if (props.page === 'people') {

      // appWindow.appWindow.emit('ui', { message: 'update_people||' })

      if (GetSharedData().tab === 'Not Followed') {

        invoke('get_people', { isFollowed: false })
          .then((res) => {
            let server_people = JSON.parse(res)
            if (JSON.stringify(server_people) !== JSON.stringify(people_not_followed)) {
              // cl("inner")
              // cl(server_people)
              people_not_followed = server_people
              BroadcastSharedData("people_not_followed", server_people)
            }
          })
          .catch((e) => console.error(e));

        return (
          <>
            <Views.PageHeader page_title="People" />
            <Views.Tabs />
            <Views.PeopleRows people={people_not_followed} />
          </>
        )
      } else {

        if (GetSharedData().tab !== 'Following') {
          BroadcastSharedData("tab", "Not Followed")
        }

        invoke('get_people', { isFollowed: true })
          .then((res) => {
            let server_people = JSON.parse(res)
            if (JSON.stringify(server_people) !== JSON.stringify(people_followed)) {
              people_followed = server_people
              BroadcastSharedData("people_followed", server_people)
            }
          })
          .catch((e) => console.error(e));

        return (
          <>
            <Views.PageHeader page_title="People" />
            <Views.Tabs />
            <Views.PeopleRows people={people_followed} />
          </>
        )
      }
    } else {
      return null
    }
  },

  PeopleRows: function (props) {
    const people = props.people ? props.people : []

    if (JSON.stringify(people) !== "[]" && people !== undefined) {

      const person_row = people.map(row => {
        // remove myself
        if (row.person_id === config.my_person_id) {
          return null

        } else {
          const is_followed = row.is_followed
          const bio = row.bio === "" ? "No bio provided" : row.bio
          const icon = Actions.getIcon(row.icon_cid);

          return (
            <x-row
              key={row.person_id}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  paddingTop: '15px',
                  paddingBottom: '15px',
                  borderBottomStyle: 'solid',
                  borderBottomWidth: '1px',
                  borderBottomColor: 'var(--grey400)',
                  marginLeft: '-10px',
                  marginRight: '-10px',
                }}
              >

                <div style={{
                  display: 'flex',
                  marginLeft: '10px',
                  marginRight: '10px',
                }}>

                  <div style={{
                    maxWidth: '44px',
                    maxHeight: '44px',
                    height: '44px',
                    width: '44px',
                  }}>
                    {icon}
                  </div>

                  <div style={{
                    maxWidth: '500px',
                    alignSelf: 'center',
                    marginLeft: '20px',
                  }}>

                    <div
                      style={{
                        fontSize: '14px',
                        fontWeight: 'bold',
                      }}>
                      {row.name}
                    </div>

                    <div
                      style={{
                        marginTop: "5px",
                        fontSize: '14px',
                        fontStyle: row.bio === "" ? 'italic' : '',
                        color: row.bio === "" ? "var(--grey600)" : "var(--grey900)",
                      }}>
                      {bio}
                    </div>

                  </div>


                </div>

                <x-button-primary
                  style={{
                    height: '20px',
                    alignSelf: 'center',
                    marginRight: '10px',
                  }}
                  onClick={() => {
                    is_followed
                      ? appWindow.emit('ui', { command: 'unfollow', payload: row.person_id })
                      : appWindow.emit('ui', { command: 'follow', payload: row.person_id })
                  }}
                  type="button"
                >

                  {is_followed ? "Unfollow" : "Follow"}
                </x-button-primary>
              </div>
            </x-row>

          )
        }
      })
      return person_row
    } else {
      return (
        <>
          <x-row>
            <x-row-1>
              No available people
            </x-row-1>
          </x-row>

        </>
      )
    }
  },

  Invite: function (props) {

    let [copied, setCopied] = useState(false)

    useEffect(() => {
      if (copied === true) {
        const resetCopy = async () => {
          await new Promise(r => setTimeout(r, 2000));
          setCopied(false)
        }
        resetCopy()
      }
    })

    function copyTextToClipboard(text) {
      navigator.clipboard.writeText(text).then(function () {
        setCopied(true)
      }, function (err) {
        console.error('Could not copy text: ', err);
      });
    }

    if (props.page === 'invite') {

      invoke('get_invite_code')
        .then((res) => {
          cl("invoke | get_invite_code")
          cl(res)
          if (GetSharedData()?.invite_code !== res) {
            BroadcastSharedData('invite_code', res)
          }
        })
        .catch((e) => console.error(e));


      return (
        <>
          <Views.PageHeader page_title="Invite" />
          <div
            style={{
              marginLeft: '20px',
              fontSize: '14px',
              maxWidth: '500px',
            }}>
            This invite code allows others to find and follow you on Tickle.
            Share it privately, or publicly on social media.
          </div>
          <div style={{
            display: 'flex',
            paddingLeft: '20px',
          }}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                marginTop: '20px',
                alignItems: 'center',
              }}>
              <div style={{
                textAlign: 'center',
                fontSize: '13px',
                paddingLeft: '20px',
                paddingRight: '20px',
                lineBreak: 'anywhere',
                maxWidth: '475px',
                borderColor: 'var(--grey600)',
                borderWidth: '1px',
                borderStyle: 'solid',
                borderRadius: '10px',
                padding: '20px',
              }}>
                {GetSharedData()?.invite_code}
              </div>
              <div
                style={{
                  paddingTop: '10px',
                  color: copied ? 'var(--grey600)' : 'var(--purple500',
                  fontWeight: '500',
                  fontSize: '14px',
                  cursor: copied ? "" : 'pointer',
                }}
                onClick={() => {
                  copyTextToClipboard(GetSharedData()?.invite_code)

                }}
              >
                {copied === true ? "Copied!" : "Copy to clipboard"}
              </div>

            </div>


          </div>
        </>

      )
    } else {
      return null
    }
  },

  Connections: function (props) {
    if (props.page === 'connections') {

      invoke('get_all_connections')
        .then((res) => {
          let server_connections = JSON.parse(res)
          if (JSON.stringify(server_connections) !== JSON.stringify(connections)) {
            // cl("inner")
            // cl(server_peers)
            connections = server_connections
            BroadcastSharedData("connections", server_connections)
          }
        })
        .catch((e) => console.error(e));

      return (
        <>
          <Views.PageHeader page_title="Connections" />
          <x-actions>
            <x-link
              id="new_connection"
              onClick={(e) => { if (e.target.id === 'new_connection') { toggleModal("Views.ConnectionsNew") } }}
            >
              New connection
            </x-link>
          </x-actions>
          <Views.ConnectionRows connections={connections} />
        </>
      )
    } else {
      return null
    }
  },

  ConnectionRows: function (props) {

    const connections = props.connections ? props.connections : []

    if (JSON.stringify(connections) !== "[]" && connections !== undefined) {

      const connection_row = connections.map(row => {
        return (
          <x-row key={row.peer_id}>
            <x-row-1>
              {row.peer_id}
            </x-row-1>
          </x-row>
        )
      })
      return connection_row
    } else {
      return (
        <x-row>
          <x-title>
            No connections
          </x-title>
        </x-row>
      )
    }
  },

  ConnectionsNew: function () {

    let is_connected = GetSharedData().app_status === 'Online' ? true : false
    cl(is_connected)

    // Close the modal if the profile saved successfully
    if (GetSharedData().NewConnectionSuccess !== undefined) {
      DeleteSharedData("NewConnectionSuccess")
      BroadcastSharedData('isModalOpen', 'false')
    }

    const [addr, setAddr] = useState('')

    if (is_connected) {
      return (
        <div style={{
          width: '700px',
          margin: 'auto',
          backgroundColor: 'white',
          borderStyle: 'solid',
          borderWidth: '4px',
          borderColor: '#630090',
          borderRadius: '24px',
        }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              paddingRight: '15px',
              paddingTop: '15px',
              cursor: 'pointer',
            }}
            onClick={toggleModal}
          >
            <CloseIcon height='20px' fill='var(--grey600)' />
          </div>
          <div style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}>
            <x-modal-title>
              Follow someone
            </x-modal-title>
            <x-modal-subtitle>
              Enter an invite code from someone you trust
              (must be online right now).
            </x-modal-subtitle>
            <textarea
              style={{
                width: '550px',
                marginTop: '20px',
                textAlign: 'center',
              }}
              type="textarea"
              placeholder='eyJhIjoiMTJEM0tvb1dIU1ltNHczc2yjbWJmbzVmbzhGaVRzcXBaM0dBeHpiQWZ0cENhd0NTNmRtMSIsImIiOiIvaXA0LzE5Mi4xNjguMS44NS90Y3AvNTUyNjIifQ'
              value={addr}
              onChange={(e) => setAddr(e.target.value)}
            />
            <div style={{ paddingTop: '25px' }} />
            <x-button-primary
              type="submit"
              onClick={() => appWindow.emit('ui', { command: "dial", payload: addr })}
            >
              Connect
            </x-button-primary>
            <div style={{ paddingTop: '35px' }} />
  
          </div>
        </div>
      )
    } else {
      return (
        <div style={{
          width: '700px',
          margin: 'auto',
          backgroundColor: 'white',
          borderStyle: 'solid',
          borderWidth: '4px',
          borderColor: '#630090',
          borderRadius: '24px',
        }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              paddingRight: '15px',
              paddingTop: '15px',
              cursor: 'pointer',
            }}
            onClick={toggleModal}
          >
            <CloseIcon height='20px' fill='var(--grey600)' />
          </div>
          <div style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}>
            <x-modal-title>
              Connecting...
            </x-modal-title>
            <x-modal-subtitle>
              It may take up to a minute to connect to the network. 
            </x-modal-subtitle>
            
            <div style={{ paddingTop: '25px' }} />
          
            <div style={{ paddingTop: '35px' }} />
  
          </div>
        </div>
      )
    }
   
  },

  Settings: function (props) {

    if (props.page === 'settings') {
      if (GetSharedData().tab === 'System Log') {
        return (
          <>
            <Views.PageHeader page_title="Settings" />
            <Views.Tabs />
            <x-table style={{ maxHeight: (window.innerHeight - 118) + "px" }}>
              <Views.SettingsEventRows events={all_events} />
            </x-table>
          </>
        )
      } else {
        if (GetSharedData().tab !== 'Config') {
          BroadcastSharedData("tab", "Config")
        }

        let content = []
        content.push(<x-section-title>Network</x-section-title>)
        content.push(<x-menu-top-hr />)
        content.push(<x-label>App ID</x-label>)
        content.push(<x-field>{config.my_peer_id}</x-field>)

        if (config.my_listeners !== undefined && config.my_listeners.length > 0) {
          config.my_listeners.forEach(item => {
            content.push(<x-label>App listening on</x-label>)
            content.push(<x-field>{item}</x-field>)
          })
        }

        if (config.my_ipfs_listeners !== undefined && config.my_ipfs_listeners.length > 0) {
          config.my_ipfs_listeners.forEach(item => {
            content.push(<x-label>IPFS listening on</x-label>)
            content.push(<x-field>{item}</x-field>)
          })
        }


        return (
          <>
            <Views.PageHeader page_title="Settings" />
            <Views.Tabs />
            <x-box>
              {content}
            </x-box>
          </>
        )
      }
    } else {
      return null
    }

  },

  SettingsEventRows: function (props) {
    const events = props.events ? props.events : []

    if (JSON.stringify(events) !== "[]") {

      let arr = [...events];
      let rev = arr.reverse();

      const event_row = rev.map(row => {

        const timestamp = new Date(row.timestamp).toLocaleTimeString()

        return (
          <x-row key={row.timestamp}>
            <x-row-1>
              <x-event>
                {/* <RightChevron height='10px' fill='black' /> */}
                <x-row-title>
                  {row.event_name}
                </x-row-title>
              </x-event>
              <x-event-timestamp>
                {timestamp}
              </x-event-timestamp>
            </x-row-1>
            <x-row-2>
              {row.message}
            </x-row-2>
          </x-row>
        )
      })
      return event_row
    } else {
      return (
        <x-row>
          <x-row-1>
            <x-event>
              {/* <RightChevron height='10px' fill='black' /> */}
              <x-event-name>
                No Events
              </x-event-name>
            </x-event>
          </x-row-1>
        </x-row>
      )
    }
  },

  ManageProfile: function () {

    // Close the modal if the profile saved successfully
    if (GetSharedData().SaveProfileSuccess === '' && GetSharedData().isModalOpen === 'true') {
      DeleteSharedData("SaveProfileSuccess")
      BroadcastSharedData('isModalOpen', 'false')
    }

    const [img, setImg] = useState(0)
    const [nick, setNick] = useState(0)
    const [bio, setBio] = useState(0)

    let icon_path = Actions.getPathForCid(config.my_icon_cid)
    let icon_url = icon_path ? icon_path : ""

    const send = async () => {
      let payload;

      if (img.images !== undefined) {

        if (img.images.length > 0) {
          // cl(img.images)
          const file = img.images[0]
          const reader = new FileReader();

          reader.onload = async function (event) {
            // cl(event)
            const img_data = event.target.result

            payload = {
              "name": nick !== 0 ? nick : config.my_name,
              "icon": window.btoa(img_data),
              'icon_name': file.name,
              "bio": bio !== 0 ? bio : config.my_bio,
            }

            // cl(payload)
            appWindow.emit('ui', { command: "update_profile", payload: JSON.stringify(payload) })
          }
          reader.readAsBinaryString(file)

        } else {
          payload = {
            "name": nick !== 0 ? nick : config.my_name,
            "icon": "delete",
            "icon_name": "",
            "bio": bio !== 0 ? bio : config.my_bio,
          }
          // cl(payload)
          appWindow.emit('ui', { command: "update_profile", payload: JSON.stringify(payload) })
        }

      } else {
        payload = {
          "name": nick !== 0 ? nick : config.my_name,
          "icon": img !== 0 ? "delete" : "no_change",
          "icon_name": "",
          "bio": bio !== 0 ? bio : config.my_bio,
        }
        cl("payload 3")
        appWindow.emit('ui', { command: "update_profile", payload: JSON.stringify(payload) })
      }
    }

    return (
      <div style={{
        width: '700px',
        margin: 'auto',
        backgroundColor: 'white',
        borderStyle: 'solid',
        borderWidth: '4px',
        borderColor: '#630090',
        borderRadius: '24px',
      }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            paddingRight: '15px',
            paddingTop: '15px',
            cursor: 'pointer',
          }}
          onClick={toggleModal}
        >
          <CloseIcon height='20px' fill='var(--grey600)' />
        </div>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}>
          <x-modal-title>
            Manage profile
          </x-modal-title>
          <x-modal-subtitle>
            These details are shown on your posts and your public profile page
          </x-modal-subtitle>

          <div
            style={{
              display: 'flex',
              paddingTop: '30px',
            }}>

            <div>
              <div
                style={{
                  fontSize: '13px',
                  fontWeight: '500',
                  color: 'var(--grey600)',
                  paddingBottom: '8px',
                }}>
                Profile icon
              </div>

              <ImageField
                value={img ? img : icon_url}
                onChange={setImg}
                style={{
                  height: '150px',
                  width: '150px',
                  maxWidth: '150px',
                }}
                icon_style={{
                  maxWidth: '148px',
                  borderRadius: '12px'
                }}
                max_file_size={1000000}
                max_file_num={1}
              />

            </div>


            <div
              style={{
                marginLeft: '40px'
              }}
            >

              <div
                style={{
                  fontSize: '13px',
                  fontWeight: '500',
                  color: 'var(--grey600)',
                  paddingBottom: '8px',
                }}>
                Not your real name (unless you want to disclose it)
              </div>

              <input
                id="nickname"
                style={{
                  width: '350px',
                  // height: '4em',
                  fontSize: '14px',
                  textAlign: 'left',
                  paddingLeft: '10px',
                  paddingRight: '10px',
                  wordBreak: 'break-word',
                  overflow: 'hidden',
                  resize: 'none',
                }}
                type="text"
                placeholder="Nickname"
                value={nick !== 0 ? nick : config.my_name}
                onChange={(e) => setNick(e.target.value)}
              />

              <div
                style={{
                  marginTop: '30px',
                  fontSize: '13px',
                  fontWeight: '500',
                  color: 'var(--grey600)',
                  paddingBottom: '8px',
                }}>
                Not your real bio (unless you want to disclose it)
              </div>

              <textarea
                id="bio"
                style={{
                  width: '350px',
                  height: '100px',
                }}
                type="textarea"
                placeholder="Bio"
                value={bio !== 0 ? bio : config.my_bio}
                onChange={(e) => setBio(e.target.value)}
              />

            </div>

          </div>

          <div style={{ paddingTop: '25px' }} />
          <x-button-primary
            type="submit"
            onClick={send}
          >
            Save
          </x-button-primary>
          <div style={{ paddingTop: '35px' }} />
        </div>
      </div>
    )
  },

  ProgressWidget: function (props) {
    const pct_processed = props.pct_processed

    return (
      <svg 
      style={{
        width: '100%',
        height: '100%',
      }}
      viewBox="0 0 36 36" className="circular-chart purple">
        <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <path className="circle"
          strokeDasharray={pct_processed + ", 100"}
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <text x="18" y="20.35" className="percentage">{pct_processed > 0 ? pct_processed + "%" : "0%"}</text>
      </svg>
    )
  },

  VideoPlayer: function (props) {
    return (

      <div style={{
        width: '90%',
        margin: 'auto',
        backgroundColor: 'black',
        borderStyle: 'solid',
        borderWidth: '4px',
        borderColor: '#630090',
        borderRadius: '24px',
      }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            paddingRight: '15px',
            paddingTop: '15px',
            cursor: 'pointer',
          }}
          onClick={toggleModal}
        >
          <CloseIcon height='20px' fill='var(--grey600)' />
        </div>

        <video controls
          src={props.path}
          style={{
            margin: 'auto',
            paddingTop: '1%',
            paddingBottom: '4%',
            width: 'inherit',
          }}
        />

      </div>
    )
  },

  ImageViewer: function (props) {
    // cl(props.path)
    return (

      <div style={{
        width: '90%',
        margin: 'auto',
        backgroundColor: 'black',
        borderStyle: 'solid',
        borderWidth: '4px',
        borderColor: '#630090',
        borderRadius: '24px',
      }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            paddingRight: '15px',
            paddingTop: '15px',
            cursor: 'pointer',
          }}
          onClick={toggleModal}
        >
          <CloseIcon height='20px' fill='var(--grey600)' />
        </div>

        <img
          src={props.path}
          style={{
            margin: 'auto',
            paddingTop: '1%',
            paddingBottom: '4%',
            width: 'inherit',
          }}
          alt=''
        />

      </div>
    )
  },

  EmbeddableViewer: function (props) {
    // cl(props)
    return (

      <div style={{
        width: '90%',
        height: '90%',
        margin: 'auto',
        backgroundColor: 'black',
        borderStyle: 'solid',
        borderWidth: '4px',
        borderColor: '#630090',
        borderRadius: '24px',
      }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            paddingRight: '15px',
            paddingTop: '15px',
            cursor: 'pointer',
          }}
          onClick={toggleModal}
        >
          <CloseIcon height='20px' fill='var(--grey600)' />
        </div>

        <embed
          src={props.path}
          style={{
            margin: 'auto',
            paddingTop: '1%',
            paddingBottom: '4%',
            width: 'inherit',
            height: 'inherit',
          }}
          alt=''
        />

      </div>
    )

  },

  Test: function () {



    // cl(config.my_icon)

    // return (
    //   null
    // )
    // let img = ''

    // async function selected (e) {
    //   cl(e.target.files)

    //   img = await openDialog({
    //       multiple: false,
    //       filters: [{
    //         name: 'Image',
    //         extensions: ['png', 'jpeg']
    //       }]
    //     });

    //   cl(img)

    //   const reader = new FileReader();

    //   reader.onload = function(event) {
    //     cl(event.target.result)

    //   };

    //   reader.readAsDataURL(img)

    // }

    // const send = () => {
    //   var fileInput = document.getElementById('fileInput');
    //   cl("Send")
    //   cl(fileInput)
    // }

    // const selected = await openDialog({
    //   multiple: false,
    //   filters: [{
    //     name: 'Image',
    //     extensions: ['png', 'jpeg']
    //   }]
    // });

    // if (selected === null) {
    //   cl("User canceled")
    //   // user cancelled the selection
    // } else {
    //   cl("File selected")

    //   // user selected a single file
    // }


    // https://github.com/tauri-apps/tauri/pull/4028




    return (
      null
      // <div>
      //   {Actions.getFilePreview()}
      // </div>
    )

  },

}


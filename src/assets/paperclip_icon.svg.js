import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill, height, width }) {
    return (
        <svg className={className} width={width} height={height} viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M396.199 83.8001C371.799 59.4001 332.199 59.4001 307.799 83.8001L123.799 267.8C81.6992 309.9 81.6992 378.1 123.799 420.2C165.899 462.3 234.099 462.3 276.199 420.2L428.199 268.2C439.099 257.3 456.899 257.3 467.799 268.2C478.699 279.1 478.699 296.9 467.799 307.8L315.799 459.8C251.799 523.8 148.199 523.8 84.1992 459.8C20.1992 395.8 20.1992 292.2 84.1992 228.2L268.199 44.2001C314.499 -2.0999 389.499 -2.0999 435.799 44.2001C482.099 90.5001 482.099 165.5 435.799 211.8L259.799 387.8C231.199 416.4 184.799 416.4 156.199 387.8C127.599 359.2 127.599 312.8 156.199 284.2L300.199 140.2C311.099 129.3 328.899 129.3 339.799 140.2C350.699 151.1 350.699 168.9 339.799 179.8L195.799 323.8C189.099 330.5 189.099 341.5 195.799 348.2C202.499 354.9 213.499 354.9 220.199 348.2L396.199 172.2C420.599 147.8 420.599 108.2 396.199 83.8001V83.8001Z" fill={fill} />
        </svg>
    );
}

export default Image;

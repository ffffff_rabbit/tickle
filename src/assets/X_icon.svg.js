import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill, height }) {
    return (
        <svg viewBox="0 0 217 216" className={className} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
        <line x1="24.9941" y1="24" x2="192.083" y2="191.089" stroke={fill} strokeWidth="33.9328" strokeLinecap="round"/>
        <line x1="24.9941" y1="24" x2="192.083" y2="191.089" stroke={fill} strokeWidth="33.9328" strokeLinecap="round"/>
        <line x1="16.9664" y1="-16.9664" x2="253.266" y2="-16.9664" transform="matrix(-0.707107 0.707107 0.707107 0.707107 216 24)" stroke={fill} strokeWidth="33.9328" strokeLinecap="round"/>
        <line x1="16.9664" y1="-16.9664" x2="253.266" y2="-16.9664" transform="matrix(-0.707107 0.707107 0.707107 0.707107 216 24)" stroke={fill} strokeWidth="33.9328" strokeLinecap="round"/>
    </svg>
    );
  }

  export default Image;



import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS
// Import the file like this ... import CloseIcon from '../assets/X_icon.svg'
// Use it like this ... <CloseIcon className='icon-close' fill='#737074' /> 


function Image({ className, fill }) {
    return (
        <svg className={className} viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M39.8451 0H60.1549V100H39.8451V0Z" fill={fill}/>
            <path d="M0 60.0688V39.9312H100V60.0688H0Z" fill={fill}/>
        </svg>
    );
  }

  export default Image;






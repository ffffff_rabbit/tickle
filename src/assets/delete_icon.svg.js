import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill }) {
    return (
        <svg viewBox="0 0 100 100" className={className} fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="50" cy="50" r="50" fill={fill}/>
            <line x1="30.6569" y1="31" x2="70.0498" y2="70.3929" stroke="white" strokeWidth="8" strokeLinecap="round"/>
            <line x1="30.6569" y1="31" x2="70.0498" y2="70.3929" stroke="white" strokeWidth="8" strokeLinecap="round"/>
            <line x1="4" y1="-4" x2="59.71" y2="-4" transform="matrix(-0.707107 0.707107 0.707107 0.707107 75.6885 31)" stroke="white" strokeWidth="8" strokeLinecap="round"/>
            <line x1="4" y1="-4" x2="59.71" y2="-4" transform="matrix(-0.707107 0.707107 0.707107 0.707107 75.6885 31)" stroke="white" strokeWidth="8" strokeLinecap="round"/>
        </svg>
    );
  }

  export default Image;



import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill, height, width }) {
    return (
        <svg className={className} style={{ maxHeight: '68%', minHeight: '50%', height: '60%', maxWidth: '75px', }} viewBox="-4 4 161 207" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="161" height="206" fill="white" />
            <path d="M3 11C3 6.58173 6.58172 3 11 3H150C154.418 3 158 6.58172 158 11V195C158 199.418 154.418 203 150 203H11C6.58172 203 3 199.418 3 195V11Z" fill={fill} />
            <path d="M99 3H150C154.418 3 158 6.58172 158 11V62H107C102.582 62 99 58.4183 99 54V3Z" fill="white" />
            <path d="M106.5 52.9V5.06985C106.5 3.6444 108.223 2.93053 109.231 3.93848L157.062 51.7686C158.069 52.7766 157.356 54.5 155.93 54.5H108.1C107.216 54.5 106.5 53.7837 106.5 52.9Z" fill={fill} stroke={fill} strokeMiterlimit="2.41327" />
        </svg>
    );
}

export default Image;





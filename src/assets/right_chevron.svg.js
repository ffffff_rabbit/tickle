import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, height, fill }) {
    return (
        <svg viewBox="0 0 58 100" className={className} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.426 53.6503C58.5247 51.6549 58.5247 48.4197 56.426 46.4244L9.17394 1.49653C7.07533 -0.498847 3.67281 -0.498843 1.57421 1.49654C-0.524403 3.49192 -0.524403 6.72707 1.57421 8.72244L48.8263 53.6503C50.9249 55.6456 54.3274 55.6456 56.426 53.6503Z" fill={fill}/>
            <path d="M1.57396 98.5035C3.67257 100.499 7.07509 100.499 9.17369 98.5035L56.4258 53.5756C58.5244 51.5803 58.5244 48.3451 56.4258 46.3497C54.3272 44.3544 50.9247 44.3544 48.8261 46.3497L1.57396 91.2776C-0.524654 93.2729 -0.524647 96.5081 1.57396 98.5035Z" fill={fill}/>
        </svg>
    );
  }

  export default Image;


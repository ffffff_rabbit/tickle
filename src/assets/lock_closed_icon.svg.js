import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill }) {
    return (
        <svg viewBox="0 0 202 294" className={className} fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect y="109" width="202" height="185" rx="21" fill={fill}/>
            <path d="M41 236V74.5C41 41.0868 68.0868 14 101.5 14V14C134.913 14 162 41.0868 162 74.5V110.5" stroke={fill} strokeWidth="28" strokeLinecap="round"/>
        </svg>  
    );
  }

  export default Image;


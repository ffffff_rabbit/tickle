import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill, height, width, fillOpacity}) {
    return (
        <svg className={className} width={width} height={height} viewBox="0 0 62 62" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="31" cy="31" r="30.6173" fill={fill} fillOpacity={fillOpacity} stroke="" strokeWidth="0.765432" />
            <path d="M43.28 29.8122C44.0027 30.31 44.0027 31.3774 43.28 31.8752L26.0738 43.7283C25.2428 44.3007 24.1107 43.7059 24.1107 42.6968L24.1107 18.9906C24.1107 17.9815 25.2428 17.3867 26.0738 17.9591L43.28 29.8122Z" fill="white" />
        </svg>
    );
}

export default Image;

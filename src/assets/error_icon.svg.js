import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill }) {
    return (
        <svg className={className}  style={{ maxHeight: 'inherit', height: 'inherit', minHeight: 'inherit', minWidth: 'inherit' }} viewBox="0 0 318 288" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M140.692 11.582C148.718 -2.70183 169.282 -2.70181 177.308 11.582L314.625 255.963C322.491 269.961 312.374 287.25 296.317 287.25H21.6829C5.6258 287.25 -4.49068 269.961 3.37509 255.963L140.692 11.582Z" fill={fill} />
            <path d="M178.482 67.4941C178.482 72.377 176.871 88.7832 173.648 116.713C170.426 144.643 167.594 171.645 165.152 197.719H153.727C151.578 171.645 148.893 144.643 145.67 116.713C142.545 88.7832 140.982 72.377 140.982 67.4941C140.982 61.9277 142.74 57.2402 146.256 53.4316C149.771 49.623 154.264 47.7188 159.732 47.7188C165.201 47.7188 169.693 49.623 173.209 53.4316C176.725 57.1426 178.482 61.8301 178.482 67.4941ZM179.801 244.154C179.801 249.623 177.799 254.311 173.795 258.217C169.889 262.025 165.201 263.93 159.732 263.93C154.264 263.93 149.527 262.025 145.523 258.217C141.617 254.311 139.664 249.623 139.664 244.154C139.664 238.686 141.617 233.998 145.523 230.092C149.527 226.088 154.264 224.086 159.732 224.086C165.201 224.086 169.889 226.088 173.795 230.092C177.799 233.998 179.801 238.686 179.801 244.154Z" fill="white" />
        </svg>
    );
}

export default Image;





import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill, height, width}) {
    return (
        <svg className={className} height={height} width={width} viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="100" height="100" rx="21" fill={fill} />
            <circle cx="77.5" cy="23.5" r="9.5" fill="white" />
            <path d="M62.8004 44.46C63.9032 42.4821 66.7159 42.3925 67.9425 44.2961L89.7385 78.1252C91.0247 80.1215 89.5914 82.75 87.2166 82.75H46.5583C44.2688 82.75 42.8231 80.2887 43.9381 78.289L62.8004 44.46Z" fill="#FDFBFB" />
            <path d="M36.7935 37.9078C37.9751 36.0762 40.6538 36.0761 41.8354 37.9078L67.7793 78.1237C69.0671 80.12 67.634 82.75 65.2583 82.75H13.3706C10.995 82.75 9.56187 80.12 10.8497 78.1237L36.7935 37.9078Z" fill="white" />
        </svg>
    );
}

export default Image;

import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, height, fill }) {
    return (
        <svg className={className} height={height} viewBox="0 0 177 200" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="88.0013" cy="53.0017" r="53.0017" fill={fill} />
            <path d="M176.661 200C176.661 176.573 167.354 154.106 150.789 137.541C134.224 120.976 111.757 111.67 88.3303 111.67C64.9037 111.67 42.4365 120.976 25.8714 137.541C9.30621 154.106 3.53733e-06 176.573 0 200L88.3303 200H176.661Z" fill={fill} />
        </svg>
    );
}

export default Image;





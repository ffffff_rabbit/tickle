import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS
// Import the file like this ... import CloseIcon from '../assets/X_icon.svg'
// Use it like this ... <CloseIcon className='icon-close' fill='#737074' /> 


function Image({ className, fill }) {
    return (
        <svg className={className} viewBox="0 0 134 100" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="0.280273" width="133.486" height="23.0747" rx="11.5373" fill={fill}/>
        <rect x="0.280273" y="38.4502" width="133.486" height="23.0747" rx="11.5373" fill={fill}/>
        <rect x="0.280273" y="76.9258" width="133.486" height="23.0747" rx="11.5373" fill={fill}/>
        </svg>
    );
  }

  export default Image;





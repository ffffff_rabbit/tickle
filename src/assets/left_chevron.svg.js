import React from 'react';

// Instructions: 
// + Replace the <svg> content with the content of the SVG image.
// + Delete height and width in the SVG (only within the first <svg> line) 
// add "className={className}" to the first <svg> line
// Replace fill=, and stroke= with ={fill} so that you can set the color via CSS


function Image({ className, fill }) {
    return (
        <svg viewBox="0 0 6 11" className={className} fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect y="5.10364" width="7.2176" height="1" rx="0.5" transform="rotate(-45 0 5.10364)" fill={fill} />
            <rect x="5.10352" y="10.2" width="7.2176" height="1" rx="0.5" transform="rotate(-135 5.10352 10.2)" fill={fill} />
        </svg>
    );
  }

  export default Image;


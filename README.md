# Tickle

Tickle is a R&D app written to demonstrate [libp2p-protocol-feedsub](https://codeberg.org/ffffff_rabbit/feedsub) and [libp2p-transport-veilid](https://codeberg.org/ffffff_rabbit/libp2p-transport-veilid). Feedsub enables content broadcasting over Libp2p, while [Veilid](https://veilid.com/) is a new privacy-focused p2p communication network.

Tickle works like Twitter, you can follow users and receive their updates. Those updates can include text messages and files. You can open multiple app instances on your computer to test larger networks of content sources and followers.

Tickle uses [rust-libp2p](https://github.com/libp2p/rust-libp2p) as its core p2p networking stack. [Feedsub](https://codeberg.org/ffffff_rabbit/feedsub) is a custom libp2p protocol to manage data exchange between nodes. Feedsub manages the content discovery, distribution, and file "swarming" to receive parts of files from other followers.

This version of Tickle offers three libp2p transport options:

- VeilidSafe, an implementation of Veilid that uses [private routing](https://veilid.com/docs/private-routing/) to hide the IP addresses of the nodes in the network

- VeilidUnsafe, an implementation of Veilid that uses direct connections between nodes (where IP addresses are NOT hidden)

- TCP, the standard Libp2p TCP Transport

Tickle uses [Tauri](https://tauri.app/) (React and Rust) as its application framework.

[Connect with us - join our SimpleX group](https://simplex.chat/contact#/?v=2-5&smp=smp%3A%2F%2FSkIkI6EPd2D63F4xFKfHk7I1UGZVNn6k1QWZ5rcyr6w%3D%40smp9.simplex.im%2FSWxE-5AB6wXyqvx94xB_aelg7Kw3SCjd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAXEixok41ekdED6xD0iia9NiZO3F0BbJ1ygKGTh6ROGs%253D%26srv%3Djssqzccmrcws6bhmn77vgmhfjmhwlyr3u7puw4erkyoosywgl67slqqd.onion&data=%7B%22type%22%3A%22group%22%2C%22groupLinkId%22%3A%22Lke8fQgDE3km91u7qth7dA%3D%3D%22%7D)

## Get Started

**You'll need:**

- [NodeJS / NPM](https://nodejs.org/en/download)
- [Rust / Cargo](https://www.rust-lang.org/tools/install)

**To download the React / JS dependencies:**

From root: `npm install`

### Configure the desired transport

```rust
    // choose which transport to use (VeilidSafe, VeilidUnsafe, or Tcp)
    const SETTINGS: Settings = Settings {
        transport: TransportType::VeilidSafe,
        asset_max_fragment_size_bytes: 100000,
    };

```

See [libp2p-transport-veilid](https://codeberg.org/ffffff_rabbit/libp2p-transport-veilid) for details on the Veilid transport.

The TCP transport should work on your local network but generally won't work remotely because it doesn't support WiFi holepunching.

### Start the development app

- from root: `npm run tauri dev`
- edit settings: /src-tauri/.veilid/settings
- view dev logs: /src-tauri/.tickle/log/log0.log (which overflows to log1.log)
- change logger level filtering in /src-tauri/src/logger.rs

### Start annother testing app

- launch /src-tauri/target/debug/tickle
- edit the settings file: username/.tickle/.veilid/settings (macOS)

### Testing instructions

To test Tickle, you can use two (or more) app instances on your computer to broadcast content from content creators to content followers.

Build and start a development app (see above). Then launch another testing app instance (/src-tauri/target/debug/tickle).

Each app will open to "Get online", which is expecting an invite code (click ONLINE (0) if you need to reopen this modal). In one of the apps, click on "Invite to Tickle" to get an invite code. This app will be a content creator. Paste it into the other app (which will attempt to connect and follow that content creator).

This should start the connection handshake which, with Veilid, might take a while. Once the connection has established, you should see "ONLINE (1)" displayed in each app instance as both nodes are connected to each other.

In the content creator instance, you can send text messages and files. You should see those transmitted to the follower(s). You can open up other instances and connect them using the instance's invite code. You should see instances discover potential content creators through a network of trust (if I follow a content creator, I trust them and will see potential content creators that they follow).

To reopen the "Get online" connection window, click the "OFFLINE" indicator and "New connection" button.

### Build the production app

From root: `npm run tauri build`

use std::io;

use base64::encode;
use feedsub::{Feed, FeedsubEvent, Update, FeedType};
use libp2p::{identity::Keypair, swarm::StreamUpgradeError, PeerId, Swarm};
use tauri::Window;

use crate::{
    feeds::{FeedId, UpdateData},
    send_msg_to_ui, Msg, Person, SysEvent, SysEventType,
    TickleBehaviour, UiEvent, files::{FileFragment, self},
};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Clone, PartialEq)]
pub enum MsgOrigination {
    Peer,
    Local
}

pub struct FeedsubEventHandler {}

impl FeedsubEventHandler {
    pub async fn handle_event(
        main_window: &Window,
        swarm: &mut Swarm<TickleBehaviour>,
        feedsub_event: FeedsubEvent<FeedId>,
        person_keys: &Keypair
    ) {
        debug!("FeedsubEventHandler::handle_event");

        match feedsub_event {
            FeedsubEvent::DialPeer { addr } => {
                debug!("FeedsubEventHandler | DialPeer {:?}", addr);

                let result = swarm.dial(addr);
                match result {
                    Ok(address) => {
                        trace!("Dial: Ok {:?}", address);
                        send_msg_to_ui(UiEvent::NewConnectionSuccess, None, main_window).await;
                    }
                    Err(e) => {
                        send_msg_to_ui(
                            UiEvent::NewConnectionFailure,
                            Some(e.to_string()),
                            main_window,
                        )
                        .await;
                        error!("Dial: Error {:?}", e)
                    }
                }
            },
            FeedsubEvent::DisconnectPeer { peer_id } => {
                let result = swarm.disconnect_peer_id(peer_id);
                match result {
                    Ok(_) => {
                        debug!("DisconnectPeer: Ok {:?}", peer_id);
                        send_msg_to_ui(UiEvent::Refresh, None, main_window).await;
                    },
                    Err(_) => {
                        error!("DisconnectPeer: Error");
                    },
                }
            },

            FeedsubEvent::PeerConnected { peer_id } => {
                debug!("FeedsubEventHandler | PeerConnected {:?}", peer_id);

            }

            FeedsubEvent::PeerDisconnected { peer_id  } => {
                debug!("FeedsubEventHandler | PeerDisconnected {:?}", peer_id);
                let feedsub = &mut swarm.behaviour_mut().feedsub;
                let feed_ids = vec![FeedId::People(peer_id.to_string())];
                feedsub.unsubscribe(feed_ids);
            }

            FeedsubEvent::SelfSubscribe { feeds } => {
                Self::handle_self_subscribe(swarm, main_window, feeds, person_keys).await;
            }

            FeedsubEvent::SelfUnsubscribe { feed_ids } => {
                debug!("FeedsubEventHandler | SelfUnsubscribe {:?}", feed_ids);
            }

            FeedsubEvent::PeerSubscribe { peer_id, feeds } => {
                debug!(
                    "FeedsubEventHandler | PeerSubscribe {:?} {:?}",
                    peer_id, feeds
                );

                Self::handle_peer_subscribe(main_window, peer_id, feeds).await
            }

            FeedsubEvent::PeerUnsubscribe { peer_id, feed_ids } => {
                debug!(
                    "FeedsubEventHandler | PeerUnsubscribe {:?} {:?}",
                    peer_id, feed_ids
                );
                Self::handle_peer_unsubscribe(main_window, peer_id, feed_ids).await;
            }

            FeedsubEvent::SelfUpdate { update, feed } => {
                trace!("FeedsubEventHandler | SelfUpdate {:?} {:?}", feed, update);
                debug!("FeedsubEventHandler | SelfUpdate {:?}", feed);  

                Self::handle_self_update(swarm, main_window, feed, &update).await;

            }

            FeedsubEvent::PeerUpdate {
                peer_id,
                updates,
            } => {
                debug!(
                    "FeedsubEventHandler | PeerUpdate {:?} updates {:?}",
                    peer_id, updates.len()
                );
                Self::handle_peer_update(swarm, main_window, peer_id, updates, person_keys).await;
            }
            FeedsubEvent::PeerTask { task } => {
                debug!("FeedsubEventHandler | PeerTask {:?}", task);
            }
            FeedsubEvent::SelfTask { task } => {
                debug!("FeedsubEventHandler | SelfTask {:?}", task);
            }
            FeedsubEvent::Error { peer_id, error } => {
                error!("FeedsubEventHandler | Error {:?} {:?}", peer_id, error);
                Self::handle_error(main_window, peer_id, error).await
            }
        }
    }

    pub async fn handle_peer_subscribe(
        main_window: &Window,
        peer_id: PeerId,
        feeds: Vec<Feed<FeedId>>,
    ) {
        debug!(
            "FeedsubEventHandler::handle_subscribe {:?} {:?}",
            peer_id, feeds
        );

        for feed in feeds {
            let id = feed.id;

            let msg = format!("Peer: {} subscribed to Feed ID: {}", peer_id, id);

            if let Some(feed_type) = feed.feed_type{

                if let FeedId::File { cid, .. } = id {
                    if let FeedType::FiniteSeq { last_seq, .. } = feed_type{
                        if let Some(file) = files::fetch::by_cid(&cid).await{
                            if file.last_seq.is_none(){
                                let _ = file.update_last_seq(last_seq.to_u64()).set().await;
                            }
                        }
                    }
                }
            }

            SysEvent::publish(
                main_window,
                11,
                SysEventType::Swarm,
                "New subscription",
                msg.to_string(),
            )
            .await;
        }
    }

    pub async fn handle_peer_unsubscribe(
        main_window: &Window,
        peer_id: PeerId,
        feed_ids: Vec<FeedId>,
    ) {
        debug!(
            "FeedsubEventHandler::handle_unsubscribe {:?} {:?}",
            peer_id, feed_ids
        );

        for feed_id in feed_ids {

            let msg = format!("Peer: {} unsubscribed from Feed ID: {}", peer_id, feed_id);
            SysEvent::publish(
                main_window,
                14,
                SysEventType::Swarm,
                "Unsubscribed",
                msg.to_string(),
            )
            .await;
        }
    }

    async fn handle_peer_update(
        swarm: &mut Swarm<TickleBehaviour>,
        main_window: &Window,
        peer_id: PeerId,
        updates: Vec<Update<FeedId>>,
        person_keys: &Keypair,
    ) {
        debug!(
            "FeedsubEventHandler::handle_peer_update {:?}",
            peer_id
        );

        trace!(
            "FeedsubEventHandler::handle_peer_update {:?}",
            updates
        );
        
        if !updates.is_empty() {
            debug!(
                "FeedsubEventHandler::handle_peer_update | Updates {:?}",
                updates.len()
            );
            for update in updates {
                let feed_id = &update.feed_id;
                let feed_update = UpdateData::decode(&update.data);
                let signature = update.sig;

                match feed_id {
                    FeedId::File { cid, name, total_size, slice_size, public_key } => {
                        let seq = update.seq;

                        debug!("FeedsubEventHandler::handle_peer_update | File | {:?} | {:?} | {:?} | {:?} | {:?} | {:?}", seq, cid, name, total_size, slice_size, public_key);
                    
                        if let UpdateData::FileFragment(FileFragment(data)) = feed_update {
                            debug!("FeedsubEventHandler::handle_peer_update | File | received data {:?}", data.len());
                            files::update::handle_fragment(cid, seq, data.to_vec(), signature, main_window).await;
                        }
                        
                    },

                    FeedId::People(_) => match feed_update {
                        UpdateData::Person(person_id) => {
                            debug!(
                                "FeedsubEventHandler::handle_peer_update | Person {:?}",
                                person_id
                            );

                            match Person::get(&person_id).await{
                                Some(_) => {},
                                None => {
                                    match Person::default().update_person_id(&person_id).set().await{
                                        Ok(person) => {
                                            let feedsub = &mut swarm.behaviour_mut().feedsub;
                                            let person_pub_key_string = encode(person_keys.public().encode_protobuf());

                                            let _ = feedsub.broadcast_update(FeedId::People(person_pub_key_string), UpdateData::Person(person_id.to_string()).encode(), person_keys);
                
                                            Person::broadcast_to_ui(main_window, person).await.unwrap();
                                        },
                                        Err(_) => error!("Failed to create a new person"),
                                    }
                                },
                            }



                        }
                        _ => error!(
                            "FeedsubEventHandler::handle_peer_update | wrong update for FeedId::People"
                        ),
                    },
                    FeedId::Blog(_) => match feed_update {
                        UpdateData::BlogMessage(msg) => {
                            debug!("FeedsubEventHandler::handle_peer_update | BlogMessage {:?}", msg);

                            let seq = update.seq;
                            
                            let msg = msg.clone().update_sequence_number(&seq);

                            Self::handle_message(swarm, main_window, msg, MsgOrigination::Peer).await;
                                
                        }
                        _ => error!(
                            "FeedsubEventHandler::handle_peer_update | wrong update for FeedId::Blog"
                        ),
                    },
                    FeedId::Person(_) => {
                        match UpdateData::decode(&update.data) {
                            // UpdateData::PersonProfile(_) => todo!(),
                            UpdateData::NewFeed(feed_id) => {
                                match feed_id{
                                    FeedId::Blog(id) => {
                                        let feedsub = &mut swarm.behaviour_mut().feedsub;
                                        
                                        let blog_feed = Feed::new(
                                            FeedId::Blog(id.to_string()), None);
                                        
                                        feedsub.subscribe(vec![blog_feed]);
                                    },
                                    _ => error!(
                                        "FeedsubEventHandler::handle_peer_update | wrong update for UpdateData::NewFeed"
                                    ),
                                    //
                                }
                            },
                            _ => error!(
                                "FeedsubEventHandler::handle_peer_update | wrong update for FeedId::Person"
                            ),
                        }
                    }
                }
            }
        }
    }

    pub async fn handle_error(
        _main_window: &Window,
        peer_id: PeerId,
        error: StreamUpgradeError<io::Error>,
    ) {
        debug!(
            "FeedsubEventHandler::handle_error {:?} {:?}",
            peer_id, error
        );

        todo!()
    }


    pub async fn handle_message(swarm: &mut Swarm<TickleBehaviour>, main_window: &Window, msg: Msg, origination: MsgOrigination) {
        debug!("FeedsubEventHandler::handle_message | {:?}", msg);

        let json = serde_json::to_string(&msg).unwrap();

        // Save the message
        match msg.clone().set().await {
            Ok(_) => {
                let _ = &main_window
                    .emit("new_message", json)
                    .expect("failed to emit");
            }
            Err(e) => {
                error!("FeedsubEvent::Blog {:?}", e);
            }
        }

        // If Peer update, subscribe to the files
        if origination == MsgOrigination::Peer {
            let mut feeds = Vec::new();
            match &msg.file_ids {
                None => {}
                Some(file_ids) => {
                    debug!(
                        "FeedsubEvent::Blog | received file_ids {:?}",
                        file_ids
                    );
                    for feed_id in file_ids {
                        if let FeedId::File { cid, name, total_size, slice_size, public_key } = feed_id {

                            if let (Some(name), Some(total_size), Some(slice_size)) = (name, total_size, slice_size){

                                let last_seq = (total_size + slice_size - 1 ) / slice_size;

                                let feed_id_file = FeedId::File {
                                    cid: cid.to_string(),
                                    public_key: public_key.to_string(),
                                    name: Some(name.to_string()),
                                    total_size: Some(*total_size), 
                                    slice_size: Some(*slice_size)
                                };

                                let feed_type = FeedType::new_finite_seq(last_seq as u64, "");
    
                                let feed = Feed::new(feed_id_file.clone(), Some(feed_type));
    
                                feeds.push(feed);

                                let _ = files::create::full(cid, name, total_size, slice_size, public_key).await;

                            }
                        }
                    }
                }
            }
            if !feeds.is_empty(){
                let _ = &swarm
                .behaviour_mut()
                .feedsub.subscribe(feeds);
            }
        }

        // Fetch the message icon
        // match &msg.icon_cid {
        //     None => {}
        //     Some(cid) => match assets::fetch::asset_by_cid(&cid.to_string()).await {
        //         None => {
        //             assets::bitswap::request_remote_file_by_cid(
        //                 cid.to_string(),
        //                 Some(ContentType::Image),
        //             )
        //             .await;
        //         }
        //         Some(_) => {}
        //     },
        // }

        SysEvent::publish(
            main_window,
            12,
            SysEventType::Swarm,
            "New message",
            msg.content,
        )
        .await;
    }


    async fn handle_self_subscribe(swarm: &mut Swarm<TickleBehaviour>,
        main_window: &Window, feeds: Vec<Feed<FeedId>>, person_keys: &Keypair) {
            debug!("FeedsubEventHandler | SelfSubscribe {:?}", feeds);

            for feed in feeds {

                match feed.id {
                    FeedId::File { .. } => {},

                    FeedId::People(_) => {},
                    
                    FeedId::Blog(_) => {},
                    
                    FeedId::Person(person_id) => {
                        let config = crate::sys_config::fetch::sysconfig().await;
                        let my_person_id = config.my_person_id;

                        if person_id != my_person_id{
                            let person = Person::get_or_new(&person_id).await
                            .update_person_id(&person_id)
                            .update_is_followed(Some(true))
                            .set().await.unwrap();


                            let feedsub = &mut swarm.behaviour_mut().feedsub;

                            // subscribe to their people feed
                            let feed_type = FeedType::new_infinite_seq(0, "");                  
                                
                            let people_feed = Feed::new(
                                    FeedId::People(person_id.clone()), Some(feed_type));
                                
                            feedsub.subscribe(vec![people_feed]);

                            // announce new person
                            let person_pub_key_string = encode(person_keys.public().encode_protobuf());

                            let _ = feedsub.broadcast_update(FeedId::People(person_pub_key_string), UpdateData::Person(person_id).encode(), person_keys);

                            Person::broadcast_to_ui(main_window, person).await.unwrap();
                        }

                    },
                }
            }

       
    }

    async fn handle_self_update(
        swarm: &mut Swarm<TickleBehaviour>,
        main_window: &Window,
        _feed: Feed<FeedId>,
        update: &Update<FeedId>,
    ) {

        match &update.feed_id {
            FeedId::File { .. } => {},

            FeedId::People(_) => {},

            FeedId::Blog(_) => {
                trace!("FeedsubEventHandler::handle_self_update | FeedId::Blog {:?}", update);

                let seq = update.seq;

                let msg = match UpdateData::decode(&update.data) {
                    UpdateData::BlogMessage(msg) => Some(msg.clone()),
                    _ => None,
                };

                if let Some(msg) = msg {
                    let msg = msg.update_sequence_number(&seq);
                    Self::handle_message(swarm, main_window, msg, MsgOrigination::Local).await;
                }
            }
            FeedId::Person(_) => {
                trace!("FeedsubEventHandler::handle_self_update | FeedId::Person {:?}", update);
            }
            
        }
    }
}


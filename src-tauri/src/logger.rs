use log::LevelFilter;

use log4rs::{
    append::{
        console::{ConsoleAppender, Target},
        rolling_file::{
            policy::compound::{
                roll::fixed_window::FixedWindowRoller, trigger::size::SizeTrigger, CompoundPolicy,
            },
            RollingFileAppender,
        },
    },
    config::{Appender, Config, Logger, Root},
    encode::pattern::PatternEncoder,
    filter::threshold::ThresholdFilter,
};

pub fn init_logger() {
    let file_path = "./.tickle/log/log0.log";

    let window_size = 2;

    let fixed_window_roller = FixedWindowRoller::builder()
        .build("./log/log{}.log", window_size)
        .unwrap();

    let size_limit = 1024 * 1024 * 5;

    let size_trigger = SizeTrigger::new(size_limit);

    let compound_policy =
        CompoundPolicy::new(Box::new(size_trigger), Box::new(fixed_window_roller));

    // Build a stderr logger.
    let stderr = ConsoleAppender::builder().target(Target::Stderr).build();

    // Logging to log file.
    let logfile = RollingFileAppender::builder()
        // Pattern: https://docs.rs/log4rs/*/log4rs/encode/pattern/index.html
        .encoder(Box::new(PatternEncoder::new("{d} {l} - {m}\n")))
        .build(file_path, Box::new(compound_policy))
        .unwrap();

    // Log Trace level output to file where trace is the default level
    // and the programmatically specified level to stderr.
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .appender(
            Appender::builder()
                .filter(Box::new(ThresholdFilter::new(log::LevelFilter::Debug)))
                .build("stderr", Box::new(stderr)),
        )
        .logger(Logger::builder().build("tickle", LevelFilter::Info))
        .logger(Logger::builder().build("feedsub", LevelFilter::Info))
        .logger(Logger::builder().build("libp2p", LevelFilter::Info))
        .logger(Logger::builder().build("libp2p-core", LevelFilter::Info))
        .logger(Logger::builder().build("yamux", LevelFilter::Info))
        .logger(Logger::builder().build("veilid_core", LevelFilter::Info))
        .logger(Logger::builder().build("libp2p_tcp", LevelFilter::Info))
        .logger(Logger::builder().build("libp2p_transport_veilid", LevelFilter::Info))
        .build(
            Root::builder()
                .appender("logfile")
                .appender("stderr")
                .build(LevelFilter::Error),
        )
        .unwrap();

    log4rs::init_config(config).unwrap();
}

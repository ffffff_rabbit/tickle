use crate::feeds::{FeedId, TickleUpdateFetcher};
use crate::feedsub::{Feedsub, FeedsubConfig, FeedsubEvent};
use libp2p::{identity, swarm::NetworkBehaviour};

#[derive(NetworkBehaviour)]
#[behaviour(to_swarm = "BehaviourEvent")]
pub struct TickleBehaviour {
    pub feedsub: Feedsub<FeedId, TickleUpdateFetcher>,
}

fn get_feedsub_config(peer_keys: identity::Keypair) -> FeedsubConfig<FeedId> {
    FeedsubConfig::new(peer_keys)
        .with_max_outbound_connections(10)
        .with_max_inbound_connections(10)
        .with_target_peers_per_feed(10)
        .with_heartbeat_interval(5)
        .with_max_feeds_to_handshake(100)
        .with_max_feeds_to_fetch_peer_cache(10)
        .with_max_peers_to_send_on_overloaded(10)
        .with_disconnect_timeout(10)
}

impl TickleBehaviour {
    pub fn new(peer_keys: identity::Keypair) -> Self {
        Self {
            feedsub: Feedsub::new(get_feedsub_config(peer_keys), TickleUpdateFetcher),
        }
    }
}

// This is a straight pass-through of Feedsub events.
// Feedsub sends this to the Swarm. The Swarm provides them to the app.
#[derive(Debug)]
pub enum BehaviourEvent {
    Feedsub(FeedsubEvent<FeedId>),
}

impl From<FeedsubEvent<FeedId>> for BehaviourEvent {
    fn from(v: FeedsubEvent<FeedId>) -> Self {
        Self::Feedsub(v)
    }
}

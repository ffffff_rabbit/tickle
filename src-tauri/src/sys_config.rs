pub use crate::people::Person;
use crate::Msg;
use lazy_static::lazy_static;
use libp2p::Multiaddr;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;

type Result<T> = std::result::Result<T, Box<String>>;

lazy_static! {
    static ref CONFIG: Mutex<SysConfig> = Mutex::new(Default::default());
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub enum AppStatus {
    #[default]
    Offline,
    Online,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct SysConfig {
    pub my_node_id: String,
    pub my_person_id: String,
    pub my_name: String,
    pub my_listeners: Vec<Multiaddr>,
    pub my_icon_cid: Option<String>,
    pub my_bio: String,
    pub my_user_profile_cid: Option<String>,
    pub app_status: AppStatus,
}

#[derive(Debug, Serialize, Deserialize, Clone)]

pub struct UserProfile {
    pub person_id: String,
    pub name: String,
    pub icon_cid: Option<String>,
    pub bio: String,
    pub messages: Vec<Msg>,
}

impl SysConfig {
    pub fn update_my_node_id(mut self, node_id: &String) -> SysConfig {
        trace!("SysConfig::update_my_peer_id");
        self.my_node_id = node_id.to_string();
        self
    }

    pub fn update_my_person_id(mut self, person_id: &String) -> SysConfig {
        trace!("SysConfig::update_my_person_id");
        self.my_person_id = person_id.to_string();
        self
    }

    pub fn update_my_name(mut self, name: &Option<String>) -> SysConfig {
        trace!("SysConfig::update_my_name");

        match name {
            None => {
                let name = self.my_person_id[44..].to_string();

                trace!("My name is... {}", name);

                self.my_name = name;

                self
            }
            Some(name) => {
                let name = name.to_string();

                trace!("My name is... {}", name);

                self.my_name = name;

                self
            }
        }
    }

    pub fn update_my_bio(mut self, bio: &Option<String>) -> SysConfig {
        trace!("SysConfig::update_my_bio");

        match bio {
            None => {
                self.my_bio = String::from("");

                self
            }
            Some(bio) => {
                let bio = bio.to_string();

                self.my_bio = bio;

                self
            }
        }
    }

    pub fn update_my_user_profile_cid(mut self, cid: &String) -> SysConfig {
        trace!("SysConfig::update_my_user_profile_cid");
        self.my_user_profile_cid = Some(cid.to_string());
        self
    }

    pub fn update_app_status(mut self, status: AppStatus) -> SysConfig {
        trace!("SysConfig::update_my_user_profile_cid");
        self.app_status = status.clone();
        self
    }

    pub fn add_my_listener(mut self, listener: &Multiaddr) -> SysConfig {
        trace!("SysConfig::add_listener");

        if !self.my_listeners.contains(listener) {
            self.my_listeners.push(listener.to_owned());
        }

        self
    }

    pub fn remove_my_listener(mut self, listener: &Multiaddr) -> SysConfig {
        trace!("SysConfig::remove_listener");

        let mut i = 0;

        while i < self.my_listeners.len() {
            if &self.my_listeners[i] == listener {
                self.my_listeners.remove(i);
            } else {
                i += 1;
            }
        }

        self
    }

    pub fn update_my_icon_cid(mut self, icon_cid: &Option<String>) -> SysConfig {
        trace!("SysConfig::update_my_icon_cid {:?}", icon_cid);

        match icon_cid {
            None => {
                self.my_icon_cid = None;

                self
            }
            Some(icon_cid) => {
                self.my_icon_cid = Some(icon_cid.to_string());

                self
            }
        }
    }

    pub async fn set(self) -> Result<SysConfig> {
        debug!("SysConfig::set | start {:?}", self);

        async fn update_my_person_record(my_person_id: &String, new_cid: &Result<String>) {
            if new_cid.is_ok() {
                match Person::get(my_person_id).await {
                    Some(me) => {
                        trace!("SysConfig::set | update my Person record");

                        me.update_user_profile_cid(new_cid.as_ref().unwrap())
                            .set()
                            .await
                            .unwrap();
                    }
                    None => warn!("SysConfig::set | my Person profile wasn't found"),
                }
            }
        }

        let new_config = self.clone();

        let my_person_id = &self.my_person_id;

        let old_cid = &self.my_user_profile_cid;

        let new_cid = create::user_profile(self.clone()).await;

        // update my_user_profile_cid if there is a new user profile
        let new_config = if let Ok(new_cid) = &new_cid {
            new_config.update_my_user_profile_cid(new_cid)
        } else {
            new_config
        };

        // set the object
        let mut config = CONFIG.lock().await;

        *config = new_config.clone();

        // update my person record
        update_my_person_record(my_person_id, &new_cid).await;

        // if the old cid changed, delete the user profile on disk
        if let Some(old_cid) = old_cid {
            if let Ok(new_cid) = new_cid {
                if old_cid != &new_cid {
                    delete::user_profile_by_cid(old_cid).await;
                }
            }
        }

        debug!("SysConfig::set | {:?}", new_config);

        Ok(new_config)
    }

    // pub fn is_my_address(self, address: &Multiaddr) -> bool {
    //     trace!("SysConfig::is_my_address");

    //     self.my_listeners.contains(address)

    //     // if self.my_listeners.contains(address) {
    //     //     return true;
    //     // } else {
    //     //     return false;
    //     // }
    // }
}

pub mod create {

    use super::Result;
    // use crate::assets;
    use crate::{sys_config::UserProfile, Msg, SysConfig};
    use base64::encode;
    #[allow(unused_imports)]
    use log::{debug, error, info, trace, warn};

    // This is the user's profile data published to other nodes
    pub async fn user_profile(sysconfig: SysConfig) -> Result<String> {
        trace!("UserProfile | create | user_profile | {:?}", sysconfig);

        let my_id = sysconfig.my_person_id;

        let messages = Msg::get_all_messages_for_person(&my_id).await;

        let messages = match messages.len() {
            0..=25 => messages,
            _ => {
                let index = messages.len() - (25);

                let last = messages.get(index..);

                last.unwrap().to_vec()
            }
        };

        let user_profile = UserProfile {
            person_id: my_id,
            name: sysconfig.my_name.clone(),
            icon_cid: sysconfig.my_icon_cid.clone(),
            bio: sysconfig.my_bio.clone(),
            messages,
        };

        let json = serde_json::to_string(&user_profile).unwrap();

        let _json_as_base64 = encode(json);

        // let save = assets::create::file_from_base64(
        //     json_as_base64,
        //     None,
        //     crate::ContentType::UserProfile,
        //     Some(Origination::Internal),
        // )
        // .await;

        // match save {
        //     Ok(cid) => {
        //         trace!(
        //             "SysConfig | save_user_profile | Saved to /content {:?}",
        //             cid
        //         );

        //         if sysconfig.my_user_profile_cid.is_some() {
        //             let old_cid = sysconfig.my_user_profile_cid.unwrap();

        //             if cid != old_cid {
        //                 debug!(
        //                     "SysConfig | save_user_profile | looking to decrement {:?}",
        //                     old_cid
        //                 );

        //                 // assets::update::decrease_ref_count_for_cid(&old_cid)
        //                 //     .await
        //                 //     .unwrap();
        //             }
        //         }

        //         Ok(cid.to_string())
        //     }
        //     Err(e) => {
        //         let error = Box::new(format!("{:?}", e));

        //         Err(error)
        //     }
        // }
        Ok(String::from("Incomplete"))
    }
}

pub mod fetch {

    use crate::{sys_config::UserProfile, sys_config::CONFIG, SysConfig};
    #[allow(unused_imports)]
    use log::{debug, error, info, trace, warn};
    use std::fs;

    // This is the user's profile data published to other nodes
    pub async fn user_profile_by_cid(cid: &String) -> Option<UserProfile> {
        trace!("SysConfig | get_user_profile");

        let dir = format!(
            "/.tickle/content/{}",
            crate::sys_config::fetch::sysconfig().await.my_name
        );

        let path = format!("{}/{}", dir, cid);

        let read = fs::read(path);

        match read {
            Ok(data) => {
                trace!("SysConfig | get_user_profile | Success");

                let string = String::from_utf8_lossy(&data).to_string();

                let user_profile = serde_json::from_str::<UserProfile>(&string).unwrap();

                Some(user_profile)
            }
            Err(_) => {
                trace!("SysConfig | get_user_profile | Not found");

                None
            }
        }
    }

    // This is the internal config for the app
    pub async fn sysconfig() -> SysConfig {
        trace!("SysConfig::get");

        let config = CONFIG.lock().await;

        config.clone()
    }
}

pub mod update {

    use super::Result;
    use crate::sys_config::fetch;
    #[allow(unused_imports)]
    use log::{debug, error, info, trace, warn};

    /// To refresh the user profile published to other nodes (eg. new messages)

    pub async fn refresh_user_profile() -> Result<()> {
        trace!("SysConfig | update | refresh_user_profile");

        let sysconfig = fetch::sysconfig().await;

        let new = sysconfig.set().await;

        match new {
            Ok(_) => Ok(()),
            Err(e) => {
                let error = Box::new(format!("{:?}", e));

                Err(error)
            }
        }
    }
}

pub mod delete {

    #[allow(unused_imports)]
    use log::{debug, error, info, trace, warn};

    pub async fn user_profile_by_cid(cid: &String) {
        trace!("SysConfig | delete | user_profile_by_cid {:?}", cid);

        // let delete = crate::assets::delete::mark_shoulddeleteit_by_cid(cid).await;

        // match delete {
        //     Err(e) => {
        //         error!("Sysconfig | delete | user_profile_by_cid {:?}", e)
        //     }
        //     Ok(_) => {
        //         trace!("SysConfig::set | old user_profile deleted {:?}", cid);
        //     }
        // }
    }
}

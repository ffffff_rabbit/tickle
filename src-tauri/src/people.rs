// use crate::feedsub::structs_proto::rebase::Subscription as ProtoSubscription;
use lazy_static::lazy_static;
use libp2p::Multiaddr;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tauri::Wry;
use tokio::sync::Mutex;

// use tokio::time::{sleep, Duration};

// use libp2p::swarm::Swarm;

pub use crate::behaviour::{BehaviourEvent, TickleBehaviour};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// pub use crate::connections::{Connection, ConnectionStatus};
pub use crate::messages::Msg;
pub use crate::sys_config::SysConfig;

type Result<T> = std::result::Result<T, Box<String>>;

lazy_static! {
    static ref PEOPLE: Mutex<People> = Mutex::new(Default::default());
}

pub type ID = String;

pub type People = HashMap<ID, Person>;

#[derive(Debug, Serialize, Deserialize, Clone)]

pub struct Person {
    pub person_id: String,
    pub is_followed: Option<bool>,
    pub msg_ranges: String,
    pub addresses: Option<Vec<Multiaddr>>,
    pub user_profile_cid: Option<String>,
}

impl Default for Person {
    fn default() -> Person {
        Person {
            person_id: String::from(""),
            is_followed: None,
            msg_ranges: String::from("0"),
            addresses: None,
            user_profile_cid: None,
        }
    }
}

impl Person {
    pub async fn get(person_id: &String) -> Option<Person> {
        // Lock people
        let people = PEOPLE.lock().await;
        people.get(person_id).cloned()
    }

    pub async fn get_or_new(person_id: &String) -> Person {
        trace!("Person::get_or_new");

        // Lock people
        let people = PEOPLE.lock().await;

        //  return existing record or, if not found, a new record
        let record = people.get(person_id);

        match record {
            Some(person) => person.clone(),
            None => {
                trace!("Person not found {}, returning default", person_id);

                Person::default().update_person_id(person_id)
            }
        }
    }

    pub async fn get_all() -> Vec<Person> {
        let people = PEOPLE.lock().await;

        let mut person_list = Vec::new();

        for person in people.iter() {
            person_list.push(person.1.clone());
        }

        person_list
    }

    pub fn update_person_id(mut self, person_id: &String) -> Person {
        trace!("Person::update_person_id");

        self.person_id = person_id.to_string();

        self
    }

    /// Updates the is_followed status which determines whether we receive the person's content

    pub fn update_is_followed(mut self, is_followed: Option<bool>) -> Person {
        trace!("Person::update_is_followed");

        self.is_followed = is_followed;

        trace!("update_is_followed | Self {:?}", self);

        self
    }

    pub fn update_user_profile_cid(mut self, cid: &String) -> Person {
        trace!("Person::update_user_profile_cid");

        self.user_profile_cid = Some(cid.to_string());

        self
    }

    pub async fn set(self) -> Result<Person> {
        trace!("Person::set");

        if self.person_id.is_empty() {
            error!("Person ID cannot be empty: {:?}", self);

            let error = Box::new("Person ID cannot be empty".to_string());

            Err(error)
        } else {
            let mut people = PEOPLE.lock().await;

            people.insert(self.person_id.clone(), self.clone());

            info!("Person::set PEOPLE: {:?}", people);

            Ok(self)
        }
    }

    // pub async fn is_followed(person_id: &String) -> bool {
    //     trace!("Person::is_followed");

    //     let people = Person::get_all().await;

    //     trace!("Person::is_followed {:?}", people);

    //     for person in people {
    //         if &person.person_id == person_id {
    //             match person.is_followed {
    //                 Some(true) => return true,
    //                 _ => return false,
    //             }
    //         } else {
    //             continue;
    //         }
    //     }

    //     false
    // }

    // pub async fn delete_address_from_all(addr: &Multiaddr) -> Result<String> {
    //     trace!("Person::remove_address_from_all | address: {}", addr);

    //     let mut people = PEOPLE.lock().await;

    //     let clone = people.clone();

    //     for person in clone.iter() {
    //         let person = person.1;

    //         if let Some(addresses) = person.addresses.clone() {
    //             let mut address_list = Vec::new();

    //             let mut new = Person {
    //                 person_id: person.person_id.to_string(),
    //                 is_followed: person.is_followed,
    //                 msg_ranges: person.msg_ranges.to_string(),
    //                 addresses: None,
    //                 user_profile_cid: person.user_profile_cid.clone(),
    //             };

    //             for address in addresses {
    //                 if &address != addr {
    //                     address_list.push(address);
    //                 }
    //             }

    //             if !address_list.is_empty() {
    //                 new.addresses = Some(address_list);
    //             }

    //             people.insert(new.person_id.to_string(), new);
    //         }
    //     }

    //     trace!("Person::remove_address_from_all | {:?}", people);

    //     Ok(String::from("Removed"))
    // }

    // broadcasts a new person to the UI
    pub async fn broadcast_to_ui(window: &tauri::Window<Wry>, person: Person) -> Result<Person> {
        trace!("Person::broadcast_to_ui");

        // Send record to the UI
        let json = serde_json::to_string(&person).unwrap();

        window.emit("new_person", json).expect("failed to emit");

        Ok(person)
    }
}

use chrono::Utc;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use tauri::Wry;
use tokio::sync::Mutex;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

lazy_static! {
    pub static ref SYSEVENTS: Mutex<SysEvents> = Mutex::new(Default::default());
}

#[derive(Debug, Serialize, Deserialize, Clone)]

pub enum SysEventType {
    Swarm,
    UI,
}

pub type SysEvents = Vec<SysEvent>;

#[derive(Debug, Serialize, Deserialize, Clone)]

pub struct SysEvent {
    pub id: u16,
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub event_type: SysEventType,
    pub event_name: String,
    pub message: String,
}

impl SysEvent {
    pub fn new(id: u16, event_type: SysEventType, name: String, message: String) -> SysEvent {
        SysEvent {
            id,
            timestamp: Utc::now(),
            event_type,
            event_name: name,
            message,
        }
    }

    pub async fn set(window: &tauri::Window<Wry>, event: SysEvent) -> Result<SysEvent> {
        let mut sysevents = SYSEVENTS.lock().await;

        let json = serde_json::to_string(&event).unwrap();

        let clone = event.clone();

        sysevents.push(event);

        // println!("{:?}", sysevents);

        window.emit("new_sysevent", json).expect("failed to emit");

        Ok(clone)
    }

    pub async fn publish(
        main_window: &tauri::Window<Wry>,
        id: u16,
        event_type: SysEventType,
        event: &str,
        details: String,
    ) {
        SysEvent::set(
            main_window,
            SysEvent::new(id, event_type, String::from(event), details.to_string()),
        )
        .await
        .unwrap();
    }
}

// async fn publish_sysevent (
//     main_window: &tauri::Window,
//     id: u16,
//     event_type: SysEventType,
//     event: &str,
//     details: String ){

//     SysEvent::set(
//         &main_window,
//         SysEvent::new(
//             id,
//             event_type,
//             String::from(event),
//             details.to_string())
//     )
//     .await
//     .unwrap();
// }

// EVENTS
// 00001: My app ID
// 00002: My topic
// 00003: Incoming connection
// 00004: ERROR: Incoming connection
// 00005: Connection established
// 00006: Connection closed
// 00007: Unreachable address
// 00008: Unreachable peer
// 00009: New listener
// 00010: Dialing...
// 00011: New subscription
// 00012: New message
// 00013: Expired listener
// 00014: Unsubscribed
// 00015: New IPFS listener

// main_window.emit("event-name", Payload { message: "Tauri is awesome!".into() }).unwrap();

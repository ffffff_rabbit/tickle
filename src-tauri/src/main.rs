#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use base64::{decode, encode};
use chrono::{Utc, DateTime};
use crypto::Crypto;
use futures::executor::block_on;
use lazy_static::lazy_static;
use logger::init_logger;
use libp2p::{
    core::upgrade, futures::StreamExt, identity::Keypair, noise::Config as NoiseConfig, swarm::{Swarm, SwarmEvent}, tcp, yamux::Config as YamuxConfig, Multiaddr, PeerId, SwarmBuilder, Transport
};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{
    cmp,
    env,
    fs::{canonicalize, read},
    error::Error,
    path::PathBuf, 
    sync::Arc, 
};
use tauri::{
    http::{Request, Response, ResponseBuilder},
    AppHandle, Manager, Wry,
};
use tokio::{
    runtime::Handle,
    sync::{mpsc, Mutex},
};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use libp2p_transport_veilid::{VeilidTransport, VeilidTransportType};

use feedsub::{self, Feed, Connection, ConnectionStatus, Direction, FeedType};

mod behaviour;
mod crypto;
mod feeds;
mod feedsub_event_handler;
mod files;
mod logger;
mod messages;
mod people;
mod sys_config;
mod sys_events;

use crate::{
    behaviour::{BehaviourEvent, TickleBehaviour},
    feeds::{UpdateData, FeedId},
    feedsub_event_handler::FeedsubEventHandler, 
    files::{helpers, FileType, FileStatus}, 
    messages::Msg,
    people::Person,
    sys_config::{AppStatus, SysConfig, UserProfile},
    sys_events::{SysEvent, SysEventType},
};

pub mod proto {
    include!(concat!(env!("OUT_DIR"), "/update_data.rs"));
}

lazy_static! {
    static ref LAST_FRAG_BROADCAST: Mutex<chrono::DateTime<chrono::Utc>> = Mutex::new(Utc::now());
    static ref CONNS: Arc<Mutex<Vec<Connection<FeedId>>>> = Arc::new(Mutex::new(Vec::new()));
}

struct Settings {
    /// Transport type (Veilid, TCP, etc.)
    transport: TransportType,
    /// size of file slices
    asset_max_fragment_size_bytes: usize,
}

const SETTINGS: Settings = Settings {
    transport: TransportType::VeilidSafe,
    asset_max_fragment_size_bytes: 100000,
};

#[allow(dead_code)]
enum TransportType {
    /// VeilidTransport with safe routes (IP address hidden)
    VeilidSafe, 
    /// VeilidTransport with direct connections (IP address not hidden) 
    VeilidUnsafe,
    /// Standard Tokio TCP transport for Libp2p
    Tcp,
}

#[tokio::main]
async fn main() {

    init_logger();

    // Start the app
    tauri::Builder::default()
        .setup(move |app| {
            // unify both Tauri and Rust under the same runtime
            let handle = Handle::current();

            let main_window = app.get_window("main").unwrap();

            let node_keys = Keypair::generate_ed25519();

            let transport = match SETTINGS.transport {
                TransportType::VeilidUnsafe => {
                    // VeilidTransport with direct connections (IP address not hidden) 
                    let cloned_handle = handle.clone();

                    VeilidTransport::new(Some(cloned_handle), node_keys.clone(), VeilidTransportType::Unsafe)
                        .upgrade(upgrade::Version::V1)
                        .authenticate(NoiseConfig::new(&node_keys).unwrap())
                        .multiplex(YamuxConfig::default())
                        .boxed()
                },
                TransportType::VeilidSafe => {
                    // VeilidTransport using private / safe routes (app IP address is hidden) 
                    let cloned_handle = handle.clone();

                    VeilidTransport::new(Some(cloned_handle), node_keys.clone(), VeilidTransportType::Safe)
                        .upgrade(upgrade::Version::V1)
                        .authenticate(NoiseConfig::new(&node_keys).unwrap())
                        .multiplex(YamuxConfig::default())
                        .boxed()
                },
                TransportType::Tcp => {

                    let config = tcp::Config::default();

                    tcp::tokio::Transport::new(config)
                        .upgrade(upgrade::Version::V1)
                        .authenticate(NoiseConfig::new(&node_keys).unwrap())
                        .multiplex(YamuxConfig::default())
                        .boxed()
                },
            };
    
            // configure the swarm
            let mut swarm = SwarmBuilder::with_existing_identity(node_keys.clone())
                .with_tokio()

                // Transport
                .with_other_transport(|_key| {
                    Ok(transport)
                }).unwrap()

                .with_behaviour(|_| {
                    Ok(TickleBehaviour::new(node_keys.clone()))
                }).unwrap()
                .build();

            Swarm::listen_on(
                &mut swarm,
                "/ip4/0.0.0.0/tcp/0"
                    .parse()
                    .expect("can get a local socket"),
            )
            .expect("swarm can be started");


            let node_id = swarm.local_peer_id().to_string();
            let person_keys = Keypair::generate_ed25519();
            
            handle.spawn(async move {
                Crypto::fetch().await.update_person_keys(&person_keys).set().await;
                Crypto::fetch().await.update_node_keys(&node_keys).set().await;

                let feedsub = &mut swarm.behaviour_mut().feedsub;

                // In the future, this will be the user's static keypair, probably fetched from disk. 
                let person_pub_key_string = encode(person_keys.public().encode_protobuf());

                // define the people feed for this user
                let feed_id_people = FeedId::People(person_pub_key_string.clone());
                
                let feed_type_people = FeedType::new_infinite_seq(0, "");
                let people = Feed::new(feed_id_people.clone(), Some(feed_type_people));

                //  define the person and blog feed for this user
                let feed_id_person = FeedId::Person(person_pub_key_string.clone());
                let feed_type_person = FeedType::new_infinite_seq(0, "");
                let person = Feed::new(feed_id_person.clone(), Some(feed_type_person));

                let feed_id_blog = FeedId::Blog(person_pub_key_string.clone());
                let feed_type_blog = FeedType::new_infinite_seq(0, "");
                let blog = Feed::new(feed_id_blog.clone(), Some(feed_type_blog));
            
                // call subscribe() with the list of feeds            
                feedsub.subscribe(vec![people, blog, person]);

                // Broadcast that my person feed has a new blog feed
                let new_feed_update = UpdateData::NewFeed(feed_id_blog);
                // broadcast_update() takes the author's keys so it can sign updates
                let _ = feedsub.broadcast_update(feed_id_person, new_feed_update.encode(), &person_keys);

                // Broadcast that the people feed has a new person
                let person_update = UpdateData::Person(person_pub_key_string.clone());
                // broadcast_update() takes the author's keys so it can sign updates
                let _ = feedsub.broadcast_update(feed_id_people, person_update.encode(), &person_keys);


                // Listen to events from UI
                let (ui_send, mut ui_recv) = mpsc::channel(128);

                main_window.listen("ui", move |event| {
                    block_on(ui_send.send(event)).unwrap();
                    trace!("Event from UI Sent Internally");
                });

                // Create a channel between the file processing threads and the main thread
                let (file_event_send, mut file_event_recv) = mpsc::channel(128);

                // Publish Peer ID and topic as a SysEvent
                SysEvent::set(
                    &main_window,
                    SysEvent::new(
                        1,
                        SysEventType::Swarm,
                        String::from("My app ID"),
                        node_id.to_string(),
                    ),
                )
                .await
                .unwrap();

                SysEvent::set(
                    &main_window,
                    SysEvent::new(
                        2,
                        SysEventType::Swarm,
                        String::from("My person ID"),
                        person_pub_key_string.clone().to_string(),
                    ),
                )
                .await
                .unwrap();

                // Add peer and person ID to app configuration settings
                crate::sys_config::fetch::sysconfig()
                    .await
                    .update_my_person_id(&person_pub_key_string)
                    .update_my_node_id(&node_id.to_string())
                    .update_my_name(&None)
                    .set()
                    .await
                    .unwrap();

                loop {
                    tokio::select! {

                        event = swarm.select_next_some() => {

                            match event {

                                //Incoming Connection
                                SwarmEvent::IncomingConnection {send_back_addr, .. } => {
                                    trace!("SwarmEvent::IncomingConnection {:?}", send_back_addr);
                                    SysEvent::publish(&main_window, 3, SysEventType::Swarm, "Incoming connection", send_back_addr.to_string()).await;
                                },

                                // Incoming Connection Error
                                SwarmEvent::IncomingConnectionError {local_addr, error, ..} => {
                                    trace!("SwarmEvent::IncomingConnectionError {:?} {:?}", local_addr, error);
                                    let msg = format!("{} from {}", error, local_addr);
                                    SysEvent::publish(&main_window, 4, SysEventType::Swarm, "ERROR: Incoming connection", msg.to_string()).await;
                                },

                                // Connection Established
                                SwarmEvent::ConnectionEstablished {peer_id, endpoint, ..} => {
                                    debug!("SwarmEvent::ConnectionEstablished {:?} {:?}", peer_id, endpoint);
                                    let msg = format!("{} connected to {:?}", peer_id, endpoint);
                                    SysEvent::publish(&main_window, 5, SysEventType::Swarm, "Connection established", msg.to_string()).await;

                                    match endpoint {
                                        libp2p::core::ConnectedPoint::Dialer {address, ..} => {
                                            debug!("SwarmEvent::ConnectionEstablished | Dialer {}", address);
                                        },

                                        libp2p::core::ConnectedPoint::Listener {local_addr, send_back_addr} => {
                                            debug!("Listener {} {}", local_addr, send_back_addr);
                                        },
                                    }

                                    // broadcast connection_count
                                    let feedsub = &mut swarm.behaviour_mut().feedsub;

                                    let mut conns = CONNS.lock().await;
                                    let count = feedsub.connection_count();
                                    *conns = feedsub.connections();

                                    debug!("SwarmEvent::ConnectionEstablished | count {:?}", count);
                                    send_msg_to_ui(UiEvent::NewConnectionCount, Some(count.to_string()), &main_window).await;

                                },

                                // Connection Closed
                                SwarmEvent::ConnectionClosed {connection_id, peer_id, endpoint, cause, num_established} => {
                                    info!("SwarmEvent::ConnectionClosed | {:?} {:?} {:?} {:?} {:?}", connection_id, peer_id, endpoint, cause, num_established);
                                    let msg = format!("{:?}, {:?}", endpoint, cause);
                                    SysEvent::publish(&main_window, 6, SysEventType::Swarm, "Connection closed", msg.to_string()).await;

                                    match endpoint {
                                        libp2p::core::ConnectedPoint::Dialer {address, ..} => {
                                            trace!("SwarmEvent::ConnectionClosed | Dialer {}", address);
                                        },

                                        libp2p::core::ConnectedPoint::Listener {local_addr, send_back_addr} => {
                                            trace!("Listener {} {}", local_addr, send_back_addr);
                                        },
                                    }

                                    // broadcast connection_count
                                    let feedsub = &mut swarm.behaviour_mut().feedsub;

                                    let mut conns = CONNS.lock().await;
                                    let count = feedsub.connection_count();
                                    *conns = feedsub.connections();
                                    send_msg_to_ui(UiEvent::NewConnectionCount, Some(count.to_string()), &main_window).await;
                                },

                                // Listeners
                                SwarmEvent::NewListenAddr { address, .. } => {
                                    info!("SwarmEvent::NewListenAddr {:?}", address);
                                    SysEvent::publish(&main_window, 9, SysEventType::Swarm, "New app listener", address.to_string()).await;

                                    // update SysConfig with the listener
                                    crate::sys_config::fetch::sysconfig().await
                                        .add_my_listener(&address)
                                        .update_app_status(AppStatus::Online)
                                        .set().await.unwrap();

                                        send_msg_to_ui(UiEvent::NewAppStatus, Some("Online".to_string()), &main_window).await;

                                },

                                // Expired Listener
                                SwarmEvent::ExpiredListenAddr { address, .. } => {
                                    trace!("SwarmEvent::ExpiredListenAddr {:?}", address);
                                    SysEvent::publish(&main_window, 13, SysEventType::Swarm, "Expired listener", address.to_string()).await;

                                    // remove the listener from the config struct
                                    crate::sys_config::fetch::sysconfig().await
                                        .remove_my_listener(&address)
                                        .set().await.unwrap();
                                },

                                SwarmEvent::ListenerClosed { addresses, reason, .. } => {
                                    warn!("SwarmEvent::ListenerClosed {:?} {:?}", addresses, reason);
                                    send_msg_to_ui(UiEvent::NewAppStatus, Some("Offline".to_string()), &main_window).await;

                                },

                                SwarmEvent::ListenerError { listener_id, error } => {
                                    warn!("SwarmEvent::ListenerError {:?} {:?}", listener_id, error);
                                },

                                // Dialing
                                SwarmEvent::Dialing { peer_id, connection_id } => {
                                    trace!("SwarmEvent::Dialing Peer: {:?} Connection: {:?}", peer_id, connection_id);
                                    SysEvent::publish(&main_window, 10, SysEventType::Swarm, "Dialing...", peer_id.unwrap().to_string()).await;
                                },

                                SwarmEvent::OutgoingConnectionError{connection_id, peer_id, error} => {
                                    trace!("SwarmEvent::OutgoingConnectionError {:?} {:?} {}", connection_id, peer_id, error);

                                    match error {
                                        libp2p::swarm::DialError::LocalPeerId { endpoint } => {
                                            warn!("Unhandled error: LocalPeerId {:?}", endpoint);
                                        },
                                        libp2p::swarm::DialError::NoAddresses => {
                                            warn!("Unhandled error: NoAddresses");
                                        },
                                        libp2p::swarm::DialError::DialPeerConditionFalse(_) => {
                                            warn!("Unhandled error: DialPeerConditionFalse");
                                        },
                                        libp2p::swarm::DialError::Aborted => {
                                            warn!("Unhandled error: Aborted");
                                        },
                                        libp2p::swarm::DialError::WrongPeerId{..}  => {
                                            warn!("Unhandled error: WrongPeerId");
                                        },
                                        libp2p::swarm::DialError::Transport(_) => {
                                            // broadcast connection_count
                                            let feedsub = &mut swarm.behaviour_mut().feedsub;

                                            let mut conns = CONNS.lock().await;
                                            let count = feedsub.connection_count();
                                            *conns = feedsub.connections();
                                            send_msg_to_ui(UiEvent::NewConnectionCount, Some(count.to_string()), &main_window).await;


                                        },
                                        libp2p::swarm::DialError::Denied { cause } => {
                                            warn!("Unhandled error: DialError::Denied {:?}", cause);

                                        }
                                    };
                                },

                                // Feedsub Events
                                SwarmEvent::Behaviour(BehaviourEvent::Feedsub(feedsub_event)) => {
                                    trace!("SwarmEvent::Behaviour(BehaviourEvent::Feedsub");
                                    FeedsubEventHandler::handle_event(&main_window, &mut swarm, feedsub_event, &person_keys).await;
                                },
                            }
                        },


                        // A channel to receive events from the UI
                        command = ui_recv.recv() => {
                            trace!("main | tokio::select! UI Command: {:?}", command);

                            #[derive(Debug, Serialize, Deserialize)]
                            struct UIMsg{
                                command: String,
                                payload: String,
                            }

                            let ui_msg: Option<UIMsg> = if let Some(event) = command {
                                if let Some(encoded_data) = &event.payload() {
                                    info!("main | tokio::select! UI Command encoded_data: {:?}", encoded_data);

                                    // // The encoded_data is a stringified JSON, parse this to get the actual JSON string
                                    // let unescaped_data: String = serde_json::from_st::<UIMsg>(encoded_data).unwrap();

                                    // Now, deserialize the unescaped_data into UIMsg
                                    let ui_msg: UIMsg = serde_json::from_str(encoded_data).unwrap();
                                    trace!("UI Command: {:?}", ui_msg);

                                    Some(ui_msg)
                                } else {
                                    trace!("No data in the event");
                                    None
                                }
                            } else {
                                trace!("No event available");
                                None
                            };

                            match ui_msg {

                                Some(ui_msg) => {
                                    match ui_msg.command.as_str(){
                                        // Dial -- decodes the invite code, follows the person, and dials the listener
                                        "dial" => {
                                        trace!("UI Command: dial {:?}", ui_msg);

                                        //TODO: remove whitespace from payload 
                                        if let Ok(decode) = decode(ui_msg.payload){
                                            trace!("dial | decode {:?}", String::from_utf8_lossy(&decode));

                                            let invite_code = String::from_utf8_lossy(&decode).to_string();
    
                                            #[derive(Debug, Serialize, Deserialize)]
                                            struct X {
                                                a: String, // person_id
                                                b: String, // libp2p listener
                                            }
    
                                            trace!("dial | invite_code {:?}", invite_code);
                                            let json = serde_json::from_str::<X>(invite_code.as_str());
                                            match json {
                                                Err(e) => {
                                                    error!("dial | Error {:?}", e);
                                                },
                                                Ok(x) => {
                                                    warn!("dial | json {:?}", x);
    
                                                    let person_id = x.a;
                                                    // I automatically subscribe / follow the person I dial
                                                    let feedsub = &mut swarm.behaviour_mut().feedsub;
    
                                                    let person_feed = Feed::default(FeedId::Person(person_id.clone()));
    
                                                    feedsub.subscribe(vec![person_feed]);
                                                     
                                                    match x.b.parse(){
                                                        Ok(address) => {
                                                            feedsub.dial(address);
                                                        },
                                                        Err(e) => error!("dial | failed to parse {:?}", e),
                                                    };
                                                },
                                            }
                                        };
                                    }

                                        // Follow -- updates the Person as is_followed Some(true)
                                        "follow" => {
                                            trace!("UI Command: follow {:?}", ui_msg);
                                            let person_id = ui_msg.payload;

                                            // Subscribe
                                            let feedsub = &mut swarm.behaviour_mut().feedsub;

                                            let person = Feed::default(FeedId::Person(person_id.clone()));

                                            feedsub.subscribe(vec![person]);


                                            let set = Person::get_or_new(&person_id.clone()).await
                                                .update_is_followed(Some(true))
                                                .set().await;

                                            send_msg_to_ui(UiEvent::Refresh, None, &main_window).await;

                                            match set {
                                                Err(e) => error!("UI Command: follow {}", e),
                                                Ok(_) =>  trace!("UI Command: follow {:?}", person_id),
                                            }
                                        }

                                        // Unfollow -- updates the Person as is_followed Some(false)
                                        "unfollow" => {
                                            trace!("UI Command: unfollow {:?}", ui_msg);
                                            let person_id = ui_msg.payload;

                                            // Unsubscribe
                                            let feedsub = &mut swarm.behaviour_mut().feedsub;
                                            feedsub.unsubscribe(vec![FeedId::Person(person_id.clone()), FeedId::Blog(person_id.clone())]);

                                            let set = Person::get_or_new(&person_id.clone()).await
                                                .update_is_followed(Some(false))
                                                .set().await;

                                            send_msg_to_ui(UiEvent::Refresh, None, &main_window).await;

                                            match set {
                                                Err(e) => error!("UI Command: unfollow {}", e),
                                                Ok(_) =>  trace!("UI Command: unfollow {:?}", person_id),
                                            }

                                        }

                                        // Publish message
                                        "publish_msg" => {
                                            trace!("UI Command: publish_msg");

                                            // let mut test_content = String::new();

                                            #[derive(Debug, Serialize, Deserialize, Clone)]
                                            struct Payload {
                                                person_id: String,
                                                msg: String,
                                                file_cids: Vec<String>,
                                            }

                                            let payload = serde_json::from_str::<Payload>(&ui_msg.payload);
                                            match payload {
                                                Err(e) => error!("publish_msg | {:?}", e),
                                                Ok(payload) => {
                                                    debug!("UI Command: publish_msg {:?}", payload);

                                                    let config = crate::sys_config::fetch::sysconfig().await;
                                                    let icon_cid = config.my_icon_cid;
                                                    let name = config.my_name;

                                                    let person_id = &payload.person_id;
                                                    let content = payload.msg;

                                                    let stub_cids = payload.file_cids;

                                                    match content.len() > 280 {
                                                        true => error!("publish_msg | Message cannot exceed 280 chars"),
                                                        false => {
                                                            // test_content = content.clone();
                                                            // Prepare the FeedIds for sending
                                                            let mut feed_ids: Vec<FeedId> = Vec::new();

                                                            // Collect the feeds to subscribe
                                                            let mut feeds = Vec::new();

                                                            for stub_cid in &stub_cids{
                                                                
                                                                if let Some(stub) = files::fetch::by_cid(stub_cid).await{
                                                                    if let (Some(cid), Some(total_size)) = (stub.dependent_cid, stub.file_size) {
                                                                        
                                                                        let slice_size = cmp::min(total_size, SETTINGS.asset_max_fragment_size_bytes);
                                                                        warn!("UI Command: publish_msg | slice_size {:?}", slice_size);

                                                                        let feed_id_file = FeedId::File{
                                                                            cid,
                                                                            public_key: encode(person_keys.public().encode_protobuf()),
                                                                            name: stub.file_name, 
                                                                            total_size: Some(total_size), 
                                                                            slice_size: Some(slice_size),
                                                                        };

                                                                        feed_ids.push(feed_id_file.clone());

                                                                        let last_seq = (total_size + slice_size - 1 ) / slice_size;
                                                                        let inventory = format!("1-{:?}", last_seq);

                                                                        let feed_type_file = FeedType::new_finite_seq(last_seq as u64, &inventory);
                                                                        let feed = Feed::new(feed_id_file, Some(feed_type_file));

                                                                        feeds.push(feed);
                                                        
                                                                    }
                                                                }
                                                        
                                                            }

                                                           
                                                            // Prepare the message
                                                            let msg = Msg::new()
                                                                .update_content(&content)
                                                                .update_person_id(person_id)
                                                                .update_name(&name)
                                                                .update_icon_cid(&icon_cid)
                                                                .generate_timestamp()
                                                                .update_file_ids(feed_ids.clone());
                                                                
                                                            
                                                            // Broadcast the update to Feedsub
                                                            let update = UpdateData::BlogMessage(msg).encode();
                                                            let send = &swarm
                                                                .behaviour_mut()
                                                                .feedsub
                                                                .broadcast_update(FeedId::Blog(person_id.to_string()), update, &person_keys);

                                                            match send {
                                                                Ok(x) => {
                                                                    send_msg_to_ui(UiEvent::Refresh, None, &main_window).await;
                                                                    trace!("Sent message {:?}", x);


                                                                    // Subscribe to the File feeds
                                                                    trace!("Sent message | Feeds {:?}", feeds);
                                                                    if !feeds.is_empty(){
                                                                        let _ = &swarm
                                                                        .behaviour_mut()
                                                                        .feedsub.subscribe(feeds);
                                                                    }

                                                                    // delete the stubs
                                                                    for cid in stub_cids {
                                                                        debug!("Sent message | cid {:?}", cid);
                                                                        let _ = files::delete::file_by_key(&cid).await;
                                                                    }
                                                                    // refresh our user profile
                                                                    sys_config::update::refresh_user_profile().await.unwrap();
                                                                },
                                                                Err(e) => {
                                                                    send_msg_to_ui(UiEvent::Refresh, None, &main_window).await;
                                                                    trace!("Publish Err: {:?}", e);
                                                                    // delete the local assets
                                                                    for cid in stub_cids {
                                                                        trace!("Sent message | cid {:?}", cid);
                                                                        let _ = files::delete::file_by_key(&cid).await;
                                                                    }
                                                                    // refresh our user profile
                                                                    sys_config::update::refresh_user_profile().await.unwrap();
                                                                },
                                                            };
                                                        },
                                                    };
                                                }
                                            }

                                            // Let's TEST it!

                                            // if test_content == "Start"{
                                            //     let config = crate::sys_config::fetch::sysconfig().await;
                                            //     let name = config.my_name.clone();
                                            //     let person_keys = person_keys.clone();
                                            //     let my_person_id = config.my_person_id;
                                            //     let main_window = main_window.clone();
                                            //     let swarm = sa

                                            //     tokio::task::spawn(async move {

                                            //         for count in 0..2000 {
                                            //             let msg = Msg::new()
                                            //                 .update_content(&format!("{} {}", my_person_id, count))
                                            //                 .update_person_id(&my_person_id)
                                            //                 .update_name(&name)
                                            //                 .generate_timestamp();
    
                                            //             info!("test_content {:?}", msg);
        
                                            //             let update = UpdateData::BlogMessage(msg);
    
                                            //             let send = &swarm
                                            //                 .behaviour_mut()
                                            //                 .feedsub
                                            //                 .broadcast_update(FeedId::Blog(my_person_id.to_string()), update.encode(), &person_keys);
        
                                            //             match send {
                                            //                 Ok(_) => send_msg_to_ui(UiEvent::Refresh, None, &main_window).await,
                                            //                 Err(_) => {},
                                            //             };
    
                                            //             use tokio::time::{self, Duration};
                                            //             time::sleep(Duration::from_secs(30)).await;
                                            //         }
                                            //     });
                                                
                                            // }
                             
                                        }

                                        "update_profile" => {
                                            debug!("UI Command: update_profile");

                                            #[derive(Debug, Serialize, Deserialize, Clone)]
                                            struct Payload {
                                                name: String,
                                                icon: String,
                                                icon_name: String,
                                                bio: String,
                                            }

                                            let payload = serde_json::from_str::<Payload>(&ui_msg.payload).unwrap();
                                            // trace!("UI Command | update_profile payload {:?}", payload);

                                            // Update SysConfig
                                            let config = crate::sys_config::fetch::sysconfig().await;

                                            let config = match payload.name.as_str() {
                                                "" => {config.update_my_name(&None)},
                                                _ => {config.update_my_name(&Some(payload.name))},
                                            };

                                            let config = match payload.bio.as_str() {
                                                "" => {config.update_my_bio(&None)},
                                                _ => {config.update_my_bio(&Some(payload.bio))},
                                            };

                                            let config = match payload.icon.as_str() {
                                                "delete" => {
                                                    // see if we have an icon to delete
                                                    match config.clone().my_icon_cid {
                                                        None => config,
                                                        Some(cid) => {
                                                            // if we do, we remove the icon cid
                                                            // assets::delete::mark_shoulddeleteit_by_cid(&cid).await.unwrap();
                                                            config.update_my_icon_cid(&None)
                                                        },
                                                    }
                                                },
                                                "no_change" => {
                                                    config
                                                },

                                                icon => {
                                                    // let cid = assets::create::file_from_base64(icon.to_string(), Some(payload.icon_name), ContentType::Image, Some(Origination::ManageProfile)).await.unwrap();
                                                    // trace!("update_my_icon_cid {}", cid.to_string());
                                                    // let old_cid = config.my_icon_cid.clone();

                                                    // if let Some(old_cid) = old_cid{
                                                    //     if old_cid != cid {
                                                    //         assets::delete::mark_shoulddeleteit_by_cid(&old_cid).await.unwrap();
                                                    //     }
                                                    // }
                                                    // config.update_my_icon_cid(&Some(cid))
                                                    config
                                                },

                                            };
                                            trace!("UI Command | update_profile | after icon {:?}", config);
                                            let save = config.set().await;

                                            match save {
                                                Ok(_) => {
                                                    send_msg_to_ui(UiEvent::SaveProfileSuccess, None, &main_window).await;
                                                },
                                                Err(e) => {
                                                    send_msg_to_ui(UiEvent::SaveProfileFailure, Some(e.to_string()), &main_window).await;
                                                },
                                            }
                                        }

                                        "add_files" => {
                                            trace!("UI Command: add_file");

                                            #[derive(Debug, Serialize, Deserialize, Clone)]
                                            struct Payload {
                                                paths: Vec<String>,
                                            }

                                            let payload = serde_json::from_str::<Payload>(&ui_msg.payload).unwrap();
                                            trace!("UI Command | add_file payload {:?}", payload);

                                            for path_str in payload.paths {
                                                //check if the incoming path already exists in FILES
                                                let path = PathBuf::from(path_str);
                                                if files::fetch::by_path(&path).await.is_some(){
                                                    debug!("UI Command: add_file | path already exists");
                                                    continue;
                                                } else {
                                                    // if it doesn't exist, then create it
                                                    let file = files::create::internal_stub(&path).await;
                                                    debug!("UI Command: add_file | file: {:?}", file);

                                                }
                                            }

                                            send_msg_to_ui(UiEvent::NewInternalStubStatus, Some(get_internal_stubs().await), &main_window).await;
                                        }

                                        "delete_file_by_cid" => {
                                            trace!("UI Command: delete_file_by_cid");

                                            #[derive(Debug, Serialize, Deserialize, Clone)]
                                            struct Payload {
                                                cid: String,
                                            }
                                            let payload = serde_json::from_str::<Payload>(&ui_msg.payload).unwrap();
                                            warn!("UI Command | delete_file_by_cid {:?}", payload);

                                            let result = files::delete::file_and_dependent_by_cid(&payload.cid).await;
                                            match result {
                                                Err(e) => trace!("delete_file_by_cid: {:?}", e),
                                                Ok(_) => trace!("UI Command: delete_file_by_cid | success {}", payload.cid),
                                            }
                                        }

                                        _ => error!("Unhandled UI command: {:?}", ui_msg),
                                    }
                                },

                                None => println!("No JSON command available"),
                                }
                        }


                        // A channel to receive events from Files
                        event = file_event_recv.recv() => {
                            let event: String = event.unwrap();

                            debug!("main | tokio::select! File Event : {:?}", event);

                            match event.as_str() {

                                "refresh" => {
                                    trace!("Event from Files | refresh");
                                    send_msg_to_ui(UiEvent::Refresh, None, &main_window).await;
                                },

                                "file_saved" => {
                                    trace!("Event from Files | file_saved");
                                    send_msg_to_ui(UiEvent::NewInternalStubStatus, Some(get_internal_stubs().await), &main_window).await;
                                },

                                "file_restored" => {
                                    trace!("Event from Files | file_restored");
                                    send_msg_to_ui(UiEvent::NewInternalStubStatus, Some(get_internal_stubs().await), &main_window).await;
                                },

                                "file_deleted" => {
                                    debug!("Event from Files | file_deleted");
                                    send_msg_to_ui(UiEvent::NewInternalStubStatus, Some(get_internal_stubs().await), &main_window).await;
                                    send_msg_to_ui(UiEvent::Refresh, None, &main_window).await;
                                },

                                "fragment_received" => {
                                    trace!("Event from Assets | fragment_received");

                                    let mut last_broadcast = LAST_FRAG_BROADCAST.lock().await;
                                    let expired_time = Utc::now().time() - last_broadcast.time();

                                    // debounce to avoid hammering the UI
                                    if expired_time.num_milliseconds() > 300 {
                                        *last_broadcast = Utc::now();
                                        send_msg_to_ui(UiEvent::NewDownloadFileStatus, Some(get_download_file_status().await), &main_window).await;
                                    }
                                },

                                "fragment_merged" => {
                                    trace!("Event from Assets | fragment_merged");

                                    // let mut last_broadcast = LAST_FRAG_BROADCAST.lock().await;
                                    // let expired_time = Utc::now().time() - last_broadcast.time();

                                    // // debounce to avoid hammering the UI
                                    // if expired_time.num_milliseconds() > 300 {
                                    //     *last_broadcast = Utc::now();
                                    //     send_msg_to_ui(UiEvent::NewUploadStatus, Some(get_asset_upload_status().await), &main_window).await;
                                    // }
                                },
                                
                                _ => error!("Unhandled file event: {:?}", event),
                            }
                        }


                    }
                    // End of loop

                    // Process files into assets, as needed
                    files::lifecycle::manager(file_event_send.clone(), &mut swarm).await;
                }
                
            });

            Ok(())
        })
        .register_uri_scheme_protocol("tickle", handle_register_uri_scheme_protocol)
        .invoke_handler(tauri::generate_handler![
            get_config,
            get_connection_count,
            get_app_status,
            get_people,
            get_all_connections,
            get_all_messages,
            get_invite_code,
            get_internal_url_for_file,
            get_internal_stubs,
            get_download_file_status,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

fn handle_register_uri_scheme_protocol(
    _app: &AppHandle,
    request: &Request,
) -> Result<Response, Box<dyn Error>> {
    trace!(
        "handle_register_uri_scheme_protocol | request {:?}",
        request
    );

    let path = request.uri().replace("tickle://", "");

    trace!("canonicalize path: {:?}", canonicalize(&path));

    let content = read(canonicalize(&path)?)?;

    // ResponseBuilder::new().header("Access-Control-Allow-Origin", "tickle://").body(content)
    ResponseBuilder::new().body(content)
}

#[derive(Debug, Serialize, Deserialize)]
pub enum UiEvent {
    Refresh,
    SaveProfileSuccess,
    SaveProfileFailure,
    NewConnectionSuccess,
    NewConnectionFailure,
    NewConnectionCount,
    NewInternalStubStatus,
    NewDownloadFileStatus,
    NewAppStatus,
}

async fn send_msg_to_ui(event: UiEvent, msg: Option<String>, main_window: &tauri::Window<Wry>) {
    trace!("send_msg_to_ui | {:?}", event);

    let msg = match msg {
        None => String::from(""),
        Some(msg) => msg,
    };

    #[derive(Debug, Serialize, Deserialize)]

    struct UIMsg {
        event: UiEvent,
        msg: String,
    }

    match event {
        UiEvent::Refresh => {
            let payload = UIMsg {
                event: UiEvent::Refresh,
                msg: Utc::now().to_string(),
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::SaveProfileSuccess => {
            let payload = UIMsg {
                event: UiEvent::SaveProfileSuccess,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::SaveProfileFailure => {
            let payload = UIMsg {
                event: UiEvent::SaveProfileFailure,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::NewConnectionSuccess => {
            let payload = UIMsg {
                event: UiEvent::NewConnectionSuccess,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::NewConnectionFailure => {
            let payload = UIMsg {
                event: UiEvent::NewConnectionFailure,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::NewConnectionCount => {
            let payload = UIMsg {
                event: UiEvent::NewConnectionCount,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::NewInternalStubStatus => {
            let payload = UIMsg {
                event: UiEvent::NewInternalStubStatus,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }

        UiEvent::NewDownloadFileStatus => {
            let payload = UIMsg {
                event: UiEvent::NewDownloadFileStatus,
                msg,
            };

            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }
        UiEvent::NewAppStatus => {
            let payload = UIMsg {
                event: UiEvent::NewAppStatus,
                msg,
            };
            let json = serde_json::to_string(&payload).unwrap();

            main_window
                .emit("send_msg_to_ui", json)
                .expect("failed to emit");
        }
    }
}

#[tauri::command]
async fn get_config() -> String {
    trace!("tauri::command | get_config");

    let config = crate::sys_config::fetch::sysconfig().await;

    let json = serde_json::to_string::<SysConfig>(&config).unwrap();

    trace!("tauri::command | get_config {:?}", config);

    json

}

#[tauri::command]
async fn get_connection_count() -> String {
    trace!("tauri::command | get_connection_count");

    let count = CONNS.lock().await.len();
            
    trace!(
        "tauri::command | get_connection_count | peer list {:?}",
        count
    );

    serde_json::to_string(&count).unwrap()
}

#[tauri::command]
async fn get_app_status() -> String {
    trace!("tauri::command | get_app_status");

    let config = crate::sys_config::fetch::sysconfig().await;
    trace!("tauri::command | get_app_status {:?}", config.app_status);

    serde_json::to_string(&config.app_status).unwrap()
}

#[tauri::command]
async fn get_people(is_followed: bool) -> String {
    trace!("tauri::command | get_people");

    #[derive(Debug, Serialize, Deserialize, Clone)]

    struct UIPersonProfile {
        person_id: String,
        name: String,
        is_followed: bool,
        icon_cid: Option<String>,
        bio: String,
    }

    let people = Person::get_all().await;
    debug!("tauri::command | get_people is_followed {:?} {:?}", is_followed, people);


    let mut person_list: Vec<UIPersonProfile> = Vec::new();

    for person in people.iter() {
        
        if !is_followed && (person.is_followed == Some(false) || person.is_followed.is_none()) {
            // person_list.push(person);
            let profile: Option<UserProfile> = match &person.user_profile_cid {
                None => None,
                Some(user_profile_cid) => {
                    crate::sys_config::fetch::user_profile_by_cid(user_profile_cid).await
                }
            };

            let row = match profile {
                None => {
                    UIPersonProfile {
                        person_id: person.person_id.to_string(),
                        name: person.person_id.to_string(),
                        is_followed: false,
                        icon_cid: None,
                        bio: String::from(""),
                    }
                }
                Some(user_profile) => {
                    UIPersonProfile {
                        person_id: person.person_id.to_string(),
                        name: user_profile.name,
                        is_followed: false,
                        icon_cid: user_profile.icon_cid,
                        bio: user_profile.bio,
                    }
                }
            };

            person_list.push(row);
        }

       
        if is_followed && person.is_followed == Some(true) {
            let profile: Option<UserProfile> = match &person.user_profile_cid {
                None => None,
                Some(user_profile_cid) => {
                    crate::sys_config::fetch::user_profile_by_cid(user_profile_cid).await
                }
            };

            let row = match profile {
                None => {
                    UIPersonProfile {
                        person_id: person.person_id.to_string(),
                        name: person.person_id.to_string(),
                        is_followed: true,
                        icon_cid: None,
                        bio: String::from(""),
                    }
                }
                Some(user_profile) => {
                    UIPersonProfile {
                        person_id: person.person_id.to_string(),
                        name: user_profile.name,
                        is_followed: true,
                        icon_cid: user_profile.icon_cid,
                        bio: user_profile.bio,
                    }
                }
            };

            person_list.push(row);
        }
    }

    debug!("tauri::command | get_people {:?}", person_list);

    serde_json::to_string(&person_list).unwrap()
}

#[tauri::command]
async fn get_all_connections() -> String {
    trace!("tauri::command | get_all_connections");

    #[derive(Debug, Serialize, Deserialize, Clone)]
    struct UIConnection {
        address: Multiaddr,
        peer_id: Option<PeerId>,
        direction: Option<Direction>,
        connection_status: ConnectionStatus,
        last_disconnect: Option<chrono::DateTime<chrono::Utc>>,
        retries: u16,
    }

    let mut conn_list = Vec::new();
    let conns = CONNS.lock().await;

    for conn in &*conns {
        conn_list.push(
            UIConnection{
                address: conn.address.clone(),
                peer_id: conn.peer_id,
                direction: conn.direction.clone(),
                connection_status: conn.status.clone(),
                last_disconnect: conn.last_disconnect,
                retries: conn.retries,
            }
        )
    }

    serde_json::to_string(&conn_list).unwrap()
}

#[tauri::command]
async fn get_all_messages() -> String {
    trace!("tauri::command | get_all_messages");

    let msg_list = Msg::get_all().await;
    trace!("tauri::command | get_all_messages {:?}", msg_list);

    serde_json::to_string(&msg_list).unwrap()
}

#[tauri::command]
async fn get_invite_code() -> String {
    trace!("tauri::command | get_invite_code");

    let mut config = crate::sys_config::fetch::sysconfig().await;

    debug!("get_invite_code | Config {:?}", config);

    let regex: Regex = Regex::new(r"^/ip4/127.0.*").unwrap();

    if config.my_listeners.len() > 1 {

        if let Some(index) = config
            .my_listeners
            .iter()
            .position(|listener| regex.is_match(&listener.to_string()))
            {
                config.my_listeners.remove(index);
                trace!("tauri::command | my_listeners {:?}", config.my_listeners);
            }
    };

    #[derive(Debug, Serialize, Deserialize)]

    struct X {
        // person_id
        a: String,
        // listener
        b: String,
    }

    if !config.my_listeners.is_empty() {
        let my_listener = config.my_listeners[0].clone();

        let invite_code = X {
            a: config.my_person_id,
            b: my_listener.to_string(),
        };

        trace!("tauri::command | invite_code {:?}", invite_code);

        let json = serde_json::to_string(&invite_code).unwrap();
        trace!("tauri::command | json {:?}", json);

        let encode = encode(json);

        trace!("tauri::command | encode {:?}", encode);

        encode
    } else {
        "".to_string()
    }
}

#[tauri::command]
async fn get_internal_url_for_file(cid_str: String) -> String {
    debug!("tauri::command | get_internal_url_for_file");

    let path = files::fetch::internal_url_for_key(&cid_str).await;

    match path {
        Some(path) => {
            debug!("tauri::command | get_internal_url_for_file path {:?}", path);

            let url = format!("tickle://{}", path);

            url
        }
        None => String::from(""),
    }
}

#[tauri::command]
async fn get_internal_stubs() -> String {
    trace!("tauri::command | get_internal_stubs");


    #[derive(Debug, Serialize, Deserialize, Clone)]
    struct UIInternalStub {
        timestamp: DateTime<Utc>,
        path: Option<String>,
        status: Option<FileStatus>,
        pct_processed: u32,
        cid: String,
        dependent_cid: Option<String>,
        file_name: Option<String>,
        file_ext: Option<String>,
    }

    let mut stub_list = Vec::new();

    let internal_stubs = files::fetch::all_internal_stubs().await;

    if !internal_stubs.is_empty(){
        debug!("tauri::command | get_internal_stubs {:?}", internal_stubs);
    }

    for file in internal_stubs {
        if file.file_type == Some(FileType::InternalStub) {
            let pct_processed = 100;
            
            let file_ext = helpers::extract_file_ext_from_name(
                &file.file_name.clone().unwrap_or_default(),
            );
            
            let path_str = file.path.as_ref().and_then(|pathbuf| {
                pathbuf.to_str().map(|s| s.to_string())
            });
            
            let dependent_cid = file.dependent_cid.map(|cid| cid.to_string());

            if let Some(cid) = file.cid {
                let stub = UIInternalStub {
                    timestamp: file.timestamp,
                    path: path_str,
                    status: file.status,
                    pct_processed: pct_processed as u32,
                    cid: cid.to_string(),
                    dependent_cid,
                    file_name: file.file_name,
                    file_ext,
                };
                stub_list.push(stub);

            }
        }
    }

    debug!("tauri::command | get_internal_stubs | local_list {:?}", stub_list);

    serde_json::to_string(&stub_list).unwrap()

}

#[tauri::command]
async fn get_download_file_status() -> String {
    trace!("tauri::command | get_download_file_status");

    #[derive(Debug, Serialize, Deserialize, Clone)]
    struct UIDownloadFile {
        timestamp: DateTime<Utc>,
        path: String,
        status: Option<FileStatus>,
        pct_processed: u32,
        cid: String,
        file_type: Option<FileType>,
        file_name: Option<String>,
        file_ext: Option<String>,
    }

    let mut download_list = Vec::new();

    let downloads = files::fetch::all_downloads().await;

    if !downloads.is_empty(){
        debug!("tauri::command | get_download_file_status {:?}", downloads);
    }

    for file in downloads {
        let pct_processed = file.pct_complete;

        let file_ext = files::helpers::extract_file_ext_from_name(
            &file.file_name.clone().unwrap_or_default(),
        );



        if let Some(path) = file.path {

            if let Some(path) = path.to_str() {

                if let Some(cid) = file.cid {
                    let local_file = UIDownloadFile {
                        file_type: file.file_type,
                        timestamp: file.timestamp,
                        path: path.to_string(),
                        status: file.status,
                        pct_processed: pct_processed.ceil() as u32,
                        cid: cid.to_string(),
                        file_name: file.file_name,
                        file_ext,
                    };
            
                    download_list.push(local_file);
                }
            }

 
        }
    }

    // download_list.sort_by(|a, b| {
    //     a.timestamp.cmp(&b.timestamp)

    //     // if a.timestamp < b.timestamp {
    //     //     Ordering::Less
    //     // } else if a.timestamp == b.timestamp {
    //     //     Ordering::Equal
    //     // } else {
    //     //     Ordering::Greater
    //     // }
    // });

    serde_json::to_string(&download_list).unwrap()
}

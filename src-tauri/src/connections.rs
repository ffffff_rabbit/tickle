use chrono::Utc;
use lazy_static::lazy_static;
use libp2p::{Multiaddr, PeerId};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tokio::sync::Mutex;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

pub use crate::behaviour::{BehaviourEvent, TickleBehaviour};
pub use crate::peers::Peer;
pub use crate::sys_events::{SysEvent, SysEventType};
use crate::SETTINGS;

// pub use crate::hosts::{Host};

type Result<T> = std::result::Result<T, Box<String>>;

// Connections are incoming or outgoing links to Peers
lazy_static! {
    pub static ref CONNECTIONS: Mutex<Connections> = Mutex::new(Default::default());
}

pub type Connections = HashMap<Multiaddr, Connection>;

#[derive(Debug, Serialize, Deserialize, Clone)]

pub enum ConnectionStatus {
    None,
    Connected,
    NotConnected,
    Pending,
    TemporarilyDead,
}

#[derive(Debug, Serialize, Deserialize, Clone)]

pub enum Direction {
    Internal,
    Incoming,
    Outgoing,
}

#[derive(Debug, Serialize, Deserialize, Clone)]

pub struct Connection {
    pub address: Multiaddr,
    pub peer_id: Option<PeerId>,
    pub direction: Option<Direction>,
    pub connection_status: ConnectionStatus,
    pub last_disconnect: Option<chrono::DateTime<chrono::Utc>>,
    pub retries: u16,
}

impl Default for Connection {
    fn default() -> Self {
        Self::new()
    }
}

impl Connection {
    pub fn new() -> Connection {
        Connection {
            address: Multiaddr::empty(),
            peer_id: None,
            direction: None,
            connection_status: ConnectionStatus::None,
            last_disconnect: None,
            retries: 0,
        }
    }

    pub async fn get(address: &Multiaddr) -> Option<Connection> {
        trace!("Connection::get");

        let connections = CONNECTIONS.lock().await;

        let connection = connections.get(address);

        connection.cloned()
    }

    pub fn update_address(mut self, address: &Multiaddr) -> Connection {
        trace!("Connection::update_address");

        self.address = address.to_owned();

        self
    }

    pub fn update_peer_id(mut self, peer_id: &PeerId) -> Connection {
        trace!("Connection::update_peer_id");

        self.peer_id = Some(*peer_id);

        self
    }

    pub fn update_connection_status(mut self, status: ConnectionStatus) -> Connection {
        trace!("Connection::update_connection_status");

        self.connection_status = status;

        self
    }

    pub fn update_direction(mut self, direction: Direction) -> Connection {
        trace!("Connection::update_direction");

        self.direction = Some(direction);

        self
    }

    pub fn update_last_disconnected(mut self) -> Connection {
        trace!("Connection::update_last_disconnected");

        self.last_disconnect = Some(Utc::now());

        self
    }

    pub fn increment_retries(mut self) -> Connection {
        trace!("Connection::increment_retries");

        self.retries += 1;

        self
    }

    /// Persists the new connection and purges expired connections

    pub async fn set(self) -> Result<String> {
        trace!("Connection::set");

        if self.address.is_empty() {
            error!("Address cannot be empty: {:?}", self);

            let error = Box::new("Address cannot be empty".to_string());

            Err(error)
        } else {
            let mut connections = CONNECTIONS.lock().await;

            connections.insert(self.address.clone(), self);

            drop(connections);

            // purge expired connections
            Connection::purge_expired_connections().await;

            let connections = Connection::get_all_connections().await;

            info!("Connection::set | {:?}", connections);

            Ok(String::from("Success"))
        }
    }

    pub async fn delete(self) -> Result<String> {
        trace!("Connection::delete {:?}", self.address);

        let mut connections = CONNECTIONS.lock().await;

        if connections.contains_key(&self.address) {
            connections.remove(&self.address);

            trace!("Connection::delete | deleted: {:?}", connections);

            Ok(String::from("Connection removed"))
        } else {
            trace!("Connection::delete not found: {:?}", connections);

            Ok(String::from("Connection not found"))
        }
    }

    pub fn is_external(&self) -> bool {
        trace!("Connection::is_external");

        match self.direction {
            None => false,
            Some(Direction::Internal) => false,
            Some(Direction::Outgoing) => true,
            Some(Direction::Incoming) => true,
        }
    }

    /// removes any (incoming + not connected) connections

    pub async fn purge_expired_connections() {
        trace!("Connection::purge_expired_connections");

        let connections = CONNECTIONS.lock().await;

        let clone = connections.clone();

        trace!("Connection::purge_expired_connections | {:?}", connections);

        drop(connections);

        for connection in clone.iter() {
            let key = connection.0;

            let connection = connection.1;

            if let (Some(Direction::Incoming), ConnectionStatus::NotConnected) =
                (&connection.direction, &connection.connection_status)
            {
                trace!("Deleting expired connection: {}", key);

                let mut connections = CONNECTIONS.lock().await;

                let peer_id = connection.peer_id.clone();

                connections.remove(key);

                trace!("Connection::set | Connections {:?}", connections);

                drop(connections);

                // Remove address from Peer
                if let Some(peer_id) = peer_id {
                    Peer::get_or_new(&peer_id)
                        .await
                        .delete_address(key)
                        .await
                        .set()
                        .await
                        .unwrap();
                }
            }

            // if let Some(direction) = &connection.direction {
            //     match direction {
            //         Direction::Incoming => {
            //             match connection.connection_status {
            //                 ConnectionStatus::NotConnected => {
            //                     trace!("Deleting expired connection: {}", key);

            //                     let mut connections = CONNECTIONS.lock().await;

            //                     let peer_id = connection.peer_id.clone();

            //                     connections.remove(key);

            //                     trace!("Connection::set | Connections {:?}", connections);

            //                     drop(connections);

            //                     // Remove address from Peer
            //                     if let Some(peer_id) = peer_id {
            //                         Peer::get_or_new(&peer_id)
            //                             .await
            //                             .delete_address(key)
            //                             .await
            //                             .set()
            //                             .await
            //                             .unwrap();
            //                     }
            //                 }
            //                 _ => {}
            //             }
            //         }
            //         _ => {}
            //     }
            // }
        }
    }

    pub fn is_active(&self) -> bool {
        trace!("Connection::is_active");

        let status = Connection::get_connection_status(self);

        match status {
            ConnectionStatus::None => false,
            ConnectionStatus::NotConnected => false,
            ConnectionStatus::TemporarilyDead => false,

            ConnectionStatus::Connected => true,
            ConnectionStatus::Pending => true,
        }
    }

    // Returns TemporarilyDead if disconnected within 1 minute, otherwise returns self.connection_status;
    pub fn get_connection_status(&self) -> ConnectionStatus {
        trace!("Connection::get_connection_status");

        let status = &self.connection_status;

        match status {
            ConnectionStatus::None => ConnectionStatus::None,
            ConnectionStatus::Connected => ConnectionStatus::Connected,
            ConnectionStatus::Pending => ConnectionStatus::Pending,
            ConnectionStatus::TemporarilyDead => ConnectionStatus::TemporarilyDead,

            ConnectionStatus::NotConnected => {
                if let Some(last_disconnect) = self.last_disconnect {
                    let expired_time = Utc::now().time() - last_disconnect.time();

                    // Here's the magic
                    if expired_time.num_minutes() < SETTINGS.disconnected_peer_retry_minutes {
                        let address = &self.address;

                        let time = expired_time.num_minutes();

                        trace!(
                            "Connection::get_connection_status | TempDead | {:?}, {}",
                            address,
                            time
                        );

                        return ConnectionStatus::TemporarilyDead;
                    }
                }

                ConnectionStatus::NotConnected
            }
        }
    }

    pub async fn get_all_connections() -> Vec<Connection> {
        trace!("Connection::get_all_connections");

        let connections = CONNECTIONS.lock().await;

        let mut connection_list = Vec::new();

        for connection in connections.iter() {
            connection_list.push(connection.1.clone());
        }

        connection_list
    }
}

use chrono::{TimeZone, Utc};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::sync::Mutex;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::feeds::FeedId;
pub use crate::people::Person;
use crate::proto::ProtoBlogMessage;

type Result<T> = std::result::Result<T, Box<String>>;

lazy_static! {
    static ref MESSAGES: Mutex<BTreeMap<String, Msg>> = Mutex::new(BTreeMap::new());
}

lazy_static! {
    static ref MY_SEQ: Mutex<u64> = Mutex::new(0);
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Msg {
    pub content: String,
    pub person_id: String,
    pub name: String,
    pub icon_cid: Option<String>,
    pub sequence_number: u64,
    pub file_ids: Option<Vec<FeedId>>,
    pub timestamp: Option<chrono::DateTime<chrono::Utc>>,
}

impl From<ProtoBlogMessage> for Msg {
    fn from(proto_msg: ProtoBlogMessage) -> Self {
        Msg {
            content: proto_msg.content,
            person_id: proto_msg.person_id,
            name: proto_msg.name,
            icon_cid: proto_msg
                .icon_cid
                .is_empty()
                .then(|| None)
                .unwrap_or(Some(proto_msg.icon_cid)),
            sequence_number: proto_msg.sequence_number,
            file_ids: Some(proto_msg.file_ids.into_iter().map(FeedId::from).collect()),
            timestamp: Utc.timestamp_opt(proto_msg.timestamp, 0).single(),
        }
    }
}

impl Default for Msg {
    fn default() -> Self {
        Self::new()
    }
}

impl Msg {
    pub fn new() -> Msg {
        trace!("Msg::new");

        Msg {
            content: String::from(""),
            person_id: String::from(""),
            name: String::from(""),
            icon_cid: None,
            sequence_number: 0,
            file_ids: None,
            timestamp: None,
        }
    }

    pub async fn get_all() -> Vec<Msg> {
        let messages = MESSAGES.lock().unwrap();

        let mut list = Vec::new();

        for message in messages.iter() {
            list.push(message.1.clone());
        }

        list
    }

    pub fn update_content(mut self, content: &String) -> Msg {
        trace!("Msg::update_content");

        self.content = content.to_string();

        self
    }

    pub fn update_person_id(mut self, person_id: &String) -> Msg {
        trace!("Msg::update_person_id");

        self.person_id = person_id.to_string();

        self
    }

    pub fn update_name(mut self, name: &String) -> Msg {
        trace!("Msg::update_name");

        self.name = name.to_string();

        self
    }

    pub fn update_icon_cid(mut self, icon_cid: &Option<String>) -> Msg {
        trace!("Msg::update_icon");
        self.icon_cid = icon_cid.clone();
        self
    }

    pub fn update_sequence_number(mut self, number: &u64) -> Msg {
        trace!("Msg::update_sequence_number");

        self.sequence_number = *number;

        self
    }

    pub fn update_file_ids(mut self, ids: Vec<FeedId>) -> Msg {
        trace!("Msg::update_file_cids");

        self.file_ids = Some(ids);

        self
    }

    pub fn generate_timestamp(mut self) -> Msg {
        trace!("Msg::generate_timestamp");

        self.timestamp = Some(Utc::now());

        self
    }

    fn generate_key(&self) -> Result<String> {
        trace!("Msg::generate_key");

        match self.timestamp {
            Some(timestamp) => Ok(format!(
                "{:?}|{}|{}",
                timestamp, self.person_id, self.sequence_number
            )),
            _ => {
                error!("Msg::generate_key | Error: no timestamp");

                let error = Box::new("Timestamp cannot be missing".to_string());

                Err(error)
            }
        }
    }

    pub async fn set(self) -> Result<Msg> {
        trace!("Msg::set {:?}", self);

        if self.content.is_empty()
            || self.person_id.is_empty()
            || self.name.is_empty()
            || self.sequence_number == 0
            || self.timestamp.is_none()
        {
            error!("Msg cannot be empty {:?}", self);

            let error = Box::new("Msg cannot be empty".to_string());

            Err(error)
        } else {
            if self.content.len() > 280 {
                let error = Box::new("Msg::set | Content cannot exceed 280 chars".to_string());

                return Err(error);
            }

            let key = Msg::generate_key(&self);

            match key {
                Ok(key) => {
                    let mut messages = MESSAGES.lock().unwrap();

                    messages.insert(key, self.clone());

                    trace!("Msg::set | {:?}", messages);

                    Ok(self)
                }
                Err(e) => {
                    let error = Box::new(format!("{}", e));

                    Err(error)
                }
            }
        }
    }

    pub async fn get_all_messages_for_person(person_id: &String) -> Vec<Msg> {
        trace!("Msg::get_all_messages_for_person {:?}", person_id);

        let mut msg_list = Vec::new();

        let messages = Msg::get_all().await;

        for message in messages {
            if &message.person_id == person_id {
                msg_list.push(message);
            }
        }

        msg_list
    }
}

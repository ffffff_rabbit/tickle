use async_trait::async_trait;
use base64::encode;
use feedsub::{FeedIdTrait, UpdateFetcher, UpdateItem};
use libp2p::identity::PublicKey;
use prost::Message;
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::path::Path;
use tokio::fs::File as TokioFile;

use crate::crypto::Crypto;
use crate::files::helpers;
use crate::{
    files::{self, FileFragment},
    proto::{
        proto_feed_id, proto_update_data, ProtoBlogMessage, ProtoFeedId, ProtoFile, ProtoUpdateData,
    },
    Msg,
};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

impl FeedIdTrait for FeedId {
    /// We're having Feedsub cache all FeedId variants except for File.
    fn is_cache_enabled(&self) -> bool {
        match self {
            FeedId::People(_) => true,
            FeedId::Blog(_) => true,
            FeedId::Person(_) => true,
            FeedId::File { .. } => false,
        }
    }

    /// Each feed has the author's base64 encoded public key as part of its identifier.
    fn public_key(&self) -> PublicKey {
        let key_data = match self {
            FeedId::People(public_key) => public_key,
            FeedId::Blog(public_key) => public_key,
            FeedId::Person(public_key) => public_key,
            FeedId::File { public_key, .. } => public_key,
        };

        let key_bytes = base64::decode(key_data).expect("Failed to decode base64 public key");
        PublicKey::try_decode_protobuf(&key_bytes).expect("Invalid public key")
    }
}

#[derive(Debug, Clone, Eq, Hash, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub enum FeedId {
    // Each person has a People feed with a String being the person's public key.
    // Updates to this feed are newly discovered people
    People(String),

    // Each person has a Blog feed with a String being the person's public key
    // Updates are blog posts (tweets)
    Blog(String),

    // Each person has a Person feed with a String being the person's public key
    // Updates are new feeds they publish and updates to their user profile
    Person(String),

    // File feeds are images, video and other files. Cid is the item's unique content ID, public key is the author's public key, name is the optional file name, total size in bytes, and slice size in bytes for the individual file parts.
    // Updates are the slices. Each file will have a specific number of slices to reconstitute the file.
    File {
        cid: String,
        public_key: String,
        name: Option<String>,
        total_size: Option<usize>,
        slice_size: Option<usize>,
    },
}

impl From<ProtoFeedId> for FeedId {
    fn from(proto_feed_id: ProtoFeedId) -> Self {
        match proto_feed_id.id {
            Some(proto_feed_id::Id::People(people)) => FeedId::People(people),
            Some(proto_feed_id::Id::Blog(blog)) => FeedId::Blog(blog),
            Some(proto_feed_id::Id::Person(person)) => FeedId::Person(person),
            Some(proto_feed_id::Id::File(proto_file)) => FeedId::File {
                cid: proto_file.cid,
                name: if proto_file.name.is_empty() {
                    None
                } else {
                    Some(proto_file.name)
                },
                public_key: proto_file.public_key,
                total_size: Some(proto_file.total_size as usize),
                slice_size: Some(proto_file.slice_size as usize),
            },
            None => panic!("Invalid ProtoFeedId: no field set"),
        }
    }
}

impl From<FeedId> for ProtoFeedId {
    fn from(feed_id: FeedId) -> Self {
        let id = match feed_id {
            FeedId::People(people) => Some(proto_feed_id::Id::People(people)),
            FeedId::Blog(blog) => Some(proto_feed_id::Id::Blog(blog)),
            FeedId::Person(person) => Some(proto_feed_id::Id::Person(person)),
            FeedId::File {
                cid,
                name,
                total_size,
                slice_size,
                public_key,
            } => {
                let proto_file = ProtoFile {
                    cid,
                    name: name.unwrap_or_default(),
                    public_key,
                    total_size: total_size.map_or(0, |size| size as u64),
                    slice_size: slice_size.map_or(0, |size| size as u64),
                };
                Some(proto_feed_id::Id::File(proto_file))
            }
        };
        ProtoFeedId { id }
    }
}

impl Display for FeedId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FeedId::People(_) => write!(f, "People"),

            FeedId::Blog(id) => write!(f, "BlogId({})", id),

            FeedId::Person(id) => write!(f, "PersonId({})", id),

            FeedId::File {
                cid,
                name,
                total_size,
                slice_size,
                public_key,
            } => write!(
                f,
                "FileId({} | {:?} | {:?} | {:?} | {:?})",
                cid, name, total_size, slice_size, public_key
            ),
        }
    }
}

#[derive(Debug, Clone)]
pub enum UpdateData {
    // Person with ID, published on a People feed
    Person(String),
    // PersonProfile(String),

    // BlogMessage with a Msg struct, published on a Blog feed
    BlogMessage(Msg),

    // NewFeed with FeedId, published on a Person feed
    NewFeed(FeedId),

    //FileFragment with bytes, published on a File feed.
    FileFragment(FileFragment),

    // default value
    None,
}

impl UpdateData {
    pub fn decode(data: &[u8]) -> Self {
        match ProtoUpdateData::decode(data) {
            Ok(proto_update_data) => match proto_update_data.data {
                Some(proto_update_data::Data::Person(person)) => UpdateData::Person(person),

                Some(proto_update_data::Data::BlogMsg(proto_msg)) => {
                    let blog_msg = Msg::from(proto_msg);
                    UpdateData::BlogMessage(blog_msg)
                }

                Some(proto_update_data::Data::NewFeed(proto_feed_id)) => {
                    let feed_id = FeedId::from(proto_feed_id);
                    UpdateData::NewFeed(feed_id)
                }
                Some(proto_update_data::Data::Frag(frag)) => {
                    UpdateData::FileFragment(files::FileFragment(frag))
                }
                None => UpdateData::None,
            },
            Err(e) => {
                error!("Failed to decode: {}", e);
                UpdateData::None
            }
        }
    }

    pub fn encode(&self) -> Vec<u8> {
        let mut proto_update_data = ProtoUpdateData { data: None };

        match self {
            UpdateData::Person(person) => {
                proto_update_data.data = Some(proto_update_data::Data::Person(person.clone()));
            }
            UpdateData::BlogMessage(msg) => {
                let proto_blog_message = ProtoBlogMessage {
                    content: msg.content.clone(),
                    person_id: msg.person_id.clone(),
                    name: msg.name.clone(),
                    icon_cid: msg.icon_cid.clone().unwrap_or_default(),
                    sequence_number: msg.sequence_number,
                    file_ids: msg
                        .file_ids
                        .as_ref()
                        .unwrap_or(&Vec::new())
                        .iter()
                        .map(|f| ProtoFeedId::from(f.clone()))
                        .collect(),
                    timestamp: msg.timestamp.map_or(0, |t| t.timestamp()),
                };
                proto_update_data.data = Some(proto_update_data::Data::BlogMsg(proto_blog_message));
            }
            UpdateData::NewFeed(feed_id) => {
                proto_update_data.data = Some(proto_update_data::Data::NewFeed(ProtoFeedId::from(
                    feed_id.clone(),
                )));
            }
            UpdateData::FileFragment(file_fragment) => {
                proto_update_data.data =
                    Some(proto_update_data::Data::Frag(file_fragment.clone().0));
            }
            UpdateData::None => {}
        }

        let mut buf = Vec::new();
        proto_update_data
            .encode(&mut buf)
            .expect("Failed to encode");
        buf
    }
}

#[derive(Debug)]
pub struct TickleUpdateFetcher;

#[async_trait]
impl UpdateFetcher<FeedId, TickleUpdateFetcher> for TickleUpdateFetcher
where
    FeedId: feedsub::FeedIdTrait,
{
    async fn fetch_by_sequence_list(&self, feed_id: &FeedId, seq: &u64) -> UpdateItem {
        match feed_id {
            FeedId::People(_) => {
                error!("UpdateFetcher:: People | cache miss");
                return UpdateItem {
                    data: Vec::new(),
                    sig: Vec::new(),
                };
            }

            FeedId::Blog(_) => {
                error!("UpdateFetcher:: Blog | cache miss");
                return UpdateItem {
                    data: Vec::new(),
                    sig: Vec::new(),
                };
            }

            FeedId::Person(_) => {
                error!("UpdateFetcher:: Person | cache miss");
                return UpdateItem {
                    data: Vec::new(),
                    sig: Vec::new(),
                };
            }

            FeedId::File {
                cid,
                name,
                total_size,
                slice_size,
                public_key,
            } => {
                debug!(
                    "UpdateFetcher:: File | {:?} | {:?} | {:?} | {:?} | {:?} | {:?}",
                    cid, seq, name, total_size, slice_size, public_key
                );

                let my_person_keys = Crypto::fetch().await.person_keys.unwrap();

                let my_public_key = encode(my_person_keys.public().encode_protobuf());
                let is_my_public_key = public_key == &my_public_key;

                if let (Some(name), Some(slice_size)) = (name, slice_size) {
                    let content_path = format!(
                        "./.tickle/content/{}",
                        crate::sys_config::fetch::sysconfig().await.my_name
                    );

                    let target_directory = Path::new(&content_path);
                    let file_ext = helpers::extract_file_ext_from_name(name).unwrap_or_default();
                    let target_name = format!("{}{}", cid, file_ext);
                    let target_file_path = target_directory.join(&target_name);

                    if let Ok(mut file) = TokioFile::open(&target_file_path).await {
                        match files::fetch::slice(&mut file, cid, *seq, slice_size).await {
                            Ok(slice) => {
                                debug!(
                                    "UpdateFetcher::File for seq {:?} found {:?}",
                                    seq,
                                    slice.len()
                                );

                                if slice.is_empty() {
                                    warn!(
                                        "UpdateFetcher::File | expected data but found nothing | Cid {:?} | seq {:?}",
                                            cid,
                                            seq,
                                        );
                                }

                                let data =
                                    UpdateData::FileFragment(files::FileFragment(slice.clone()))
                                        .encode();

                                let sig = match is_my_public_key {
                                    true => match my_person_keys.sign(&data) {
                                        Ok(sig) => sig,
                                        Err(e) => {
                                            error!("UpdateFetcher:: signature {:?}", e);
                                            Vec::new()
                                        }
                                    },
                                    false => {
                                        if let Some(file) = files::fetch::by_cid(cid).await {
                                            if let Some(sig) = file.get_frag_sig(seq) {
                                                sig
                                            } else {
                                                error!(
                                                    "UpdateFetcher:: sig not found for file {:?}",
                                                    cid
                                                );
                                                Vec::new()
                                            }
                                        } else {
                                            error!("UpdateFetcher:: file not found {:?}", cid);
                                            Vec::new()
                                        }
                                    }
                                };

                                if !data.is_empty() && !sig.is_empty() {
                                    debug!(
                                        "UpdateFetcher::File | seq {:?} | data len {:?}",
                                        seq,
                                        data.len()
                                    );
                                    debug!("UpdateFetcher::File | sig {:?}", sig);

                                    return UpdateItem { data, sig };
                                }
                            }

                            Err(e) => {
                                error!("UpdateFetcher:: File {:?}", e);
                            }
                        }
                    }
                }
                // Couldn't find it
                return UpdateItem {
                    data: Vec::new(),
                    sig: Vec::new(),
                };
            }
        };
    }
}

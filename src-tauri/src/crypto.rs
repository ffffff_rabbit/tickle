use lazy_static::lazy_static;
use libp2p::identity::Keypair;
use tokio::sync::Mutex;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

lazy_static! {
    static ref CRYPTO: Mutex<Crypto> = Mutex::new(Default::default());
}

#[derive(Debug, Default, Clone)]
pub struct Crypto {
    pub person_keys: Option<Keypair>,
    pub node_keys: Option<Keypair>,
}

impl Crypto {
    pub fn update_person_keys(mut self, keys: &Keypair) -> Crypto {
        trace!("Crypto::update_person_keys");
        self.person_keys = Some(keys.clone());
        self
    }

    pub fn update_node_keys(mut self, keys: &Keypair) -> Crypto {
        trace!("Crypto::update_node_keys");
        self.node_keys = Some(keys.clone());
        self
    }

    pub async fn fetch() -> Crypto {
        let crypto = CRYPTO.lock().await;
        crypto.clone()
    }

    pub async fn set(self) -> Crypto {
        let mut crypto = CRYPTO.lock().await;
        *crypto = self;
        crypto.clone()
    }
}

use std::{collections::HashMap, path::Path};

use chrono::{DateTime, Utc};
use lazy_static::lazy_static;
use libipld::Cid;
use feedsub::RangeSet;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use tokio::{sync::mpsc::Sender, fs};
use tokio::sync::Mutex;
use tokio::fs::File as TokioFile;
use tokio::io::AsyncReadExt;
use std::io;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

type Result<T> = std::result::Result<T, Box<String>>;

lazy_static! {
    static ref FILES: Mutex<HashMap<String, File>> = Mutex::new(Default::default());
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum FileType {
    /// represents a file, selected by the user to be processed for external distribution
    InternalStub,
    /// represents a complete file
    Full,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum FileStatus {
    ShouldProcess,
    Processing,
    FinishedProcessing,
    DontHave,
    Have,
    ShouldDelete,
    Deleting,
}

#[derive(Debug, Clone)]
pub struct File {
    pub file_type: Option<FileType>,
    pub status: Option<FileStatus>,
    pub cid: Option<String>,
    pub dependent_cid: Option<String>,
    pub path: Option<PathBuf>,
    pub file_name: Option<String>,
    pub file_size: Option<usize>,
    pub timestamp: DateTime<Utc>,
    pub slice_size: Option<usize>,
    pub public_key: Option<String>,
    pub last_seq: Option<u64>,
    pub slice_inventory: Option<RangeSet>,
    pub pct_complete: f64,
    pub frag_sigs: HashMap<u64, Vec<u8>>,
}

impl File {
    pub fn new() -> Self {
        Self {
            file_type: None,
            status: None,
            cid: None,
            path: None,
            file_name: None,
            file_size: None,
            timestamp: Utc::now(),
            dependent_cid: None,
            slice_size: None,
            public_key: None,
            last_seq: None,
            slice_inventory: None,
            pct_complete: 0.0,
            frag_sigs: HashMap::new()
        }
    }

    pub fn get_frag_sig(self, seq: &u64) -> Option<Vec<u8>> {
        if let Some(sig) = self.frag_sigs.get(seq).cloned(){
            Some(sig)
        } else {
            error!("File | get_frag_sig | sig not found for seq {:?}", seq);
            None
        }
    }

    pub fn update_file_type(mut self, file_type: FileType) -> Self {
        self.file_type = Some(file_type);
        self
    }

    pub fn update_status(mut self, new_status: FileStatus) -> Self {
        self.status = Some(new_status);
        self
    }    

    pub fn update_cid(mut self, cid: String) -> Self {
        self.cid = Some(cid);
        self
    }    

    pub fn update_path(mut self, path: &Path) -> Self {
        self.path = Some(path.to_path_buf());
        self
    }

    pub fn update_file_name(mut self, file_name: String) -> Self {
        self.file_name = Some(file_name);
        self
    }

    pub fn update_file_size(mut self, file_size: usize) -> Self {
        self.file_size = Some(file_size);
        self
    }

    pub fn update_slice_size(mut self, slice_size: usize) -> Self {
        self.slice_size = Some(slice_size);
        self
    }

    pub fn update_public_key(mut self, key: &String) -> Self {
        self.public_key = Some(key.to_string());
        self
    }

    pub fn update_timestamp(mut self) -> Self {
        self.timestamp = Utc::now();
        self
    }

    pub fn update_dependent_cid(mut self, cid: String) -> Self {
        self.dependent_cid = Some(cid);
        self
    }   

    pub fn update_slice_inventory(mut self, inventory: RangeSet) -> Self {
        self.slice_inventory = Some(inventory);
        self
    }

    pub fn update_last_seq(mut self, last_seq: u64) -> Self {
        self.last_seq = Some(last_seq);
        self
    }

    pub fn update_pct_complete(mut self, pct: f64) -> Self {
        self.pct_complete = pct;
        self
    }

    pub fn update_frag_sig(mut self, seq: &u64, sig: Vec<u8>) -> Self {
        debug!("File | update_frag_sig | seq {:?} sig {:?}", seq, sig);
        self.frag_sigs.insert(*seq, sig);
        trace!("File | update_frag_sig | {:?}", self);

        self
    }

    pub async fn set(self) -> Result<Self> {
        if let Some(cid) = &self.cid{
            let mut files = FILES.lock().await;

            files.insert(cid.to_string(), self.clone());

            debug!("File | set | Files {:?}", files);

            Ok(self.clone())
        } else {
            error!("File | set | missing cid");
            Err(Box::new(String::from("File | set | missing path")))
        }
    }

    async fn delete(self) {
        trace!("File::delete {:?}", self);
        if let Some(cid) = &self.cid{
            let mut files = FILES.lock().await;

            let file_type = &self.file_type;
            let path = &self.path;

            let result = files.remove_entry(cid);

            match result {
                Some(_) => {
                    if file_type == &Some(FileType::Full){
                        if let Some(path) = &path{
                            if path.exists(){
                                warn!("File | lifecycle | path exists | {:?}", path);
            
                                let _ = fs::remove_file(path).await;
                            }
                        }
                    }
                    debug!("File::delete | success")},
                None => error!("File::delete | asset not found key {:?} | {:?}", cid, self),
            }
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct FileFragment(pub Vec<u8>);



#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum Origination {
    Internal,
    MsgBox,
    ManageProfile,
}

pub mod lifecycle {
    use std::{path::Path, cmp};

    use libp2p::Swarm;
    use tokio::{fs, runtime::Handle};

    use crate::{files, TickleBehaviour, SETTINGS};

    use super::*;

    pub async fn manager(event_channel: Sender<String>, _swarm: &mut Swarm<TickleBehaviour>) {
        trace!("File | lifecycle | manager");
        let files = fetch::all_files().await;

        for file in files {
            trace!("File | lifecycle | manager {:?}", file);

            let channel = event_channel.clone();

            match &file.file_type{
                Some(FileType::InternalStub) => {
                    if let Some(status) = &file.status{
                        match status {
                            FileStatus::ShouldProcess => {
                                debug!("File | lifecycle | manager | ShouldProcessLocal {:?}", file);
    
                                match &file.path {
                                    None => error!(
                                        "File | lifecycle | manager | ShouldProcessLocal | path missing {:?}",
                                        file
                                    ),
                                    Some(_) => {
                                        let file = file
                                            .update_status(FileStatus::Processing)
                                            .set()
                                            .await
                                            .unwrap();
    
                                        lifecycle::process_internal_stub(file, channel).await;
                                    }
                                }
                            }
    
                            FileStatus::ShouldDelete => {
                                trace!("File | lifecycle | manager | ShouldMarkDeleted {:?}", file);
    
                                let file = file
                                    .update_status(FileStatus::Deleting)
                                    .update_timestamp()
                                        .set()
                                        .await
                                        .unwrap();
    
                                lifecycle::delete_it(file, channel).await;
                            }
                            
                            _ => {}    
                            
                        }
                    }
                }
    
                Some(FileType::Full) => {
                    if let Some(FileStatus::ShouldDelete) = file.status {
                        let _ = file.clone().update_status(FileStatus::Deleting).set().await;
                        file.delete().await;
                    }
                },
                None => {}
        }

    }

}

    async fn process_internal_stub(file: File, channel: Sender<String>) {
        let handle = Handle::current();

        if let Some(FileType::InternalStub) = &file.file_type{
    
            handle.spawn(async move {
                let path = file.path.clone().unwrap_or_default();
    
                let file_name = helpers::extract_file_name_from_path(&path)
                    .unwrap_or_default();
    
                match fs::read(&path).await {
                    Err(e) => error!("File | process_internal_stub | read | {:?}", e),
                    Ok(data) => {
                        let file_size = data.len();
                        debug!(
                            "File | process_internal_stub | read | success {:?}",
                            file_size
                        );
    
                        if let Some(cid) = file.cid{
                            match fetch::by_cid(&cid).await {
                                None => {
                                    // file was deleted
                                    trace!("File | process_internal_stub | sending asset_deleted");
                                    let msg = String::from("file_deleted");
                                    channel.send(msg.clone()).await.unwrap();
                                }
        
                                Some(internal_stub) => if file.status == Some(FileStatus::Processing) {
                                        trace!("File | process_internal_stub | ProcessingIt {:?}", internal_stub);
        
                                        let full_cid = helpers::generate_cid(data).unwrap().to_string();

                                        debug!(
                                            "File | process_internal_stub | ProcessingIt | file_cid {:?}",
                                            full_cid
                                        );
        
                                        // copy the file to /content
                                        let content_path = format!("./.tickle/content/{}", crate::sys_config::fetch::sysconfig().await.my_name);

                                        debug!(
                                            "File | process_internal_stub | ProcessingIt | content_path {:?}",
                                            content_path
                                        );
                                        let _dir = fs::create_dir_all(&content_path).await;
                                        debug!(
                                            "File | process_internal_stub | ProcessingIt | _dir {:?}",
                                            _dir
                                        );

                                        let source_file_path = path.clone();
        
                                        let file_ext = helpers::extract_file_ext_from_name(&file_name)
                                            .unwrap_or_default();
        
                                        let target_directory = Path::new(&content_path);
                                        let target_name = format!("{}{}", full_cid, file_ext);
                                        let target_file_path = target_directory.join(&target_name);
        
        
                                        let mut should_continue = false;
                                        if !target_file_path.exists() {
                                            match fs::copy(source_file_path, &target_file_path).await {
                                                Ok(_) => {
                                                    debug!(
                                                        "File | process_internal_stub | ProcessingIt | file copied {:?}",
                                                        full_cid
                                                    );
                                                    should_continue = true;
                                                },
                                                Err(_) => {
                                                    error!(
                                                        "File | process_internal_stub | ProcessingIt | file failed to copy {:?}",
                                                        full_cid
                                                    );
                                                },
                                            }
                                        } else {
                                            debug!(
                                                "File | process_internal_stub | ProcessingIt | file already copied {:?}",
                                                full_cid
                                            );
                                            should_continue = true;
        
                                        }
        
                                        if !should_continue {
                                            let msg = String::from("file_not_saved");
                                            channel.send(msg.clone()).await.unwrap();
                                        } else {
                                            // update the local file record to keep the UI informed
                                            let internal_stub = fetch::by_cid(&cid).await;
                                            match internal_stub {
                                                None => error!("File | process_internal_stub | ProcessingIt | File not found {:?}", internal_stub),
                                                Some(internal_stub) => {
        
                                                    let slice_size = cmp::min(file_size, SETTINGS.asset_max_fragment_size_bytes);

                                                    internal_stub.clone()
                                                        .update_file_type(FileType::InternalStub)
                                                        .update_file_size(file_size)
                                                        .update_slice_size(slice_size)
                                                        .update_status(FileStatus::FinishedProcessing)
                                                        .update_dependent_cid(full_cid.clone())
                                                        .update_file_name(file_name.clone())
                                                        .set().await.unwrap();
        
                                                    // create the full object
                                                    let _ = File::new() 
                                                        .update_file_type(FileType::Full)
                                                        .update_path(&target_file_path) 
                                                        .update_file_name(file_name) 
                                                        .update_file_size(file_size) 
                                                        .update_slice_size(slice_size)
                                                        .update_timestamp()
                                                        .update_status(FileStatus::Have)
                                                        .update_cid(full_cid)
                                                        .update_dependent_cid(cid) 
                                                        .update_pct_complete(100.0)
                                                        .set().await;
        
                                                    trace!("Asset | process_internal_stub | ProcessingIt | sending asset_saved");
                                                    let msg = String::from("file_saved");
                                                    channel.send(msg.clone()).await.unwrap();
        
                                                },
                                            }
        
        
                                        }
                                    }
                  
                                
                            }
                        
                        }
                    }
                }
            });
        }

  
    }

    pub async fn delete_it(file: File, channel: Sender<String>) {
        debug!("File | lifecycle | delete_it | {:?}", file);

        file.delete().await;

        debug!(
            "File | lifecycle | delete_it | All files {:?}",
            files::fetch::all_files().await
        );

        let msg = String::from("file_deleted");
        channel.send(msg.clone()).await.unwrap();
    }
}

pub mod fetch {

    use tokio::{io::AsyncSeekExt, time::{timeout, Duration}};

    use super::*;

    pub async fn all_files() -> Vec<File> {
        trace!("file | fetch::all_files");
        let files = FILES.lock().await;

        debug!("file | fetch::all_files {:?}", files);

        files.values().cloned().collect()
    }

    pub async fn all_internal_stubs() -> Vec<File> {
        trace!("file | fetch::all_internal_stubs");

        let files = FILES.lock().await;

        debug!("file | fetch::all_internal_stubs {:?}", files);

        let mut stub_list = Vec::new();

        for file in files.values() {
            if file.file_type == Some(FileType::InternalStub) {
                stub_list.push(file.clone());
            }
        }

        trace!("file | read::all_internal_stubs {:?}", stub_list);

        stub_list
    }

    pub async fn by_cid(cid: &String) -> Option<File> {
        trace!("file | fetch::by_cid");

        let files = FILES.lock().await;

        debug!("file | fetch::by_cid {:?}", files);

        // Find the file with the matching cid
        files.get(cid).cloned()
    }

    pub async fn by_path(path: &PathBuf) -> Option<File> {
        trace!("file | fetch::by_cid");

        let files = FILES.lock().await;

        debug!("file | fetch::by_cid {:?}", files);

        // Iterate through the HashMap and find the file with the matching cid
        files
            .values()
            .find(|file| file.path.as_ref() == Some(path))
            .cloned()
    }

    pub async fn internal_url_for_key(cid: &String) -> Option<String> {
        trace!("File | internal_url_for_key {:?}", cid);

        let mut url = None;
        let content_path = format!("./.tickle/content/{}", crate::sys_config::fetch::sysconfig().await.my_name);


        if let Some(file) = fetch::by_cid(cid).await {
            debug!("File | internal_url_for_key File {:?}", file);

            if let Some(file_type) = file.file_type{
                match file_type {
                    FileType::InternalStub  => {
                        if let Some(full_cid) = file.dependent_cid {
                            if let Some(file_name) = file.file_name {
                                if let Some(file_ext) = helpers::extract_file_ext_from_name(&file_name) {
                                    if file.status == Some(FileStatus::FinishedProcessing) {
                                        url = Some(format!("{}/{}{}", content_path, full_cid, file_ext));
                                    }
                                }
                            }
                        }
                    },
                    FileType::Full => {
                        if let Some(file_name) = file.file_name {
                            if let Some(file_ext) = helpers::extract_file_ext_from_name(&file_name) {
                                if file.status == Some(FileStatus::Have) {
                                    url = Some(format!("{}/{}{}", content_path, cid, file_ext));
                                }
                            }
                        }
                    },
                }
            }
        }

        debug!("File | internal_url_for_key {:?}", url);

        url
    }

    pub async fn all_downloads() -> Vec<File> {
        trace!("file | fetch::all_downloads");

        let files = FILES.lock().await;

        let mut download_list = Vec::new();

        for entry in files.iter() {
            let file = entry.1;

            if file.file_type == Some(FileType::Full) {

                download_list.push(file.clone());
            }
        }

        trace!("asset | fetch::all_downloads {:?}", download_list);

        download_list
    }

    pub async fn slice(file: &mut TokioFile, cid: &String, seq: u64, slice_size: &usize) -> io::Result<Vec<u8>>{
        debug!("file | fetch::slice | {:?} | {:?} | {:?}", file, seq, slice_size);

        let content_path = format!("./.tickle/content/{}", crate::sys_config::fetch::sysconfig().await.my_name);

            let metadata_result = timeout(Duration::from_secs(5), file.metadata()).await;
            trace!("file | fetch::slice | metadata_result {:?}", metadata_result);

            if let Ok(Ok(metadata)) = metadata_result {
                let file_size = metadata.len();
                let start = (seq - 1) * *slice_size as u64;
                trace!("file | fetch::slice | start {:?}", start);
    
            
                if start < file_size {
                    trace!("file | fetch::slice | seeking...");
    
                    // Move the cursor to the start position
                    file.seek(io::SeekFrom::Start(start)).await?;
                    trace!("file | fetch::slice | position found...");

                    let mut buffer = vec![0; *slice_size];
                    let bytes_read = file.read(&mut buffer).await?;
                    trace!("file | fetch::slice | bytes_read {:?}", bytes_read);


                    buffer.truncate(bytes_read);
                    trace!("file | fetch::slice | truncated...");

            
                    if !buffer.is_empty() {
                        return Ok(buffer);
                    }
                }
            }
        

        // If the main file doesn't contain the slice, check for a temp file
        let temp_file_path = PathBuf::from(format!("{}/{}|{}", content_path, cid, seq));
        if let Ok(mut temp_file) = TokioFile::open(&temp_file_path).await {
            trace!("file | fetch::slice | temp file open {:?}", temp_file_path);

            let mut buffer = Vec::new();
            temp_file.read_to_end(&mut buffer).await?;

            debug!("file | fetch::slice | temp file finished read");

            return Ok(buffer);
        }

        // Return an empty vector if neither file has the data
        Ok(Vec::new())

    }

}

pub mod create {
    use super::{helpers::generate_cid, *};

    pub async fn internal_stub(path: &PathBuf) -> Result<File> {
        if let Some(file_name) = helpers::extract_file_name_from_path(path) {
            if let Some(path_str) = path.to_str(){
                if let Ok(cid) = generate_cid(path_str) {
                   
                    File::new()
                        .update_cid(cid.to_string())
                        .update_status(FileStatus::ShouldProcess)
                        .update_file_type(FileType::InternalStub)
                        .update_file_name(file_name)
                        .update_path(path)
                        .set()
                        .await

                } else {
                    Err(Box::new(String::from("Could not generate Cid")))
                }
            } else {
                Err(Box::new(String::from("Path failed to extract")))

            }
        } else {
            Err(Box::new(String::from("File name failed to extract")))
        }
    }

    pub async fn full(cid: &String, file_name: &String, file_size: &usize, slice_size: &usize, public_key: &String) -> Result<File> {
        
        if let Some(file_ext) = helpers::extract_file_ext_from_name(file_name) {

            let content_path = format!("./.tickle/content/{}", crate::sys_config::fetch::sysconfig().await.my_name);
            let _dir = fs::create_dir_all(&content_path).await;

            let path = format!("{}/{}{}", content_path, cid, file_ext);

            File::new()
            .update_cid(cid.to_string())
            .update_status(FileStatus::DontHave)
            .update_file_type(FileType::Full)
            .update_file_name(file_name.to_string())
            .update_path(Path::new(&path))
            .update_file_size(*file_size)
            .update_slice_size(*slice_size)
            .update_public_key(public_key)
            .update_slice_inventory(RangeSet::new())
            .set()
            .await

        } else {
            Err(Box::new(String::from("File ext failed to extract")))
        }
    }



}

pub mod update {
    use tauri::Window;
    use tokio::{fs::OpenOptions, io::AsyncWriteExt};

    use crate::{files, send_msg_to_ui, UiEvent, get_download_file_status};
    use super::*;

    pub async fn handle_fragment(cid: &String, seq: u64, data: Vec<u8>, signature: Vec<u8>, main_window: &Window) {
        debug!("Files | update | handle_fragment {} | {} | {}", cid, seq, data.len());
    
        async fn append_data_to_file(path: &PathBuf, data: &[u8]) -> std::io::Result<()> {
            let mut file = OpenOptions::new().append(true).create(true).open(path).await?;
            file.write_all(data).await?;
            Ok(())
        }
    
        async fn find_and_append_next_slice(cid: &String, seq: u64, main_file_path: &PathBuf) -> std::io::Result<Vec<u64>> {
            debug!("Files | update | handle_fragment | find_and_append_next_slice {:?}", seq);

            let mut next_seq = seq + 1;
            let mut processed_seqs = Vec::new();
            let content_path = format!("./.tickle/content/{}", crate::sys_config::fetch::sysconfig().await.my_name);
            let _dir = fs::create_dir_all(&content_path).await;

            loop {
                
                let temp_file_path = PathBuf::from(format!("{}/{}|{}", content_path, cid, next_seq));
        
                if !temp_file_path.exists() {
                    break;
                }
        
                // Read data from the temporary file
                let mut temp_file = OpenOptions::new().read(true).open(&temp_file_path).await?;
                let mut data = Vec::new();
                temp_file.read_to_end(&mut data).await?;
        
                // Append data to the main file
                append_data_to_file(main_file_path, &data).await?;
        
                // Add the sequence number to the processed list and move to the next sequence
                processed_seqs.push(next_seq);
                next_seq += 1;
        
                fs::remove_file(&temp_file_path).await?;
            }
        
            Ok(processed_seqs)

        }
    
        async fn write_slice_to_temp_file(cid: &str, seq: u64, data: &[u8]) -> std::io::Result<()> {
            debug!("Files | update | handle_fragment | write_slice_to_temp_file {:?}", seq);

            let content_path = format!("./.tickle/content/{}", crate::sys_config::fetch::sysconfig().await.my_name);

            let temp_file_path = PathBuf::from(format!("{}/{}|{}", content_path, cid, seq));
            let mut temp_file = OpenOptions::new()
                .create(true)
                .write(true)
                .open(temp_file_path)
                .await?;
        
            temp_file.write_all(data).await?;
            Ok(())
        }


        if let Some(file) = files::fetch::by_cid(cid).await {

            let mut new_seqs = Vec::new();

            let full_file_path = file.path.clone();
            let inventory = file.slice_inventory.clone();
            let last_seq = file.last_seq;
    
            if let (Some(ref mut inventory), Some(ref full_file_path), Some(last_seq)) = (inventory, full_file_path, last_seq) {
                
                // Check if seq is the next in the set and append to file if so
                if inventory.is_next(seq) {
                    debug!("Files | update | handle_fragment | is next {:?}", seq);

                    if seq == 1 && full_file_path.exists(){
                        let _ =fs::remove_file(full_file_path).await;
                    }

                    if append_data_to_file(full_file_path, &data).await.is_err() {
                        error!("Failed to write data for CID: {}", cid);
                    }
                    new_seqs.push(seq);

                    match find_and_append_next_slice(cid, seq, full_file_path).await{
                        Ok(seqs) => new_seqs.extend(seqs),
                        Err(e) => error!("File | update | handle_fragment {:?}", e)
                    }
                    
                } else {
                    debug!("Files | update | handle_fragment | NOT next {:?}", seq);

                    if write_slice_to_temp_file(cid, seq, &data).await.is_err(){
                        error!("Failed to write data for CID: {}", cid);
                    }

                    match find_and_append_next_slice(cid, seq, full_file_path).await{
                        Ok(seqs) => new_seqs.extend(seqs),
                        Err(e) => error!("File | update | handle_fragment {:?}", e)
                    }
                }

                let mut is_last = false;
    
                // Update inventory for all new seqs
                for seq in new_seqs {
                    inventory.insert_num(seq);

                    if last_seq == seq {
                        is_last = true
                    }
                };

                if is_last {

                    let _ = file.update_slice_inventory(inventory.clone())
                    .update_status(FileStatus::Have)
                    .update_pct_complete(100.0)
                    .update_timestamp()
                    .update_frag_sig(&seq, signature)
                    .set().await;

                    send_msg_to_ui(UiEvent::NewDownloadFileStatus, Some(get_download_file_status().await), main_window).await;


                } else {

                    let pct = inventory.calc_pct_complete(last_seq);
                    let _ = file.update_slice_inventory(inventory.clone())
                        .update_pct_complete(pct)
                        .update_timestamp()
                        .update_frag_sig(&seq, signature)
                        .set().await;

                    send_msg_to_ui(UiEvent::NewDownloadFileStatus, Some(get_download_file_status().await), main_window).await;

                }
            }
        }

    }


}

pub mod delete {
    use crate::files;

    use super::*;

    pub async fn file_by_key(cid: &String) -> Result<()>{
        debug!("Files | delete | file_by_key {}", cid);

        if let Some(file) = &files::fetch::by_cid(cid).await {
            let _ = file.clone().update_status(FileStatus::ShouldDelete).set().await;
            Ok(())
        } else {
            Err(Box::new(String::from("File not found")))
        }
    }

    pub async fn file_and_dependent_by_cid(cid: &String) -> Result<()> {
        debug!("Files | delete | stub_and_full_by_key {}", cid);

        if let Some(file) = &files::fetch::by_cid(cid).await {
            if let Some(file_type) = &file.file_type{
                match file_type{
                    FileType::InternalStub => {
                        let _ = file.clone().update_status(FileStatus::ShouldDelete).set().await;

                        if let Some(dependent_cid) = &file.dependent_cid {
                            if let Some(dep_file) = files::fetch::by_cid(dependent_cid).await {
                                let _ = dep_file.update_status(FileStatus::ShouldDelete).set().await;
                            }
                        }
                    },
                    FileType::Full => {
                        let _ = file.clone().update_status(FileStatus::ShouldDelete).set().await;

                        if let Some(dependent_cid) = &file.dependent_cid {
                            if let Some(dep_file) = files::fetch::by_cid(dependent_cid).await {
                                let _ = dep_file.update_status(FileStatus::ShouldDelete).set().await;
                            }
                        }
                    },
                }
            }


        } else {
            return Err(Box::new(String::from("File not found")));
        }

        Ok(())
    }
}

pub mod helpers {
    use libipld::{
        cid::Version,
        multihash::{Code, MultihashDigest},
    };
    use regex::Regex;

    use super::*;

    /// parses a path to extract the file extension (eg. ".mov")
    pub fn extract_file_ext_from_name(name: &String) -> Option<String> {
        trace!("Files | extract_file_ext_from_path {}", name);

        let re = Regex::new(r"(\.)\w+$").unwrap();

        let file_ext: Option<String> = match re.captures(name) {
            Some(cap) => {
                let ext = cap.get(0).unwrap().as_str();

                Some(String::from(ext))
            }
            None => None,
        };

        file_ext
    }

    /// parses a path to extract the file name (eg. "blah.mov")
    pub fn extract_file_name_from_path(path: &PathBuf) -> Option<String> {
        trace!("Asset | extract_file_name_from_path {:?}", path);

        let re = Regex::new(r"[^/]+$");

        match re {
            Err(e) => {
                error!("Asset | extract_file_name_from_path {}", e);

                None
            }
            Ok(re) => {
                if let Some(path) = path.to_str() {
                    let file_name: Option<String> = match re.captures(path) {
                        Some(cap) => {
                            let name = cap.get(0).unwrap().as_str();
    
                            Some(String::from(name))
                        }
                        None => None,
                    };
    
                    trace!("Asset | extract_file_name_from_path {:?}", file_name);
    
                    file_name
                } else {
                    None
                }
            }
        }
    }

    pub fn generate_cid<T>(data: T) -> Result<Cid>
    where
        T: AsRef<[u8]>,
    {
        trace!("File | generate_cid");
        let bytes: &[u8] = data.as_ref();

        let hash = Code::Sha2_256.digest(bytes);

        let cid = Cid::new(Version::V1, 0x70, hash).unwrap_or_else(|e| {
            error!("File | generate_cid {:?}", e);
            Cid::default()
        });

        Ok(cid)
    }
}

#[cfg(test)]
mod tests {
    use super::helpers::*;

    #[test]
    fn test_generate_cid() {
        let path = "/Users/username/Documents/example1.txt".to_string();
        let cid = generate_cid(path).unwrap().to_string();

        assert_eq!(
            cid,
            "bafybeiak5hdq6q6dokuaxf3y2fakp37k4opdxd5qmexfed3r27ggpquaeu".to_string()
        );
    }
}

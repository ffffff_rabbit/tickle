fn main() {
    tauri_build::build();

    let proto_root = format!("{}/proto", std::env::var("CARGO_MANIFEST_DIR").unwrap());

    prost_build::Config::new()
        .compile_protos(
            &[format!("{}/update_data.proto", proto_root)],
            &[&proto_root],
        )
        .unwrap();
}

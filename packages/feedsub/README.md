# Feedsub

## PRE-ALPHA

Feedsub is a content-sharing protocol for [Libp2p](https://github.com/libp2p/rust-libp2p). You can use Feedsub to create decentralized content broadcasting apps that function like Twitter, YouTube, etc, where users share content directly with each other.  

In Feedsub, content is modeled as feeds -- each feed is a series of updates. Your app defines the feeds and the update types. Feeds can contain blog posts, images, video, files, or any other data.

Users produce and subscribe to feeds using your app. Feedsub manages the feed subscription and the synchronization of updates. For each user, Feedsub determines which feed updates are missing from their subscribed feeds, and retrieves the missing updates from other subscribers.

Feedsub enables pseudoanonymous content broadcasting. Each user is identified by a public key.

[Connect with us - join our SimpleX group](https://simplex.chat/contact#/?v=2-5&smp=smp%3A%2F%2FSkIkI6EPd2D63F4xFKfHk7I1UGZVNn6k1QWZ5rcyr6w%3D%40smp9.simplex.im%2FSWxE-5AB6wXyqvx94xB_aelg7Kw3SCjd%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAXEixok41ekdED6xD0iia9NiZO3F0BbJ1ygKGTh6ROGs%253D%26srv%3Djssqzccmrcws6bhmn77vgmhfjmhwlyr3u7puw4erkyoosywgl67slqqd.onion&data=%7B%22type%22%3A%22group%22%2C%22groupLinkId%22%3A%22Lke8fQgDE3km91u7qth7dA%3D%3D%22%7D)

### Feedsub Features

- Create and subscribe to feeds

- Broadcast feed updates

- Discover other feed subscribers and connect to synchronize updates

- Verify feed updates signatures against the feed author's public key

### Feedsub Limitations

- Feedsub doesn't support feeds with multiple authors. Each feed is currently authored by one public key.

- Feedsub doesn't support bi-directional messaging. A user can subscribe to a feed and receive updates, but cannot contact the author of that feed.

- If the app uses an open transport like TCP, a determined attacker can link a feed to the author's IP address.

### Development Status

Feedsub is pre-alpha and under active development. Known deficiencies include:

- no embedded database or persistence (everything is fresh on startup and kept in memory)

- feed updates are not encypted

- updates are not dynamically fetched from prior known peers (no embedded db yet)

- large feeds are not handled appropriately (currently all updates are downloaded)

## Get Started

**You'll need:**

- [Libp2p](https://github.com/libp2p/rust-libp2p)

- [Rust / Cargo](https://www.rust-lang.org/tools/install)

- Feedsub uses Tokio and a custom version of [asynchronous-codec](https://github.com/mxinden/asynchronous-codec) with a larger payload size. See Cargo.toml for the specific dependencies.

## Tickle App

Download and play around with the Tickle app to explore how Feedsub works. The Tickle app is a Tauri (React and Rust) app similar to Twitter. Tickle uses Feedsub to send tweets containing images, videos, and files.

[Tickle app source code](https://codeberg.org/ffffff_rabbit/tickle)

## Feedsub Data Model

### Feeds

Feeds are pretty simple. Your app defines feeds which include an id (must implement the FeedIdTrait). The FeedType and the cache_enabled flag are described below. See below also for an example of how to define feeds (using the Tickle app as a reference).

```rust
// from /feedsub/src/feeds.rs

pub struct Feed<FI: FeedIdTrait> {
    pub id: FI,
    pub feed_type: Option<FeedType>,
    pub cache_enabled: bool,
}

```

### The cache_enabled flag

This flag allows you as an app developer to manage and optimize persistance so that Feedsub doesn't duplicate data storage.

Nodes relay updates to each other. These updates can either be managed by Feedsub or by your app. The cache_enabled flag tells Feedsub whether to cache and provide a feed's updates directly or to fetch them from your app (using the UpdateFetcher function defined in the FeedIdTrait). For example, it might make more sense for video or file parts to be sourced directly from the file on disk rather than redundantly cached by Feedsub.

### FeedType

There are two feed types:

- Infinite feeds, for content feeds that can continue indefinitly.

- Finite feeds, for items like images, videos, and files that have a known end.

```rust

pub enum FeedType {
    InfiniteSeq {
        latest_seq: Count,
        inventory: RangeSet,
    },
    FiniteSeq {
        last_seq: Count,
        inventory: RangeSet,
    },
}

```

Count is a unique u64 sequence number. RangeSet is a custom type that tracks inventory of sequence numbers (eg. "1-10,15,18,20-55").

## Configure Feedsub In Your App

**Configure the Libp2p swarm with your customized Feedsub behaviour:**

Example: here's how the behaviour was defined for the Tickle app.

```rust

    // Libp2p is a core dependency
    use libp2p::{
        core::upgrade,
        futures::StreamExt,
        identity::Keypair,
        Multiaddr,
        noise::Config as NoiseConfig,
        PeerId,
        swarm::{Swarm, SwarmEvent},
        SwarmBuilder,
        tcp,
        Transport,
        yamux::Config as YamuxConfig,
    };

    // Configure the swarm within your main loop. 
    // See Libp2p for details.

    let node_keys = Keypair::generate_ed25519();
    
    // Reduce compile size by choosing a transport (and removing the other) instead
    // of making them switchable as was done for the R&D app

    let transport = match SETTINGS.transport {
        TransportType::VeilidUnsafe => {
            // VeilidTransport with direct connections (IP address not hidden) 
            let cloned_handle = handle.clone();

            VeilidTransport::new(Some(cloned_handle), node_keys.clone())
                .upgrade(upgrade::Version::V1)
                .authenticate(NoiseConfig::new(&node_keys).unwrap())
                .multiplex(YamuxConfig::default())
                .boxed()
        },
        TransportType::Tcp => {

            let config = tcp::Config::default();

            tcp::tokio::Transport::new(config)
                .upgrade(upgrade::Version::V1)
                .authenticate(NoiseConfig::new(&node_keys).unwrap())
                .multiplex(YamuxConfig::default())
                .boxed()
        },
    };

    // configure the swarm
    let mut swarm = SwarmBuilder::with_existing_identity(node_keys.clone())
        .with_tokio()

        // Transport
        .with_other_transport(|_key| {
            Ok(transport)
        }).unwrap()

        .with_behaviour(|_| {
            Ok(TickleBehaviour::new(node_keys))
        }).unwrap()
        .build();

    Swarm::listen_on(
        &mut swarm,
        "/ip4/0.0.0.0/tcp/0"
            .parse()
            .expect("can get a local socket"),
    )
    .expect("swarm can be started");

```

**Define the Libp2p behaviour in your app:**

Example: here's how the behaviour was defined for the Tickle app.

```rust
    // See /tickle/src-tauri/src/behaviour.rs
    // In your app, you'll define your FeedId enums and a function that implements Feedsub's UpdateFetcher trait.
    // FeedIds define your feeds. 
    // UpdateFetcher is used by Feedsub to retrieve updates that you decided Feedsub shouldn't cache (these 
    // updates probably fetched from files or your app's database). In this example, TickleUpdateFetcher is 
    // the function in the Tickle app that implements the UpdateFetcher trait.

    use crate::feeds::{FeedId, TickleUpdateFetcher};
    use crate::feedsub::{Feedsub, FeedsubConfig, FeedsubEvent};
    use libp2p::{identity, swarm::NetworkBehaviour};

    #[derive(NetworkBehaviour)]
    #[behaviour(to_swarm = "BehaviourEvent")]
    pub struct TickleBehaviour {
        pub feedsub: Feedsub<FeedId, UpdateFetcher>,
    }

    // /feedsub/src/config.rs has descriptions for these settings
    fn get_feedsub_config(peer_keys: identity::Keypair) -> FeedsubConfig<FeedId> {
        FeedsubConfig::new(peer_keys)
            .with_max_outbound_connections(20)
            .with_max_inbound_connections(20)
            .with_target_peers_per_feed(10)
            .with_heartbeat_interval(5)
            .with_max_feeds_to_handshake(100)
            .with_max_feeds_to_fetch_peer_cache(10)
            .with_max_peers_to_send_on_overloaded(100)
            .with_disconnect_timeout(10)
}

    impl TickleBehaviour {
        pub fn new(peer_keys: identity::Keypair) -> Self {
            Self {
                feedsub: Feedsub::new(get_feedsub_config(peer_keys), TickleUpdateFetcher),
            }
        }
    }

    // This is a straight pass-through of Feedsub events to your app.
    // Feedsub sends these to the Swarm. The Swarm delivers them to the app. (see below)
    // For the FeedsubEvent list, see /feedsub/src/behaviour.rs
    #[derive(Debug)]
    pub enum BehaviourEvent {
        Feedsub(FeedsubEvent<FeedId>),
    }

    impl From<FeedsubEvent<FeedId>> for BehaviourEvent {
        fn from(v: FeedsubEvent<FeedId>) -> Self {
            Self::Feedsub(v)
        }
    }
```

**Define your app's FeedId enums:**

Here's an example how FeedId was defined for the Tickle app:

```rust
// see /tickle/src-tauri/src/feeds.rs
// FeedIdTrait requires two functions for your FeedId: is_cache_enabled() and public_key(). 

impl FeedIdTrait for FeedId {
    /// In this example, we're having Feedsub cache all FeedId variants except for File.
    fn is_cache_enabled(&self) -> bool {
        match self {
            FeedId::People(_) => true,
            FeedId::Blog(_) => true,
            FeedId::Person(_) => true,
            FeedId::File { .. } => false,
        }
    }

    /// In this example, each feed has the author's base64 encoded public key as part of its identifier.
    fn public_key(&self) -> PublicKey {
        let key_data = match self {
            FeedId::People(public_key) => public_key,
            FeedId::Blog(public_key) => public_key,
            FeedId::Person(public_key) => public_key,
            FeedId::File { public_key, .. } => public_key,
        };

        let key_bytes = base64::decode(key_data).expect("Failed to decode base64 public key");
        PublicKey::try_decode_protobuf(&key_bytes).expect("Invalid public key")
    }
}

#[derive(Debug, Clone, Eq, Hash, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub enum FeedId {
    // Each person has a People feed with a String being the author's base64 encoded public key. 
    // Updates to this feed are newly discovered people
    People(String),

    // Each person has a Blog feed with a String being the person's base64 encoded public key
    // Updates are blog posts (tweets)
    Blog(String),

    // Each person has a Person feed with a String being the person's base64 encoded public key
    // Updates are new feeds they publish and updates to their user profile
    Person(String),

    // File feeds are images, video and other files. Cid is the item's unique content ID; public_key is the author's base64 encoded public key; name is the optional file name; total size in bytes; and slice size in bytes for the individual file parts. 

    // Updates are the slices. Each file will have a specific number of slices to reconstitute the file.
    File {
        cid: String,
        public_key: String,
        name: Option<String>,
        total_size: Option<usize>,
        slice_size: Option<usize>,
    },
}

```

**Define your app's UpdateData enums:**

Feeds are a series of updates and UpdateData enums are these update payloads that are published on feeds.

Example: Here's how UpdateData was defined for the Tickle app:

```rust
#[derive(Debug, Clone)]
pub enum UpdateData {
    
    // Person with ID, published on the People feed
    Person(String),

    // BlogMessage with a Msg struct, published on the Blog feed
    BlogMessage(Msg),

    // NewFeed with FeedId, published on the Person feed
    NewFeed(FeedId),

    //FileFragment with bytes, published on a File feed.
    FileFragment(FileFragment),

    // default value
    None,
}

```

**Define your app's UpdateFetcher function:**

Feeds are a series of updates and each update has a sequence number. When you define a FeedId, you decide whether Feedsub will cache and provide updates directly to other nodes. If you disable this cache for a FeedId, these updates will be fetched from your app using this UpdateFetcher function. In this function, you decide from where to retrieve the applicable update. It might be from disk, memory, or your app's database.

Example: Here's how UpdateFetcher was defined for the Tickle app:

```rust

#[derive(Debug)]
pub struct TickleUpdateFetcher;

#[async_trait]
impl UpdateFetcher<FeedId, TickleUpdateFetcher> for TickleUpdateFetcher
where
    FeedId: feedsub::FeedIdTrait,
{
       async fn fetch_by_sequence_list(&self, feed_id: &FeedId, seq: &u64) -> Vec<u8> {
        match feed_id {
            FeedId::People(_) => {
                error!("UpdateFetcher:: People | cache miss");
                return Vec::new();
\            }

            FeedId::Blog(_) => {
                error!("UpdateFetcher:: Blog | cache miss");
                return Vec::new();
            }

            FeedId::Person(_) => {
                error!("UpdateFetcher:: Person | cache miss");
                return Vec::new();
            }

            FeedId::File {
                cid,
                name,
                total_size,
                slice_size,
            } => {
                // fetch the slice(s) and signatures from disk 
            }
        };
    }
}

```

**Subscribe to feeds:**

Example: here's now the Tickle app subscribes to feeds. Notice that feeds have two FeedTypes (finite sequence -- for files, and infinite sequence for content feeds). This example shows infinite types.

```rust

// This happens in main(), at app startup. 
// see /tickle/src-tauri/src/main.rs

let feedsub = &mut swarm.behaviour_mut().feedsub;

// In the future, this will be the user's static keypair, probably fetched from disk. 
let person_pub_key_string = encode(person_keys.public().encode_protobuf());

// define the people feed for this user
let feed_id_people = FeedId::People(person_pub_key_string.clone());

let feed_type_people = FeedType::new_infinite_seq(0, "");
let people = Feed::new(feed_id_people.clone(), Some(feed_type_people));

//  define the person and blog feed for this user
let feed_id_person = FeedId::Person(person_pub_key_string.clone());
let feed_type_person = FeedType::new_infinite_seq(0, "");
let person = Feed::new(feed_id_person.clone(), Some(feed_type_person));

let feed_id_blog = FeedId::Blog(person_pub_key_string.clone());
let feed_type_blog = FeedType::new_infinite_seq(0, "");
let blog = Feed::new(feed_id_blog.clone(), Some(feed_type_blog));

// call subscribe() with the list of feeds            
feedsub.subscribe(vec![people, blog, person]);

```

**Broadcast feed updates from your app:**

Example: here's how the Tickle app broadcasts feed updates.

```rust
    // see /tickle/src-tauri/src/main.rs

    let feedsub = &mut swarm.behaviour_mut().feedsub;

    // Broadcast that my person feed has a new blog feed
    let new_feed_update = UpdateData::NewFeed(feed_id_blog);
    // broadcast_update() takes the author's keys so it can sign updates
    let _ = feedsub.broadcast_update(feed_id_person, new_feed_update.encode(), &person_keys);

    // Broadcast that the people feed has a new person
    let person_update = UpdateData::Person(person_pub_key_string.clone());
    // broadcast_update() takes the author's keys so it can sign updates
    let _ = feedsub.broadcast_update(feed_id_people, person_update.encode(), &person_keys);


```

**Receive Feedsub events in your app:**

Feedsub events are provided by Libp2p's Swarm. Here's how the Tickle app receives Feedsub events.

```rust

loop {
    tokio::select! {

        event = swarm.select_next_some() => {
            // Feedsub Events
            SwarmEvent::Behaviour(BehaviourEvent::Feedsub(feedsub_event)) => {
                // You can create a separate handler to process events
                FeedsubEventHandler::handle_event(&main_window, &mut swarm, feedsub_event, &person_keys).await;
            },
            // Not shown: all the other swarm events
        }
    }
}

```

**Here's the current list of events from Feedsub:**

```rust

/// Event emitted  by the `Feedsub` behaviour.
/// see feedsub/src/behaviour.rs
pub enum FeedsubEvent<FI>
where
    FI: FeedIdTrait,
{
    /// Feedsub is asking your app to call swarm.dial for an addr (as it can't do it directly)
    DialPeer { addr: Multiaddr },
    /// Feedsub is asking your app to call swarm.disconnect_peer_id for a peerId (as it can't do it directly)
    DisconnectPeer { peer_id: PeerId },
    /// A new peer just connected
    PeerConnected { peer_id: PeerId, is_trusted: bool },
    /// A peer just disconnected
    PeerDisconnected { peer_id: PeerId },
    /// This node's user subscribes to one or more feeds
    SelfSubscribe { feeds: Vec<Feed<FI>> },
    /// This node's user just unsubscribed from a feed
    SelfUnsubscribe { feed_ids: Vec<FI> },
    /// A peer subscribes to one or more feeds
    PeerSubscribe {
        peer_id: PeerId,
        feeds: Vec<Feed<FI>>,
    },
    /// A peer unsubscribes to one ore more feeds
    PeerUnsubscribe { peer_id: PeerId, feed_ids: Vec<FI> },
    /// This node's user sends updates on one or more feeds
    SelfUpdate { feed: Feed<FI>, update: Update<FI> },
    /// A peer sends updates on one or more feeds
    PeerUpdate {
        peer_id: PeerId,
        updates: Vec<Update<FI>>,
    },
    /// This node's user sends a task to retrieve updates
    SelfTask { task: Task<FI> },
    /// A peer sends a task to retrieve updates
    PeerTask { task: Task<FI> },
    /// Error while attempting to communicate with the remote.
    Error {
        peer_id: PeerId,
        /// The error that occurred.
        error: StreamUpgradeError<io::Error>,
    },
}

```

**Handle Feedsub events:**

Example: here's how the Tickle app handles Feedsub events.

```rust
// see /tickle/src-tauri/src/feedsub_event_handler.rs

pub struct FeedsubEventHandler {}

impl FeedsubEventHandler {
    pub async fn handle_event(
        main_window: &Window,
        swarm: &mut Swarm<TickleBehaviour>,
        feedsub_event: FeedsubEvent<FeedId>,
        person_keys: &Keypair
    ) {
        match feedsub_event {
            // you can invoke swarm methods, etc.
        }
    }
}

```

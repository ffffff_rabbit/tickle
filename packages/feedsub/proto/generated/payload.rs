#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Payload {
    #[prost(string, tag="1")]
    pub protocol_version: ::prost::alloc::string::String,
    #[prost(bytes="vec", tag="2")]
    pub signature: ::prost::alloc::vec::Vec<u8>,
    #[prost(message, optional, tag="3")]
    pub message: ::core::option::Option<ProtoMessage>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoMessage {
    #[prost(oneof="proto_message::MsgData", tags="1, 2, 3, 4, 5, 6, 7")]
    pub msg_data: ::core::option::Option<proto_message::MsgData>,
}
/// Nested message and enum types in `ProtoMessage`.
pub mod proto_message {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum MsgData {
        #[prost(message, tag="1")]
        Subscribe(super::ProtoSubscribe),
        #[prost(message, tag="2")]
        Unsubscribe(super::ProtoUnsubscribe),
        #[prost(message, tag="3")]
        Update(super::ProtoUpdate),
        #[prost(message, tag="4")]
        Task(super::ProtoTask),
        #[prost(message, tag="5")]
        PeerCache(super::ProtoPeerCache),
        #[prost(message, tag="6")]
        Handshake(super::ProtoHandshake),
        #[prost(message, tag="7")]
        Peers(super::ProtoDisconnect),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoSubscribe {
    #[prost(message, repeated, tag="1")]
    pub feeds: ::prost::alloc::vec::Vec<ProtoFeed>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoUnsubscribe {
    #[prost(string, repeated, tag="1")]
    pub feed_ids: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoUpdate {
    #[prost(message, repeated, tag="1")]
    pub subscriptions: ::prost::alloc::vec::Vec<ProtoFeed>,
    #[prost(message, repeated, tag="2")]
    pub updates: ::prost::alloc::vec::Vec<ProtoUpdateItem>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoUpdateItem {
    #[prost(string, tag="1")]
    pub feed_id: ::prost::alloc::string::String,
    #[prost(uint64, tag="2")]
    pub seq: u64,
    #[prost(bytes="vec", tag="3")]
    pub sig: ::prost::alloc::vec::Vec<u8>,
    #[prost(bytes="vec", tag="4")]
    pub data: ::prost::alloc::vec::Vec<u8>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoTask {
    #[prost(oneof="proto_task::TaskData", tags="1, 2")]
    pub task_data: ::core::option::Option<proto_task::TaskData>,
}
/// Nested message and enum types in `ProtoTask`.
pub mod proto_task {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum TaskData {
        #[prost(message, tag="1")]
        TaskFetchUpdatesBySeq(super::ProtoTaskFetchUpdatesbySeq),
        #[prost(message, tag="2")]
        TaskFetchPeers(super::ProtoTaskFetchPeers),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoTaskFetchUpdatesbySeq {
    #[prost(string, tag="1")]
    pub feed_id: ::prost::alloc::string::String,
    #[prost(string, tag="2")]
    pub requested_seqs: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoTaskFetchPeers {
    #[prost(bytes="vec", tag="1")]
    pub task_id: ::prost::alloc::vec::Vec<u8>,
    #[prost(string, repeated, tag="2")]
    pub feed_ids: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoFeed {
    #[prost(string, tag="1")]
    pub id: ::prost::alloc::string::String,
    #[prost(message, optional, tag="2")]
    pub feed_type: ::core::option::Option<ProtoFeedType>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoFeedType {
    #[prost(oneof="proto_feed_type::TypeData", tags="1, 2")]
    pub type_data: ::core::option::Option<proto_feed_type::TypeData>,
}
/// Nested message and enum types in `ProtoFeedType`.
pub mod proto_feed_type {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum TypeData {
        #[prost(message, tag="1")]
        TypeInfiniteSeq(super::ProtoFeedTypeInfiniteSeq),
        #[prost(message, tag="2")]
        TypeFiniteSeq(super::ProtoFeedTypeFiniteSeq),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoFeedTypeInfiniteSeq {
    #[prost(uint64, tag="1")]
    pub latest_seq: u64,
    #[prost(string, tag="2")]
    pub inventory: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoFeedTypeFiniteSeq {
    #[prost(uint64, tag="1")]
    pub last_seq: u64,
    #[prost(string, tag="2")]
    pub inventory: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoPeerCache {
    #[prost(bytes="vec", tag="1")]
    pub task_id: ::prost::alloc::vec::Vec<u8>,
    #[prost(message, repeated, tag="2")]
    pub peer_subscriptions: ::prost::alloc::vec::Vec<ProtoPeerSubscription>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoPeerSubscription {
    #[prost(string, tag="1")]
    pub address: ::prost::alloc::string::String,
    #[prost(string, tag="2")]
    pub feed_id: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoHandshake {
    #[prost(bytes="vec", tag="1")]
    pub public_key: ::prost::alloc::vec::Vec<u8>,
    #[prost(message, optional, tag="2")]
    pub peer: ::core::option::Option<ProtoPeer>,
    #[prost(message, repeated, tag="3")]
    pub feeds: ::prost::alloc::vec::Vec<ProtoFeed>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoDisconnect {
    #[prost(message, repeated, tag="1")]
    pub peers: ::prost::alloc::vec::Vec<ProtoPeer>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ProtoPeer {
    #[prost(bytes="vec", optional, tag="1")]
    pub peer_id: ::core::option::Option<::prost::alloc::vec::Vec<u8>>,
    #[prost(string, tag="2")]
    pub address: ::prost::alloc::string::String,
}

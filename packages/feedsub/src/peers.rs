use chrono::{DateTime, Duration, Utc};
use indexmap::IndexSet;
use libp2p::{Multiaddr, PeerId};
use lru::LruCache;
use rand::{
    distributions::{Distribution, Uniform},
    thread_rng,
};
use std::{
    collections::{HashMap, HashSet},
    fmt, io,
    num::NonZeroUsize,
    sync::{Arc, RwLock},
};
use uuid::Uuid;

use crate::{
    proto::{ProtoPeer, ProtoPeerCache, ProtoPeerSubscription},
    Feed, FeedIdTrait,
};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Clone)]
pub struct Peer<FI> {
    pub id: Option<PeerId>,
    pub address: Multiaddr,
    pub subscriptions: HashSet<FI>,
    pub count_updates: usize,
    pub last_retry: DateTime<Utc>,
    pub last_connected: DateTime<Utc>,
}

impl<FI> Peer<FI>
where
    FI: FeedIdTrait,
{
    pub fn new(peer_id: Option<PeerId>, address: Multiaddr) -> Self {
        Peer {
            id: peer_id,
            address,
            subscriptions: HashSet::new(),
            count_updates: 0,
            last_retry: DateTime::default(),
            last_connected: DateTime::default(),
        }
    }

    pub fn add_subscription(&mut self, feed_id: &FI) -> &mut Self {
        trace!("Peer::add_subscription {:?} {:?}", feed_id, self);
        self.subscriptions.insert(feed_id.clone());
        self
    }

    fn update_is_connected(&mut self) -> &mut Self {
        trace!("Peer::is_connected {:?}", self);
        self.last_connected = Utc::now();
        self
    }

    pub fn convert_to_proto(&self) -> ProtoPeer {
        let peer_id = self.id.as_ref().map(|id| id.to_bytes());
        let address = self.address.to_string();

        ProtoPeer { peer_id, address }
    }

    pub fn convert_from_proto(peer: ProtoPeer) -> Result<Self, io::Error> {
        let ProtoPeer { peer_id, address } = peer;

        let id = match peer_id {
            Some(bytes) => Some(
                PeerId::from_bytes(&bytes)
                    .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?,
            ),
            None => None,
        };

        let address = address
            .parse()
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "Invalid address format"))?;

        Ok(Peer::new(id, address))
    }
}

#[derive(Debug)]
pub struct PeerCacheConfig {
    target_peers_per_feed: usize,
    retry_threshold_sec: i64,
}

impl PeerCacheConfig {
    pub fn new() -> Self {
        Self {
            target_peers_per_feed: 10,
            retry_threshold_sec: 30,
        }
    }
    pub fn with_target_peers_per_feed(mut self, target: &usize) -> Self {
        trace!("PeerCacheConfig::with_target_peers_per_feed {:?}", target);
        self.target_peers_per_feed = *target;
        self
    }
    pub fn with_retry_threshold_sec(mut self, secs: &i64) -> Self {
        trace!("PeerCacheConfig::with_retry_threshold_sec {:?}", secs);
        self.retry_threshold_sec = *secs;
        self
    }
}

#[derive(Debug)]
pub struct PeerCache<FI: FeedIdTrait> {
    peers: HashMap<Multiaddr, Peer<FI>>,
    subscribed_feeds: HashMap<FI, HashSet<Multiaddr>>,
    pub unsubscribed_feeds: UnsubscribedFeeds<FI>,
    config: PeerCacheConfig,
}

impl<FI: FeedIdTrait> PeerCache<FI> {
    pub fn new(config: PeerCacheConfig) -> Self {
        Self {
            peers: HashMap::new(),
            subscribed_feeds: HashMap::new(),
            unsubscribed_feeds: UnsubscribedFeeds::new(),
            config,
        }
    }

    pub fn get_peer(&self, addr: &Multiaddr) -> Option<&Peer<FI>> {
        trace!("PeerCache::get_peer {:?}", addr);
        self.peers.get(addr)
    }

    pub fn get_peer_mut(&mut self, addr: &Multiaddr) -> Option<&mut Peer<FI>> {
        debug!("PeerCache::get_peer_mut {:?}", addr);
        self.peers.get_mut(addr)
    }

    pub fn get_some(&self, limit: &usize) -> Vec<Peer<FI>> {
        // If the limit is greater than or equal to the number of peers, return all peers.
        if limit >= &self.peers.len() {
            return self.peers.values().cloned().collect();
        }

        // Create a random number generator
        let mut rng = thread_rng();
        let peer_indices = Uniform::from(0..self.peers.len());

        // Randomly select indices and collect the corresponding peers
        std::iter::repeat_with(|| peer_indices.sample(&mut rng))
            .collect::<std::collections::HashSet<_>>() // Ensure indices are unique
            .into_iter()
            .take(*limit) // Take only up to the limit
            .map(|idx| self.peers.values().nth(idx).unwrap().clone()) // Get the peer at each index
            .collect()
    }

    pub fn insert(&mut self, peer: &Peer<FI>) {
        debug!("PeerCache::insert {:?}", peer);

        if !peer.address.is_empty() {
            let address = &peer.address;

            self.peers.insert(address.clone(), peer.clone());

            for feed_id in &peer.subscriptions {
                self.subscribed_feeds
                    .entry(feed_id.clone())
                    .or_insert(HashSet::new())
                    .insert(address.clone());
            }
        }
    }

    pub fn subscribe_feed(&mut self, peer_id: &Option<PeerId>, addr: &Multiaddr, feed_id: &FI) {
        debug!("PeerCache::subscribe_feed {:?} {:?}", addr, feed_id);

        if !addr.is_empty() {
            if let Some(peer) = self.peers.get_mut(addr) {
                peer.subscriptions.insert(feed_id.clone());

                self.subscribed_feeds
                    .entry(feed_id.clone())
                    .or_insert(HashSet::new())
                    .insert(addr.clone());
            } else {
                // we haven't connected to this peer yet

                let mut peer = Peer::new(peer_id.clone(), addr.clone());
                peer.add_subscription(feed_id);
                self.peers.insert(addr.clone(), peer);
            }
        } else {
            error!("PeerCache::subscribe_feed | empty addr {:?}", feed_id);
        }
    }

    pub fn unsubscribe_feed(&mut self, addr: &Multiaddr, feed_id: &FI) {
        debug!("PeerCache::unsubscribe_feed {:?} {:?}", addr, feed_id);

        if let Some(peer) = self.peers.get_mut(addr) {
            peer.subscriptions.remove(feed_id);

            if let Some(subscribers) = self.subscribed_feeds.get_mut(feed_id) {
                subscribers.remove(addr);
                if subscribers.is_empty() {
                    self.subscribed_feeds.remove(feed_id);
                }
            }
        }
    }

    fn calculate_score(&self, peer: &Peer<FI>) -> i64 {
        fn calculate_recency_penalty(last_connected: DateTime<Utc>) -> i64 {
            let now = Utc::now();
            let duration = now.signed_duration_since(last_connected);

            // Calculate decay based on the number of days
            duration.num_days() * duration.num_days()
        }

        debug!("PeerCache::calculate_peer_scores | {:?}", peer);

        let update_score = peer.count_updates as i64 * 1;
        let recency_penalty = calculate_recency_penalty(peer.last_connected);

        trace!(
            "PeerCache::calculate_peer_scores | {:?} update_score",
            update_score
        );
        debug!(
            "PeerCache::calculate_peer_scores | {:?} total score",
            update_score - recency_penalty
        );

        update_score - recency_penalty
    }

    pub fn get_subscribers_for_feed_ids(&self, feed_ids: &Vec<FI>) -> Vec<(FI, Multiaddr)> {
        debug!("PeerCache::get_address_for_feed_ids {:?}", feed_ids);

        let mut result = Vec::new();
        for feed_id in feed_ids {
            if let Some(subscribers) = self.subscribed_feeds.get(feed_id) {
                for peer_id in subscribers {
                    result.push((feed_id.clone(), peer_id.clone()));
                }
            }
        }
        debug!("PeerCache::get_address_for_feed_ids | result {:?}", result);

        result
    }

    pub fn is_feed_id_missing_subscribers(&self, feed_id: &FI) -> bool {
        if let Some(set) = self.subscribed_feeds.get(feed_id) {
            return set.len() < self.config.target_peers_per_feed;
        }
        false
    }

    /// return the address with the oldest retry time which subscribes
    /// to one of our incomplete feeds, or the oldest if none.
    pub fn get_address_to_dial(
        &mut self,
        incomplete_feeds: Vec<Arc<RwLock<Feed<FI>>>>,
        connected: &[Multiaddr],
    ) -> Option<Multiaddr> {
        trace!("PeerCache::get_address_to_dial");

        let current_time = Utc::now();
        let retry_threshold = current_time - Duration::seconds(self.config.retry_threshold_sec);

        // Collect incomplete feed IDs for comparison
        let incomplete_feed_ids: HashSet<FI> = incomplete_feeds
            .iter()
            .map(|feed| feed.read().unwrap().id.clone())
            .collect();

        // Step 1: Try to find a peer that subscribes to an incomplete feed
        let mut selected_peer = self
            .peers
            .iter_mut()
            .filter(|(_, peer)| {
                !connected.contains(&peer.address) // Exclude already connected peers
                && peer.last_retry < retry_threshold // Exclude peers dialed within the retry threshold
                && peer.subscriptions.iter().any(|feed_id| incomplete_feed_ids.contains(feed_id))
                // Peer must subscribe to an incomplete feed
            })
            .min_by_key(|(_, peer)| peer.last_retry);

        // Step 2: If no such peer is found, select the oldest peer regardless of subscription
        if selected_peer.is_none() {
            selected_peer = self
                .peers
                .iter_mut()
                .filter(|(_, peer)| {
                    !connected.contains(&peer.address) // Exclude already connected peers
                    && peer.last_retry < retry_threshold // Exclude peers dialed within the retry threshold
                })
                .min_by_key(|(_, peer)| peer.last_retry);
        }

        // If a peer is found, update its last_retry time and return its address
        selected_peer.map(|(_, peer)| {
            debug!(
                "PeerCache::get_address_to_dial | selected peer with delay {:?}",
                peer.last_retry
            );
            peer.last_retry = current_time;

            peer.address.clone()
        })
    }

    pub fn is_receive_update(&mut self, addr: &Multiaddr) {
        trace!("PeerCache::is_receive_update {:?}", addr);
        if let Some(peer) = self.peers.get_mut(addr) {
            peer.count_updates += 1;
            debug!("PeerCache::is_receive_update | after {:?}", peer);
        }
    }

    pub fn is_connected(&mut self, peer_id: &Option<PeerId>, addr: &Multiaddr) {
        debug!("PeerCache::is_connected {:?}", addr);

        if let Some(peer) = self.get_peer_mut(addr) {
            peer.update_is_connected();
        } else {
            let mut peer = Peer::new(peer_id.clone(), addr.clone());
            peer.update_is_connected();
            self.insert(&peer);
        }
    }

    pub fn remove_feed_ids(&mut self, feed_ids: &[FI]) {
        trace!("PeerCache::remove_feed_ids {:?}", feed_ids);

        for feed_id in feed_ids {
            self.subscribed_feeds.remove(feed_id);

            for peer in self.peers.values_mut() {
                peer.subscriptions.remove(feed_id);
            }
        }
    }

    pub fn cleanup_peers(&mut self) {
        trace!("PeerCache::cleanup_peers {:?}", self);

        let mut peers_to_remove = Vec::new();
        let target_peers_per_feed = self.config.target_peers_per_feed;

        for (feed_id, subscribers) in &self.subscribed_feeds {
            debug!("PeerCache::cleanup_peers | {:?} {:?}", feed_id, subscribers);
            debug!(
                "PeerCache::cleanup_peers | target_peers_per_feed {:?}",
                target_peers_per_feed
            );

            let excess_count = subscribers
                .len()
                .saturating_sub(self.config.target_peers_per_feed);

            debug!("PeerCache::cleanup_peers | excess count {:?}", excess_count);

            if excess_count > 0 {
                let mut peers_scores: Vec<(Multiaddr, i64)> = subscribers
                    .iter()
                    .filter_map(|addr| {
                        self.peers
                            .get(addr)
                            .map(|peer| (addr.clone(), self.calculate_score(peer)))
                    })
                    .collect();

                peers_scores.sort_by_key(|&(_, score)| score);

                peers_to_remove.extend(
                    peers_scores
                        .iter()
                        .take(excess_count)
                        .map(|(addr, _)| (feed_id.clone(), addr.clone())),
                );
            }
        }

        if !peers_to_remove.is_empty() {
            debug!(
                "PeerCache::cleanup_peers | peers_to_remove {:?}",
                peers_to_remove
            );

            for (feed_id, addr) in peers_to_remove {
                if let Some(subscribers) = self.subscribed_feeds.get_mut(&feed_id) {
                    subscribers.remove(&addr);
                }
                self.peers.remove(&addr);
            }
        }
    }

    pub fn convert_to_proto(subscriptions: Vec<(FI, Multiaddr)>, task_id: Uuid) -> ProtoPeerCache {
        trace!("PeerCache::convert_to_proto");

        let mut proto_peer_subscriptions = Vec::new();

        for (feed_id, addr) in subscriptions {
            let feed_peer = ProtoPeerSubscription {
                feed_id: serde_json::to_string(&feed_id).unwrap_or_default(),
                address: addr.to_string(),
            };

            proto_peer_subscriptions.push(feed_peer);
        }

        ProtoPeerCache {
            task_id: task_id.as_bytes().to_vec(),
            peer_subscriptions: proto_peer_subscriptions,
        }
    }
}

pub struct UnsubscribedFeeds<FI: FeedIdTrait>(LruCache<FI, IndexSet<Multiaddr>>);

impl<FI: FeedIdTrait> UnsubscribedFeeds<FI> {
    pub fn new() -> Self {
        Self(LruCache::new(NonZeroUsize::new(10).unwrap()))
    }

    pub fn insert(&mut self, feed_id: FI, address: Multiaddr) {
        let addresses_set = self.0.get_mut(&feed_id);

        match addresses_set {
            Some(addresses) => {
                // Insert the new address, IndexSet will automatically handle uniqueness
                addresses.insert(address);

                // Ensure the IndexSet does not exceed 10 items
                while addresses.len() > 10 {
                    addresses.pop(); // Remove the oldest address
                }
            }
            None => {
                // Create a new IndexSet with the address and insert it into the LRU cache
                let mut new_addresses = IndexSet::new();
                new_addresses.insert(address);
                self.0.put(feed_id, new_addresses);
            }
        }
    }

    pub fn get(&mut self, feed_id: &FI) -> Vec<Multiaddr> {
        if let Some(addresses) = self.0.get(feed_id) {
            addresses.iter().cloned().collect()
        } else {
            Vec::new()
        }
    }

    pub fn delete(&mut self, feed_id: &FI) {
        self.0.pop(feed_id);
    }
}

impl<FI: FeedIdTrait + fmt::Debug> fmt::Debug for UnsubscribedFeeds<FI> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "UnsubscribedFeeds {{ ")?;
        for (feed_id, addresses) in self.0.iter() {
            write!(f, "{:?}: {:?}, ", feed_id, addresses)?;
        }
        write!(f, "}}")
    }
}

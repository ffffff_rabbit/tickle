use std::{cmp::Ordering, collections::HashSet, ops::RangeInclusive};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq)]
pub struct RangeSet(String);

impl RangeSet {
    pub fn new() -> Self {
        RangeSet(String::from(""))
    }

    pub fn to_string(&self) -> String {
        self.0.to_string()
    }

    fn parse(&self) -> Vec<RangeInclusive<u64>> {
        if self.0 == "0" {
            error!("RangeSet::parse | found a 0");
        }

        let set = if self.0.is_empty() { "" } else { &self.0 };

        let mut ranges = Vec::new();

        for item in set.split(',') {
            let mut parts = item.split('-').map(|s| s.trim().parse::<u64>());

            match (parts.next(), parts.next()) {
                (Some(Ok(start)), Some(Ok(end))) => ranges.push(start..=end),
                (Some(Ok(start)), None) => ranges.push(start..=start),
                _ => {
                    continue;
                }
            }
        }
        ranges
    }

    pub fn insert_num(&mut self, new: u64) -> &mut Self {
        trace!("RangeSet::insert_num {:?}: {}", self.0, new);

        let mut range_set = self.parse();

        // Check if 'new' is already included in any range
        if range_set.iter().any(|r| r.contains(&new)) {
            trace!("RangeSet::insert_num | 1 {:?}", self.0);

            return self;
        }

        // Check if 'new' can extend any existing range
        for i in 0..range_set.len() {
            let range = &range_set[i];
            if *range.end() + 1 == new {
                // Extend the range
                range_set[i] = *range.start()..=new;
                trace!("RangeSet::insert_num | 2 {:?}", self.0);
                self.0 = stringify(dedup(range_set));
                return self;
            }
        }

        // Insert 'new' as a new range if it does not extend any existing range
        range_set.push(new..=new);

        // Sort and merge overlapping or consecutive ranges
        range_set.sort_by(|a, b| a.start().cmp(b.start()));

        // Update the internal representation
        self.0 = stringify(dedup(range_set));

        trace!("RangeSet::insert_num | final {:?}", self.0);

        self
    }

    pub fn insert_rangeset(&mut self, new: RangeSet) -> &mut Self {
        let mut old_set: Vec<RangeInclusive<u64>> = self.parse();
        let new_set: Vec<RangeInclusive<u64>> = new.parse();

        for new_range in new_set {
            let mut added = false;

            for old_range in &mut old_set {
                if new_range.end() < old_range.start() || new_range.start() > old_range.end() {
                    continue;
                }

                // Merge ranges
                let start = std::cmp::min(*old_range.start(), *new_range.start());
                let end = std::cmp::max(*old_range.end(), *new_range.end());
                *old_range = start..=end;
                added = true;
                break;
            }

            if !added {
                old_set.push(new_range);
            }
        }

        // Sort and merge overlapping or consecutive ranges
        old_set.sort_unstable_by(|a, b| a.start().cmp(b.start()));
        old_set = dedup(old_set);

        self.0 = stringify(old_set);
        self
    }

    pub fn remove(&mut self, seq: u64) -> &mut Self {
        trace!("RangeSet::remove {:?}: {}", self.0, seq);

        let mut new_range_set = Vec::new();

        let range_set: Vec<RangeInclusive<u64>> = self.parse();

        for range in &range_set {
            if *range.start() == seq && *range.end() == seq {
                // Skip this range, effectively removing it
            } else if *range.start() == seq {
                // Adjust the start of the range
                new_range_set.push((seq + 1)..=*range.end());
            } else if *range.end() == seq {
                // Adjust the end of the range
                new_range_set.push(*range.start()..=(seq - 1));
            } else if *range.start() < seq && seq < *range.end() {
                // Split the range
                new_range_set.push(*range.start()..=(seq - 1));
                new_range_set.push((seq + 1)..=*range.end());
            } else {
                // Keep the range as is
                new_range_set.push(range.clone());
            }
        }

        // Update the internal state with the new set of ranges
        self.0 = stringify(dedup(new_range_set));

        trace!(
            "RangeSet::remove | removed {:?} and sequence is now {:?}",
            seq,
            self.0
        );

        self
    }

    pub fn subtract(&mut self, other: &RangeSet) -> &mut Self {
        trace!("RangeSet::subtract {:?} from {:?}", other.0, self.0);

        let self_ranges = self.parse();
        let other_ranges = other.parse();
        let mut new_range_set = Vec::new();

        for self_range in &self_ranges {
            let mut start = *self_range.start();
            let end = *self_range.end();

            for other_range in &other_ranges {
                let other_start = *other_range.start();
                let other_end = *other_range.end();

                if start > end {
                    break;
                }

                if other_start > end || other_end < start {
                    // No overlap with this other_range
                    continue;
                }

                if other_start <= start && start <= other_end {
                    // Adjust the start to exclude the overlapping part
                    start = other_end + 1;
                }

                if start > end {
                    // The entire self_range is overlapped by other_range
                    break;
                }

                if other_start > start && other_end < end {
                    // other_range splits self_range into two parts
                    new_range_set.push(start..=(other_start - 1));
                    start = other_end + 1;
                }

                if other_end >= end && other_start <= end {
                    // Trim the end of the self_range
                    if other_start > start {
                        new_range_set.push(start..=(other_start - 1));
                    }
                    start = end + 1; // Move start beyond the end
                    break;
                }
            }

            if start <= end {
                new_range_set.push(start..=end);
            }
        }

        // Update the internal state with the new set of ranges
        self.0 = stringify(dedup(new_range_set));

        trace!(
            "RangeSet::subtract | subtracted {:?} and sequence is now {:?}",
            other.0,
            self.0
        );

        self
    }

    pub fn contains(&self, num: u64) -> bool {
        let range_set = self.parse();
        trace!("RangeSet::contains| {:?}: {}", range_set, num);
        range_set.iter().any(|range| range.contains(&num))
    }

    pub fn is_next(&self, num: u64) -> bool {
        trace!("RangeSet::is_next| {:?}: {}", self, num);
        let range_set = self.parse();

        if range_set.is_empty() {
            return num == 1;
        }

        for range in &range_set {
            if num > *range.start() && num <= range.end() + 1 {
                return true;
            }
        }

        false
    }

    pub fn is_complete(&self, last_seq: u64) -> bool {
        let set = &self.0;
        let test = match last_seq {
            0 => "".to_string(),
            1 => "1".to_string(),
            _ => format!("1-{}", last_seq),
        };

        debug!("RangeSet::is_complete | set {:?} test {:?}", set, test);

        // Simplify comparison logic
        *set == test || (last_seq == 1 && *set == "1-1")
    }

    pub fn calc_pct_complete(&self, num: u64) -> f64 {
        let ranges = self.parse();

        if num == 0 {
            return 0.0;
        }

        let mut numbers_present = HashSet::new();
        for range in ranges {
            for i in *range.start().max(&1)..=*range.end().min(&num) {
                numbers_present.insert(i);
            }
        }

        let total_count = num; // Total numbers from 1 to `num`
        let present_count = numbers_present.len() as u64;
        let missing_count = total_count - present_count;

        (1.0 - (missing_count as f64 / total_count as f64)) * 100.0
    }

    pub fn is_empty(&self) -> bool {
        self.0 == "" || self.0 == "0"
    }

    pub fn count(&self) -> u64 {
        let set = self.parse();
        self.count_range_elements(set)
    }

    fn count_range_elements(&self, ranges: Vec<RangeInclusive<u64>>) -> u64 {
        ranges
            .into_iter()
            .map(|range| range.end() - range.start() + 1)
            .sum()
    }

    /// Compares self with other. Returns Greater, Less, or Equal.
    pub fn compare(&self, other: &RangeSet) -> Result<Ordering, String> {
        let self_ranges = self.parse();
        let other_ranges = other.parse();

        let self_count = self.count_range_elements(self_ranges);
        let other_count = other.count_range_elements(other_ranges);

        Ok(self_count.cmp(&other_count))
    }

    pub fn list_missing_ranges(&self, last_seq: u64, limit: u64) -> Option<Vec<RangeSet>> {
        let mut missing_ranges = Vec::new();
        let mut current = 1;

        let ranges: Vec<RangeInclusive<u64>> = self.parse();

        for range in ranges {
            let start = *range.start();
            let end = *range.end();

            if start > current {
                missing_ranges.push(current..=std::cmp::min(start - 1, last_seq));
            }

            current = std::cmp::max(current, end + 1);
        }

        if current <= last_seq {
            missing_ranges.push(current..=last_seq);
        }

        // Slice missing_ranges into groups
        let mut grouped_ranges = Vec::new();
        let mut current_group = Vec::new();
        let mut current_sum = 0;

        for range in missing_ranges {
            let mut start = *range.start();
            while start <= *range.end() {
                let remaining = limit - current_sum;
                let end = std::cmp::min(start + remaining - 1, *range.end());
                let range_count = end - start + 1;

                current_group.push(start..=end);
                current_sum += range_count;

                if current_sum >= limit {
                    grouped_ranges.push(RangeSet(stringify(current_group)));
                    current_group = Vec::new();
                    current_sum = 0;
                }

                start = end + 1;
            }
        }

        if !current_group.is_empty() {
            grouped_ranges.push(RangeSet(stringify(current_group)));
        }

        if grouped_ranges.len() > 0 {
            Some(grouped_ranges)
        } else {
            None
        }
    }

    pub fn intersect_and_difference(&self, range2: &RangeSet) -> (RangeSet, RangeSet) {
        let ranges1 = self.parse();
        let ranges2 = range2.parse();

        trace!(
            "RangeSet::intersect_and_difference | self: {:?} | range2: {:?}",
            ranges1,
            ranges2
        );

        let mut intersections = Vec::new();
        let mut non_overlaps = Vec::new();

        for range_a in &ranges1 {
            let mut non_overlap_start = range_a.start().clone();

            for range_b in &ranges2 {
                let start = std::cmp::max(*range_a.start(), *range_b.start());
                let end = std::cmp::min(*range_a.end(), *range_b.end());

                if start <= end {
                    intersections.push(start..=end);

                    if non_overlap_start < start {
                        non_overlaps.push(non_overlap_start..=(start - 1));
                    }
                    non_overlap_start = std::cmp::max(non_overlap_start, end + 1);
                }
            }

            if non_overlap_start <= *range_a.end() {
                non_overlaps.push(non_overlap_start..=*range_a.end());
            }
        }

        // Convert the intersections and non-overlaps into the desired format (RangeSet)
        (
            RangeSet(stringify(intersections)),
            RangeSet(stringify(non_overlaps)),
        )
    }

    pub fn get_first(&self, limit: u64) -> RangeSet {
        let ranges = self.parse(); // Assuming this parses into Vec<RangeInclusive<u64>>

        let mut first_numbers = Vec::new();
        let mut count = 0;

        'outer: for range in ranges {
            let mut start = std::cmp::max(*range.start(), 1);
            let end = *range.end();

            // Skip empty ranges
            if start > end {
                continue;
            }

            while start <= end && count < limit {
                let remaining = limit - count;
                let segment_end = std::cmp::min(start + remaining - 1, end);
                first_numbers.push(start..=segment_end);
                count += segment_end - start + 1;

                // Move to the next segment
                start = segment_end + 1;

                // Break if the limit is reached
                if count >= limit {
                    break 'outer;
                }
            }
        }

        if first_numbers.is_empty() {
            first_numbers.push(0..=0);
        }

        // Convert first_numbers into RangeSet format
        RangeSet(stringify(first_numbers))
    }
}

impl From<String> for RangeSet {
    fn from(value: String) -> Self {
        RangeSet(value)
    }
}

impl From<&str> for RangeSet {
    fn from(value: &str) -> Self {
        if value == "0" {
            return RangeSet::new();
        }
        RangeSet(String::from(value))
    }
}

impl From<&u64> for RangeSet {
    fn from(value: &u64) -> Self {
        RangeSet(format!("{}", value))
    }
}

impl From<u64> for RangeSet {
    fn from(value: u64) -> Self {
        RangeSet(format!("{}", value))
    }
}

impl Into<Vec<u64>> for RangeSet {
    fn into(self) -> Vec<u64> {
        let ranges: Vec<RangeInclusive<u64>> = self.parse();

        let mut expanded = Vec::new();
        for range in ranges {
            for number in range {
                expanded.push(number);
            }
        }
        expanded
    }
}

impl From<Vec<u64>> for RangeSet {
    fn from(numbers: Vec<u64>) -> Self {
        let mut range_set = RangeSet::new();
        for number in numbers {
            range_set.insert_num(number);
        }
        range_set
    }
}

fn dedup(mut ranges: Vec<RangeInclusive<u64>>) -> Vec<RangeInclusive<u64>> {
    if ranges.is_empty() {
        trace!("dedup | 0 {:?}", ranges);
        return ranges.to_vec();
    }

    // Sort ranges by their start values
    ranges.sort_by(|a, b| a.start().cmp(b.start()));
    trace!("dedup | 1 {:?}", ranges);

    let mut deduped = Vec::new();
    let mut current_range = ranges[0].clone();

    for range in ranges.into_iter().skip(1) {
        trace!("dedup | 2 {:?} | {:?}", current_range, range);
        if *current_range.end() + 1 >= *range.start() {
            // Extend the current range if overlapping or consecutive
            current_range =
                *current_range.start()..=std::cmp::max(*range.end(), *current_range.end());
        } else {
            trace!("dedup | 3 {:?} | {:?}", current_range, range);
            // Push the current range to deduped and start a new range
            deduped.push(current_range);
            current_range = range.clone();
        }
    }

    // Push the last range
    deduped.push(current_range);
    trace!("dedup | final {:?}", deduped);

    deduped
}

fn stringify(range_set: Vec<RangeInclusive<u64>>) -> String {
    let mut output: Vec<String> = Vec::new();

    for range in range_set {
        if range.start() == range.end() {
            // Single value range
            output.push(format!("{:?}", range.start()));
        } else {
            // Range with start and end
            output.push(format!("{:?}-{:?}", range.start(), range.end()));
        }
    }

    output.join(",")
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Range {
    start: u64,
    end: u64,
}

#[cfg(test)]
mod tests {
    // RUSTFLAGS="-A warnings" RUST_LOG=trace cargo test
    use super::*;
    use std::sync::Once;

    #[allow(unused_imports)]
    use log::{debug, error, info, trace, warn};

    static INIT: Once = Once::new();

    pub fn initialize() {
        INIT.call_once(|| {
            let _ = env_logger::builder().is_test(true).try_init();
        });
    }

    #[test]
    fn test_seq_ranges() {
        initialize();

        let to_generate_missing = vec![
            (
                RangeSet("1-10".to_string()),
                12,
                "Some([RangeSet(\"11-12\")])".to_string(),
            ),
            (
                RangeSet("1-8,10".to_string()),
                12,
                "Some([RangeSet(\"9,11-12\")])".to_string(),
            ),
            (
                RangeSet("1".to_string()),
                20,
                "Some([RangeSet(\"2-11\"), RangeSet(\"12-20\")])".to_string(),
            ),
            (
                RangeSet("1-3,10".to_string()),
                30,
                "Some([RangeSet(\"4-9,11-14\"), RangeSet(\"15-24\"), RangeSet(\"25-30\")])"
                    .to_string(),
            ),
            (
                RangeSet("1-3,10,13-14".to_string()),
                30,
                "Some([RangeSet(\"4-9,11-12,15-16\"), RangeSet(\"17-26\"), RangeSet(\"27-30\")])"
                    .to_string(),
            ),
        ];

        for (range, seq, result) in to_generate_missing {
            let value = format!("{:?}", range.list_missing_ranges(seq, 10));
            assert_eq!(value, result);
        }

        let check_complete = vec![
            (RangeSet("".to_string()), 0, true),
            (RangeSet("1-1".to_string()), 1, true),
            (RangeSet("1".to_string()), 1, true),
            (RangeSet("1".to_string()), 3, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 3, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 5, false),
            (RangeSet("1-200".to_string()), 200, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 3, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 5, false),
            (RangeSet("1-200".to_string()), 200, true),
        ];

        for (range, seq, result) in check_complete {
            let value = range.is_complete(seq);
            assert_eq!(value, result);
        }

        let to_insert = vec![
            (RangeSet("".to_string()), 1, "1".to_string()),
            (RangeSet("1".to_string()), 3, "1,3".to_string()),
            (RangeSet("1,3".to_string()), 2, "1-3".to_string()),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                0,
                "0-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                1,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                2,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                3,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                4,
                "1-5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                5,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                6,
                "1-3,5-6,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                7,
                "1-3,5,7-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                8,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                9,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                10,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                8,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                13,
                "1-3,5,8-14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                8,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                21,
                "1-3,5,8-12,14,19-21".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                23,
                "1-3,5,8-12,14,19-20,23".to_string(),
            ),
        ];

        for (mut seq, new, result) in to_insert {
            let value = seq.insert_num(new);
            assert_eq!(value.to_string(), result);
        }

        let to_remove = vec![
            (RangeSet("0".to_string()), 0, "".to_string()),
            (RangeSet("0-1".to_string()), 1, "0".to_string()),
            (RangeSet("0-2,3".to_string()), 3, "0-2".to_string()),
            (RangeSet("1".to_string()), 1, "".to_string()),
            (RangeSet("1-3".to_string()), 3, "1-2".to_string()),
            (RangeSet("0-3".to_string()), 2, "0-1,3".to_string()),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                3,
                "1-2,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                4,
                "1-3,5,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                5,
                "1-3,8-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                8,
                "1-3,5,9-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                10,
                "1-3,5,8-9,11-12,14,19-20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                19,
                "1-3,5,8-12,14,20".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                20,
                "1-3,5,8-12,14,19".to_string(),
            ),
            (
                RangeSet("1-3,5,8-12,14,19-20".to_string()),
                12,
                "1-3,5,8-11,14,19-20".to_string(),
            ),
        ];

        for (mut seq, old, result) in to_remove {
            let value = seq.remove(old);
            assert_eq!(value.to_string(), result);
        }

        let check_contained = vec![
            (RangeSet("0".to_string()), 0, true),
            (RangeSet("0-1".to_string()), 1, true),
            (RangeSet("1".to_string()), 1, true),
            (RangeSet("0-1".to_string()), 2, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 0, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 2, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 3, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 4, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 5, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 6, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 7, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 8, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 11, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 12, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 13, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 14, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 19, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 20, true),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 21, false),
            (RangeSet("1-3,5,8-12,14,19-20".to_string()), 200, false),
        ];

        for (range, seq, result) in check_contained {
            let value = range.contains(seq);
            assert_eq!(value, result);
        }

        let check_intersect_and_difference = vec![
            (
                RangeSet("0".to_string()),
                RangeSet("0".to_string()),
                "0".to_string(),
                "".to_string(),
            ),
            (
                RangeSet("1".to_string()),
                RangeSet("1".to_string()),
                "1".to_string(),
                "".to_string(),
            ),
            (
                RangeSet("0-5".to_string()),
                RangeSet("0-5".to_string()),
                "0-5".to_string(),
                "".to_string(),
            ),
            (
                RangeSet("0-5,7".to_string()),
                RangeSet("0-5,8".to_string()),
                "0-5".to_string(),
                "7".to_string(),
            ),
            (
                RangeSet("0-5,7,9-10".to_string()),
                RangeSet("0-5,8-9,11".to_string()),
                "0-5,9".to_string(),
                "7,10".to_string(),
            ),
            (
                RangeSet("0-5,7,9-10,13".to_string()),
                RangeSet("0-5,8-9,11,13".to_string()),
                "0-5,9,13".to_string(),
                "7,10".to_string(),
            ),
            (
                RangeSet("0-5,7,9-10,12,19-20".to_string()),
                RangeSet("0-5,8-9,11,13".to_string()),
                "0-5,9".to_string(),
                "7,10,12,19-20".to_string(),
            ),
        ];

        for (range1, range2, result1, result2) in check_intersect_and_difference {
            let (value1, value2) = range1.intersect_and_difference(&range2);
            assert_eq!(value1.to_string(), result1);
            assert_eq!(value2.to_string(), result2);
        }

        let check_subtract = vec![
            (
                RangeSet("0".to_string()),
                RangeSet("0".to_string()),
                "".to_string(),
            ),
            (
                RangeSet("1".to_string()),
                RangeSet("1".to_string()),
                "".to_string(),
            ),
            (
                RangeSet("0-1".to_string()),
                RangeSet("0-1".to_string()),
                "".to_string(),
            ),
            (
                RangeSet("0-1".to_string()),
                RangeSet("1".to_string()),
                "0".to_string(),
            ),
            (
                RangeSet("0-1".to_string()),
                RangeSet("0".to_string()),
                "1".to_string(),
            ),
            (
                RangeSet("0-1".to_string()),
                RangeSet("3".to_string()),
                "0-1".to_string(),
            ),
            (
                RangeSet("0-1,5,9".to_string()),
                RangeSet("5".to_string()),
                "0-1,9".to_string(),
            ),
            (
                RangeSet("0-1,5,9".to_string()),
                RangeSet("5,9".to_string()),
                "0-1".to_string(),
            ),
            (
                RangeSet("0-1,5,9".to_string()),
                RangeSet("1,9".to_string()),
                "0,5".to_string(),
            ),
            (
                RangeSet("0-1,5,9-20".to_string()),
                RangeSet("1,10".to_string()),
                "0,5,9,11-20".to_string(),
            ),
            (
                RangeSet("0-1,5,9-20".to_string()),
                RangeSet("1,9-13".to_string()),
                "0,5,14-20".to_string(),
            ),
            (
                RangeSet("0-1,5,9-20".to_string()),
                RangeSet("1,20-23".to_string()),
                "0,5,9-19".to_string(),
            ),
        ];

        for (mut range1, range2, result1) in check_subtract {
            range1.subtract(&range2);
            assert_eq!(range1.to_string(), result1);
        }

        let check_is_greater = vec![
            (RangeSet("".to_string()), RangeSet("0".to_string()), false),
            (RangeSet("0".to_string()), RangeSet("0".to_string()), false),
            (RangeSet("0-1".to_string()), RangeSet("0".to_string()), true),
            (RangeSet("0-1".to_string()), RangeSet("0".to_string()), true),
            (
                RangeSet("0-5".to_string()),
                RangeSet("0-6".to_string()),
                false,
            ),
            (
                RangeSet("0-5".to_string()),
                RangeSet("0-6".to_string()),
                false,
            ),
            (
                RangeSet("0,3,5,10".to_string()),
                RangeSet("0-6".to_string()),
                false,
            ),
            (
                RangeSet("0,3,5,10-50".to_string()),
                RangeSet("0-30".to_string()),
                true,
            ),
            (
                RangeSet("0-5".to_string()),
                RangeSet("1-5".to_string()),
                true,
            ),
        ];

        for (range1, range2, result) in check_is_greater {
            let test = range1.is_greater_than(&range2);
            assert_eq!(result, test);
        }

        let check_list_missing_ranges = vec![
            (RangeSet("0".to_string()), 0 as u64, None),
            (
                RangeSet("0-1".to_string()),
                2 as u64,
                Some(vec![RangeSet::from("2")]),
            ),
            (
                RangeSet("0-1".to_string()),
                3 as u64,
                Some(vec![RangeSet::from("2"), RangeSet::from("3")]),
            ),
            (
                RangeSet("1".to_string()),
                2 as u64,
                Some(vec![RangeSet::from("2")]),
            ),
            (
                RangeSet("1-3".to_string()),
                5 as u64,
                Some(vec![RangeSet::from("4"), RangeSet::from("5")]),
            ),
        ];

        for (range1, seq, result) in check_list_missing_ranges {
            let test = range1.list_missing_ranges(seq, 1);
            assert_eq!(result, test);
        }

        let check_is_next = vec![
            (RangeSet("".to_string()), 1 as u64, true),
            (RangeSet("0".to_string()), 1 as u64, true),
            (RangeSet("0-1".to_string()), 2 as u64, true),
            (RangeSet("0-1".to_string()), 3 as u64, false),
            (RangeSet("0-2,5".to_string()), 3 as u64, true),
        ];

        for (range1, seq, result) in check_is_next {
            let test = range1.is_next(seq);
            assert_eq!(result, test);
        }

        let check_pct_complete = vec![
            (RangeSet("0".to_string()), 1 as u64, 0.0 as f64),
            (RangeSet("1".to_string()), 1 as u64, 100.0 as f64),
            (RangeSet("1-10".to_string()), 20 as u64, 50.0 as f64),
            (RangeSet("1-7,10".to_string()), 20 as u64, 40.0 as f64),
            (RangeSet("1-10".to_string()), 10 as u64, 100.0 as f64),
        ];

        for (range1, seq, result) in check_pct_complete {
            let test = range1.calc_pct_complete(seq);
            assert_eq!(result, test);
        }

        let check_insert_rangeset = vec![
            (
                RangeSet("".to_string()),
                RangeSet("0".to_string()),
                "0".to_string(),
            ),
            (
                RangeSet("0".to_string()),
                RangeSet("0".to_string()),
                "0".to_string(),
            ),
            (
                RangeSet("0".to_string()),
                RangeSet("0-10".to_string()),
                "0-10".to_string(),
            ),
            (
                RangeSet("0-3".to_string()),
                RangeSet("5-7".to_string()),
                "0-3,5-7".to_string(),
            ),
            (
                RangeSet("0-3".to_string()),
                RangeSet("2-7".to_string()),
                "0-7".to_string(),
            ),
            (
                RangeSet("0-3".to_string()),
                RangeSet("2-7,10".to_string()),
                "0-7,10".to_string(),
            ),
            (
                RangeSet("0-3".to_string()),
                RangeSet("0-3,5,10".to_string()),
                "0-3,5,10".to_string(),
            ),
            (
                RangeSet("0-3,5,7,10-20".to_string()),
                RangeSet("4,6,21".to_string()),
                "0-7,10-21".to_string(),
            ),
        ];

        for (mut range1, new, result) in check_insert_rangeset {
            let value = range1.insert_rangeset(new);
            assert_eq!(value.to_string(), result);
        }

        let check_get_first = vec![
            (RangeSet("0".to_string()), 1, "0".to_string()),
            (RangeSet("0".to_string()), 2, "0".to_string()),
            (RangeSet("0-10".to_string()), 2, "1-2".to_string()),
            (RangeSet("1-10".to_string()), 2, "1-2".to_string()),
            (RangeSet("0-10".to_string()), 10, "1-10".to_string()),
            (RangeSet("0-2,8-10".to_string()), 10, "1-2,8-10".to_string()),
            (RangeSet("0-2,8-10".to_string()), 3, "1-2,8".to_string()),
            (
                RangeSet("0-2,5-6,8-10,19-30".to_string()),
                10,
                "1-2,5-6,8-10,19-21".to_string(),
            ),
        ];

        for (range1, limit, result) in check_get_first {
            let value = range1.get_first(limit);
            assert_eq!(value.to_string(), result);
        }
    }
}

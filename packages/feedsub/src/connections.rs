use std::collections::HashMap;

use chrono::{DateTime, Utc};
use libp2p::{identity::PublicKey, swarm::ConnectionId, Multiaddr, PeerId};
use serde::{Deserialize, Serialize};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::{subscriptions::Subscriptions, FeedIdTrait};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum ConnectionStatus {
    None,
    Connected,
    NotConnected,
    PendingConnect,
    PendingDisconnect,
    TemporarilyDead,
}

#[derive(Debug, Serialize, Deserialize, Clone)]

pub enum Direction {
    Internal,
    Incoming,
    Outgoing,
}

#[derive(Debug, Clone)]
pub struct Connection<FI>
where
    FI: FeedIdTrait,
{
    pub address: Multiaddr,
    pub peer_id: Option<PeerId>,
    pub public_key: Option<PublicKey>,
    pub subscriptions: Subscriptions<FI>,
    pub connection_id: Option<ConnectionId>,
    pub direction: Option<Direction>,
    pub status: ConnectionStatus,
    pub last_request: Option<DateTime<Utc>>,
    pub last_receive: Option<DateTime<Utc>>,
    pub last_disconnect: Option<DateTime<Utc>>,
    pub retries: u16,
}

impl<FI: FeedIdTrait> Default for Connection<FI> {
    fn default() -> Self {
        Self::new()
    }
}

impl<FI: FeedIdTrait> Connection<FI> {
    pub fn new() -> Connection<FI> {
        Connection {
            address: Multiaddr::empty(),
            peer_id: None,
            public_key: None,
            subscriptions: Subscriptions::new(),
            connection_id: None,
            direction: None,
            status: ConnectionStatus::None,
            last_request: None,
            last_receive: None,
            last_disconnect: None,
            retries: 0,
        }
    }

    pub fn add_public_key(&mut self, public_key: &PublicKey) -> &mut Self {
        trace!("Connection::add_public_key {:?}", public_key);
        self.public_key = Some(public_key.clone());
        self
    }

    pub fn update_address(&mut self, address: &Multiaddr) -> &mut Connection<FI> {
        trace!("Connection::update_address");
        self.address = address.to_owned();
        self
    }
    pub fn update_connection_id(&mut self, connection_id: &ConnectionId) -> &mut Connection<FI> {
        trace!("Connection::update_address");
        self.connection_id = Some(connection_id.clone());
        self
    }

    pub fn update_peer_id(&mut self, peer_id: &PeerId) -> &mut Connection<FI> {
        trace!("Connection::update_peer_id");
        self.peer_id = Some(*peer_id);
        self
    }

    pub fn update_direction(&mut self, direction: Direction) -> &mut Connection<FI> {
        trace!("Connection::update_direction");
        self.direction = Some(direction);
        self
    }

    pub fn update_last_request(&mut self) -> &mut Self {
        trace!("Connection::update_last_request");
        self.last_request = Some(Utc::now());
        self
    }

    pub fn update_last_receive(&mut self) -> &mut Self {
        trace!("Connection::update_last_receive");
        self.last_receive = Some(Utc::now());
        self
    }

    pub fn update_status(&mut self, status: ConnectionStatus) -> &mut Connection<FI> {
        trace!("Connection::update_connection_status");
        self.status = status;
        self
    }
}

#[derive(Debug, Clone)]
pub struct Connections<FI: FeedIdTrait> {
    pending: HashMap<Multiaddr, Connection<FI>>,
    active: HashMap<PeerId, Connection<FI>>,
}

impl<FI: FeedIdTrait> Connections<FI> {
    pub fn new() -> Self {
        Self {
            pending: HashMap::new(),
            active: HashMap::new(),
        }
    }

    pub fn count_connected(&self) -> usize {
        self.active
            .values()
            .filter(|conn| conn.status == ConnectionStatus::Connected)
            .count()
    }

    pub fn count_active(&self) -> usize {
        self.active
            .values()
            .filter(|conn| {
                conn.status == ConnectionStatus::Connected
                    || conn.status == ConnectionStatus::PendingConnect
            })
            .count()
    }

    pub fn count_active_outbound(&self) -> usize {
        self.active
            .values()
            .filter(|conn| {
                (conn.status == ConnectionStatus::Connected
                    || conn.status == ConnectionStatus::PendingConnect)
                    && matches!(conn.direction, Some(Direction::Outgoing))
            })
            .count()
    }

    pub fn count_active_inbound(&self) -> usize {
        debug!("Connection::count_active_inbound {:?}", self.active);

        self.active
            .values()
            .filter(|conn| {
                (conn.status == ConnectionStatus::Connected
                    || conn.status == ConnectionStatus::PendingConnect)
                    && matches!(conn.direction, Some(Direction::Incoming))
            })
            .count()
    }

    pub fn insert_pending(&mut self, connection: Connection<FI>) {
        self.pending.insert(connection.clone().address, connection);
    }

    pub fn insert_active(&mut self, connection: Connection<FI>) {
        if let Some(peer_id) = connection.peer_id {
            self.active.insert(peer_id, connection);
        } else {
            error!(
                "Connections::insert_active | missing peer_id {:?}",
                connection
            );
        }
    }

    pub fn get_pending(&self, addr: &Multiaddr) -> Option<&Connection<FI>> {
        self.pending.get(&addr)
    }

    pub fn get_pending_mut(&mut self, addr: &Multiaddr) -> Option<&mut Connection<FI>> {
        self.pending.get_mut(&addr)
    }

    pub fn get_active(&self, peer_id: &PeerId) -> Option<&Connection<FI>> {
        self.active.get(peer_id)
    }

    pub fn is_connected(&self, addr: &Multiaddr) -> bool {
        if self.pending.contains_key(addr) {
            return true;
        } else {
            for (_, connection) in &self.active {
                if &connection.address == addr {
                    return true;
                }
            }
        }
        false
    }

    pub fn get_connected_addresses(&self) -> Vec<Multiaddr> {
        let mut pending: Vec<Multiaddr> = self.pending.keys().cloned().collect();
        let active: Vec<Multiaddr> = self
            .active
            .values()
            .map(|value| value.address.clone())
            .collect();
        pending.extend(active);
        pending
    }

    pub fn get_connected_peer_id(&self, addr: &Multiaddr) -> Option<PeerId> {
        for (peer_id, connection) in &self.active {
            if &connection.address == addr {
                return Some(peer_id.clone());
            }
        }
        None
    }

    pub fn get_active_mut(&mut self, peer_id: &PeerId) -> Option<&mut Connection<FI>> {
        self.active.get_mut(peer_id)
    }

    pub fn get_all_pending(&self) -> Vec<&Connection<FI>> {
        self.pending.values().collect()
    }

    pub fn get_all_active(&self) -> Vec<&Connection<FI>> {
        self.active.values().collect()
    }

    pub fn get_all_pending_mut(&mut self) -> Vec<&mut Connection<FI>> {
        self.pending.values_mut().collect()
    }

    pub fn get_all_active_mut(&mut self) -> Vec<&mut Connection<FI>> {
        self.active.values_mut().collect()
    }

    pub fn get_active_by_status(&self, status: ConnectionStatus) -> Vec<&Connection<FI>> {
        self.active
            .values()
            .filter(|conn| conn.status == status)
            .collect()
    }

    pub fn get_subscribers(&self, feed_id: &FI) -> Vec<Multiaddr> {
        let mut list = Vec::new();

        for connection in self.get_all_active() {
            if connection.subscriptions.get(feed_id).is_some() {
                list.push(connection.address.clone());
            }
        }
        list
    }

    pub fn remove_pending(&mut self, addr: &Multiaddr) {
        self.pending.remove(addr);
    }

    pub fn remove_active(&mut self, peer_id: &PeerId) {
        self.active.remove(peer_id);
    }

    pub fn clear_feed_ids(&mut self, feed_ids: &Vec<FI>) {
        for (_, connection) in &mut self.active {
            connection.subscriptions.remove_feed_ids(feed_ids);
        }
    }
}

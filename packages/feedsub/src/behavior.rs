use crate::config::FeedsubConfig;
use crate::connections::{Connection, ConnectionStatus, Connections, Direction};
use crate::feeds::Feed;
use crate::handler::{FeedsubHandler, FromBehaviourEvent, PeerMessage, ToBehaviourEvent};
use crate::peers::{PeerCache, PeerCacheConfig, Peer};
use crate::proto::proto_message::MsgData;
use crate::proto::proto_task::TaskData;
use crate::proto::{Payload, ProtoDisconnect, ProtoHandshake, ProtoPeer, ProtoPeerCache, ProtoPeerSubscription, ProtoSubscribe, ProtoTask, ProtoTaskFetchPeers, ProtoTaskFetchUpdatesbySeq, ProtoUnsubscribe, ProtoUpdate};
use crate::protocol::FeedsubCodec;
use crate::range_set::RangeSet;
use crate::subscriptions::Subscriptions;
use crate::task_manager::processor::{ProcessorWorkType, TaskProcessor};
use crate::task_manager::tasks::{TaskFetchPeers, TaskFetchUpdatesBySeqData, TaskId, TaskType};
use crate::task_manager::{tasks::Task, TaskManager};
use crate::{FeedIdTrait, FeedType, FeedsubError, Update};
use async_trait::async_trait;
use chrono::{Utc, Duration};
use futures::StreamExt;
use libp2p::core::Multiaddr;
use libp2p::identity::{Keypair, PublicKey};
use libp2p::swarm::{
    behaviour::ConnectionEstablished, ConnectionId, NetworkBehaviour, NotifyHandler,
    PollParameters, StreamUpgradeError, ToSwarm,
};
use libp2p::swarm::{ConnectionClosed, ConnectionDenied, NewListenAddr, THandler};
use libp2p::PeerId;
use prost::Message;
use tokio::sync::mpsc::{Sender, Receiver, self};
use std::cmp::{Ordering, self};
use std::fmt::Debug;
use std::marker::PhantomData;
use std::sync::{RwLock, RwLockReadGuard};
// use std::cmp;
use std::{
    collections::{HashMap, VecDeque},
    io,
    sync::{Arc, Mutex},
    task::{Context, Poll, Waker},
};
use tokio;
use uuid::Uuid;
use wasm_timer::{Instant, Interval};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};



#[derive(Debug)]
pub struct Feedsub<FI, UF>
where
    FI: FeedIdTrait,
{
    pub config: FeedsubConfig<FI>,
    /// For each peer we're connected to, the observed address to send back to it.
    pub connections: Connections<FI>,
    /// Tasks performed for other peers
    pub task_manager: TaskManager<FI>,
    /// The state of our feed subscriptions
    pub my_subscriptions: Subscriptions<FI>,
    /// Pending events to be emitted when polled.
    events: VecDeque<ToSwarm<FeedsubEvent<FI>, FromBehaviourEvent>>,
    /// All peers that we have discovered.
    pub peers: PeerCache<FI>,
    /// a callback fn to fetch historical feed updates by Vec<seq>
    // update_fetcher: Arc<UF>,
    update_fetcher: PhantomData<UF>,
    /// a scheduler for background processes
    heartbeat: Interval,
    /// a thread waker to re-poll Feedsub when out-of-process events occur
    waker: Option<Arc<Mutex<Waker>>>,
    /// this node's listeners
    my_listeners: Vec<Multiaddr>,
    /// Channels to exchange tasks and updates with TaskProcessor
    to_processor_send: Sender<ProcessorWorkType<FI>>,
    from_processor_recv: Receiver<ProcessorWorkType<FI>>,

}

impl<FI, UF> Feedsub<FI, UF>
where
    FI: FeedIdTrait + 'static + Sync,
    UF: UpdateFetcher<FI, UF> + Sync + Send + 'static,
{
    /// Creates a new `Feedsub` network behaviour.

    pub fn new(config: FeedsubConfig<FI>, update_fetcher: UF) -> Self {
        debug!("Feedsub::new");

        let target_peers = config.target_peers_per_feed();
        let peer_cache_config = PeerCacheConfig::new().with_target_peers_per_feed(target_peers);

        let heartbeat = config.heartbeat_interval().clone();

        let (to_processor_send, to_processor_recv) = mpsc::channel::<ProcessorWorkType<FI>>(128);
        let (from_processor_send, from_processor_recv) = mpsc::channel::<ProcessorWorkType<FI>>(128);

        let target_bytes_to_send = config.target_payload_size();

        let task_processor =
            TaskProcessor::new(to_processor_recv, from_processor_send, update_fetcher, *target_bytes_to_send);

        tokio::spawn(async {
            task_processor.run().await;
        });

        Feedsub {
            config,
            connections: Connections::new(),
            my_subscriptions: Subscriptions::new(),
            task_manager: TaskManager::new(),
            events: VecDeque::new(),
            peers: PeerCache::new(peer_cache_config),
            update_fetcher: PhantomData,
            heartbeat: Interval::new_at(Instant::now() + heartbeat.clone(), heartbeat.clone()),
            waker: None,
            my_listeners: Vec::new(),
            to_processor_send,
            from_processor_recv,
        }
    }

    pub fn dial(&mut self, addr: Multiaddr) {
        debug!("Feedsub::dial {}", addr);

        // ignore the dial request if already connected
        if self.connections.is_connected(&addr){
            warn!("Feedsub::dial | already connected {}", addr);
            return
        }

        let connection = Connection::new()
            .update_address(&addr)
            .update_status(ConnectionStatus::PendingConnect)
            .update_direction(Direction::Outgoing)
            .clone();

            if !self.my_listeners.contains(&addr) {
                if self.peers.get_peer(&addr).is_none() {
                    let peer = Peer::new(None, addr.clone());

                    self.peers.insert(&peer);
                }
            }

            self.connections.insert_pending(connection);
      

        debug!("Feedsub::dial | connections {:?}", self.connections);

        self.events
            .push_back(ToSwarm::GenerateEvent(FeedsubEvent::DialPeer { addr }));
    }

    pub fn disconnect(&mut self, peer_id: &PeerId) {
        debug!("Feedsub::disconnect {}", peer_id);

        self.connections.remove_active(peer_id);

        self.events
            .push_back(ToSwarm::GenerateEvent(FeedsubEvent::DisconnectPeer {
                peer_id: *peer_id,
            }));
    }

    pub fn connections(&self) -> Vec<Connection<FI>> {
        self.connections
            .get_all_active()
            .into_iter()
            .cloned()
            .collect()
    }

    pub fn connection_count(&self) -> usize {
        self.connections.count_connected()
    }

    pub fn subscribe(&mut self, feeds: Vec<Feed<FI>>) {
        trace!("Feedsub::subscribe: {:?}", feeds);
        self.node_workflow(NodeProcess::Subscribe(feeds));
    }

    pub fn unsubscribe(&mut self, feed_ids: Vec<FI>) {
        debug!("Feedsub::unsubscribe: {:?}", feed_ids);

        // remove from my_subscriptions
        self.my_subscriptions.remove_feed_ids(&feed_ids);

        // clear all tasks
        self.task_manager.clear_all_for_feed_ids(&feed_ids);

        // clear all from connections
        self.connections.clear_feed_ids(&feed_ids);

        // clear all from peers?
        self.peers.remove_feed_ids(&feed_ids);

        self.events
            .push_back(ToSwarm::GenerateEvent(FeedsubEvent::SelfUnsubscribe {
                feed_ids,
            }));

        self.wake();
    }

    pub fn broadcast_update(&mut self, feed_id: FI, data: Vec<u8>, author_keys: &Keypair) -> Result<(), FeedsubError> {
        trace!("Feedsub::broadcast_update: {:?} {:?}", feed_id, data);
        debug!("Feedsub::broadcast_update: {:?} {:?}", feed_id, data.len());


        // Find peers
        let mut receipients = Vec::new();

        let connections = self
            .connections
            .get_active_by_status(ConnectionStatus::Connected);

        for connection in connections {
            // check if peer is subscriber
            if connection.subscriptions.is_subscribed(&feed_id) && connection.peer_id.is_some() {
                receipients.push(connection.peer_id.unwrap());
            }
        }

        // Prepare outbound message

        let feed = self.my_subscriptions.get_or_new_mut(&feed_id);

        let sig = match author_keys.sign(&data){
            Ok(sig) => sig,
            Err(e) => {
                error!("Feedsub::broadcast_update | {:?}", e);
                Vec::new()
            },
        };

        let update = Update::new(feed, data, None, sig);
        debug!("Feedsub::broadcast_update | {:?} | {:?}", update, feed);

        let self_update = update.clone();

        if let Some(self_update) = self_update {
            let feed_clone = Arc::try_unwrap(feed.clone()).map_or_else(
                |arc| arc.read().unwrap().clone(), 
                |rwlock| rwlock.into_inner().unwrap().clone(), 
            );

            self.events
                .push_back(ToSwarm::GenerateEvent(FeedsubEvent::SelfUpdate {
                    feed: feed_clone,
                    update: self_update,
                }));

            let codec = FeedsubCodec::<FI>::new(
                self.config.node_keys().clone(),
                self.config.protocol_version(),
            );

            // We send our current set of subscriptions
            // and the remote peer will send a task to fetch the update, if needed
            let sub = self.my_subscriptions.get(&feed_id);

            if !receipients.is_empty(){
                debug!("Feedsub::broadcast_update | receipients {:?}", receipients);
                if let Some(subscription) = sub {

                    if let Ok(payload) = codec.encode_subscribe(
                        vec![subscription.clone()]
                    ){
                        self.send_to_some(payload, receipients.clone());

                    } else {
                        error!("Feedsub::broadcast_update | failed to encode_subscribe");

                    }
                }
            }
        }
        self.wake();
        Ok(())
    }

    pub fn resolve_feed_tasks(&mut self, peer_id: &PeerId, feed_id: &FI, updates: Vec<Update<FI>>) {
        debug!(
            "Feedsub::resolve_feed_tasks  {:?} {:?}  {:?}",
            peer_id, feed_id, updates
        );

        let node_keys = self.config.node_keys().clone();
        let protocol_version = self.config.protocol_version();

        let codec = FeedsubCodec::<FI>::new(node_keys, protocol_version);
        let subscription = self.my_subscriptions.get(feed_id).unwrap().clone();

        let payloads = codec.encode_update(
            Some(vec![subscription.clone()]),
            Some(updates),
            *self.config.target_payload_size(),
        );

        for payload in payloads {
            self.send_to_some(payload, vec![*peer_id]);
        }
    }

    pub fn send_to_all(&mut self, payload: Payload) {
        let connections = self.connections.get_all_active();

        debug!("Feedsub::send_to_all");
        trace!("Feedsub::send_to_all | payload {:?}", payload);

        for connection in connections {
            debug!("Feedsub::send_to_all | connection {:?}", connection);

            if let Some(peer_id) = connection.peer_id {
                match connection.status {
                    ConnectionStatus::Connected => {
                        if let Some(connection_id) = connection.connection_id {
                            self.events.push_back(ToSwarm::NotifyHandler {
                                peer_id,
                                handler: NotifyHandler::One(connection_id),
                                event: FromBehaviourEvent::Message(payload.clone()),
                            });
                        }
                    }
                    _ => {}
                }
            }
        }
        self.wake();
    }

    pub fn send_to_some(&mut self, payload: Payload, peer_ids: Vec<PeerId>) {
        debug!("Feedsub::send_to_some {:?}", peer_ids,);

        let Payload {message, .. } = &payload;
        
        use crate::proto::ProtoMessage;
        if let Some(ProtoMessage {msg_data}) = &message {
            if let Some(msg_data) = msg_data {
                match msg_data {
                    MsgData::Subscribe(subscribe) => {
                        let ProtoSubscribe {feeds} = subscribe;
                        debug!("Feedsub::send_to_some | Subscribe |  Num feeds {:?}", feeds.len());
                    },
                    MsgData::Unsubscribe(unsubscribe) => {
                        let ProtoUnsubscribe {feed_ids} = unsubscribe;
                        debug!("Feedsub::send_to_some | Unsubscribe |  Num feed_ids {:?}", feed_ids.len());
                    },
                    MsgData::Update(update) => {
                        let ProtoUpdate {subscriptions, updates} = update;
                        debug!("Feedsub::send_to_some | Update |  Num subscriptions {:?} | Num updates {:?}", subscriptions.len(), updates.len());
                    },
                    MsgData::Task(task) => {
                        let ProtoTask { task_data } = task;
                        if let Some(task_data) = task_data {
                            match task_data {
                                TaskData::TaskFetchUpdatesBySeq(task) => {
                                    let ProtoTaskFetchUpdatesbySeq { feed_id, requested_seqs } = task;
                                    debug!("Feedsub::send_to_some | TaskFetchUpdatesbySeq | feed_id {:?} | req seqs {:?}", feed_id, requested_seqs);
                                },
                                TaskData::TaskFetchPeers(task) => {
                                    let ProtoTaskFetchPeers { task_id, feed_ids } = task;
                                    debug!("Feedsub::send_to_some | TaskFetchPeers | task_id {:?} | feed_ids {:?}", task_id, feed_ids);
                                },
                            }
                        }
                    },
                    MsgData::PeerCache(msg) => {
                        let ProtoPeerCache { task_id, peer_subscriptions } = msg;
                        debug!("Feedsub::send_to_some | PeerCache | task_id {:?} | peer_subscriptions {:?}", task_id, peer_subscriptions);
                    },
                    MsgData::Handshake(msg) => {
                        let ProtoHandshake { peer, feeds , public_key} = msg;
                        debug!("Feedsub::send_to_some | Handshake | peer {:?} | feeds {:?} | public_key {:?}", peer, feeds, public_key);                        
                    },
                    MsgData::Peers(msg) => {
                        let ProtoDisconnect { peers } = msg;
                        debug!("Feedsub::send_to_some | Peers | {:?}", peers);                        
                    },
                }
            }
        }

        

        for peer_id in peer_ids {
            if let Some(connection) = self.connections.get_active(&peer_id) {
                if let Some(connection_id) = connection.connection_id {
                    debug!("Feedsub::send_to_some | added to queue");

                    self.events.push_back(ToSwarm::NotifyHandler {
                        peer_id,
                        handler: NotifyHandler::One(connection_id),
                        event: FromBehaviourEvent::Message(payload.clone()),
                    });
                }
            }
        }
        self.wake();
    }

    fn on_connection_established(
        &mut self,
        ConnectionEstablished {
            peer_id,
            connection_id: conn,
            endpoint,
            failed_addresses,
            ..
        }: ConnectionEstablished,
    ) {
        debug!(
            "Feedsub::on_connection_established: {:?} {:?} {:?} {:?}",
            peer_id, conn, endpoint, failed_addresses
        );

        self.connection_workflow(ConnectionProcess::PendingConnect(peer_id));

    }

    fn on_connection_closed(
        &mut self,
        ConnectionClosed {
            peer_id,
            connection_id,
            endpoint,
            ..
        }: ConnectionClosed<FeedsubHandler<FI>>,
    ) {
        debug!(
            "Feedsub::on_connection_closed: {:?} {:?} {:?}",
            peer_id, connection_id, endpoint
        );

        self.task_manager.clear_all_for_peer(&peer_id);

        self.connections.remove_active(&peer_id);

        debug!("Feedsub::on_connection_closed: PeerCache {:?}", self.peers);

        self.connection_workflow(ConnectionProcess::DialPeers);

        self.events
            .push_back(ToSwarm::GenerateEvent(FeedsubEvent::PeerDisconnected {
                peer_id,
            }))
    }

    fn heartbeat(&mut self) {
        debug!("Feedsub::heartbeat Connections {:?}", self.connections);
        debug!("Feedsub::heartbeat Events {:?}", self.events);
        debug!(
            "Feedsub::heartbeat My Subscriptions {:?}",
            self.my_subscriptions
        );
        debug!("Feedsub::heartbeat Peers {:?}", self.peers);
        debug!("Feedsub::heartbeat Task Manager {:?}", self.task_manager);

  
        self.connection_workflow(ConnectionProcess::DialPeers);

        self.connection_workflow(ConnectionProcess::CleanPeerCache);

        self.connection_workflow(ConnectionProcess::CleanTaskManager);

        self.connection_workflow(ConnectionProcess::CheckIfWorkComplete);
    }

    pub fn wake(&self) {
        trace!("Feedsub::wake");
        if let Some(waker) = &self.waker {
            if let Ok(waker_guard) = waker.lock() {
                waker_guard.wake_by_ref();
                debug!("Feedsub::wake | Done");
            }
        }
    }

 
    fn connection_workflow(&mut self, item: ConnectionProcess<FI>){
        match item {

            ConnectionProcess::DialPeers => {
                debug!("Feedsub::connection_workflow | DialPeers");

                if self.connections.count_active_outbound() < *self.config.max_outbound_connections() {
                    
                    let connected = self.connections.get_connected_addresses();
                    let incomplete = self.my_subscriptions.get_incomplete();

                    if let Some(address)= self.peers.get_address_to_dial(incomplete, &connected) {
                                
                        debug!(
                            "Feedsub::connection_workflow | DialPeers | address {:?}",
                            address
                        );
                        self.dial(address);
                    } else {
                        debug!(
                            "Feedsub::connection_workflow | DialPeers | no available peers"
                        );
                    }
                }
            },

            ConnectionProcess::PendingConnect(peer_id) => {
                debug!("Feedsub::connection_workflow | PendingConnect {:?}", peer_id);
                self.connection_workflow(ConnectionProcess::CheckIfOverloaded(peer_id));
            }

            ConnectionProcess::CheckIfOverloaded(peer_id) => {
                debug!("Feedsub::connection_workflow | CheckIfOverloaded {:?}", peer_id);

                let mut to_disconnect = None;

                if let Some(_) = self.connections.get_active(&peer_id){       

                    if &self.connections.count_active_inbound() > self.config.max_inbound_connections() {
                        debug!("NetworkBehaviour for Feedsub:: handle_pending_inbound_connection | too many inbound"); 
            
                        let codec = FeedsubCodec::<FI>::new(
                            self.config.node_keys().clone(),
                            self.config.protocol_version(),
                        );

                        to_disconnect = Some(&peer_id);

                        // Send disconnect with some other peers
                        let peers = self.peers.get_some(self.config.max_peers_to_send_on_overloaded());
                        if let Ok(payload) = codec.encode_disconnect(&peers){
                            info!("NetworkBehaviour for Feedsub:: handle_pending_inbound_connection | too many inbound | forwarding peers: {:?}", peers); 

                            self.send_to_some(payload, vec![peer_id]);
                        }

                    }

                    if let Some(peer_id) = to_disconnect {
                        if let Some(connection) = self.connections.get_active_mut(peer_id){
                            connection.update_status(ConnectionStatus::PendingDisconnect);
                        }
                    } else {
                        self.connection_workflow(ConnectionProcess::Connect(peer_id)); 
                    }

                } else {
                    error!("Feedsub::connection_workflow | CheckIfOverloaded | connection not active")
                }
            },

            ConnectionProcess::Connect(peer_id) => {
                debug!("Feedsub::connection_workflow | Connect {:?}", peer_id);

                self.connection_workflow(ConnectionProcess::SendHandshake(peer_id));
            }

            ConnectionProcess::ReceiveDisconnect(peer_id, peers) => {
                info!("Feedsub::connection_workflow | ReceiveDisconnect {:?}", peers);


                if let Some(connection) = self.connections.get_active(&peer_id) {
                    let address = &connection.address;
                    // mark peer as connected to record last connected time
                    self.peers.is_connected(&Some(peer_id), address);
                }


                for peer in peers{
                    let addr = &peer.address;
                    if !self.my_listeners.contains(addr) && !addr.is_empty() {
                        if self.peers.get_peer(addr).is_none() {
                            self.peers.insert(&peer);
                        }
                    }
                }
                self.disconnect(&peer_id);
            }

            ConnectionProcess::SendHandshake(peer_id) => {
                // handshaking is the process of sending a set of subscribed feeds to the other node
                debug!("Feedsub::connection_workflow | SendHandshake {:?}", peer_id);
        
                if let Some(local_addr) = self.my_listeners.first() {
                    let codec = FeedsubCodec::<FI>::new(
                        self.config.node_keys().clone(),
                        self.config.protocol_version(),
                    );

                    // update connections' last_request time
                    if let Some(connection) = self.connections.get_active_mut(&peer_id){
                        connection.update_last_request();
}
     
                    // Send Handshake
                    let my_feeds = self.my_subscriptions.get_some(&self.config.max_feeds_to_handshake());
                    let my_peer_id = self.config.node_keys().public().to_peer_id();
                    let public_key = self.config.node_keys().public();
                    if let Ok(payload) = codec.encode_handshake(&Some(my_peer_id), local_addr, my_feeds, public_key){
                        self.send_to_some(payload, vec![peer_id]);

                    }
                }
            },
            
            ConnectionProcess::ReceiveHandshake(peer_id, address, feeds, public_key) => {
                // handshaking is the process of sending a set of subscribed feeds to the other node
                debug!("Feedsub::connection_workflow | ReceiveHandshake {:?} {:?} {:?}", peer_id, address, feeds);

                let mut disconnect = false;

                if let Some(peer_id) = peer_id {

                    if let Some(connection) = self.connections.get_active_mut(&peer_id){

                        // set the timer on expiry and disconnect
                        if connection.last_receive.is_none(){
                            connection.update_last_receive();
                        }

                        // disconnect if it was pending
                        if connection.status == ConnectionStatus::PendingDisconnect {
                            disconnect = true;
                            self.disconnect(&peer_id);
                        }
                    }
                };

                if !disconnect {
                     // update the connection
                    if let Some(peer_id) = peer_id{
                        if let Some(connection) = self.connections.get_active_mut(&peer_id) {
                            connection.update_address(&address);

                            if connection.public_key.is_none(){
                                if let Ok(public_key) = PublicKey::try_decode_protobuf(&public_key){
                                    connection.add_public_key(&public_key);
                                } else {
                                    error!("Feedsub::connection_workflow | ReceiveHandshake | failed to decode public_key");
                                }
                            }
        
                            // mark peer as connected
                            self.peers.is_connected(&Some(peer_id), &address);
    
                            self.events
                                .push_back(ToSwarm::GenerateEvent(FeedsubEvent::PeerConnected {
                                    peer_id
                            }));
                        }
                    }
    
                    if let Some(peer_id) = peer_id{ 
                        self.connection_workflow(ConnectionProcess::PeerSubscribe(peer_id, address, feeds));
                    } else {
                        error!("Feedsub::connection_workflow | ReceiveHandshake | missing peer_id")
                    }
                }
            },

            ConnectionProcess::PeerSubscribe(peer_id, address, feeds) => {
                debug!("Feedsub::connection_workflow | PeerSubscribe {:?} {:?}", peer_id, feeds);
            
                // Separate the feeds into subscribed and unsubscribed
                let (subscribed_feeds, unsubscribed_feeds): (Vec<_>, Vec<_>) = feeds
                    .into_iter()
                    .partition(|item| self.my_subscriptions.is_subscribed(&item.id));
            
                // Convert subscribed feeds to Arc<RwLock<_>>
                let arc_subscribed_feeds = subscribed_feeds
                    .iter()
                    .map(|feed| Arc::new(RwLock::new(feed.clone())))
                    .collect::<Vec<_>>();
            
                // Process subscribed feeds
                if !arc_subscribed_feeds.is_empty() {
                    self.connection_workflow(ConnectionProcess::AddSubscribedFeedsToConnection(peer_id, arc_subscribed_feeds.clone()));
                    self.connection_workflow(ConnectionProcess::UpdateMySubscriptionsIfStale(peer_id, arc_subscribed_feeds.clone()));
                    self.connection_workflow(ConnectionProcess::AddSubscribedFeedsToPeerCache(peer_id, address.clone(), arc_subscribed_feeds));
                    self.connection_workflow(ConnectionProcess::SendPeerCacheTask(peer_id, address.clone()));
            
                    self.events.push_back(ToSwarm::GenerateEvent(FeedsubEvent::PeerSubscribe { peer_id, feeds: subscribed_feeds }));
                }
            
                // Process unsubscribed feeds            
                if !unsubscribed_feeds.is_empty() {
                    self.connection_workflow(ConnectionProcess::AddUnsubscribedFeedsToPeerCache(peer_id, address.clone(), unsubscribed_feeds));
                }
            },
            
            ConnectionProcess::AddSubscribedFeedsToConnection(peer_id, feeds) => {
                debug!("Feedsub::connection_workflow | AddSubscribedFeedsToConnection {:?} {:?}", peer_id, feeds);

                for incoming_feed in feeds {
                    // get the peer's connection
                    if let Some(connection) = self.connections.get_active_mut(&peer_id) {

                        let seq_compare: Result<Ordering, String> = connection.subscriptions.compare_seq(&incoming_feed);
                        debug!("Feedsub::connection_workflow | AddSubscribedFeedsToConnection | seq_compare {:?}", seq_compare);

                        let inventory_compare = connection.subscriptions.compare_inventory(&incoming_feed);
                        debug!("Feedsub::connection_workflow | AddSubscribedFeedsToConnection | inventory_compare {:?}", inventory_compare
                    );

                        let is_greater = [seq_compare, inventory_compare]
                        .iter()
                        .filter_map(|result| result.as_ref().ok()) // Filter out Ok values, ignore errors
                        .any(|&ordering| ordering == Ordering::Greater);        

                        if is_greater {
                            debug!("Feedsub::connection_workflow | AddSubscribedFeedsToConnection | inserting {:?}", incoming_feed);
                            connection.subscriptions.insert(incoming_feed.clone());
                        }
   
                    } else {
                        error!("Feedsub::connection_workflow | AddSubscribedFeedsToConnection | active connection not found")
                    }
                }
            },

            ConnectionProcess::UpdateMySubscriptionsIfStale(peer_id, feeds) => {
                debug!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale {:?} {:?}", peer_id, feeds);

                let mut stale_feeds = HashMap::new();

                for incoming_feed in feeds {

                    match self.my_subscriptions.compare_seq(&incoming_feed){
                        Ok(order) => {
                            match order {
                                // If the remote copy is stale, we let them know
                                Ordering::Less => {
                                    debug!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale | remote seq is stale");

                                    if let Ok(feed) = incoming_feed.read(){
                                        if let Some(my_feed) = self.my_subscriptions.get(&feed.id){
                                            stale_feeds.insert(feed.id.clone(), my_feed.clone());
                                        }
                                    };
                                },
                                // If our seq is stale, we update it
                                Ordering::Greater => {
                                    debug!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale | local seq is stale");

                                    self.my_subscriptions.update_seq(&incoming_feed);
                                    self.connection_workflow(ConnectionProcess::SendFeedTask(peer_id))
                                },
                                _ => {},
                            } 
                        },
                        Err(e) => error!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale {:?}", e),
                    }

                    match self.my_subscriptions.compare_inventory(&incoming_feed) {
                        Ok(order) => {
                            match order {
                                // If their inventory is less, we let them know
                                Ordering::Less => {
                                    debug!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale | their inventory is stale");
                                    if let Ok(feed) = incoming_feed.read(){
                                        if let Some(my_feed) = self.my_subscriptions.get(&feed.id){
                                            stale_feeds.insert(feed.id.clone(), my_feed.clone());
                                        }
                                    };

                                },
                                // If their inventory is greater, we send tasks
                                Ordering::Greater => {
                                    debug!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale | their inventory is greater");
                                    self.connection_workflow(ConnectionProcess::SendFeedTask(peer_id));
                                },
                                _ => {}
                            }
                        },
                        Err(e) => error!("Feedsub::connection_workflow | UpdateMySubscriptionsIfStale {:?}", e),
                    }
                }

                if !stale_feeds.is_empty(){
                    let codec = FeedsubCodec::<FI>::new(
                            self.config.node_keys().clone(),
                            self.config.protocol_version(),
                    );

                    let feeds = stale_feeds.values().cloned().collect();
                    if let Ok(payload) = codec.encode_subscribe(feeds){
                        self.send_to_some(payload, vec![peer_id]);
                    }
                }
            },

            ConnectionProcess::AddSubscribedFeedsToPeerCache(peer_id, address, feeds) => {
                debug!("Feedsub::connection_workflow | UpdatePeerCache {:?} {:?} {:?}", peer_id, address, feeds);

                for incoming_feed in feeds {

                    let feed_id = incoming_feed.read().unwrap().id.clone();
                    let addr = address.clone();

                    if !self.my_listeners.contains(&addr) && !addr.is_empty() {
                        self.peers.subscribe_feed(&Some(peer_id), &addr, &feed_id);
                    }
                }
            },

            ConnectionProcess::AddUnsubscribedFeedsToPeerCache(peer_id, address, feeds ) => {
                debug!("Feedsub::connection_workflow | AddUnsubscribedFeedsToPeerCache {:?} {:?} {:?}", peer_id, address, feeds);
                for feed in feeds {
                    let feed_id = feed.id;
                    self.peers.unsubscribed_feeds.insert(feed_id, address.clone());
                }
            },

            ConnectionProcess::PeerUnsubscribe(peer_id, address, feed_ids) => {
                debug!("Feedsub::connection_workflow | PeerUnsubscribe {:?} {:?} {:?}", peer_id, address, feed_ids);

                // Separate the feeds into subscribed and unsubscribed
                let (subscribed_ids, unsubscribed_ids): (Vec<_>, Vec<_>) = feed_ids
                .into_iter()
                .partition(|id| self.my_subscriptions.is_subscribed(&id));

                if !subscribed_ids.is_empty(){
                    self.connection_workflow(ConnectionProcess::RemoveSubscribedFeedsFromConnection(peer_id, address.clone(), subscribed_ids.clone()));
                    self.connection_workflow(ConnectionProcess::RemoveSubscribedFeedsFromMySubscriptions(peer_id, address.clone(), subscribed_ids.clone()));
                    self.connection_workflow(ConnectionProcess::RemoveSubscribedFeedsFromPeerCache(peer_id, address.clone(), subscribed_ids.clone()));

                    self.events.push_back({
                        ToSwarm::GenerateEvent(FeedsubEvent::PeerUnsubscribe { peer_id, feed_ids: subscribed_ids })
                    });
                }

                if !unsubscribed_ids.is_empty(){
                    self.connection_workflow(ConnectionProcess::RemoveUnsubscribedFeedsFromPeerCache(peer_id, address, unsubscribed_ids.clone()));

                }
            },

            ConnectionProcess::RemoveSubscribedFeedsFromConnection(peer_id, address, feed_ids) => {
                warn!("Feedsub::connection_workflow | RemoveSubscribedFeedsFromConnection {:?} {:?} {:?}", peer_id, address, feed_ids);

            },

            ConnectionProcess::RemoveSubscribedFeedsFromMySubscriptions(peer_id, address, feed_ids) => {
                warn!("Feedsub::connection_workflow | RemoveSubscribedFeedsFromMySubscriptions {:?} {:?} {:?}", peer_id, address, feed_ids);

            },

            ConnectionProcess::RemoveSubscribedFeedsFromPeerCache(peer_id, address, feed_ids) => {
                warn!("Feedsub::connection_workflow | RemoveSubscribedFeedsFromPeerCache {:?} {:?} {:?}", peer_id, address, feed_ids);

            },

            ConnectionProcess::RemoveUnsubscribedFeedsFromPeerCache(peer_id, address, feed_ids) => {
                warn!("Feedsub::connection_workflow | RemoveUnsubscribedFeedsFromPeerCache {:?} {:?} {:?}", peer_id, address, feed_ids);

            },

            ConnectionProcess::SendPeerCacheTask(peer_id, address) => {
                debug!("Feedsub::connection_workflow | SendPeerCacheTask {:?}", peer_id);

                let mut feed_ids = self.my_subscriptions.get_some_incomplete_ids(self.config.max_feeds_to_fetch_peer_cache());
               
                fn send_tasks<FI: FeedIdTrait + 'static, UF: UpdateFetcher<FI, UF> + 'static>(
                    feedsub: &mut Feedsub<FI, UF>,
                    to_send: Vec<(PeerId, Multiaddr, Vec<FI>)>,
                ) where
                    UF: Send + Sync,
                    FI: Sync,
                {
                    debug!("Feedsub::connection_workflow | RequestPeers | send_tasks");
                    let codec = FeedsubCodec::<FI>::new(
                        feedsub.config.node_keys().clone(),
                        feedsub.config.protocol_version(),
                    );
        
                    for (peer_id, address, feed_ids) in to_send {
                        //assign task
        
                        let task_type =
                            TaskType::TaskFetchPeers(TaskFetchPeers::new(Uuid::new_v4(), feed_ids));
        
                        let task = Task::new(address.clone(), peer_id, task_type);
        
                        debug!(
                            "Feedsub::connection_workflow | RequestPeers | send_tasks | sending task {:?}",
                            task
                        );
        
                        feedsub
                            .task_manager
                            .outgoing_tasks
                            .insert_sent(task.clone());
        

                        // prepare and queue task for sending
                        if let Ok(payload) = codec.encode_task(&task){
                            feedsub.send_to_some(payload, vec![peer_id]);
                        }
                    }
                }
        
                let mut to_send = Vec::new();
        
                // retain only the feed_ids where PeerCache is deficient in subscribers
                feed_ids.retain(|f| self.peers.is_feed_id_missing_subscribers(f));

                debug!("Feedsub::connection_workflow | SendPeerCacheTask | feed_ids to request {:?}", feed_ids);
    
                if !feed_ids.is_empty() {
                    to_send.push((peer_id, address, feed_ids));
                }
        
                if !to_send.is_empty() {

                    // update connections' last_request time
                    if let Some(connection) = self.connections.get_active_mut(&peer_id){
                        connection.update_last_request();
                    }

                    send_tasks(self, to_send);
                }
            },

            ConnectionProcess::ReceivePeerCacheTask(peer_id, data) => {
                debug!("Feedsub::connection_workflow | ReceivePeerCacheTask {:?} {:?}", peer_id, data);

                if let Some(connection) = self.connections.get_active(&peer_id) {
                    let task = Task::new(
                        connection.address.clone(),
                        peer_id,
                        TaskType::TaskFetchPeers(data),
                    );
                    // Add this task to our backlog
                    self.task_manager.incoming_tasks.insert(task.clone());

                    self.events
                        .push_back(ToSwarm::GenerateEvent(FeedsubEvent::PeerTask { task }))
                }

            }

            ConnectionProcess::SendPeerCacheData(task) => {
                debug!("Feedsub::connection_workflow | SendPeerCacheData {:?}", task);

                let peer_id = task.peer_id;

                if let TaskType::TaskFetchPeers(task) = task.task_type {
                    let node_keys = self.config.node_keys().clone();
                    let protocol_version = self.config.protocol_version();

                    let TaskFetchPeers { feed_ids, task_id } = task;

                    let codec = FeedsubCodec::<FI>::new(node_keys, protocol_version);
    
                    let subscriptions = self.peers.get_subscribers_for_feed_ids(&feed_ids);
    
                    debug!("Feedsub::connection_workflow | SendPeerCacheData | sending... {:?}", subscriptions);

                    if let Ok(payload) = codec.encode_peer_cache(subscriptions, task_id){
                        self.send_to_some(payload, vec![peer_id]);
                    }    
                } 
            }

            ConnectionProcess::ReceivePeerCacheData(peer_id, task_id, subscriptions) => {
                debug!("Feedsub::connection_workflow | ReceivePeerCache {:?} {:?}", task_id, subscriptions);

                for (feed_id, addr) in &subscriptions {
                    if !self.my_listeners.contains(&addr) && !addr.is_empty() {
                        self.peers.subscribe_feed(&Some(peer_id), &addr, &feed_id);
                    } else {
                        trace!(
                            "Feedsub::on_connection_handler_event | discarding myself: {:?} {:?}",
                            addr, feed_id
                        );
                    }
                }

                self.task_manager
                    .outgoing_tasks
                    .remove_sent_task(&peer_id, &TaskId::Uuid(task_id));

            },

            ConnectionProcess::SendFeedTask(peer_id) => {
                debug!("Feedsub::connection_workflow | SendFeedTasks {:?}", peer_id);

                fn get_wanted_seqs<FI: FeedIdTrait, UF>(
                    feedsub: &Feedsub<FI, UF>,
                    my_inventory: &RangeSet,
                    remote_inventory: &RangeSet,
                    pending_active: &mut HashMap<FI, RangeSet>,
                    feed: &RwLockReadGuard<'_, Feed<FI>>,
                ) -> RangeSet {
                    debug!("Feedsub::connection_workflow | SendFeedTasks | get_wanted_seqs");
                    let default = RangeSet::new();
                    let feed_id = &feed.id;

                    let active_fetches = feedsub
                        .task_manager
                        .outgoing_tasks
                        .active_fetches
                        .get(&feed_id)
                        .unwrap_or(&default);

                    let mut my_inventory = my_inventory.clone();

                    let pending_tasks = pending_active
                        .get(feed_id)
                        .unwrap_or(&RangeSet::new())
                        .clone();

                    // We don't need inventory + active_tasks + pending_tasks
                    let dont_need = my_inventory
                        .insert_rangeset(active_fetches.clone())
                        .insert_rangeset(pending_tasks);

                        debug!(
                        "Feedsub::connection_workflow | SendFeedTasks | get_wanted_seqs | dont_need {:?}",
                        dont_need
                    );

                    let mut remote_inventory = remote_inventory.clone();

                    // we need remaining - ones we don't need
                    let i_need = remote_inventory.subtract(&dont_need);
                    debug!(
                        "Feedsub::connection_workflow | SendFeedTasks | get_wanted_seqs | i_need {:?}",
                        i_need
                    );

                    let limit = cmp::max((i_need.count() as f64 * 0.1).round() as u64, 1);
                    // let limit = 1;

                    debug!(
                        "Feedsub::connection_workflow | SendFeedTasks | get_wanted_seqs | asking for limit {:?}",
                        limit
                    );

                    let request = i_need.get_first(limit);
                    debug!(
                        "Feedsub::connection_workflow | SendFeedTasks | get_wanted_seqs | request {:?}",
                        request
                    );

                    request
                }

                fn send_tasks<FI: FeedIdTrait + 'static, UF: UpdateFetcher<FI, UF> + 'static>(
                    feedsub: &mut Feedsub<FI, UF>,
                    to_send: Vec<(PeerId, Multiaddr, FI, RangeSet)>,
                ) where
                    UF: std::marker::Send + Sync,
                {
                    debug!("Feedsub::connection_workflow | SendFeedTasks | send_tasks");
                    let codec = FeedsubCodec::<FI>::new(
                        feedsub.config.node_keys().clone(),
                        feedsub.config.protocol_version(),
                    );

                    for (peer_id, address, feed_id, wanted_seqs) in to_send {
                        //assign task

                        let task_type = TaskType::TaskFetchUpdatesBySeq(TaskFetchUpdatesBySeqData::new(
                            feed_id.clone(),
                            wanted_seqs,
                        ));

                        let task = Task::new(address.clone(), peer_id, task_type);

                        debug!(
                            "Feedsub::connection_workflow | SendFeedTasks | send_tasks | sending task {:?}",
                            task
                        );

                        feedsub
                            .task_manager
                            .outgoing_tasks
                            .insert_sent(task.clone());

                        // prepare and queue task for sending

                        if let Ok(payload) = codec.encode_task(&task){
                            feedsub.send_to_some(payload, vec![peer_id]);
                        }
                    }
                }

                // Collect a list of task data to send
                let mut to_send = Vec::new();
                // Collect a list of tasks that will be active so we don't duplicate
                let mut pending_active: HashMap<FI, RangeSet> = HashMap::new();

                let incomplete_feeds = &self.task_manager.outgoing_tasks.incomplete_feeds;

                debug!(
                    "Feedsub::connection_workflow | SendFeedTasks | incomplete feeds {:?}",
                    incomplete_feeds
                );

                if let Some(connection) = self.connections.get_active(&peer_id){

                    if !incomplete_feeds.is_empty() {
                        'connection_loop: for (feed_id, my_feed) in incomplete_feeds {
                            debug!(
                                "Feedsub::connection_workflow | SendFeedTasks | checking feed_id {:?}",
                                feed_id
                            );
    
                                debug!(
                                    "Feedsub::connection_workflow | SendFeedTasks | checking connection {:?}",
                                    connection
                                );
    
                                if !connection.peer_id.is_some() {
                                    trace!("Feedsub::connection_workflow | SendFeedTasks | connection has no peer_id, skipping");
                                    continue;
                                }
    
                                // only if the connection has finshed their work
                                let connection_has_no_tasks = self
                                    .task_manager
                                    .outgoing_tasks
                                    .sent_tasks
                                    .get_tasks_by_peer_id(&connection.peer_id.unwrap())
                                    .is_empty();
    
                                if connection_has_no_tasks {
                                    debug!(
                                        "Feedsub::connection_workflow | SendFeedTasks | No tasks for connection {:?}",
                                        connection.peer_id
                                    );
    
                                    let all_subs = connection.subscriptions.get_all();
    
                                    for conn_sub in &all_subs {
                                        let conn_sub = conn_sub.read().unwrap();
    
                                        if &conn_sub.id == feed_id {
                                            debug!("Feedsub::connection_workflow | SendFeedTasks | connection has this feed");
    
                                            let my_feed = my_feed.read().unwrap();
                                            if let (Some(my_feed_type), Some(remote_feed_type)) =
                                                (&my_feed.feed_type, &conn_sub.feed_type)
                                            {
                                                match (my_feed_type, remote_feed_type) {
                                                    (
                                                        FeedType::InfiniteSeq {
                                                            inventory: my_inventory,
                                                            ..
                                                        },
                                                        FeedType::InfiniteSeq {
                                                            inventory: remote_inventory,
                                                            ..
                                                        },
                                                    ) => {
                                                        debug!("Feedsub::connection_workflow | SendFeedTasks | FeedType::InfiniteSeq");
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | my_inventory {:?}",
                                                            my_inventory
                                                        );
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | remote_inventory {:?}",
                                                            remote_inventory
                                                        );
    
                                                        if my_inventory == remote_inventory {
                                                            continue;
                                                        }
    
                                                        debug!("Feedsub::connection_workflow | SendFeedTasks | FeedType::InfiniteSeq");
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | my_inventory {:?}",
                                                            my_inventory
                                                        );
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | remote_inventory {:?}",
                                                            remote_inventory
                                                        );
    
                                                        let wanted_seqs = get_wanted_seqs(
                                                            self,
                                                            my_inventory,
                                                            remote_inventory,
                                                            &mut pending_active,
                                                            &my_feed,
                                                        );
    
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | wanted_seqs {:?}",
                                                            wanted_seqs
                                                        );
    
                                                        if !wanted_seqs.is_empty()
                                                            && connection.peer_id.is_some()
                                                        {
                                                            debug!(
                                                                "Feedsub::connection_workflow | SendFeedTasks | start assigning tasks"
                                                            );
    
                                                            // add to pending_active so we don't regenerate dups
                                                            pending_active.insert(
                                                                my_feed.id.clone(),
                                                                wanted_seqs.clone(),
                                                            );
    
                                                            debug!(
                                                                "Feedsub::connection_workflow | SendFeedTasks | pending_active {:?}",
                                                                pending_active
                                                            );
    
                                                            let peer_id = connection.peer_id.unwrap();
                                                            let address = &connection.address;
    
                                                            to_send.push((
                                                                peer_id,
                                                                address.clone(),
                                                                my_feed.id.clone(),
                                                                wanted_seqs,
                                                            ));
                                                            debug!(
                                                                "Feedsub::connection_workflow | SendFeedTasks | to_send {:?}",
                                                                to_send
                                                            );
    
                                                            break 'connection_loop;
                                                        }
                                                    }
                                                    (
                                                        FeedType::FiniteSeq {
                                                            inventory: my_inventory,
                                                            ..
                                                        },
                                                        FeedType::FiniteSeq {
                                                            inventory: remote_inventory,
                                                            ..
                                                        },
                                                    ) => {
                                                        debug!("Feedsub::connection_workflow | SendFeedTasks | FeedType::InfiniteSeq");
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | my_inventory {:?}",
                                                            my_inventory
                                                        );
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | remote_inventory {:?}",
                                                            remote_inventory
                                                        );
    
                                                        let wanted_seqs = get_wanted_seqs(
                                                            self,
                                                            my_inventory,
                                                            remote_inventory,
                                                            &mut pending_active,
                                                            &my_feed,
                                                        );
    
                                                        debug!(
                                                            "Feedsub::connection_workflow | SendFeedTasks | wanted_seqs {:?}",
                                                            wanted_seqs
                                                        );
    
                                                        if !wanted_seqs.is_empty()
                                                            && connection.peer_id.is_some()
                                                        {
                                                            debug!(
                                                                "Feedsub::connection_workflow | SendFeedTasks | start assigning tasks"
                                                            );
    
                                                            // add to pending_active so we don't regenerate dups
                                                            pending_active
                                                                .entry(my_feed.id.clone())
                                                                .or_insert_with(|| RangeSet::new())
                                                                .insert_rangeset(wanted_seqs.clone());
    
                                                            debug!(
                                                                "Feedsub::connection_workflow | SendFeedTasks | pending_active {:?}",
                                                                pending_active
                                                            );
    
                                                            let peer_id = connection.peer_id.unwrap();
                                                            let address = &connection.address;
    
                                                            to_send.push((
                                                                peer_id,
                                                                address.clone(),
                                                                my_feed.id.clone(),
                                                                wanted_seqs,
                                                            ));
    
                                                            debug!(
                                                                "Feedsub::connection_workflow | SendFeedTasks | to_send {:?}",
                                                                to_send
                                                            );
                                                            break 'connection_loop;
                                                        }
                                                    }
                                                    _ => {}
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                
                    if !to_send.is_empty() {

                        // update connections' last_request time
                        if let Some(connection) = self.connections.get_active_mut(&peer_id){
                            connection.update_last_request();
}

                        send_tasks(self, to_send);
                    } 

                }
          

            },

            ConnectionProcess::ReceiveFeedTask(peer_id, data) => {
                debug!(
                    "Feedsub::connection_workflow | ReceiveFeedTasks {:?} {:?}",
                    peer_id, data.feed_id
                );
                trace!(
                    "Feedsub::connection_workflow | ReceiveFeedTasks {:?}",
                    data
                );

                let feed_id = &data.feed_id;
                if self.my_subscriptions.is_subscribed(feed_id) {
                    if let Some(connection) = self.connections.get_active(&peer_id) {
                        let task = Task::new(
                            connection.address.clone(),
                            peer_id,
                            TaskType::TaskFetchUpdatesBySeq(data),
                        );
                        // Add this task to our backlog
                        self.task_manager.incoming_tasks.insert(task.clone());

                        // update the last receive time on the connection
                        if let Some(connection) = self.connections.get_active_mut(&peer_id){
                            connection.update_last_receive();
                        }

                        self.events
                            .push_back(ToSwarm::GenerateEvent(FeedsubEvent::PeerTask { task }))
                    }
                } 
                // else {
                //     debug!("Feedsub::connection_workflow | ReceiveFeedTasks | don't subscribe {:?}", data);
                //     // we let the peer know we don't subscribe
                //     let codec = FeedsubCodec::<FI>::new(
                //         self.config.node_keys.public().clone(),
                //         self.config.protocol_version.to_string(),
                //     );

                //     let payload = codec.encode_unsubscribe(&vec![feed_id.clone()]);
                //     self.send_to_some(payload, vec![peer_id]);
                // }
            }

            ConnectionProcess::SendFeedUpdates(task) => {
                let Task { address, peer_id, request_time, task_type, ..} = &task;
                debug!("Feedsub::connection_workflow | SendFeedUpdates {:?} {:?} {:?} {:?}", address, peer_id, request_time, task_type);

                let node_keys = self.config.node_keys().clone();
                let protocol_version = self.config.protocol_version();
                let peer_id = task.peer_id;
                let data_to_send = task.updates;

                
                if let TaskType::TaskFetchUpdatesBySeq(update_task) = task.task_type {
                    trace!("Feedsub::handle_received_task | TaskType::TaskFetchUpdatesBySeq");
                    let feed_id = &update_task.feed_id;

                    debug!("Feedsub::connection_workflow | SendFeedUpdates {:?}", feed_id);

                    let subscription = self.my_subscriptions.get(&feed_id).unwrap().clone();
                    let codec = FeedsubCodec::<FI>::new(node_keys, protocol_version);

                    let mut updates_to_send = Vec::new();
                    for (seq, data, sig) in data_to_send {
                        if let Some(feed) = self.my_subscriptions.get_mut(feed_id) {
                            if let Some(update) = Update::new(feed, data, Some(seq), sig) {
                                updates_to_send.push(update);
                            }
                        }
                    } 

                    if !updates_to_send.is_empty(){
                        // Create and send payloads with the updates
                        let payloads = codec.encode_update(
                            Some(vec![subscription.clone()]),
                            Some(updates_to_send),
                            *self.config.target_payload_size(),
                        );

                        for payload in payloads {
                            self.send_to_some(payload, vec![peer_id]);
                        }
                    }
                }
            }

            ConnectionProcess::ReceiveFeedUpdates(peer_id, mut updates) => {
                debug!("Feedsub::connection_workflow | ReceiveFeedUpdates {:?}", peer_id);
                trace!("Feedsub::connection_workflow | ReceiveFeedUpdates {:?}", updates);

                // retain only if we subscribe to the feed
                updates.retain(|item| self.my_subscriptions.is_subscribed(&item.feed_id));
                debug!("Feedsub::connection_workflow | ReceiveFeedUpdates {:?}", updates.len());

                let mut updated_feeds: HashMap<FI, Arc<RwLock<Feed<FI>>>> = HashMap::new();
                let mut cacheable_updates = Vec::new();

                for update in &updates {
                    let seq = update.seq;
    
                    // Update my_subscriptions with this update?
                    let my_sub: &mut Arc<RwLock<Feed<FI>>> = self.my_subscriptions.get_or_new_mut(&update.feed_id);
    
                    if let Some(mut my_sub_guard) = my_sub.write().ok() {
                        let feed_id = my_sub_guard.id.clone();

                        // discard any updates that aren't properly signed
                        let public_key = feed_id.public_key();
                        if !public_key.verify(&update.data, &update.sig){
                            error!("Feedsub::connection_workflow | ReceiveFeedUpdates | failed signature validation {:?} | seq {:?} | sig {:?} | sig len {:?} | data len {:?}", update.feed_id, update.seq, update.sig, update.sig.len(), update.data.len());
                            break;
                        }

                        // update inventory
                        if let Some(feed_type) = &mut my_sub_guard.feed_type {
                            match feed_type {
                                FeedType::InfiniteSeq {
                                    ref mut inventory, ..
                                } => {
                                    if !inventory.contains(seq) {
                                        trace!("Feedsub::process_incoming_updates| my_subscriptions | added to inventory");

                                        inventory.insert_num(seq);
                                        updated_feeds.insert(feed_id.clone(), my_sub.clone());

                                        cacheable_updates.push(update.clone());

                                    } else {
                                        trace!("Feedsub::process_incoming_updates| my_subscriptions | already had it");
                                    }
                                }
                                FeedType::FiniteSeq {
                                    last_seq,
                                    inventory,
                                } => {
                                    if !inventory.contains(seq) {
                                        inventory.insert_num(seq);
                                        trace!("Feedsub::process_incoming_updates| my_subscriptions | added to inventory");
                                        
                                        cacheable_updates.push(update.clone());
                                        
                                        if seq == last_seq.to_u64() {
                                            trace!("Feedsub::process_incoming_updates| my_subscriptions | final task in FeedType::FiniteSeq, removing feed from incomplete feeds");
    
                                            self
                                                .task_manager
                                                .outgoing_tasks
                                                .remove_incomplete_feed(&feed_id);
                                        }
                                    } else {
                                        trace!("Feedsub::process_incoming_updates| my_subscriptions | already had it");
                                    }
                                }
                            }
                        } else {
                            debug!(
                                    "Feedsub::process_incoming_updates| my_subscriptions | No feed type {:?}",
                                    my_sub
                                );
                        }
                    }
    
                }

                self.connection_workflow(ConnectionProcess::CacheFeedUpdates(Some(peer_id), cacheable_updates));

                self.connection_workflow(ConnectionProcess::ProcessFeedTaskCompletion(peer_id, updates.clone()));

                // update connections' last_receive time
                if let Some(connection) = self.connections.get_active_mut(&peer_id){
                    connection.update_last_receive();
                }
               
                self.events.push_back({
                    ToSwarm::GenerateEvent(FeedsubEvent::PeerUpdate {
                        peer_id,
                        updates,
                    })
                })

            },

            ConnectionProcess::CacheFeedUpdates(peer_id, updates) => {
                debug!("Feedsub::connection_workflow | CacheFeedUpdates {:?}", peer_id);
                debug!("Feedsub::connection_workflow | CacheFeedUpdates | there are {:?} updates", updates.len());

                for update in &updates {
                    let Update {feed_id, seq, data, sig} = &update;
                    debug!("Feedsub::connection_workflow | CacheFeedUpdates | feed_id {:?} | seq {:?} data len {:?} sig len {:?}", feed_id, seq, data.len(), sig.len());
                }

                for update in updates {
                    let feed_id = &update.feed_id;

                    // add to task cache
                    if feed_id.is_cache_enabled(){
                        let work = ProcessorWorkType::Updates(vec![update.clone()]);
                        if let Err(e) = self.to_processor_send.try_send(work) {
                            error!(
                                "Feedsub::process_incoming_updates| my_subscriptions {:?}",
                                e
                            );
                        }
                    }
    
                    if let Some(peer_id) = peer_id {
                         // Give peer points for sending something useful
                        if let Some(connection) = self.connections.get_active(&peer_id){
                            self.peers.is_receive_update(&connection.address);
                        }
                    }
                }

            }

            ConnectionProcess::ProcessFeedTaskCompletion(peer_id, updates) => {
                debug!("Feedsub::connection_workflow | ProcessFeedTaskCompletion {:?}", peer_id);
                debug!("Feedsub::connection_workflow | ProcessFeedTaskCompletion | there are {:?} updates", updates.len());

                for update in &updates {
                    let Update {feed_id, seq, data, sig} = &update;
                    debug!("Feedsub::connection_workflow | ProcessFeedTaskCompletion | feed_id {:?} | seq {:?} data len {:?} sig len {:?}", feed_id, seq, data.len(), sig.len());
                }

                let mut sent_tasks_received = HashMap::new();

                for update in &updates {

                    let seq = update.seq;

                    let task = self
                        .task_manager
                        .outgoing_tasks
                        .sent_tasks
                        .get_task_mut(&peer_id, &TaskId::FeedId(update.feed_id.clone()));

                    if let Some(task) = task {
                        debug!("Feedsub::process_incoming_updates| sent_tasks | I'm waiting for it");

                        match &mut task.task_type {
                            TaskType::TaskFetchUpdatesBySeq(update_task) => {
                                // update requested_seqs
                                if update_task.requested_seqs.contains(seq) {
                                    update_task.requested_seqs.remove(seq);

                                    sent_tasks_received
                                        .entry(&update.feed_id)
                                        .or_insert_with(|| RangeSet::new())
                                        .insert_num(seq);

                                        debug!("Feedsub::process_incoming_updates| sent_tasks | sent_tasks_received | {:?}", sent_tasks_received);

                                        debug!("Feedsub::process_incoming_updates| sent_tasks | update_task | {:?}", update_task);

                                    // if there are no more seqs in the task, remove it
                                    if update_task.requested_seqs.is_empty() {
                                        self.task_manager.outgoing_tasks.sent_tasks.remove_task(
                                            &peer_id,
                                            &TaskId::FeedId(update.feed_id.clone()),
                                        );
                                    }
                                }
                            }
                            _ => {
                                error!("Feedsub::process_incoming_updates| sent_tasks | Incorrect TaskType for {:?}", task);
                            }
                        }
                    } 
        
                    if !sent_tasks_received.is_empty() {
                        for (feed_id, rangeset) in &sent_tasks_received {
                            self
                                .task_manager
                                .outgoing_tasks
                                .subtract_active_tasks(feed_id, rangeset.clone());
                        }

                        self.connection_workflow(ConnectionProcess::SendFeedTask(peer_id));

                    }
                }

            }

            ConnectionProcess::CheckIfWorkComplete => {
                debug!("Feedsub::connection_workflow | CheckIfWorkComplete");

                let mut to_handshake = Vec::new();
                let mut to_disconnect = Vec::new();
                let mut to_send_tasks = Vec::new();

                for connection in self.connections.get_all_active(){
                    if let Some(peer_id) = connection.peer_id{
                        let mut is_work_complete = true;
                        let mut has_tasks = false;


                        // If we've received nothing from this connection recently, then disconnect
                        if let Some(last_receive) = connection.last_receive {
                            let secs = self.config.disconnect_timeout();
                            debug!("Feedsub::connection_workflow | CheckIfWorkComplete {:?} | {:?} ", Utc::now().signed_duration_since(last_receive), Duration::seconds(*secs)) ;

                            if Utc::now().signed_duration_since(last_receive) > Duration::seconds(*secs) {
                                debug!("Feedsub::connection_workflow | CheckIfWorkComplete disconnecting ") ;
                              to_disconnect.push(peer_id);
                            }
                        } 

                        // check if any of the connection's subscriptions are greater or less than ours
                        for conn_sub in connection.subscriptions.get_all(){
                            
                            let seq_compare: Result<Ordering, String> = self.my_subscriptions.compare_seq(&conn_sub);
                            let inventory_compare = self.my_subscriptions.compare_inventory(&conn_sub);

                            let is_greater_or_less = [seq_compare, inventory_compare]
                            .iter()
                            .filter_map(|result| result.as_ref().ok()) // Filter out Ok values, ignore errors
                            .any(|&ordering| ordering == Ordering::Greater || ordering == Ordering::Less);
                    
                            if is_greater_or_less {
                                is_work_complete = false;
                            }
                            
                        }

                        // check for outstanding tasks
                        if self.task_manager.incoming_tasks.has_incoming_task(&peer_id) 
                        || self.task_manager.outgoing_tasks.has_outgoing_task(&peer_id){
                            is_work_complete = false;
                            has_tasks = true;
                        }

                        if is_work_complete {
                            debug!("Feedsub::connection_workflow | CheckIfWorkComplete | handshaking {:?}", peer_id);
                            to_handshake.push(peer_id);
                        } else {
                            if !has_tasks{
                                debug!("Feedsub::connection_workflow | CheckIfWorkComplete | sending feed tasks {:?}", peer_id);
                                to_send_tasks.push(peer_id)
                            } else {
                                debug!("Feedsub::connection_workflow | CheckIfWorkComplete | waiting on tasks {:?}", peer_id);

                            }
                        }
                    }
                }

                if !to_handshake.is_empty(){
                    for peer_id in to_handshake{
                        debug!("Feedsub::connection_workflow | CheckIfWorkComplete | handshaking {:?}", peer_id);
                        self.connection_workflow(ConnectionProcess::SendHandshake(peer_id));
                    }
                }

                if !to_disconnect.is_empty(){
                    for peer_id in to_disconnect{
                        debug!("Feedsub::connection_workflow | CheckIfWorkComplete | disconnecting {:?}", peer_id);
                        self.connection_workflow(ConnectionProcess::Disconnect(peer_id));
                    }
                }

                if !to_send_tasks.is_empty(){
                    for peer_id in to_send_tasks {
                        debug!("Feedsub::connection_workflow | CheckIfWorkComplete | sending feed tasks {:?}", peer_id);
                        self.connection_workflow(ConnectionProcess::SendFeedTask(peer_id));
                    }
                }
            },

            ConnectionProcess::CleanPeerCache => {
                debug!("Feedsub::connection_workflow | CleanPeerCache");
                self.peers.cleanup_peers();
            }

            ConnectionProcess::CleanTaskManager => {
                debug!("Feedsub::connection_workflow | CleanTaskManager");

                // Delete expired tasks so that they can be reassigned
                self.task_manager
                .outgoing_tasks
                .clear_expired_sent_tasks(30);
                
            }

            ConnectionProcess::Disconnect(peer_id) => {
                debug!("Feedsub::connection_workflow | Disconnect {:?}", peer_id);

                if let Some(connection) = self.connections.get_active_mut(&peer_id){
                    connection.update_status(ConnectionStatus::PendingDisconnect);
}

                self.connection_workflow(ConnectionProcess::SendDisconnect(peer_id));
                self.disconnect(&peer_id);
            },

            ConnectionProcess::SendDisconnect(peer_id) => {
                debug!("Feedsub::connection_workflow | SendDisconnect {:?}", peer_id);
        
                let codec = FeedsubCodec::<FI>::new(
                    self.config.node_keys().clone(),
                    self.config.protocol_version(),
                );
    
                // Send disconnect with some other peers
                let mut peers = self.peers.get_some(self.config.max_peers_to_send_on_overloaded());

                for addr in &self.my_listeners {
                    peers.push(Peer::new(None, addr.clone()));
                }

                if let Ok(payload) = codec.encode_disconnect(&peers){
                    debug!("Feedsub::connection_workflow | SendDisconnect peers {:?}", peers);

                    self.send_to_some(payload, vec![peer_id]);
                }
            },

        }
    }

    fn node_workflow(&mut self, item: NodeProcess<FI>){

        match item {
            NodeProcess::Subscribe(feeds) => {
                debug!("Feedsub::node_workflow | Subscribe {:?}", feeds);

                if !feeds.is_empty() {
                    self.node_workflow(NodeProcess::UpdateMySubscriptionsIfStale(feeds.clone()));

                    self.node_workflow(NodeProcess::RemoveFeedsFromPeerCacheUnsubscribed(feeds.clone()));

                    self.events
                        .push_back(ToSwarm::GenerateEvent(FeedsubEvent::SelfSubscribe {
                            feeds,
                    }));
                }
            },

            NodeProcess::RemoveFeedsFromPeerCacheUnsubscribed(feeds) => {
                debug!("Feedsub::node_workflow | RemoveFeedsFromPeerCacheUnsubscribed {:?}", feeds);
                for feed in &feeds {
                    self.peers.unsubscribed_feeds.delete(&feed.id);
                }

            },

            NodeProcess::UpdateMySubscriptionsIfStale(feeds) => {
                debug!("Feedsub::node_workflow | UpdateMySubscriptionsIfStale {:?}", feeds);

                let mut feed_ids = Vec::new();
                let mut wrapped_feeds = Vec::new();
        
                    for feed in feeds {
                        let feed_id = feed.id.clone();
                        feed_ids.push(feed_id.clone());
        
                        let wrapped_feed = Arc::new(RwLock::new(feed));
                        self.my_subscriptions.insert(wrapped_feed.clone());
                        wrapped_feeds.push(wrapped_feed.clone());
        
                        // Check if any current connections subscribe to this new feed and add to their subscriptions
                        let peer_addresses = self.peers.unsubscribed_feeds.get(&feed_id);
                        for addr in peer_addresses {
                            if self.connections.is_connected(&addr){
                                if let Some(peer_id) = self.connections.get_connected_peer_id(&addr){
                                    self.connection_workflow(ConnectionProcess::AddSubscribedFeedsToConnection(peer_id, vec![wrapped_feed.clone()]))
                                }
                            }
                        }
        
                    }

                    self.node_workflow(NodeProcess::AddSubscribedFeedsToTaskManager(wrapped_feeds.clone()));

                    self.node_workflow(NodeProcess::SendSubscribe(wrapped_feeds));                          
            },

            NodeProcess::AddSubscribedFeedsToTaskManager(feeds) => {
                debug!("Feedsub::node_workflow | AddSubscribedFeedsToTaskManager {:?}", feeds);

                self.task_manager
                    .outgoing_tasks
                    .insert_incomplete_feeds(feeds);
            },

            NodeProcess::SendSubscribe(feeds) => {
                debug!("Feedsub::node_workflow | SendSubscribe {:?}", feeds);
                let codec = FeedsubCodec::<FI>::new(
                    self.config.node_keys().clone(),
                    self.config.protocol_version(),
                );
    
                if let Ok(payload) = codec.encode_subscribe(feeds.clone()){
                    self.send_to_all(payload);
                }
            },
        }
    }

}

enum NodeProcess <FI: FeedIdTrait> {
    Subscribe(Vec<Feed<FI>>),
    UpdateMySubscriptionsIfStale(Vec<Feed<FI>>),
    RemoveFeedsFromPeerCacheUnsubscribed(Vec<Feed<FI>>),
    AddSubscribedFeedsToTaskManager(Vec<Arc<RwLock<Feed<FI>>>>),
    SendSubscribe(Vec<Arc<RwLock<Feed<FI>>>>),

    // Unsubscribe,
    // Delete from MySubscriptions
    // Add to PeerCache unsubscribed
    // Remove from TaskManager
    // Send Unsubscribed

}

enum ConnectionProcess <FI: FeedIdTrait>{
    DialPeers,
    CheckIfOverloaded(PeerId),
    PendingConnect(PeerId),
    Connect(PeerId),
    ReceiveDisconnect(PeerId, Vec<Peer<FI>>),

    SendHandshake(PeerId),
    ReceiveHandshake(Option<PeerId>, Multiaddr, Vec<Feed<FI>>, Vec<u8>),

    PeerSubscribe(PeerId, Multiaddr, Vec<Feed<FI>>),
    AddSubscribedFeedsToConnection(PeerId, Vec<Arc<RwLock<Feed<FI>>>>),
    UpdateMySubscriptionsIfStale(PeerId, Vec<Arc<RwLock<Feed<FI>>>>),
    AddSubscribedFeedsToPeerCache(PeerId, Multiaddr, Vec<Arc<RwLock<Feed<FI>>>>),
    AddUnsubscribedFeedsToPeerCache(PeerId, Multiaddr, Vec<Feed<FI>>),

    PeerUnsubscribe(PeerId, Multiaddr, Vec<FI>),
    RemoveSubscribedFeedsFromConnection(PeerId, Multiaddr, Vec<FI>),
    RemoveSubscribedFeedsFromMySubscriptions(PeerId, Multiaddr, Vec<FI>),
    RemoveSubscribedFeedsFromPeerCache(PeerId, Multiaddr, Vec<FI>),
    RemoveUnsubscribedFeedsFromPeerCache(PeerId, Multiaddr, Vec<FI>),
    
    SendPeerCacheTask(PeerId, Multiaddr),
    ReceivePeerCacheTask(PeerId, TaskFetchPeers<FI>),
    SendPeerCacheData(Task<FI>),
    ReceivePeerCacheData(PeerId, Uuid, Vec<(FI, Multiaddr)>),

    SendFeedTask(PeerId),
    ReceiveFeedTask(PeerId, TaskFetchUpdatesBySeqData<FI>),
    SendFeedUpdates(Task<FI>),

    ReceiveFeedUpdates(PeerId, Vec<Update<FI>>),
    CacheFeedUpdates(Option<PeerId>, Vec<Update<FI>>),
    ProcessFeedTaskCompletion(PeerId, Vec<Update<FI>>),   

    CheckIfWorkComplete,
    CleanPeerCache,
    CleanTaskManager,

    Disconnect(PeerId),
    SendDisconnect(PeerId),
}



impl<FI: FeedIdTrait + 'static, UF> NetworkBehaviour for Feedsub<FI, UF>
where
    FI: FeedIdTrait,
    UF: UpdateFetcher<FI, UF> + 'static + Debug + Send + Sync,
{
    type ConnectionHandler = FeedsubHandler<FI>;

    type ToSwarm = FeedsubEvent<FI>;

    fn handle_established_inbound_connection(
        &mut self,
        connection_id: ConnectionId,
        peer: PeerId,
        addr: &Multiaddr,
        remote_addr: &Multiaddr,
    ) -> Result<THandler<Self>, ConnectionDenied> {
        debug!(
            "Feedsub::handle_established_inbound_connection | {:?} {:?} {:?} {:?}",
            connection_id, peer, addr, remote_addr
        );

        debug!(
            "Feedsub::handle_established_inbound_connection | connections {:?}",
            self.connections.get_all_pending()
        );

        let connection = Connection::new()
            .update_status(ConnectionStatus::Connected)
            .update_direction(Direction::Incoming)
            .update_connection_id(&connection_id)
            .update_peer_id(&peer)
            .clone();

        self.connections.insert_active(connection);

        Ok(FeedsubHandler::new(self.config.clone(), peer))
    }

    fn handle_established_outbound_connection(
        &mut self,
        connection_id: libp2p::swarm::ConnectionId,
        peer: PeerId,
        addr: &Multiaddr,
        role_override: libp2p::core::Endpoint,
    ) -> Result<libp2p::swarm::THandler<Self>, libp2p::swarm::ConnectionDenied> {
        debug!(
            "NetworkBehaviour for Feedsub::handle_established_outbound_connection | {:?} {:?} {:?} {:?}",
            connection_id, peer, addr, role_override
        );

        let mut to_delete = Vec::new();

        if let Some(connection) = self.connections.get_pending(addr) {
            to_delete.push(addr);
            let mut connection = connection.clone();

            connection
                .update_address(&addr)
                .update_status(ConnectionStatus::Connected)
                .update_connection_id(&connection_id)
                .update_peer_id(&peer);

            self.connections.insert_active(connection.clone());
        } else {
            let connection = Connection::new()
                .update_address(&addr)
                .update_status(ConnectionStatus::Connected)
                .update_direction(Direction::Outgoing)
                .update_connection_id(&connection_id)
                .update_peer_id(&peer)
                .clone();
            self.connections.insert_active(connection);
        }

        for addr in to_delete {
            self.connections.remove_pending(addr);
        }

        Ok(FeedsubHandler::new(self.config.clone(), peer))
    }

    fn on_swarm_event(&mut self, event: libp2p::swarm::FromSwarm<Self::ConnectionHandler>) {
        match event {
            libp2p::swarm::FromSwarm::ConnectionEstablished(connection_established) => {
                debug!(
                    "Feedsub::on_swarm_event | ConnectionEstablished: {:?}",
                    connection_established
                );
                self.on_connection_established(connection_established)
            }
            libp2p::swarm::FromSwarm::ConnectionClosed(connection_closed) => {
                debug!("Feedsub::on_swarm_event | ConnectionClosed");
                self.on_connection_closed(connection_closed)
            }
            libp2p::swarm::FromSwarm::AddressChange(event) => {
                debug!("Feedsub::on_swarm_event | AddressChange {:?}", event)
            }
            libp2p::swarm::FromSwarm::DialFailure(event) => {
                debug!("Feedsub::on_swarm_event | DialFailure {:?}", event);

                match event.error {
                    libp2p::swarm::DialError::Transport(transport) => {
                        for (addr, ..) in transport {
                            self.connections.remove_pending(addr);
                        }
                    }
                    _ => {}
                };
            }
            libp2p::swarm::FromSwarm::ListenFailure(event) => {
                debug!("Feedsub::on_swarm_event | ListenFailure {:?}", event)
            }
            libp2p::swarm::FromSwarm::NewListener(event) => {
                debug!("Feedsub::on_swarm_event | NewListener {:?}", event)
            }
            libp2p::swarm::FromSwarm::NewListenAddr(event) => {
                debug!("Feedsub::on_swarm_event | NewListenAddr {:?}", event);
                let NewListenAddr { addr, .. } = event;
                if !addr.to_string().starts_with("/ip4/127.0.0.1/") {
                    self.my_listeners.push(addr.clone());
                }
            }
            libp2p::swarm::FromSwarm::ExpiredListenAddr(event) => {
                debug!("Feedsub::on_swarm_event | ExpiredListenAddr {:?}", event);
            }
            libp2p::swarm::FromSwarm::ListenerError(event) => {
                debug!("Feedsub::on_swarm_event | ListenerError {:?}", event)
            }
            libp2p::swarm::FromSwarm::ListenerClosed(event) => {
                debug!("Feedsub::on_swarm_event | ListenerClosed {:?}", event)
            }
            libp2p::swarm::FromSwarm::NewExternalAddrCandidate(event) => debug!(
                "Feedsub::on_swarm_event | NewExternalAddrCandidate {:?}",
                event
            ),
            libp2p::swarm::FromSwarm::ExternalAddrConfirmed(event) => debug!(
                "Feedsub::on_swarm_event | ExternalAddrConfirmed {:?}",
                event
            ),
            libp2p::swarm::FromSwarm::ExternalAddrExpired(event) => {
                debug!("Feedsub::on_swarm_event | ExternalAddrExpired {:?}", event)
            }
        }
    }

    fn on_connection_handler_event(
        &mut self,
        peer_id: PeerId,
        connection_id: libp2p::swarm::ConnectionId,
        event: libp2p::swarm::THandlerOutEvent<Self>,
    ) {
        debug!(
            "Feedsub::on_connection_handler_event | peer_id: {:?} {:?}",
            peer_id, connection_id
        );

        let ToBehaviourEvent(msg, signature) = event;


        fn validate_signature(
            public_key: &PublicKey,
            msg: &[u8],
            sig: &[u8],
        ) -> Result<bool, io::Error> {
            if sig.len() != 64 {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "Invalid signature length",
                ));
            }

            return Ok(public_key.verify(msg, sig));
        }


        let mut is_signature_validated = false;
        let mut has_public_key = false;

        let mut buf = Vec::new();
        if msg.encode(&mut buf).is_ok(){
            if let Some(connection) = self.connections.get_active(&peer_id){
                if let Some(public_key) = &connection.public_key{
                    has_public_key = true;
                    match validate_signature(public_key, &buf, &signature) {
                        Ok(true) => {
                            trace!("convert_payload_to_event | signature validation successful");
                            is_signature_validated = true;
                        }
                        Ok(false) => {
                            error!("convert_payload_to_event | signature validation failed");
                            return;
                        }
                        Err(e) => {
                            error!("convert_payload_to_event | {:?}", e);
                        }
                    }
                }
            }
        }


        let peer_message = match msg.msg_data {
            Some(MsgData::Subscribe(subscribe)) => {
                trace!("on_connection_handler_event | Subscribe");

                if is_signature_validated {
                    let feeds: Result<Vec<Feed<FI>>, _> = subscribe
                    .feeds
                    .into_iter()
                    .map(|f| Feed::convert_from_proto(f))
                    .collect();

                    match feeds {
                        Ok(feeds) => Ok(PeerMessage::PeerSubscribe { feeds }),
                        Err(e) => Err(e),
                    }
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Invalid signature",
                    ))
                }
            },
            Some(MsgData::Unsubscribe(unsubscribe)) => {
                debug!("on_connection_handler_event | Unsubscribe");

                if is_signature_validated {
                    let feed_ids: Result<Vec<FI>, _> = unsubscribe
                    .feed_ids
                    .into_iter()
                    .map(|f| serde_json::from_str::<FI>(&f))
                    .collect();

                    match feed_ids {
                        Ok(feed_ids) => Ok(PeerMessage::PeerUnsubscribe { feed_ids }),
                        Err(e) => Err(e.into()),
                    }
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Invalid signature",
                    ))
                }

  
            },
            Some(MsgData::Update(update)) => {
                debug!("on_connection_handler_event | Update");

                if is_signature_validated {
   
                    let updates: Vec<Update<FI>> = if update.updates.is_empty() {
                        Vec::new()
                    } else {
                        update
                            .updates
                            .into_iter()
                            .filter_map(|update_entry| {
                                let feed_id = match serde_json::from_str(&update_entry.feed_id) {
                                    Ok(feed_id) => feed_id,
                                    Err(e) => {
                                        error!("Failed to deserialize feed_id: {:?}", e);
                                        return None;
                                    }
                                };
    
                                let seq = update_entry.seq;
    
                                let data = update_entry.data;
                                let sig = update_entry.sig;
    
                                Some(Update { feed_id, seq, data, sig })
                            })
                            .collect()
                    };
    
                    Ok(PeerMessage::PeerUpdate { updates })
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Invalid signature",
                    ))
                }
            },
            Some(MsgData::Task(proto_task)) => {
                debug!("on_connection_handler_event | Task");

                if is_signature_validated {
   
                    match proto_task.task_data.unwrap() {
                        TaskData::TaskFetchUpdatesBySeq(update_task) => {
                            let result_id: Result<FI, _> = serde_json::from_str(&update_task.feed_id);
    
                            let requested_seqs = RangeSet::from(update_task.requested_seqs);
    
                            match result_id {
                                Ok(feed_id) => {
                                    let data = TaskFetchUpdatesBySeqData::new(feed_id, requested_seqs);
    
                                    Ok(PeerMessage::PeerTaskFetchUpdatesBySeqData { data })
                                }
                                Err(e) => Err(io::Error::new(io::ErrorKind::InvalidData, e)),
                            }
                        }
                        TaskData::TaskFetchPeers(peer_task) => {
                            let ProtoTaskFetchPeers { task_id, feed_ids } = peer_task;
    
                            let feed_ids: Vec<FI> = feed_ids
                                .into_iter()
                                .map(|id| serde_json::from_str::<FI>(&id).unwrap())
                                .collect();
    
                            let result_id = Uuid::from_slice(&task_id);
                            match result_id {
                                Ok(uuid) => {
                                    let data = TaskFetchPeers::new(uuid, feed_ids);
    
                                    Ok(PeerMessage::PeerTaskFetchPeers { data })
                                }
                                Err(e) => Err(io::Error::new(io::ErrorKind::InvalidData, e)),
                            }
                        }
                    }
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Invalid signature",
                    ))
                }
            }
            Some(MsgData::PeerCache(peer_cache)) => {
                trace!(
                    "on_connection_handler_event | PeerCache {:?}",
                    peer_cache
                );

                if is_signature_validated {

                    if peer_cache.task_id.len() == 16 {
                        let mut bytes = [0u8; 16];
                        bytes.copy_from_slice(&peer_cache.task_id);
                        let task_id = Uuid::from_bytes(bytes);

                        let mut subscriptions = Vec::new();

                        for subscription in &peer_cache.peer_subscriptions {
                            let ProtoPeerSubscription { feed_id, address } = subscription;
                            let result_feed_id: Result<FI, _> = serde_json::from_str(&feed_id);
                            let result_address: Result<Multiaddr, _> = address.parse();
                            match (result_feed_id, result_address) {
                                (Ok(feed_id), Ok(addr)) => subscriptions.push((feed_id, addr)),
                                _ => {
                                    error!(
                                        "convert_payload_to_event | MsgData::PeerCache subscription {:?}",
                                        subscription
                                    )
                                }
                            }
                        }

                        Ok(PeerMessage::PeerCache {
                            task_id,
                            subscriptions,
                        })
                    } else {
                        Err(io::Error::new(
                            io::ErrorKind::InvalidData,
                            "Unknown task_id variant",
                        ))
                    }
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Invalid signature",
                    ))
                }

            }
            Some(MsgData::Handshake(handshake)) => {
                debug!(
                    "on_connection_handler_event | Handshake {:?}",
                    handshake
                );

                if is_signature_validated || !has_public_key {
                    let ProtoHandshake {
                        peer,
                        feeds,
                        public_key,
                    } = handshake;
    
                    if let Some(ProtoPeer { peer_id, address }) = peer {
                        let peer_id = match peer_id {
                            Some(bytes) => {
                                if let Ok(peer_id) = PeerId::from_bytes(&bytes){
                                    Some(peer_id)
                                } else {
                                    None
                                }
                            }
                            None => None,
                        };
    
                        let address: Multiaddr = address.parse().unwrap();
    
                        if let Ok(feeds) = feeds
                            .into_iter()
                            .map(|f| Feed::convert_from_proto(f))
                            .collect()
                        {
                            Ok(PeerMessage::PeerHandshake {
                                peer_id,
                                address,
                                feeds,
                                public_key,
                            })
                        } else {
                            Err(io::Error::new(
                                io::ErrorKind::InvalidData,
                                "Unknown MessageData variant",
                            ))
                        }
                    } else {
                        Err(io::Error::new(
                            io::ErrorKind::InvalidData,
                            "Unknown ProtoPeer",
                        ))
                    }
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Invalid signature",
                    ))
                }

            }
            Some(MsgData::Peers(peers)) => {
                debug!("convert_payload_to_event | MsgData::Peers {:?}", peers);

                // we don't validate signatures here because this is received before the handshake
                let ProtoDisconnect { peers } = peers;

                if let Ok(peers) = peers
                    .into_iter()
                    .map(|p| Peer::convert_from_proto(p))
                    .collect()
                {
                    Ok(PeerMessage::PeerDisconnect { peers })
                } else {
                    error!("convert_payload_to_event | MsgData::Peers | invalid data");
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Unknown MessageData variant",
                    ))
                }
            },

            None => todo!(),
        };

        if let Ok(event) = peer_message {
            match event {
                PeerMessage::PeerSubscribe { feeds } => {
                    debug!(
                        "Feedsub::on_connection_handler_event | PeerSubscribe: {:?} {:?}",
                        peer_id, feeds
                    );
    
                    if let Some(connection) = self.connections.get_active(&peer_id){
                        let addr = &connection.address;
                        self.connection_workflow(ConnectionProcess::PeerSubscribe(peer_id, addr.clone(), feeds));
                    }
                }
    
                PeerMessage::PeerUnsubscribe { feed_ids } => {
                    if let Some(connection) = self.connections.get_active(&peer_id){
                        let addr = &connection.address;
                        self.connection_workflow(ConnectionProcess::PeerUnsubscribe(peer_id, addr.clone(), feed_ids));
                    }
                }
    
                PeerMessage::PeerUpdate { updates} => {
                    self.connection_workflow(ConnectionProcess::ReceiveFeedUpdates(peer_id, updates));},
                
                    PeerMessage::PeerCache { task_id, subscriptions} => {
                        self.connection_workflow(ConnectionProcess::ReceivePeerCacheData(peer_id, task_id, subscriptions));},
                
                        PeerMessage::PeerTaskFetchUpdatesBySeqData { data } => {
                    self.connection_workflow(ConnectionProcess::ReceiveFeedTask(peer_id, data));},
                
                    PeerMessage::PeerTaskFetchPeers { data } => {
                    self.connection_workflow(ConnectionProcess::ReceivePeerCacheTask(peer_id, data));
                }
    
                PeerMessage::PeerHandshake { peer_id, address, feeds , public_key} => {
                    self.connection_workflow(ConnectionProcess::ReceiveHandshake(peer_id, address, feeds, public_key));
                }
    
                PeerMessage::PeerDisconnect { peers } => {
                    self.connection_workflow(ConnectionProcess::ReceiveDisconnect(peer_id, peers));
                },

            }
                    
        }



}

    fn poll(
        &mut self,
        cx: &mut Context<'_>,
        _params: &mut impl PollParameters,
    ) -> Poll<libp2p::swarm::ToSwarm<Self::ToSwarm, libp2p::swarm::THandlerInEvent<Self>>> {
        let num_events = self.events.len();
        if num_events > 0 {
            debug!(
                "NetworkBehaviour for Feedsub::poll | unprocessed events {:?}",
                num_events
            );
        } else {
            trace!(
                "NetworkBehaviour for Feedsub::poll | unprocessed events {:?}",
                num_events
            );
        }

        let waker = Arc::new(Mutex::new(cx.waker().clone()));
        self.waker = Some(waker.clone());

        // handle events
        if let Some(event) = self.events.pop_front() {
            match &event{
                ToSwarm::GenerateEvent(ref event) => {
                    match event{
                        FeedsubEvent::DialPeer { addr } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::DialPeer {:?}", addr),
                        FeedsubEvent::DisconnectPeer { peer_id } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::DisconnectPeer {:?}", peer_id),
                        FeedsubEvent::PeerConnected { peer_id } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::PeerConnected {:?}", peer_id),
                        FeedsubEvent::PeerDisconnected { peer_id} => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::PeerDisconnected {:?}", peer_id),
                        FeedsubEvent::SelfSubscribe { feeds } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::SelfSubscribe {:?}", feeds),
                        FeedsubEvent::SelfUnsubscribe { feed_ids } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::SelfUnsubscribe {:?}", feed_ids),
                        FeedsubEvent::PeerSubscribe { peer_id, feeds } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::PeerSubscribe {:?} {:?}", peer_id, feeds),
                        FeedsubEvent::PeerUnsubscribe { peer_id, feed_ids } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::PeerUnsubscribe {:?} {:?}", peer_id, feed_ids),
                        FeedsubEvent::SelfUpdate { feed, update } => {
                            debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::SelfUpdate {:?} {:?}", feed, update.seq);
                            self.connection_workflow(ConnectionProcess::CacheFeedUpdates(None, vec![update.clone()]))        
                        },
                        FeedsubEvent::PeerUpdate { peer_id, updates } => {
                            debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::PeerUpdate | PeerId: {:?} Updates {:?}", peer_id, updates.len());
                        },
                        FeedsubEvent::SelfTask { task } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::SelfTask {:?}", task),
                        FeedsubEvent::PeerTask { task } => debug!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::PeerTask {:?}", task),
                        FeedsubEvent::Error { peer_id, error } => error!("NetworkBehaviour for Feedsub::poll | FeedsubEvent::Error {:?} {:?}", peer_id, error),
                    }
                },
                ToSwarm::Dial { ref opts } => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::Dial {:?}", opts),
                ToSwarm::ListenOn { ref opts } => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::ListenOn {:?}", opts),
                ToSwarm::RemoveListener { ref id } => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::RemoveListener {:?}", id),
                ToSwarm::NotifyHandler { ref peer_id, ref handler, ref event } => trace!("NetworkBehaviour for Feedsub::poll | ToSwarm::NotifyHandler {:?} {:?} {:?}", peer_id, handler, event),
                ToSwarm::NewExternalAddrCandidate(ref addr) => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::NewExternalAddrCandidate {:?}", addr),
                ToSwarm::ExternalAddrConfirmed(ref addr) => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::ExternalAddrConfirmed {:?}", addr),
                ToSwarm::ExternalAddrExpired(ref addr) => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::ExternalAddrExpired {:?}", addr),
                ToSwarm::CloseConnection { ref peer_id, .. } => debug!("NetworkBehaviour for Feedsub::poll | ToSwarm::CloseConnection {:?}", peer_id),
            };

            return Poll::Ready(event);
        }


        // Process my tasks
        if let Some(next_task) = self.task_manager.incoming_tasks.extract_next_task() {
            match next_task.task_type {
                TaskType::TaskFetchUpdatesBySeq(_) => {
                    let work = ProcessorWorkType::PendingTask((waker.clone(), next_task));
                    if let Err(e) = self.to_processor_send.try_send(work) {
                        error!("NetworkBehaviour for Feedsub::poll | task_send {:?}", e);
                    }
                }
                TaskType::TaskFetchPeers(_) => {
                    self.connection_workflow(ConnectionProcess::SendPeerCacheData(next_task))
                }
            }
        }

        while let Ok(work) = self.from_processor_recv.try_recv() {
            match work {
                ProcessorWorkType::CompletedTask(task) => {
                    let Task { address, peer_id, request_time, task_type, ..} = &task;
                    debug!("NetworkBehaviour for Feedsub::poll | CompletedTask | {:?} {:?} {:?} {:?}", address, peer_id, request_time, task_type);
                    self.connection_workflow(ConnectionProcess::SendFeedUpdates(task))
                },
                ProcessorWorkType::IncompleteTask(next_task) => {
                    let Task { address, peer_id, request_time, task_type, ..} = &next_task;
                    debug!("NetworkBehaviour for Feedsub::poll | IncompleteTask | {:?} {:?} {:?} {:?}", address, peer_id, request_time, task_type);
                    let work = ProcessorWorkType::PendingTask((waker.clone(), next_task));
                    if let Err(e) = self.to_processor_send.try_send(work) {
                        error!("NetworkBehaviour for Feedsub::poll | task_send {:?}", e);
                    }
                },
                _ => {
                    debug!(
                        "NetworkBehaviour for Feedsub::poll | update_recv {:?}",
                        work
                    );
                },
            }
        }

        // Trigger background maintenance process
        while let Poll::Ready(Some(())) = self.heartbeat.poll_next_unpin(cx) {
            self.heartbeat();
        }

        Poll::Pending
    }

    fn handle_pending_inbound_connection(
        &mut self,
        _connection_id: libp2p::swarm::ConnectionId,
        _local_addr: &Multiaddr,
        _remote_addr: &Multiaddr,
    ) -> Result<(), libp2p::swarm::ConnectionDenied> {
        debug!(
            "NetworkBehaviour for Feedsub:: handle_pending_inbound_connection | {:?}",
            self
        );

        Ok(())
    }

    fn handle_pending_outbound_connection(
        &mut self,
        _connection_id: libp2p::swarm::ConnectionId,
        _maybe_peer: Option<PeerId>,
        addresses: &[Multiaddr],
        _effective_role: libp2p::core::Endpoint,
    ) -> Result<Vec<Multiaddr>, libp2p::swarm::ConnectionDenied> {
        debug!(
            "NetworkBehaviour for Feedsub:: handle_pending_outbound_connection| {:?}",
            addresses
        );

        Ok(vec![])
    }
}

/// Event emitted by the `Feedsub` behaviour to the app via the swarm.
#[derive(Debug)]
pub enum FeedsubEvent<FI>
where
    FI: FeedIdTrait,
{
    /// Feedsub is asking your app to call swarm.dial for an addr (as it can't do it directly)
    DialPeer { addr: Multiaddr },
    /// Feedsub is asking your app to call swarm.disconnect_peer_id for a peerId (as it can't do it directly)
    DisconnectPeer { peer_id: PeerId },
    /// A new peer just connected
    PeerConnected { peer_id: PeerId },
    /// A peer just disconnected
    PeerDisconnected { peer_id: PeerId },
    /// This node's user subscribes to one or more feeds
    SelfSubscribe { feeds: Vec<Feed<FI>> },
    /// This node's user just unsubscribed from a feed
    SelfUnsubscribe { feed_ids: Vec<FI> },
    /// A peer subscribes to one or more feeds
    PeerSubscribe {
        peer_id: PeerId,
        feeds: Vec<Feed<FI>>,
    },
    /// A peer unsubscribes to one ore more feeds
    PeerUnsubscribe { peer_id: PeerId, feed_ids: Vec<FI> },
    /// This node's user sends updates on one or more feeds
    SelfUpdate { feed: Feed<FI>, update: Update<FI> },
    /// A peer sends updates on one or more feeds
    PeerUpdate {
        peer_id: PeerId,
        updates: Vec<Update<FI>>,
    },
    /// This node's user sends a task to retrieve updates
    SelfTask { task: Task<FI> },
    /// A peer sends a task to retrieve updates
    PeerTask { task: Task<FI> },
    /// Error while attempting to communicate with the remote.
    Error {
        peer_id: PeerId,
        /// The error that occurred.
        error: StreamUpgradeError<io::Error>,
    },
}

pub struct UpdateItem {
    pub data: Vec<u8>,
    pub sig: Vec<u8>
}

#[async_trait]
pub trait UpdateFetcher<FI, UF>
where
    FI: FeedIdTrait,
    UF: UpdateFetcher<FI, UF>,
{
    /// Fetches data by sequence number.
    ///
    /// - Parameters:
    ///   - feed_id: Identifier of the feed.
    ///   - seq: Sequence number.
    ///
    /// - Returns: A struct containing the data and the original author's signature

    async fn fetch_by_sequence_list(
        &self,
        feed_id: &FI,
        seq: &u64,
    ) -> UpdateItem;
}



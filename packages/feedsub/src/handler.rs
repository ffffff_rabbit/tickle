use crate::peers::Peer;
use crate::proto::{Payload, ProtoMessage};
use crate::protocol::{FeedsubCodec, FeedsubProtocol};

use crate::task_manager::tasks::{TaskFetchPeers, TaskFetchUpdatesBySeqData};
use crate::{proto, Feed, FeedIdTrait, FeedsubConfig, FeedsubError, Update};
use asynchronous_codec::Framed;
use futures::prelude::*;
use libp2p::swarm::{
    ConnectionHandler, ConnectionHandlerEvent, KeepAlive, Stream, SubstreamProtocol,
};
use libp2p::{Multiaddr, PeerId};
use uuid::Uuid;

use std::collections::VecDeque;
use std::fmt::Debug;
use std::time::Instant;
use std::{pin::Pin, task::Context, task::Poll};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// const STREAM_TIMEOUT: Duration = Duration::from_secs(60);
// const MAX_CONCURRENT_STREAMS_PER_CONNECTION: usize = 10;

pub struct FeedsubHandler<FI>
where
    FI: FeedIdTrait,
{
    config: FeedsubConfig<FI>,

    remote_peer_id: PeerId,

    send_queue: VecDeque<proto::Payload>,

    outbound_substream: Option<OutboundSubstreamState<FI>>,
    outbound_substream_establishing: bool,

    inbound_substream: Option<InboundSubstreamState<FI>>,
    last_io_activity: Instant,
}

/// State of the inbound substream, opened either by us or by the remote.
enum InboundSubstreamState<FI>
where
    FI: FeedIdTrait,
{
    /// Waiting for a message from the remote. The idle state for an inbound substream.
    WaitingInput(Framed<Stream, FeedsubCodec<FI>>),
    /// The substream is being closed.
    Closing(Framed<Stream, FeedsubCodec<FI>>),
    /// An error occurred during processing.
    Poisoned,
}

/// State of the outbound substream, opened either by us or by the remote.
enum OutboundSubstreamState<FI>
where
    FI: FeedIdTrait,
{
    /// Waiting for the user to send a message. The idle state for an outbound substream.
    WaitingOutput(Framed<Stream, FeedsubCodec<FI>>),
    /// Waiting to send a message to the remote.
    PendingSend(Framed<Stream, FeedsubCodec<FI>>, proto::Payload),
    /// Waiting to flush the substream so that the data arrives to the remote.
    PendingFlush(Framed<Stream, FeedsubCodec<FI>>),
    /// An error occurred during processing.
    Poisoned,
}

/// An event from `Behaviour` with the information requested by the `Handler`.
#[derive(Debug)]
pub enum FromBehaviourEvent {
    Message(Payload),
}

#[derive(Debug)]
pub struct ToBehaviourEvent(pub ProtoMessage, pub Vec<u8>);

pub enum PeerMessage<FI>
where
    FI: FeedIdTrait,
{
    /// A peer wishes to subscribe to feeds
    PeerSubscribe {
        feeds: Vec<Feed<FI>>,
    },
    /// A peer wishes to unsubscribe to feeds
    PeerUnsubscribe {
        feed_ids: Vec<FI>,
    },
    /// A peer sends updates on one or more feeds
    PeerUpdate {
        updates: Vec<Update<FI>>,
    },
    PeerTaskFetchUpdatesBySeqData {
        data: TaskFetchUpdatesBySeqData<FI>,
    },
    PeerCache {
        task_id: Uuid,
        subscriptions: Vec<(FI, Multiaddr)>,
    },
    PeerTaskFetchPeers {
        data: TaskFetchPeers<FI>,
    },
    PeerHandshake {
        peer_id: Option<PeerId>,
        address: Multiaddr,
        feeds: Vec<Feed<FI>>,
        public_key: Vec<u8>,
    },
    PeerDisconnect {
        peers: Vec<Peer<FI>>,
    },
}

impl<FI: Send> FeedsubHandler<FI>
where
    FI: FeedIdTrait,
{
    /// Creates a new `FeedsubHandler`.

    pub fn new(config: FeedsubConfig<FI>, remote_peer_id: PeerId) -> Self {
        Self {
            config,
            remote_peer_id,

            send_queue: VecDeque::new(),

            inbound_substream: None,
            outbound_substream: None,

            outbound_substream_establishing: false,

            last_io_activity: Instant::now(),
        }
    }
}

impl<FI: 'static + Send + Debug> ConnectionHandler for FeedsubHandler<FI>
where
    FI: FeedIdTrait,
{
    type FromBehaviour = FromBehaviourEvent;

    type ToBehaviour = ToBehaviourEvent;

    type Error = FeedsubError;

    type InboundProtocol = FeedsubProtocol<FI>;

    type OutboundProtocol = FeedsubProtocol<FI>;

    type InboundOpenInfo = ();

    type OutboundOpenInfo = ();

    fn listen_protocol(&self) -> SubstreamProtocol<Self::InboundProtocol, Self::InboundOpenInfo> {
        trace!(
            "ConnectionHandler for FeedsubHandler::listen_protocol | {:?}",
            self.remote_peer_id
        );

        let node_keys = self.config.node_keys().clone();
        let protocol_version = self.config.protocol_version();

        SubstreamProtocol::new(FeedsubProtocol::new(node_keys, protocol_version), ())
    }

    fn connection_keep_alive(&self) -> KeepAlive {
        trace!("ConnectionHandler for FeedsubHandler::connection_keep_alive | Incomplete",);
        KeepAlive::Yes
    }

    #[allow(deprecated)]
    fn poll(
        &mut self,
        cx: &mut Context<'_>,
    ) -> Poll<
        ConnectionHandlerEvent<
            Self::OutboundProtocol,
            Self::OutboundOpenInfo,
            Self::ToBehaviour,
            Self::Error,
        >,
    > {
        trace!(
            "ConnectionHandler for FeedsubHandler::poll | {:?}",
            self.remote_peer_id
        );

        // determine if we need to create the outbound stream
        if self.outbound_substream.is_none() && !self.outbound_substream_establishing {
            trace!("ConnectionHandler for FeedsubHandler::poll | Create outbound substream",);
            self.outbound_substream_establishing = true;
            return Poll::Ready(ConnectionHandlerEvent::OutboundSubstreamRequest {
                protocol: self.listen_protocol().clone(),
            });
        }

        loop {
            match std::mem::replace(
                &mut self.inbound_substream,
                Some(InboundSubstreamState::Poisoned),
            ) {
                // inbound idle state
                Some(InboundSubstreamState::WaitingInput(mut substream)) => {
                    trace!(
                        "ConnectionHandler for FeedsubHandler::poll | InboundSubstreamState::WaitingInput {:?}",
                        substream
                    );
                    match substream.poll_next_unpin(cx) {
                        Poll::Ready(Some(Ok(message))) => {
                            self.last_io_activity = Instant::now();
                            self.inbound_substream =
                                Some(InboundSubstreamState::WaitingInput(substream));
                            debug!(
                                    "ConnectionHandler for FeedsubHandler::poll | InboundSubstreamState::NotifyBehaviour"
                                );
                            return Poll::Ready(ConnectionHandlerEvent::NotifyBehaviour(message));
                        }
                        Poll::Ready(Some(Err(error))) => {
                            error!("Failed to read from inbound stream: {:?}", error);
                            // Close this side of the stream. If the
                            // peer is still around, they will re-establish their
                            // outbound stream i.e. our inbound stream.
                            self.inbound_substream =
                                Some(InboundSubstreamState::Closing(substream));
                        }
                        // peer closed the stream
                        Poll::Ready(None) => {
                            debug!("ConnectionHandler for FeedsubHandler::poll | Inbound stream closed by remote");
                            self.inbound_substream =
                                Some(InboundSubstreamState::Closing(substream));
                        }
                        Poll::Pending => {
                            self.inbound_substream =
                                Some(InboundSubstreamState::WaitingInput(substream));
                            break;
                        }
                    }
                }
                Some(InboundSubstreamState::Closing(mut substream)) => {
                    debug!(
                        "ConnectionHandler for FeedsubHandler::poll | InboundSubstreamState::Closing {:?}",
                        substream
                    );
                    match Sink::poll_close(Pin::new(&mut substream), cx) {
                        Poll::Ready(res) => {
                            if let Err(e) = res {
                                // Don't close the connection but just drop the inbound substream.
                                // In case the remote has more to send, they will open up a new
                                // substream.
                                error!("Inbound substream error while closing: {:?}", e);
                            }
                            self.inbound_substream = None;
                            break;
                        }
                        Poll::Pending => {
                            self.inbound_substream =
                                Some(InboundSubstreamState::Closing(substream));
                            break;
                        }
                    }
                }
                None => {
                    debug!("ConnectionHandler for FeedsubHandler::poll | InboundSubstream is None");
                    self.inbound_substream = None;
                    break;
                }
                Some(InboundSubstreamState::Poisoned) => {
                    error!("Error occurred during inbound stream processing")
                }
            }
        }

        // process the outbound stream
        loop {
            match std::mem::replace(
                &mut self.outbound_substream,
                Some(OutboundSubstreamState::Poisoned),
            ) {
                // outbound idle state
                Some(OutboundSubstreamState::WaitingOutput(substream)) => {
                    trace!(
                        "ConnectionHandler for FeedsubHandler::poll | OutboundSubstreamState::WaitingInput"
                    );

                    if let Some(message) = self.send_queue.pop_front() {
                        self.send_queue.shrink_to_fit();

                        debug!(
                            "ConnectionHandler for FeedsubHandler::poll | send_queue remaining {:?}",
                            self.send_queue.len()
                        );

                        self.outbound_substream =
                            Some(OutboundSubstreamState::PendingSend(substream, message));
                        continue;
                    }

                    self.outbound_substream =
                        Some(OutboundSubstreamState::WaitingOutput(substream));
                    break;
                }
                Some(OutboundSubstreamState::PendingSend(mut substream, message)) => {
                    debug!(
                        "ConnectionHandler for FeedsubHandler::poll | OutboundSubstreamState::PendingSend",
                    );
                    match Sink::poll_ready(Pin::new(&mut substream), cx) {
                        Poll::Ready(Ok(())) => {
                            match Sink::start_send(Pin::new(&mut substream), message) {
                                Ok(()) => {
                                    self.outbound_substream =
                                        Some(OutboundSubstreamState::PendingFlush(substream))
                                }
                                Err(e) => {
                                    error!("Failed to send message on outbound stream: {:?}", e);
                                    self.outbound_substream = None;
                                    break;
                                }
                            }
                        }
                        Poll::Ready(Err(e)) => {
                            error!("Failed to send message on outbound stream: {e}");
                            self.outbound_substream = None;
                            break;
                        }
                        Poll::Pending => {
                            self.outbound_substream =
                                Some(OutboundSubstreamState::PendingSend(substream, message));
                            break;
                        }
                    }
                }
                Some(OutboundSubstreamState::PendingFlush(mut substream)) => {
                    trace!(
                        "ConnectionHandler for FeedsubHandler::poll | OutboundSubstreamState::PendingFlush"
                    );

                    match Sink::poll_flush(Pin::new(&mut substream), cx) {
                        Poll::Ready(Ok(())) => {
                            debug!(
                                "ConnectionHandler for FeedsubHandler::poll | OutboundSubstreamState::PendingFlush OK"
                            );
                            self.last_io_activity = Instant::now();
                            self.outbound_substream =
                                Some(OutboundSubstreamState::WaitingOutput(substream));
                            break;
                        }
                        Poll::Ready(Err(e)) => {
                            error!("Failed to flush outbound stream: {e}");
                            self.outbound_substream = None;
                            break;
                        }
                        Poll::Pending => {
                            self.outbound_substream =
                                Some(OutboundSubstreamState::PendingFlush(substream));
                            break;
                        }
                    }
                }
                None => {
                    trace!(
                        "ConnectionHandler for FeedsubHandler::poll | OutboundSubstream is None"
                    );

                    self.outbound_substream = None;
                    break;
                }
                Some(OutboundSubstreamState::Poisoned) => {
                    warn!("Error occurred during outbound stream processing")
                }
            }
        }

        Poll::Pending
    }

    fn on_behaviour_event(&mut self, event: Self::FromBehaviour) {
        trace!(
            "ConnectionHandler for FeedsubHandler::on_behaviour_event | {:?}",
            event
        );

        match event {
            FromBehaviourEvent::Message(message) => self.send_queue.push_back(message),
        }
    }

    fn on_connection_event(
        &mut self,
        event: libp2p::swarm::handler::ConnectionEvent<
            Self::InboundProtocol,
            Self::OutboundProtocol,
            Self::InboundOpenInfo,
            Self::OutboundOpenInfo,
        >,
    ) {
        trace!(
            "ConnectionHandler for FeedsubHandler::on_connection_event | {:?} {:?}",
            self.remote_peer_id,
            event
        );
        match event {
            libp2p::swarm::handler::ConnectionEvent::FullyNegotiatedInbound(inbound, ..) => {
                trace!(
                "ConnectionHandler for FeedsubHandler::on_connection_event | FullyNegotiatedInbound"
            );

                let substream = inbound.protocol;
                self.inbound_substream = Some(InboundSubstreamState::WaitingInput(substream));
            }
            libp2p::swarm::handler::ConnectionEvent::FullyNegotiatedOutbound(outbound, ..) => {
                trace!(
                "ConnectionHandler for FeedsubHandler::on_connection_event | FullyNegotiatedOutbound"
            );
                let substream = outbound.protocol;
                self.outbound_substream = Some(OutboundSubstreamState::WaitingOutput(substream));
            }
            libp2p::swarm::handler::ConnectionEvent::AddressChange(event) => warn!(
                "ConnectionHandler for FeedsubHandler::on_connection_event | AddressChange {:?}",
                event
            ),
            libp2p::swarm::handler::ConnectionEvent::DialUpgradeError(event) => warn!(
                "ConnectionHandler for FeedsubHandler::on_connection_event | AddressChange {:?}",
                event
            ),
            libp2p::swarm::handler::ConnectionEvent::ListenUpgradeError(event) => warn!(
                "ConnectionHandler for FeedsubHandler::on_connection_event | ListenUpgradeError {:?}",
                event
            ),
            libp2p::swarm::handler::ConnectionEvent::LocalProtocolsChange(change) => {
                trace!(
                    "ConnectionHandler for FeedsubHandler::on_connection_event | LocalProtocolsChange {:?}",
                    change
                );
            }
            libp2p::swarm::handler::ConnectionEvent::RemoteProtocolsChange(event) => warn!(
                "ConnectionHandler for FeedsubHandler::on_connection_event | RemoteProtocolsChange {:?}",
                event
            ),
        }
    }
}

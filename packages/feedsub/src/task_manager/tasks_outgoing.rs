use chrono::{Duration, Utc};
use libp2p::PeerId;
use std::collections::HashMap;
use std::sync::{Arc, RwLock};

use crate::task_manager::tasks::{Task, TaskFetchUpdatesBySeqData, TaskId, TaskType};
use crate::{range_set::RangeSet, Feed, FeedIdTrait};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use super::tasks::TaskFetchPeers;

#[derive(Debug)]
pub struct OutgoingTasks<FI: FeedIdTrait> {
    pub incomplete_feeds: HashMap<FI, Arc<RwLock<Feed<FI>>>>,
    pub active_fetches: HashMap<FI, RangeSet>,
    pub sent_tasks: SentTasks<FI>,
}

impl<FI: FeedIdTrait> OutgoingTasks<FI> {
    pub fn new() -> Self {
        Self {
            incomplete_feeds: HashMap::new(),
            active_fetches: HashMap::new(),
            sent_tasks: SentTasks::new(),
        }
    }

    pub fn is_active(&self, feed_id: &FI, set: &RangeSet) -> bool {
        let mut result = false;

        for num in Into::<Vec<u64>>::into(set.clone()) {
            if let Some(rangeset) = self.active_fetches.get(feed_id) {
                if rangeset.contains(num) {
                    result = true;
                    break;
                }
            }
        }

        result
    }

    pub fn add_active_tasks(&mut self, feed_id: &FI, new_active: RangeSet) -> &mut Self {
        let current_active = self
            .active_fetches
            .entry(feed_id.clone())
            .or_insert_with(|| RangeSet::new());

        trace!(
            "OutgoingTasks::add_active_tasks | {:?} | {:?}",
            current_active,
            new_active
        );

        current_active.insert_rangeset(new_active);

        trace!(
            "OutgoingTasks::add_active_tasks final | {:?} {:?}",
            feed_id,
            current_active
        );

        self
    }

    pub fn subtract_active_tasks(&mut self, feed_id: &FI, old: RangeSet) -> &mut Self {
        let current_active = self
            .active_fetches
            .entry(feed_id.clone())
            .or_insert_with(|| RangeSet::from("0"));

        trace!(
            "OutgoingTasks::subtract_active_tasks | {:?} | {:?}",
            current_active,
            old
        );

        current_active.subtract(&old);

        trace!(
            "OutgoingTasks::subtract_active_tasks final | {:?} {:?}",
            feed_id,
            current_active
        );

        self
    }

    pub fn insert_incomplete_feeds(&mut self, feeds: Vec<Arc<RwLock<Feed<FI>>>>) {
        for feed in feeds {
            let feed_id = &feed.read().unwrap().id;
            self.incomplete_feeds.insert(feed_id.clone(), feed.clone());
        }
    }

    pub fn remove_incomplete_feed(&mut self, feed_id: &FI) {
        self.incomplete_feeds.remove(feed_id);
    }

    pub fn insert_sent(&mut self, task: Task<FI>) {
        trace!("OutgoingTasks::insert_sent | {:?} | {:?}", task, self);

        match &task.task_type {
            TaskType::TaskFetchUpdatesBySeq(seq_task) => {
                let TaskFetchUpdatesBySeqData {
                    feed_id,
                    requested_seqs,
                } = seq_task;

                self.add_active_tasks(&feed_id, requested_seqs.clone());

                self.sent_tasks.insert_task(task);
            }
            TaskType::TaskFetchPeers(_) => {
                self.sent_tasks.insert_task(task);
            }
        }
    }

    pub fn remove_sent_for_peer(&mut self, peer_id: &PeerId) {
        trace!(
            "OutgoingTasks::remove_sent_for_addr | {:?} | {:?}",
            peer_id,
            self
        );
        let dead_tasks = self.sent_tasks.get_tasks_by_peer_id(peer_id);

        for task in dead_tasks {
            match task.task_type {
                TaskType::TaskFetchUpdatesBySeq(seq_task) => {
                    let TaskFetchUpdatesBySeqData {
                        feed_id,
                        requested_seqs,
                    } = seq_task;

                    // remove from sent
                    self.sent_tasks
                        .remove_task(peer_id, &TaskId::FeedId(feed_id.clone()));

                    // remove from active
                    self.active_fetches
                        .get_mut(&feed_id)
                        .and_then(|set| Some(set.subtract(&requested_seqs)));
                }
                TaskType::TaskFetchPeers(peer_task) => {
                    let TaskFetchPeers { task_id, .. } = peer_task;
                    // remove from sent
                    self.sent_tasks.remove_task(peer_id, &TaskId::Uuid(task_id));
                }
            }
        }
    }

    pub fn has_outgoing_task(&self, peer_id: &PeerId) -> bool {
        let has_task = self.sent_tasks.get_tasks_by_peer_id(peer_id);
        debug!(
            "OutgoingTasks::has_outgoing_task {:?} {:?}",
            !has_task.is_empty(),
            peer_id
        );
        !has_task.is_empty()
    }

    pub fn remove_sent_task(&mut self, peer_id: &PeerId, task_id: &TaskId<FI>) {
        trace!(
            "OutgoingTasks::remove_sent_task | {:?} | {:?}",
            peer_id,
            task_id
        );

        let task = self
            .sent_tasks
            .0
            .get(peer_id)
            .and_then(|hashmap| hashmap.get(task_id))
            .cloned();

        if let Some(task) = task {
            match task.task_type {
                TaskType::TaskFetchUpdatesBySeq(seq_task) => {
                    let TaskFetchUpdatesBySeqData {
                        feed_id,
                        requested_seqs,
                    } = seq_task;

                    // remove from sent
                    self.sent_tasks
                        .remove_task(peer_id, &TaskId::FeedId(feed_id.clone()));

                    // remove from active
                    self.active_fetches
                        .get_mut(&feed_id)
                        .and_then(|set| Some(set.subtract(&requested_seqs)));
                }
                TaskType::TaskFetchPeers(peer_task) => {
                    let TaskFetchPeers { task_id, .. } = peer_task;
                    // remove from sent
                    self.sent_tasks.remove_task(peer_id, &TaskId::Uuid(task_id));
                }
            }
        }
    }

    pub fn clear_all_for_peer(&mut self, peer_id: &PeerId) {
        trace!(
            "OutgoingTasks::clear_all_for_addr | {:?} | {:?}",
            peer_id,
            self
        );

        self.remove_sent_for_peer(peer_id);
    }

    pub fn clear_all_for_feed_ids(&mut self, feed_ids: &Vec<FI>) {
        for feed_id in feed_ids {
            self.incomplete_feeds.remove(feed_id);
            self.active_fetches.remove(feed_id);
            self.sent_tasks.remove_task_by_feed_id(feed_id);
        }
    }

    pub fn clear_expired_sent_tasks(&mut self, secs: i64) {
        trace!(
            "OutgoingTasks::clear_expired_sent_tasks | {:?} ago | {:?}",
            secs,
            self
        );

        let expired_tasks = self.sent_tasks.get_expired_tasks(secs);
        for (addr, feed_id) in expired_tasks {
            self.remove_sent_task(&addr, &feed_id)
        }
    }
}

#[derive(Debug)]
pub struct SentTasks<FI: FeedIdTrait>(HashMap<PeerId, HashMap<TaskId<FI>, Task<FI>>>);

impl<FI: FeedIdTrait> SentTasks<FI> {
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    fn insert_task(&mut self, task: Task<FI>) {
        let peer_id = task.peer_id.clone();

        match &task.task_type {
            TaskType::TaskFetchUpdatesBySeq(seq_task) => {
                let TaskFetchUpdatesBySeqData { feed_id, .. } = seq_task;

                self.0
                    .entry(peer_id)
                    .or_insert_with(HashMap::new)
                    .insert(TaskId::FeedId(feed_id.clone()), task.clone());
            }
            TaskType::TaskFetchPeers(peer_task) => {
                let TaskFetchPeers { task_id, .. } = peer_task;

                self.0
                    .entry(peer_id)
                    .or_insert_with(HashMap::new)
                    .insert(TaskId::Uuid(task_id.clone()), task.clone());
            }
        }
    }

    pub fn get_tasks_by_peer_id(&self, peer_id: &PeerId) -> Vec<Task<FI>> {
        trace!("SentTasks::get_tasks_by_addr | {:?} | {:?}", peer_id, self);
        self.0
            .get(peer_id)
            .map_or_else(Vec::new, |tasks| tasks.values().cloned().collect())
    }

    pub fn get_task_mut(
        &mut self,
        peer_id: &PeerId,
        task_id: &TaskId<FI>,
    ) -> Option<&mut Task<FI>> {
        trace!(
            "SentTasks::get_task_mut | {:?} {:?} | {:?}",
            peer_id,
            task_id,
            self
        );

        self.0
            .get_mut(peer_id)
            .and_then(|feed_map| feed_map.get_mut(task_id))
    }

    pub fn get_expired_tasks(&mut self, secs: i64) -> Vec<(PeerId, TaskId<FI>)> {
        trace!(
            "SentTasks::get_expired_tasks older than {:?} secs | {:?}",
            secs,
            self
        );

        let expired_time = Utc::now() - Duration::seconds(secs);

        let mut expired_tasks = Vec::new();

        for (addr, task_map) in &self.0 {
            for (task_id, task) in task_map {
                if task.request_time <= expired_time {
                    expired_tasks.push((addr.clone(), task_id.clone()));
                }
            }
        }

        if !expired_tasks.is_empty() {
            debug!("SentTasks::get_expired_tasks {:?}", expired_tasks);
        }

        expired_tasks
    }

    pub fn remove_task(&mut self, peer_id: &PeerId, task_id: &TaskId<FI>) -> Option<Task<FI>> {
        trace!(
            "SentTasks::remove | {:?} {:?} | {:?}",
            peer_id,
            task_id,
            self
        );

        // Remove the task from the tasks HashMap
        let removed_task = self
            .0
            .get_mut(peer_id)
            .and_then(|feed_map| feed_map.remove(task_id));

        // If the task was removed, also remove any queue entries and cancels
        if removed_task.is_some() {
            // if no more tasks, then remove the addr
            if let Some(feed_map) = self.0.get(peer_id) {
                if feed_map.is_empty() {
                    self.0.remove(peer_id);
                }
            }
        }
        trace!("SentTasks::remove | removed {:?}", removed_task);
        trace!("SentTasks::remove | final {:?}", self);

        removed_task
    }

    pub fn remove_task_by_feed_id(&mut self, feed_id: &FI) {
        trace!(
            "SentTasks::remove_task_by_feed_id | {:?} | {:?}",
            feed_id,
            self
        );

        for (_, map) in &mut self.0 {
            map.remove(&TaskId::FeedId(feed_id.clone()));
        }
    }
}

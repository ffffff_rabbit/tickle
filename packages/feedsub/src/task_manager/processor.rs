use std::collections::BTreeMap;
use std::sync::{Arc, Mutex};
use std::task::Waker;

use crate::behavior::UpdateItem;
use crate::{behavior::UpdateFetcher, FeedIdTrait, Update};

use super::tasks::Task;
use crate::task_manager::tasks::TaskType::TaskFetchUpdatesBySeq;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use tokio::sync::mpsc::{Receiver, Sender};

#[derive(Debug, Clone)]
pub enum ProcessorWorkType<FI: FeedIdTrait> {
    PendingTask((Arc<Mutex<Waker>>, Task<FI>)),
    CompletedTask(Task<FI>),
    IncompleteTask(Task<FI>),
    Updates(Vec<Update<FI>>),
}

pub struct TaskProcessor<FI, UF>
where
    FI: FeedIdTrait,
    UF: UpdateFetcher<FI, UF>,
{
    from_behavior: Receiver<ProcessorWorkType<FI>>,
    to_behavior: Sender<ProcessorWorkType<FI>>,
    update_fetcher: Arc<UF>,
    updates_cache: Mutex<BTreeMap<(FI, u64), Update<FI>>>,
    target_bytes_to_send: usize,
}

impl<FI, UF> TaskProcessor<FI, UF>
where
    FI: FeedIdTrait,
    UF: UpdateFetcher<FI, UF>,
{
    pub fn new(
        from_behavior: Receiver<ProcessorWorkType<FI>>,
        to_behavior: Sender<ProcessorWorkType<FI>>,
        update_fetcher: UF,
        target_bytes_to_send: usize,
    ) -> Self {
        Self {
            from_behavior,
            to_behavior,
            update_fetcher: Arc::new(update_fetcher),
            updates_cache: Mutex::new(BTreeMap::new()),
            target_bytes_to_send,
        }
    }

    async fn process_task(&mut self, mut task: Task<FI>) {
        trace!("TaskProcessor::process_task {:?}", task);

        if let TaskFetchUpdatesBySeq(task_data) = &task.task_type {
            let feed_id = &task_data.feed_id;
            let current_seqs: Vec<u64> = task_data.requested_seqs.clone().into();

            debug!(
                "TaskProcessor::process_task | requested_seqs {:?}",
                task_data.requested_seqs
            );

            let mut processed_seqs = Vec::new();
            let mut total_size = 0usize;
            let mut updates = Vec::new();

            for seq in &current_seqs {
                // Try first from the fetch_cache_by_seqs
                let (data, sig) = self.fetch_cache_by_seqs(feed_id, seq);

                if !data.is_empty() && !sig.is_empty() {
                    processed_seqs.push(seq);
                    total_size += data.len();
                    updates.push((seq.clone(), data, sig));
                } else {
                    let UpdateItem { data, sig } = self
                        .update_fetcher
                        .clone()
                        .fetch_by_sequence_list(feed_id, seq)
                        .await;

                    debug!(
                            "TaskProcessor::process_task | update_fetcher | found data len {:?} | sig len {:?}",
                            data.len(), sig.len()
                        );

                    if !data.is_empty() && !sig.is_empty() {
                        processed_seqs.push(seq);
                        total_size += data.len();
                        updates.push((*seq, data, sig));
                    } else {
                        error!(
                            "TaskProcessor::process_task | failed lookup | data len {:?} | sig len {:?}",
                            data.len(), sig.len()
                        );
                    }
                }
                if total_size >= self.target_bytes_to_send {
                    break;
                }
            }

            if !updates.is_empty() {
                // check if the task is incomplete
                if processed_seqs.len() != current_seqs.len() {
                    // we have unprocessed seqs
                    let unfinished_seqs: Vec<u64> = current_seqs
                        .iter()
                        .filter(|seq| !processed_seqs.contains(seq))
                        .cloned()
                        .collect();

                    debug!(
                        "TaskProcessor::process_task | unfinished seqs {:?}",
                        unfinished_seqs
                    );

                    let mut unfinished_task = task.clone();

                    if let TaskFetchUpdatesBySeq(ref mut task_data) = unfinished_task.task_type {
                        task_data.requested_seqs = unfinished_seqs.into();
                    }

                    // reinsert task
                    let work = ProcessorWorkType::IncompleteTask(unfinished_task.clone());

                    if let Err(e) = self.to_behavior.send(work).await {
                        error!("TaskProcessor::process_task {:?}", e);
                    }
                }

                task.updates = updates;

                let work = ProcessorWorkType::CompletedTask(task);

                if let Err(e) = self.to_behavior.send(work).await {
                    error!("TaskProcessor::process_task {:?}", e);
                }
            }
        }
    }

    /// returns (data, sig)
    fn fetch_cache_by_seqs(&self, feed_id: &FI, seq: &u64) -> (Vec<u8>, Vec<u8>) {
        debug!(
            "TaskProcessor::fetch_cache_by_seqs | {:?} {:?}",
            seq, feed_id
        );

        if let Ok(cache) = self.updates_cache.lock() {
            let key = (feed_id.clone(), seq.clone());
            match cache.get(&key) {
                Some(sub) => {
                    let data = sub.data.clone();
                    let sig = sub.sig.clone();
                    return (data, sig);
                }
                None => return (Vec::new(), Vec::new()),
            }
        } else {
            return (Vec::new(), Vec::new());
        }
    }

    pub async fn insert_cache(&mut self, update: Update<FI>) {
        debug!("TaskProcessor::insert_cache | {:?}", update);

        let feed_id = update.feed_id.clone();
        let seq = update.seq.clone();
        let key = (feed_id, seq);

        if let Ok(mut cache) = self.updates_cache.lock() {
            cache.insert(key, update);
            trace!("TaskProcessor::insert_cache | cache {:?}", cache);
        };
    }

    pub async fn run(mut self) {
        loop {
            while let Some(work) = self.from_behavior.recv().await {
                match work {
                    ProcessorWorkType::PendingTask((waker, task)) => {
                        debug!("TaskProcessor::run | PendingTask {:?}", task);
                        self.process_task(task).await;

                        if let Ok(waker_guard) = waker.lock() {
                            waker_guard.wake_by_ref();
                        };
                    }
                    ProcessorWorkType::Updates(updates) => {
                        debug!("TaskProcessor::run | Updates {:?}", updates);
                        for update in updates {
                            self.insert_cache(update).await;
                        }
                        debug!("TaskProcessor::run | Cache final {:?}", self.updates_cache);
                    }
                    _ => {}
                }

                tokio::task::yield_now().await;
            }
        }
    }
}

// let feed_id = &update_task.feed_id;
// let mut seqs: Vec<u64> = update_task.requested_seqs.clone().into();
// let mut accumulated_updates = Vec::new();
// let mut data_size = 0;

// let is_subscribed = self.my_subscriptions.is_subscribed(feed_id);

// if is_subscribed {
//     let codec = FeedsubCodec::<FI>::new(peer_keys_public, protocol_version);
//     let subscription = self.my_subscriptions.get(&feed_id).unwrap().clone();

//     while !seqs.is_empty() && data_size < self.config.max_payload_size {
//         let current_seqs = seqs.drain(..1).collect::<Vec<_>>(); // Process one sequence at a time

//         warn!(
//             "Feedsub::handle_received_task | current seqs {:?}",
//             current_seqs
//         );

//         // let updates = block_on(async {
//         //     self.update_fetcher
//         //         .clone()
//         //         .fetch_by_sequence_list(self, &feed_id, current_seqs)
//         //         .await
//         // });

//         // if let Some(mut updates) = updates {
//         //     for update in updates.iter() {
//         //         data_size += update.data.len();
//         //     }
//         //     accumulated_updates.append(&mut updates);
//         // } else {
//         //     error!(
//         //         "Feedsub::handle_received_task | Found no updates for {:?}",
//         //         task
//         //     );
//         //     return Err(task.clone());
//         // }

//         // if data_size > self.config.max_payload_size {
//         //     break; // Break if the data size exceeds the limit
//         // }
//     }

//     // Create and send payloads with the accumulated updates
//     let payloads = codec.encode_update(
//         Some(vec![subscription.clone()]),
//         Some(accumulated_updates),
//         self.config.max_payload_size,
//     );

//     for payload in payloads {
//         self.send_to_some(payload, vec![peer_id]);
//     }

//     if !seqs.is_empty() {
//         // Update the task with the remaining sequences and reinsert into the queue
//         warn!(
//             "Feedsub::handle_received_task | Returning task to queue {:?}",
//             task
//         );
//         if let TaskType::TaskFetchUpdatesBySeq(ref mut update_task) = task.task_type
//         {
//             update_task.requested_seqs = RangeSet::from(seqs);
//         }
//         self.task_manager.incoming_tasks.insert(task);
//     }

// } else {
//     // we let the peer know we don't subscribe
//     let codec = FeedsubCodec::<FI>::new(
//         self.config.peer_keys.public().clone(),
//         self.config.protocol_version.to_string(),
//     );

//     let payload = codec.encode_unsubscribe(&vec![feed_id.clone()]);
//     self.send_to_some(payload, vec![peer_id]);

//     debug!(
//         "Feedsub::handle_received_task | My subscription wasn't found {:?}",
//         task
//     );

// }

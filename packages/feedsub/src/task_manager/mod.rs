pub mod processor;
pub mod tasks;
pub mod tasks_incoming;
pub mod tasks_outgoing;

use libp2p::PeerId;

use crate::FeedIdTrait;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use self::{tasks_incoming::IncomingTasks, tasks_outgoing::OutgoingTasks};

#[derive(Debug)]
pub struct TaskManager<FI: FeedIdTrait> {
    pub incoming_tasks: IncomingTasks<FI>,
    pub outgoing_tasks: OutgoingTasks<FI>,
}

impl<FI: FeedIdTrait> TaskManager<FI> {
    pub fn new() -> Self {
        Self {
            incoming_tasks: IncomingTasks::new(),
            outgoing_tasks: OutgoingTasks::new(),
        }
    }

    pub fn clear_all_for_peer(&mut self, peer_id: &PeerId) {
        self.incoming_tasks.clear_all_for_peer(peer_id);
        self.outgoing_tasks.clear_all_for_peer(peer_id);

        trace!("TaskManager::clear_all_for_addr | final {:?}", self);
    }

    pub fn clear_all_for_feed_ids(&mut self, feed_ids: &Vec<FI>) {
        self.incoming_tasks.clear_all_for_feed_ids(feed_ids);
        self.outgoing_tasks.clear_all_for_feed_ids(feed_ids);

        trace!("TaskManager::clear_all_for_addr | final {:?}", self);
    }
}

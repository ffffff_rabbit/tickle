use libp2p::PeerId;
use std::collections::{HashMap, HashSet, VecDeque};

use crate::task_manager::tasks::{
    Task, TaskFetchPeers, TaskFetchUpdatesBySeqData, TaskPriority, TaskType,
};
use crate::{range_set::RangeSet, FeedIdTrait};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use super::tasks::TaskId;

#[derive(Debug)]
pub struct IncomingTasks<FI: FeedIdTrait> {
    tasks: HashMap<PeerId, HashMap<TaskId<FI>, Task<FI>>>,
    active_tasks: HashMap<FI, RangeSet>,
    high_priority_queue: VecDeque<(PeerId, TaskId<FI>)>,
    low_priority_queue: VecDeque<(PeerId, TaskId<FI>)>,
    cancels: HashMap<PeerId, HashSet<TaskId<FI>>>,
}

impl<FI: FeedIdTrait> IncomingTasks<FI> {
    pub fn new() -> Self {
        Self {
            tasks: HashMap::new(),
            active_tasks: HashMap::new(),
            high_priority_queue: VecDeque::new(),
            low_priority_queue: VecDeque::new(),
            cancels: HashMap::new(),
        }
    }

    pub fn is_active(&self, feed_id: &FI, set: &RangeSet) -> bool {
        let mut result = false;

        for num in Into::<Vec<u64>>::into(set.clone()) {
            if let Some(rangeset) = self.active_tasks.get(feed_id) {
                if rangeset.contains(num) {
                    result = true;
                    break;
                }
            }
        }

        result
    }

    pub fn add_active_tasks(&mut self, feed_id: &FI, new_active: RangeSet) -> &mut Self {
        let current_active = self
            .active_tasks
            .entry(feed_id.clone())
            .or_insert_with(|| RangeSet::new());

        trace!(
            "IncomingTasks::add_active_tasks | {:?} | {:?}",
            current_active,
            new_active
        );

        current_active.insert_rangeset(new_active);

        trace!(
            "IncomingTasks::add_active_tasks final | {:?} {:?}",
            feed_id,
            current_active
        );

        self
    }

    pub fn subtract_active_tasks(&mut self, feed_id: &FI, old: RangeSet) -> &mut Self {
        let current_active = self
            .active_tasks
            .entry(feed_id.clone())
            .or_insert_with(|| RangeSet::new());

        trace!(
            "IncomingTasks::subtract_active_tasks | {:?} | {:?}",
            current_active,
            old
        );

        current_active.subtract(&old);

        trace!(
            "IncomingTasks::subtract_active_tasks final | {:?} {:?}",
            feed_id,
            current_active
        );

        self
    }

    pub fn insert(&mut self, task: Task<FI>) {
        trace!("IncomingTasks::insert | {:?} | {:?}", task, self);

        let peer_id = task.peer_id.clone();
        match &task.task_type {
            TaskType::TaskFetchUpdatesBySeq(seq_task) => {
                let TaskFetchUpdatesBySeqData {
                    feed_id,
                    requested_seqs,
                } = seq_task;

                self.add_active_tasks(&feed_id, requested_seqs.clone());

                let task_id = TaskId::FeedId(feed_id.clone());

                // Insert task into the tasks HashMap
                let task_ref = self
                    .tasks
                    .entry(peer_id.clone())
                    .or_insert_with(HashMap::new)
                    .entry(task_id.clone())
                    .or_insert(task.clone());

                // Insert task reference into the appropriate queue
                match task_ref.priority {
                    TaskPriority::High => self.high_priority_queue.push_back((peer_id, task_id)),
                    TaskPriority::Low => self.low_priority_queue.push_back((peer_id, task_id)),
                };
            }
            TaskType::TaskFetchPeers(peer_task) => {
                trace!("IncomingTasks::insert TaskFetchPeers | {:?}", peer_task);

                let TaskFetchPeers { task_id, .. } = peer_task;

                let task_id = TaskId::Uuid(task_id.clone());

                let task_ref = self
                    .tasks
                    .entry(peer_id.clone())
                    .or_insert_with(HashMap::new)
                    .entry(task_id.clone())
                    .or_insert(task.clone());

                // Insert task reference into the appropriate queue
                match task_ref.priority {
                    TaskPriority::High => self.high_priority_queue.push_back((peer_id, task_id)),
                    TaskPriority::Low => self.low_priority_queue.push_back((peer_id, task_id)),
                };
            }
        }
    }

    pub fn get_task_list(&self, peer_id: &PeerId) -> Vec<Task<FI>> {
        trace!("IncomingTasks::get_task_list | {:?} | {:?}", peer_id, self);

        if let Some(results) = self.tasks.get(peer_id) {
            results.values().cloned().collect()
        } else {
            Vec::new()
        }
    }

    pub fn get_task_mut(
        &mut self,
        peer_id: &PeerId,
        task_id: &TaskId<FI>,
    ) -> Option<&mut Task<FI>> {
        trace!(
            "IncomingTasks::get_task_mut | {:?} {:?} | {:?}",
            peer_id,
            task_id,
            self
        );

        self.tasks
            .get_mut(peer_id)
            .and_then(|feed_map| feed_map.get_mut(task_id))
    }

    pub fn get_tasks_mut(
        &mut self,
        task_id: &TaskId<FI>,
    ) -> Option<HashMap<PeerId, &mut Task<FI>>> {
        let mut results: HashMap<PeerId, &mut Task<FI>> = HashMap::new();

        for (peer_id, task_map) in &mut self.tasks {
            if let Some(task) = task_map.get_mut(task_id) {
                results.insert(peer_id.clone(), task);
            }
        }

        if results.is_empty() {
            None
        } else {
            Some(results)
        }
    }

    pub fn extract_next_task(&mut self) -> Option<Task<FI>> {
        trace!("IncomingTasks::extract_next_task {:?} ", self);

        let next_task_info = self.find_next_task_info();

        if let Some((peer_id, feed_id)) = next_task_info {
            if let Some(tasks_map) = self.tasks.get_mut(&peer_id) {
                let task = tasks_map.remove(&feed_id);

                // Check if there are no more tasks for the given Multiaddr and remove it if empty
                if tasks_map.is_empty() {
                    self.tasks.remove(&peer_id);
                }

                trace!("IncomingTasks::extract_next_task | extracted {:?} ", task);

                task
            } else {
                None
            }
        } else {
            None
        }
    }

    fn find_next_task_info(&mut self) -> Option<(PeerId, TaskId<FI>)> {
        while let Some((peer_id, task_id)) = self.high_priority_queue.pop_front() {
            if !self.is_task_canceled(&peer_id, &task_id) {
                return Some((peer_id, task_id));
            }
        }

        while let Some((peer_id, task_id)) = self.low_priority_queue.pop_front() {
            if !self.is_task_canceled(&peer_id, &task_id) {
                return Some((peer_id, task_id));
            }
        }

        None
    }

    fn is_task_canceled(&self, peer_id: &PeerId, task_id: &TaskId<FI>) -> bool {
        self.cancels
            .get(peer_id)
            .map_or(false, |tasks| tasks.contains(task_id))
    }

    pub fn remove_task(&mut self, peer_id: &PeerId, task_id: &TaskId<FI>) -> Option<Task<FI>> {
        trace!(
            "IncomingTasks::remove | {:?} {:?} | {:?}",
            peer_id,
            task_id,
            self
        );

        // Remove the task from the tasks HashMap
        let removed_task = self
            .tasks
            .get_mut(peer_id)
            .and_then(|task_map| task_map.remove(task_id));

        // If the task was removed, also remove any queue entries and cancels
        if removed_task.is_some() {
            self.remove_task_from_queue(peer_id, task_id);
            self.remove_task_from_cancels(peer_id, task_id);

            // if no more tasks, then remove the addr
            if let Some(feed_map) = self.tasks.get(peer_id) {
                if feed_map.is_empty() {
                    self.tasks.remove(peer_id);
                }
            }
        }
        trace!("IncomingTasks::remove | removed {:?}", removed_task);
        trace!("IncomingTasks::remove | final {:?}", self);

        removed_task
    }

    fn remove_task_from_queue(&mut self, peer_id: &PeerId, task_id: &TaskId<FI>) {
        self.high_priority_queue
            .retain(|(a, t)| a != peer_id || t != task_id);
        self.low_priority_queue
            .retain(|(a, t)| a != peer_id || t != task_id);
    }

    fn remove_task_from_cancels(&mut self, peer_id: &PeerId, task_id: &TaskId<FI>) {
        if let Some(tasks) = self.cancels.get_mut(peer_id) {
            tasks.remove(task_id);
            if tasks.is_empty() {
                self.cancels.remove(peer_id);
            }
        }
    }

    pub fn has_incoming_task(&self, peer_id: &PeerId) -> bool {
        let has_task = self.tasks.contains_key(peer_id);
        debug!(
            "IncomingTasks::has_incoming_task {:?} {:?}",
            has_task, peer_id
        );
        has_task
    }

    pub fn clear_all_for_peer(&mut self, peer_id: &PeerId) {
        trace!(
            "IncomingTasks::clear_all_for_addr {:?} | {:?}",
            peer_id,
            self
        );

        self.tasks.remove(peer_id);

        // Remove all entries for the addr from the high_priority_queue
        self.high_priority_queue.retain(|(a, _)| a != peer_id);

        // Remove all entries for the addr from the low_priority_queue
        self.low_priority_queue.retain(|(a, _)| a != peer_id);

        // Remove the addr from the cancels HashMap
        self.cancels.remove(peer_id);
    }

    pub fn clear_all_for_feed_ids(&mut self, feed_ids: &Vec<FI>) {
        trace!(
            "IncomingTasks::clear_all_for_feed_ids {:?} | {:?}",
            feed_ids,
            self
        );

        for feed_id in feed_ids {
            for (_, map) in &mut self.tasks {
                map.remove(&TaskId::FeedId(feed_id.clone()));
            }

            self.active_tasks.remove(feed_id);

            self.high_priority_queue
                .retain(|(_, task_id)| task_id != &TaskId::FeedId(feed_id.clone()));

            self.low_priority_queue
                .retain(|(_, task_id)| task_id != &TaskId::FeedId(feed_id.clone()));

            for (_, set) in &mut self.cancels {
                set.remove(&TaskId::FeedId(feed_id.clone()));
            }
        }
    }
}

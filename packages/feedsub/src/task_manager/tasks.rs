use chrono::{DateTime, Utc};
use libp2p::{Multiaddr, PeerId};
use uuid::Uuid;

use crate::{range_set::RangeSet, FeedIdTrait};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Clone, Eq, Hash, PartialEq)]
pub enum TaskId<FI: FeedIdTrait> {
    FeedId(FI),
    Uuid(Uuid),
}

#[derive(Debug, Clone)]
pub enum TaskPriority {
    Low,
    High,
}

#[derive(Debug, Clone)]
pub enum TaskType<FI: FeedIdTrait> {
    TaskFetchUpdatesBySeq(TaskFetchUpdatesBySeqData<FI>),
    TaskFetchPeers(TaskFetchPeers<FI>),
}

#[derive(Debug, Clone)]
pub struct TaskFetchUpdatesBySeqData<FI: FeedIdTrait> {
    pub feed_id: FI,
    pub requested_seqs: RangeSet,
}

impl<FI: FeedIdTrait> TaskFetchUpdatesBySeqData<FI> {
    pub fn new(feed_id: FI, requested_seqs: RangeSet) -> Self {
        Self {
            feed_id,
            requested_seqs,
        }
    }
}

#[derive(Debug, Clone)]
pub struct TaskFetchPeers<FI: FeedIdTrait> {
    pub task_id: Uuid,
    pub feed_ids: Vec<FI>,
}

impl<FI: FeedIdTrait> TaskFetchPeers<FI> {
    pub fn new(task_id: Uuid, feed_ids: Vec<FI>) -> Self {
        Self { task_id, feed_ids }
    }
}

#[derive(Debug, Clone)]
pub struct Task<FI>
where
    FI: FeedIdTrait,
{
    pub address: Multiaddr,
    pub peer_id: PeerId,
    pub request_time: DateTime<Utc>,
    pub priority: TaskPriority,
    pub task_type: TaskType<FI>,
    /// (seq, data, sig)
    pub updates: Vec<(u64, Vec<u8>, Vec<u8>)>,
}

impl<FI: FeedIdTrait> Task<FI> {
    pub fn new(address: Multiaddr, peer_id: PeerId, task_type: TaskType<FI>) -> Self {
        Self {
            address,
            peer_id,
            request_time: Utc::now(),
            priority: TaskPriority::Low,
            task_type,
            updates: Vec::new(),
        }
    }

    pub fn update_priority(&mut self, priority: TaskPriority) -> &mut Self {
        self.priority = priority;
        self
    }
}

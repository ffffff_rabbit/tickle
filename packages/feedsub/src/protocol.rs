use asynchronous_codec::{BytesMut, Decoder, Encoder, Framed};
use futures::prelude::*;
use libp2p::bytes::{Buf, BufMut};
use libp2p::core::upgrade::{InboundUpgrade, OutboundUpgrade, UpgradeInfo};
use libp2p::identity::PublicKey;
use libp2p::{identity, Multiaddr, PeerId};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use prost::Message;
use serde::{Deserialize, Serialize};
use serde_json;
use uuid::Uuid;

use std::fmt::Debug;
use std::io::ErrorKind;
use std::sync::{Arc, RwLock};
use std::{fmt, io, iter, pin::Pin};

use crate::feeds::{Feed, Update};
use crate::handler::ToBehaviourEvent;
use crate::peers::{Peer, PeerCache};
use crate::proto::proto_message::MsgData;
use crate::proto::proto_task::TaskData;
use crate::proto::{
    Payload, ProtoFeed, ProtoHandshake, ProtoMessage, ProtoPeer, ProtoSubscribe, ProtoTask,
    ProtoTaskFetchPeers, ProtoTaskFetchUpdatesbySeq, ProtoUnsubscribe, ProtoUpdate,
    ProtoUpdateItem,
};
use crate::task_manager::tasks::{Task, TaskFetchPeers, TaskType};
use crate::{proto, FeedIdTrait};

#[derive(Debug, Clone)]
pub struct FeedsubProtocol<FI: Serialize + Debug + Send + FeedIdTrait> {
    node_keys: identity::Keypair,
    protocol_version: String,
    _fi_marker: std::marker::PhantomData<FI>,
}

impl<FI> FeedsubProtocol<FI>
where
    FI: Serialize + Debug + Send + FeedIdTrait,
{
    pub fn new(node_keys: identity::Keypair, protocol_version: String) -> Self {
        Self {
            node_keys,
            protocol_version,
            _fi_marker: std::marker::PhantomData,
        }
    }
}

impl<FI> UpgradeInfo for FeedsubProtocol<FI>
where
    FI: Serialize + Debug + Send + FeedIdTrait,
{
    type Info = &'static str;

    type InfoIter = iter::Once<Self::Info>;

    fn protocol_info(&self) -> Self::InfoIter {
        trace!("UpgradeInfo for FeedsubProtocol | protocol_info");

        iter::once("/feedsub/0.1.0")
    }
}

impl<TSocket, FI> InboundUpgrade<TSocket> for FeedsubProtocol<FI>
where
    TSocket: AsyncRead + AsyncWrite + Unpin + Send + 'static,
    FI: Debug + Send + 'static + FeedIdTrait,
{
    type Output = Framed<TSocket, FeedsubCodec<FI>>;

    type Error = io::Error;

    type Future = Pin<Box<dyn Future<Output = Result<Self::Output, Self::Error>> + Send>>;

    fn upgrade_inbound(self, socket: TSocket, _: Self::Info) -> Self::Future {
        trace!("InboundUpgrade for FeedsubProtocol | upgrade_inbound");

        Box::pin(future::ok(Framed::new(
            socket,
            FeedsubCodec::new(self.node_keys, self.protocol_version),
        )))
    }
}

impl<TSocket, FI> OutboundUpgrade<TSocket> for FeedsubProtocol<FI>
where
    TSocket: AsyncRead + AsyncWrite + Unpin + Send + 'static,
    FI: Debug + Send + 'static + FeedIdTrait,
{
    type Output = Framed<TSocket, FeedsubCodec<FI>>;

    type Error = io::Error;

    type Future = Pin<Box<dyn Future<Output = Result<Self::Output, Self::Error>> + Send>>;

    fn upgrade_outbound(self, socket: TSocket, _: Self::Info) -> Self::Future {
        trace!("OutboundUpgrade for FeedsubProtocol | upgrade_outbound");

        Box::pin(future::ok(Framed::new(
            socket,
            FeedsubCodec::new(self.node_keys, self.protocol_version),
        )))
    }
}

pub struct FeedsubCodec<FI: Serialize + Debug + Send + FeedIdTrait> {
    node_keys: identity::Keypair,
    protocol_version: String,
    _fi_marker: std::marker::PhantomData<FI>,
}

impl<FI> FeedsubCodec<FI>
where
    FI: FeedIdTrait,
{
    pub fn new(node_keys: identity::Keypair, protocol_version: String) -> Self {
        FeedsubCodec {
            node_keys,
            protocol_version,
            _fi_marker: std::marker::PhantomData,
        }
    }

    fn create_and_sign_payload(&self, message_data: ProtoMessage) -> Result<Payload, String> {
        // let public_key = self.node_keys.public().encode_protobuf();
        let protocol_version = self.protocol_version.to_string();

        let mut buf = Vec::new();
        if let Ok(_) = message_data.encode(&mut buf) {
            if let Ok(signature) = self.node_keys.sign(&buf) {
                let payload = Payload {
                    signature,
                    protocol_version,
                    message: Some(message_data),
                };

                trace!(
                    "FeedsubCodec | create_and_sign_payload | payload {:?}",
                    payload
                );

                Ok(payload)
            } else {
                error!("FeedsubCodec | create_and_sign_payload | failed to sign data");
                Err("FeedsubCodec | create_and_sign_payload | failed to sign data".to_string())
            }
        } else {
            error!("FeedsubCodec | create_and_sign_payload | failed to encode message_data");
            Err(
                "FeedsubCodec | create_and_sign_payload | failed to encode message_data"
                    .to_string(),
            )
        }
    }

    pub fn encode_subscribe(&self, feeds: Vec<Arc<RwLock<Feed<FI>>>>) -> Result<Payload, String> {
        let feeds = feeds
            .iter()
            .map(|feed| feed.read().unwrap().convert_to_proto())
            .collect();

        let message_data = ProtoMessage {
            msg_data: Some(MsgData::Subscribe(ProtoSubscribe { feeds })),
        };
        self.create_and_sign_payload(message_data)
    }

    pub fn encode_unsubscribe(&self, feed_ids: &Vec<FI>) -> Result<Payload, String> {
        let feed_ids = feed_ids
            .into_iter()
            .map(|id| serde_json::to_string(&id).unwrap_or_default())
            .collect();

        let message_data = ProtoMessage {
            msg_data: Some(MsgData::Unsubscribe(ProtoUnsubscribe { feed_ids })),
        };

        self.create_and_sign_payload(message_data)
    }

    pub fn encode_update(
        &self,
        subscriptions: Option<Vec<Arc<RwLock<Feed<FI>>>>>,
        updates: Option<Vec<Update<FI>>>,
        max_payload_size: usize,
    ) -> Vec<Payload> {
        let mut payloads = Vec::new();
        let mut current_payload_subscriptions = Vec::new();
        let mut current_payload_updates = Vec::new();
        let mut current_size = 0;

        fn add_payload<FI: Debug + Send + Serialize + PartialOrd + FeedIdTrait>(
            codec: &FeedsubCodec<FI>,
            subscriptions: &Vec<ProtoFeed>,
            updates: &Vec<ProtoUpdateItem>,
        ) -> Result<Payload, String> {
            let message_data = ProtoMessage {
                msg_data: Some(MsgData::Update(ProtoUpdate {
                    subscriptions: subscriptions.clone(),
                    updates: updates.clone(),
                })),
            };

            codec.create_and_sign_payload(message_data)
        }

        // Process subscriptions
        if let Some(feeds) = subscriptions {
            for feed in feeds {
                let proto_feed = feed.read().unwrap().convert_to_proto();
                let encoded_size = &proto_feed.encoded_len();

                if current_size + encoded_size > max_payload_size
                    && !current_payload_subscriptions.is_empty()
                {
                    if let Ok(payload) = add_payload(self, &current_payload_subscriptions, &vec![])
                    {
                        payloads.push(payload);
                        current_payload_subscriptions = Vec::new();
                        current_size = 0;
                    }
                }
                current_payload_subscriptions.push(proto_feed);
                current_size += encoded_size;
            }
        }

        // Process updates
        if let Some(updates_vec) = updates {
            for update in updates_vec {
                let proto_update = update.convert_to_proto();
                let encoded_size = &proto_update.encoded_len();
                if current_size + encoded_size > max_payload_size
                    && (!current_payload_subscriptions.is_empty()
                        || !current_payload_updates.is_empty())
                {
                    if let Ok(payload) = add_payload(
                        self,
                        &current_payload_subscriptions,
                        &current_payload_updates,
                    ) {
                        payloads.push(payload);
                        current_payload_subscriptions = Vec::new();
                        current_payload_updates = Vec::new();
                        current_size = 0;
                    }
                }
                current_payload_updates.push(proto_update);
                current_size += encoded_size;
            }
        }

        // Add remaining data to payloads
        if !current_payload_subscriptions.is_empty() || !current_payload_updates.is_empty() {
            if let Ok(payload) = add_payload(
                self,
                &current_payload_subscriptions,
                &current_payload_updates,
            ) {
                payloads.push(payload);
            }
        }

        payloads
    }

    pub fn encode_task(&self, task: &Task<FI>) -> Result<Payload, String> {
        match &task.task_type {
            TaskType::TaskFetchUpdatesBySeq(update_task) => {
                let feed_id = serde_json::to_string(&update_task.feed_id).unwrap_or_default();
                let requested_seqs = update_task.requested_seqs.to_string();

                let task_data = TaskData::TaskFetchUpdatesBySeq(ProtoTaskFetchUpdatesbySeq {
                    feed_id,
                    requested_seqs,
                });

                let message_data = ProtoMessage {
                    msg_data: Some(MsgData::Task(ProtoTask {
                        task_data: Some(task_data),
                    })),
                };
                self.create_and_sign_payload(message_data)
            }
            TaskType::TaskFetchPeers(peer_task) => {
                let TaskFetchPeers { task_id, feed_ids } = peer_task;

                let feed_ids = feed_ids
                    .into_iter()
                    .map(|feed_id| serde_json::to_string(&feed_id).unwrap_or_default())
                    .collect();

                let task_data = TaskData::TaskFetchPeers(ProtoTaskFetchPeers {
                    task_id: task_id.as_bytes().to_vec(),
                    feed_ids,
                });

                let message_data = ProtoMessage {
                    msg_data: Some(MsgData::Task(ProtoTask {
                        task_data: Some(task_data),
                    })),
                };

                self.create_and_sign_payload(message_data)
            }
        }
    }

    pub fn encode_peer_cache(
        &self,
        subscriptions: Vec<(FI, Multiaddr)>,
        task_id: Uuid,
    ) -> Result<Payload, String> {
        let peer_cache = PeerCache::convert_to_proto(subscriptions, task_id);

        let message_data = ProtoMessage {
            msg_data: Some(MsgData::PeerCache(peer_cache)),
        };

        self.create_and_sign_payload(message_data)
    }

    pub fn encode_handshake(
        &self,
        peer_id: &Option<PeerId>,
        address: &Multiaddr,
        feeds: Vec<Arc<RwLock<Feed<FI>>>>,
        public_key: PublicKey,
    ) -> Result<Payload, String> {
        // let peer_id = peer_id.to_bytes();
        let peer_id = peer_id.as_ref().map(|id| id.to_bytes());

        let address = address.to_string();

        let feeds = feeds
            .iter()
            .map(|feed| feed.read().unwrap().convert_to_proto())
            .collect();

        let peer = ProtoPeer { peer_id, address };

        let public_key = public_key.encode_protobuf();

        let proto_handshake = ProtoHandshake {
            peer: Some(peer),
            feeds,
            public_key,
        };

        let message_data = ProtoMessage {
            msg_data: Some(MsgData::Handshake(proto_handshake)),
        };

        self.create_and_sign_payload(message_data)
    }

    pub fn encode_disconnect(&self, peers: &Vec<Peer<FI>>) -> Result<Payload, String> {
        let mut proto_peers = Vec::new();

        for peer in peers {
            proto_peers.push(peer.convert_to_proto());
        }

        let message_data = ProtoMessage {
            msg_data: Some(MsgData::Peers(proto::ProtoDisconnect {
                peers: proto_peers,
            })),
        };

        self.create_and_sign_payload(message_data)
    }
}

impl<FI> Debug for FeedsubCodec<FI>
where
    FI: FeedIdTrait,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("FeedsubCodec")
            .field("codec", &"codec")
            .finish()
    }
}

impl<FI> Encoder for FeedsubCodec<FI>
where
    FI: FeedIdTrait,
{
    type Item<'a> = Payload;

    type Error = io::Error;

    fn encode<'a>(&mut self, item: Self::Item<'a>, dst: &mut BytesMut) -> Result<(), Self::Error> {
        trace!("Encoder for FeedsubCodec::encode: {:?}", item);

        let len = item.encoded_len();
        debug!(
            "Encoder for FeedsubCodec::encode | Message Length: {:?}",
            len
        );

        // Ensure there's enough space for the length prefix plus the message
        const LENGTH_PREFIX_SIZE: usize = 4;
        if dst.remaining_mut() < len + LENGTH_PREFIX_SIZE {
            warn!("Encoder for FeedsubCodec::encode | Reserving additional space");
            dst.reserve(len + LENGTH_PREFIX_SIZE);
        }

        // Write the length prefix
        dst.put_u32(len as u32); // Adjust according to the size of your length prefix

        // Attempt to encode the item into the destination buffer
        match item.encode(dst) {
            Ok(_) => {
                trace!("Encoder for FeedsubCodec::encode | OK {:?}", dst);
                Ok(())
            }
            Err(e) => {
                error!("Encoder for FeedsubCodec::encode | {:?}", e);
                Err(io::Error::new(ErrorKind::Other, e))
            }
        }
    }
}

impl<FI> Decoder for FeedsubCodec<FI>
where
    FI: FeedIdTrait + Serialize + for<'a> Deserialize<'a>,
{
    type Item = ToBehaviourEvent;

    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        trace!("Decoder for FeedsubCodec::decode | {:?}", src);

        if src.len() > 0 {
            trace!(
                "Decoder for FeedsubCodec::decode | src bytes {:?}",
                src.len()
            );
        }

        const LENGTH_PREFIX_SIZE: usize = 4; // Size of the length prefix

        // Check if there's enough data to read the length prefix
        if src.len() < LENGTH_PREFIX_SIZE {
            return Ok(None); // Not enough data, wait for more
        }

        // Peek at the length prefix to find out how long the message is
        let msg_length = (&src[..LENGTH_PREFIX_SIZE]).get_u32() as usize;

        // Check if the full message has arrived
        if src.len() < LENGTH_PREFIX_SIZE + msg_length {
            debug!("Decoder for FeedsubCodec::decode | not enough data");
            return Ok(None); // Not enough data, wait for more
        }

        if (msg_length + LENGTH_PREFIX_SIZE) < src.len() {
            debug!("Decoder for FeedsubCodec::decode | found a merged payload");
        }

        // Remove the length prefix from the buffer
        let _ = src.split_to(LENGTH_PREFIX_SIZE);

        // Extract the message from the buffer
        let msg_buf = src.split_to(msg_length);

        match proto::Payload::decode(&mut msg_buf.freeze()) {
            Ok(payload) => {
                trace!(
                    "Decoder for FeedsubCodec::decode | payload OK {:?}",
                    msg_length
                );
                trace!(
                    "Decoder for FeedsubCodec::decode | payload OK {:?}",
                    payload
                );

                // Convert proto::Payload to FeedsubEvent
                match convert_payload_to_event(payload) {
                    Ok(event) => Ok(Some(event)),
                    Err(e) => {
                        error!("Decoder for FeedsubCodec::decode | event {:?}", e);
                        Err(e)
                    }
                }
            }
            Err(e) => {
                error!("Decoder for FeedsubCodec::decode | error {:?}", e);
                Ok(None) // Leave src unchanged in case of an error
            }
        }
    }
}

fn convert_payload_to_event(payload: proto::Payload) -> Result<ToBehaviourEvent, io::Error> {
    debug!("convert_payload_to_event");
    trace!("convert_payload_to_event | payload {:?}", payload);

    if let Some(msg) = payload.message {
        Ok(ToBehaviourEvent(msg, payload.signature))
    } else {
        Err(io::Error::other("convert_payload_to_event | no message"))
    }
}

use libp2p::identity;

use std::marker::PhantomData;
use std::time::Duration;

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Configuration for the [`Feedsub`] [`NetworkBehaviour`].
#[non_exhaustive]
#[derive(Debug, Clone)]
pub struct FeedsubConfig<FI> {
    /// Application-specific version of the protocol family used by the peer,
    /// e.g. `feedsub/0.1.0`
    protocol_version: String,
    /// The public key of the local node. To report on the wire.
    node_keys: identity::Keypair,
    /// Stop dialing outbound when we reach this limit
    max_outbound_connections: usize,
    /// How many inbound connections to allow before we start dropping them
    max_inbound_connections: usize,
    /// How many entries of discovered peers to keep per feed before we discard
    target_peers_per_feed: usize,
    /// The type representing different feed IDs.
    feed_id_type: PhantomData<FI>,
    /// Scheduler for background processes
    heartbeat_interval: Duration,
    /// best effort target bytes to send for updates
    target_payload_size: usize,
    /// how many feeds to send with the initial handshake
    max_feeds_to_handshake: usize,
    /// how many random feeds to fetch potential peers
    max_feeds_to_fetch_peer_cache: usize,
    /// how many alternative peers to send when this node is overloaded and can't accept inbound
    max_peers_to_send_on_overloaded: usize,
    /// how many seconds to wait for feed updates before disconnecting
    disconnect_timeout: i64,
}

impl<FI> FeedsubConfig<FI> {
    /// Creates a new configuration for the `Feedsub` behaviour that
    /// advertises the given protocol version and public key.

    pub fn new(node_keys: identity::Keypair) -> Self {
        let protocol_version = String::from("/feedsub/0.1.0");

        FeedsubConfig {
            protocol_version,
            node_keys,
            max_outbound_connections: 10,
            max_inbound_connections: 10,
            target_peers_per_feed: 10,
            target_payload_size: 32 * 1024,
            max_feeds_to_handshake: 100,
            feed_id_type: PhantomData,
            heartbeat_interval: Duration::from_secs(5),
            max_feeds_to_fetch_peer_cache: 10,
            max_peers_to_send_on_overloaded: 10,
            disconnect_timeout: 15,
        }
    }

    pub fn with_max_outbound_connections(mut self, limit: usize) -> Self {
        self.max_outbound_connections = limit;
        self
    }

    pub fn with_max_inbound_connections(mut self, limit: usize) -> Self {
        self.max_outbound_connections = limit;
        self
    }

    pub fn with_target_peers_per_feed(mut self, limit: usize) -> Self {
        self.target_peers_per_feed = limit;
        self
    }

    pub fn with_heartbeat_interval(mut self, secs: u64) -> Self {
        self.heartbeat_interval = Duration::from_secs(secs);
        self
    }

    pub fn with_target_payload_size(mut self, bytes: usize) -> Self {
        self.target_payload_size = bytes;
        self
    }
    pub fn with_max_feeds_to_handshake(mut self, limit: usize) -> Self {
        self.max_feeds_to_handshake = limit;
        self
    }
    pub fn with_max_feeds_to_fetch_peer_cache(mut self, limit: usize) -> Self {
        self.max_feeds_to_fetch_peer_cache = limit;
        self
    }
    pub fn with_max_peers_to_send_on_overloaded(mut self, limit: usize) -> Self {
        self.max_peers_to_send_on_overloaded = limit;
        self
    }
    pub fn with_disconnect_timeout(mut self, secs: i64) -> Self {
        self.disconnect_timeout = secs;
        self
    }

    pub fn protocol_version(&self) -> String {
        self.protocol_version.to_string()
    }
    pub fn node_keys(&self) -> &identity::Keypair {
        &self.node_keys
    }
    pub fn max_outbound_connections(&self) -> &usize {
        &self.max_outbound_connections
    }
    pub fn max_inbound_connections(&self) -> &usize {
        &self.max_inbound_connections
    }
    pub fn target_peers_per_feed(&self) -> &usize {
        &self.target_peers_per_feed
    }
    pub fn target_payload_size(&self) -> &usize {
        &self.target_payload_size
    }
    pub fn max_feeds_to_handshake(&self) -> &usize {
        &self.max_feeds_to_handshake
    }
    pub fn heartbeat_interval(&self) -> &Duration {
        &self.heartbeat_interval
    }
    pub fn max_feeds_to_fetch_peer_cache(&self) -> &usize {
        &self.max_feeds_to_fetch_peer_cache
    }
    pub fn max_peers_to_send_on_overloaded(&self) -> &usize {
        &self.max_peers_to_send_on_overloaded
    }
    pub fn disconnect_timeout(&self) -> &i64 {
        &self.disconnect_timeout
    }
}

use libp2p::identity::PublicKey;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::io;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, RwLock};
use std::{fmt::Display, hash::Hash};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::proto::{
    proto_feed_type, ProtoFeed, ProtoFeedType, ProtoFeedTypeFiniteSeq, ProtoFeedTypeInfiniteSeq,
    ProtoUpdateItem,
};
use crate::range_set::RangeSet;

#[derive(Debug, Clone)]
pub struct Feed<FI: FeedIdTrait> {
    pub id: FI,
    pub feed_type: Option<FeedType>,
    pub cache_enabled: bool,
}

impl<FI> Feed<FI>
where
    FI: FeedIdTrait,
{
    pub fn convert_to_proto(&self) -> ProtoFeed {
        match &self.feed_type {
            Some(feed_type) => {
                let feed_type = match &feed_type {
                    FeedType::InfiniteSeq {
                        latest_seq,
                        inventory,
                    } => ProtoFeedType {
                        type_data: Some(proto_feed_type::TypeData::TypeInfiniteSeq(
                            ProtoFeedTypeInfiniteSeq {
                                latest_seq: latest_seq.to_u64(),
                                inventory: inventory.to_string(),
                            },
                        )),
                    },
                    FeedType::FiniteSeq {
                        last_seq,
                        inventory,
                    } => ProtoFeedType {
                        type_data: Some(proto_feed_type::TypeData::TypeFiniteSeq(
                            ProtoFeedTypeFiniteSeq {
                                last_seq: last_seq.to_u64(),
                                inventory: inventory.to_string(),
                            },
                        )),
                    },
                };

                ProtoFeed {
                    id: serde_json::to_string(&self.id).unwrap_or_default(),
                    feed_type: Some(feed_type),
                }
            }
            None => ProtoFeed {
                id: serde_json::to_string(&self.id).unwrap_or_default(),
                feed_type: None,
            },
        }
    }

    pub fn convert_from_proto(proto: ProtoFeed) -> Result<Self, io::Error> {
        let id: FI = serde_json::from_str(&proto.id)
            .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?;

        let feed_type = match proto.feed_type.and_then(|ft| ft.type_data) {
            Some(proto_feed_type::TypeData::TypeInfiniteSeq(proto_seq)) => {
                Some(FeedType::InfiniteSeq {
                    latest_seq: Count::from(proto_seq.latest_seq),
                    inventory: RangeSet::from(proto_seq.inventory),
                })
            }
            Some(proto_feed_type::TypeData::TypeFiniteSeq(proto_seq)) => {
                Some(FeedType::FiniteSeq {
                    last_seq: Count::from(proto_seq.last_seq),
                    inventory: RangeSet::from(proto_seq.inventory),
                })
            }
            None => None,
        };
        let cache_enabled = id.is_cache_enabled();

        Ok(Feed {
            id,
            feed_type,
            cache_enabled,
        })
    }
}

impl<FI: FeedIdTrait> Feed<FI> {
    pub fn default(id: FI) -> Self {
        let cache_enabled = id.is_cache_enabled();
        Self {
            id,
            feed_type: None,
            cache_enabled,
        }
    }

    pub fn new(id: FI, feed_type: Option<FeedType>) -> Self {
        let cache_enabled = id.is_cache_enabled();

        Self {
            id,
            feed_type,
            cache_enabled,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Update<FI: FeedIdTrait> {
    pub feed_id: FI,
    pub seq: u64,
    pub sig: Vec<u8>,
    pub data: Vec<u8>,
}

impl<FI> Update<FI>
where
    FI: FeedIdTrait,
{
    pub fn new(
        feed: &mut Arc<RwLock<Feed<FI>>>,
        data: Vec<u8>,
        seq: Option<u64>,
        sig: Vec<u8>,
    ) -> Option<Self> {
        let feed_id = feed.read().unwrap().id.clone();

        debug!("Update::new |  feed_id {:?} seq {:?}", feed_id, seq);

        let feed_id = feed.read().unwrap().id.clone();

        match feed.write().unwrap().feed_type.as_mut() {
            Some(feed_type) => match feed_type {
                FeedType::InfiniteSeq {
                    ref mut latest_seq,
                    ref mut inventory,
                } => {
                    let next_seq = if seq.is_none() {
                        latest_seq.increment()
                    } else {
                        seq.unwrap()
                    };

                    inventory.insert_num(next_seq);

                    Some(Self {
                        feed_id,
                        seq: next_seq,
                        sig,
                        data,
                    })
                }
                FeedType::FiniteSeq { .. } => {
                    if seq.is_none() {
                        error!("Missing seq {:?}", feed);
                        return None;
                    }

                    Some(Self {
                        feed_id,
                        seq: seq.unwrap(),
                        sig,
                        data,
                    })
                }
            },
            None => {
                error!("Update::new | missing feed_type {:?}", feed_id);
                None
            }
        }
    }

    pub fn convert_to_proto(&self) -> ProtoUpdateItem {
        ProtoUpdateItem {
            feed_id: serde_json::to_string(&self.feed_id).unwrap_or_default(),
            seq: self.seq,
            sig: self.sig.clone(),
            data: self.data.clone(),
        }
    }
}

// #[derive(Debug, Clone)]
// pub enum Fetch {
//     All,
//     Tail(u64),
//     None,
// }

#[derive(Debug, Clone)]
pub enum FeedType {
    InfiniteSeq {
        latest_seq: Count,
        inventory: RangeSet,
    },
    FiniteSeq {
        last_seq: Count,
        inventory: RangeSet,
    },
}

impl FeedType {
    pub fn new_infinite_seq(latest_seq: u64, inventory: &str) -> Self {
        FeedType::InfiniteSeq {
            latest_seq: Count::from(latest_seq),
            inventory: RangeSet::from(inventory),
        }
    }
    pub fn new_finite_seq(last_seq: u64, inventory: &str) -> Self {
        FeedType::FiniteSeq {
            last_seq: Count::from(last_seq),
            inventory: RangeSet::from(inventory),
        }
    }
}

impl PartialEq for FeedType {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (
                FeedType::InfiniteSeq {
                    latest_seq: seq1,
                    inventory: inv1,
                },
                FeedType::InfiniteSeq {
                    latest_seq: seq2,
                    inventory: inv2,
                },
            ) => seq1 == seq2 && inv1 == inv2,
            (
                FeedType::FiniteSeq {
                    last_seq: seq1,
                    inventory: inv1,
                },
                FeedType::FiniteSeq {
                    last_seq: seq2,
                    inventory: inv2,
                },
            ) => seq1 == seq2 && inv1 == inv2,
            _ => false,
        }
    }
}

impl Eq for FeedType {}

#[derive(Debug, Clone)]
pub struct Count(Arc<AtomicU64>);

impl Count {
    pub fn to_u64(&self) -> u64 {
        self.0.load(Ordering::SeqCst)
    }

    pub fn increment(&mut self) -> u64 {
        self.0.fetch_add(1, Ordering::SeqCst) + 1
    }

    pub fn set(&mut self, value: u64) -> u64 {
        let atomic_value = AtomicU64::new(value);
        self.0 = Arc::new(atomic_value);
        self.to_u64()
    }
}

impl PartialEq for Count {
    fn eq(&self, other: &Self) -> bool {
        self.to_u64() == other.to_u64()
    }
}

impl From<u64> for Count {
    fn from(value: u64) -> Self {
        Count(Arc::new(AtomicU64::new(value)))
    }
}

pub trait FeedIdTrait:
    Display
    + Clone
    + Eq
    + PartialEq
    + Hash
    + Send
    + Sync
    + Debug
    + Serialize
    + for<'a> Deserialize<'a>
    + Ord
{
    fn is_cache_enabled(&self) -> bool;
    fn public_key(&self) -> PublicKey;
}

use rand::{distributions::Uniform, Rng};
use std::cmp::Ordering::{self, *};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};

use crate::behavior::UpdateFetcher;
use crate::{proto::Payload, protocol::FeedsubCodec, Feed, FeedIdTrait, FeedType, Feedsub};

#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Clone, PartialEq)]
pub enum UpdateResult {
    LocalNotFound,
    LocalUpdated,
    RemoteStale,
    LocalUnchanged,
    DataError,
}

#[derive(Debug, Clone)]
pub struct Subscriptions<FI: FeedIdTrait>(HashMap<FI, Arc<RwLock<Feed<FI>>>>);

impl<FI: FeedIdTrait> Subscriptions<FI> {
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    pub fn get_all(&self) -> Vec<Arc<RwLock<Feed<FI>>>> {
        trace!("Subscriptions::get_all");

        let mut sub_vec = Vec::new();
        let my_subs = &self.0;
        for sub in my_subs {
            sub_vec.push(sub.1.clone());
        }

        trace!("Subscriptions::get_all {:?}", sub_vec);

        sub_vec
    }

    pub fn get_some(&self, limit: &usize) -> Vec<Arc<RwLock<Feed<FI>>>> {
        trace!("Subscriptions::get_some");

        let my_subs = &self.0;
        let subs_len = my_subs.len();

        // If the limit is greater than or equal to the total number, return all.
        if limit >= &subs_len {
            return my_subs.values().cloned().collect();
        }

        let mut rng = rand::thread_rng();
        let range = Uniform::new(0, subs_len);
        let mut indices = std::collections::HashSet::new();

        // Generate unique random indices
        while &indices.len() < limit {
            indices.insert(rng.sample(range));
        }

        // Collect feeds at the random indices
        my_subs
            .values()
            .enumerate()
            .filter(|(idx, _)| indices.contains(idx))
            .map(|(_, feed)| feed.clone())
            .collect()
    }

    pub fn get_incomplete(&self) -> Vec<Arc<RwLock<Feed<FI>>>> {
        let mut incomplete = Vec::new();

        for sub in self.get_all() {
            let is_incomplete = {
                let sub_read = sub.read().unwrap(); // Acquire lock
                match &sub_read.feed_type {
                    Some(feed_type) => match feed_type {
                        FeedType::InfiniteSeq {
                            latest_seq,
                            inventory,
                        } => !inventory.is_complete(latest_seq.to_u64()),
                        FeedType::FiniteSeq {
                            last_seq,
                            inventory,
                        } => !inventory.is_complete(last_seq.to_u64()) || last_seq.to_u64() == 0,
                    },
                    None => {
                        debug!(
                            "Subscriptions::get_incomplete | missing FeedType | {:?}",
                            sub
                        );
                        true
                    }
                }
            };

            if is_incomplete {
                incomplete.push(sub);
            }
        }

        incomplete
    }

    pub fn get_incomplete_ids(&self) -> Vec<FI> {
        let mut incomplete = Vec::new();

        for sub in self.get_all() {
            let is_incomplete = {
                let sub_read = sub.read().unwrap();

                match &sub_read.feed_type {
                    Some(feed_type) => match feed_type {
                        FeedType::InfiniteSeq { .. } => (true, sub_read.id.clone()),
                        FeedType::FiniteSeq {
                            last_seq,
                            inventory,
                        } => (
                            !inventory.is_complete(last_seq.to_u64()) || last_seq.to_u64() == 0,
                            sub_read.id.clone(),
                        ),
                    },
                    None => {
                        debug!(
                            "Subscriptions::get_incomplete | missing FeedType | {:?}",
                            sub
                        );
                        (true, sub_read.id.clone())
                    }
                }
            };

            if is_incomplete.0 {
                incomplete.push(is_incomplete.1);
            }
        }
        debug!("Subscriptions::get_incomplete | final | {:?}", incomplete);
        incomplete
    }

    pub fn get_some_incomplete_ids(&self, limit: &usize) -> Vec<FI> {
        let mut incomplete = Vec::new();

        for sub in self.get_all() {
            let is_incomplete = {
                let sub_read = sub.read().unwrap();

                match &sub_read.feed_type {
                    Some(feed_type) => match feed_type {
                        FeedType::InfiniteSeq { .. } => (true, sub_read.id.clone()),
                        FeedType::FiniteSeq {
                            last_seq,
                            inventory,
                        } => (
                            !inventory.is_complete(last_seq.to_u64()) || last_seq.to_u64() == 0,
                            sub_read.id.clone(),
                        ),
                    },
                    None => {
                        debug!(
                            "Subscriptions::get_incomplete | missing FeedType | {:?}",
                            sub
                        );
                        (true, sub_read.id.clone())
                    }
                }
            };

            if is_incomplete.0 {
                incomplete.push(is_incomplete.1);
            }
        }
        debug!("Subscriptions::get_incomplete | final | {:?}", incomplete);

        let my_subs = &incomplete;
        let subs_len = my_subs.len();

        // If the limit is greater than or equal to the total number, return all.
        if limit >= &subs_len {
            return my_subs.to_vec();
        }

        let mut rng = rand::thread_rng();
        let range = Uniform::new(0, subs_len);
        let mut indices = std::collections::HashSet::new();

        // Generate unique random indices
        while &indices.len() < limit {
            indices.insert(rng.sample(range));
        }

        // Collect feeds at the random indices
        my_subs
            .into_iter()
            .enumerate()
            .filter(|(idx, _)| indices.contains(idx))
            .map(|(_, feed)| feed.clone())
            .collect()
    }

    pub fn get(&self, feed_id: &FI) -> Option<&Arc<RwLock<Feed<FI>>>> {
        debug!("Subscriptions::get: {:?}", feed_id);
        self.0.get(feed_id)
    }

    pub fn get_mut(&mut self, feed_id: &FI) -> Option<&mut Arc<RwLock<Feed<FI>>>> {
        debug!("Subscriptions::get: {:?}", feed_id);
        self.0.get_mut(feed_id)
    }

    pub fn get_or_new_mut(&mut self, feed_id: &FI) -> &mut Arc<RwLock<Feed<FI>>> {
        debug!("Subscriptions::get_or_new_mut: {:?}", feed_id);

        self.0
            .entry(feed_id.clone())
            .or_insert_with(|| Arc::new(RwLock::new(Feed::default(feed_id.clone()))))
    }

    pub fn update_seq(&mut self, second_feed: &Arc<RwLock<Feed<FI>>>) -> UpdateResult {
        debug!("Subscriptions::update_seq");

        let second_feed = second_feed.write().unwrap();

        let feed_id = &second_feed.id;

        if let Some(first_feed_lock) = self.0.get_mut(feed_id) {
            let mut first_feed = first_feed_lock.write().unwrap();

            debug!("Subscriptions::update_seq | First feed: {:?}", first_feed,);
            debug!("Subscriptions::update_seq | Second feed: {:?}", second_feed,);

            match (&mut first_feed.feed_type, &second_feed.feed_type) {
                (Some(first_feed_type), Some(second_feed_type)) => {
                    match (first_feed_type, &second_feed_type) {
                        (
                            FeedType::InfiniteSeq {
                                latest_seq: ref mut local_latest_seq, ..
                                // inventory: local_inventory,
                            },
                            FeedType::InfiniteSeq {
                                latest_seq: remote_latest_seq, ..
                                // inventory: remote_inventory,
                            },
                        ) => match local_latest_seq.to_u64().cmp(&remote_latest_seq.to_u64()) {
                            std::cmp::Ordering::Less => {
                                debug!("Subscriptions::update | local_latest_seq is Less");
                                local_latest_seq.set(remote_latest_seq.to_u64());
                                UpdateResult::LocalUpdated
                            }
                            std::cmp::Ordering::Equal => {
                                debug!("Subscriptions::update | local_latest_seq is Equal");
                                UpdateResult::LocalUnchanged
                            }
                            std::cmp::Ordering::Greater => {
                                debug!("Subscriptions::update | local_latest_seq is Greater");
                                UpdateResult::RemoteStale
                            }
                        },
                        (
                            FeedType::FiniteSeq {
                                last_seq: ref mut local_last_seq, ..
                                // inventory: local_inventory,
                            },
                            FeedType::FiniteSeq {
                                last_seq: remote_last_seq, ..
                                // inventory: remote_inventory,
                            },
                        ) => match local_last_seq.to_u64().cmp(&remote_last_seq.to_u64()) {
                            std::cmp::Ordering::Less => {
                                debug!("Subscriptions::update | local_last_seq is Less");
                                local_last_seq.set(remote_last_seq.to_u64());
                                UpdateResult::LocalUpdated
                            }
                            std::cmp::Ordering::Equal => {
                                debug!("Subscriptions::update | local_last_seq is Equal");
                                UpdateResult::LocalUnchanged
                            }
                            std::cmp::Ordering::Greater => {
                                debug!("Subscriptions::update | local_last_seq is Greater");
                                UpdateResult::RemoteStale
                            }
                        },
                        _ => UpdateResult::DataError,
                    }
                }

                (Some(_), None) => {
                    // The case when the remote is subscribing but hasn't yet received the actual feed
                    debug!("Subscriptions::update | Second feed is stale");
                    return UpdateResult::RemoteStale;
                }

                (None, Some(second_feed_type)) => {
                    // The case when we haven't yet received the actual feed
                    // Update our feed_type
                    debug!("Subscriptions::update | First feed is stale");
                    match second_feed_type {
                        FeedType::InfiniteSeq {
                            latest_seq, ..
                            // inventory,
                        } => {
                            let new_feed_type = FeedType::new_infinite_seq(latest_seq.to_u64(), "0");
                            first_feed.feed_type = Some(new_feed_type);
                            UpdateResult::LocalUpdated
                        },
                        FeedType::FiniteSeq { .. } => {
                            error!("Subscriptions::update | FeedType::FiniteSeq is incomplete");
                            UpdateResult::DataError
                        }
                    }
                }
                (None, None) => {
                    // local and remote are both stale
                    return UpdateResult::LocalUnchanged;
                }
            }
        } else {
            debug!("Subscriptions::update | Not Found {:?}", second_feed);
            return UpdateResult::LocalNotFound;
        }
    }

    pub fn compare_seq(&self, second_feed: &Arc<RwLock<Feed<FI>>>) -> Result<Ordering, String> {
        debug!("Subscriptions::compare_seq");
        let second_feed = second_feed.read().unwrap();

        let first_feed = match self.0.get(&second_feed.id) {
            Some(feed) => feed,
            None => return Ok(Greater),
        };

        debug!("Subscriptions::compare_seq | First feed: {:?}", first_feed,);
        debug!(
            "Subscriptions::compare_seq | Second feed: {:?}",
            second_feed,
        );

        match (
            &first_feed.read().unwrap().feed_type,
            &second_feed.feed_type,
        ) {
            (None, Some(_)) => Ok(Greater),
            (Some(_), None) => Ok(Less),
            (None, None) => Ok(Equal),

            (Some(first_feed_type), Some(second_feed_type)) => {
                match (first_feed_type, second_feed_type) {
                    (
                        FeedType::InfiniteSeq {
                            latest_seq: first_latest_seq,
                            ..
                        },
                        FeedType::InfiniteSeq {
                            latest_seq: second_latest_seq,
                            ..
                        },
                    ) => {
                        let seq_compare =
                            second_latest_seq.to_u64().cmp(&first_latest_seq.to_u64());

                        debug!(
                            "Subscriptions::compare_seq | first: {:?} second: {:?} | seq_compare {:?}", first_latest_seq, second_latest_seq,
                            seq_compare
                        );

                        return Ok(seq_compare);
                    }
                    (
                        FeedType::FiniteSeq {
                            last_seq: first_last_seq,
                            ..
                        },
                        FeedType::FiniteSeq {
                            last_seq: second_last_seq,
                            ..
                        },
                    ) => {
                        let seq_compare = second_last_seq.to_u64().cmp(&first_last_seq.to_u64());

                        return Ok(seq_compare);
                    }
                    _ => Err("Data Error".to_string()),
                }
            }
        }
    }

    pub fn compare_inventory(
        &self,
        second_feed: &Arc<RwLock<Feed<FI>>>,
    ) -> Result<Ordering, String> {
        debug!("Subscriptions::compare_inventory");
        let second_feed = second_feed.read().unwrap();

        let first_feed = match self.0.get(&second_feed.id) {
            Some(feed) => feed,
            None => return Ok(Greater),
        };

        debug!(
            "Subscriptions::compare_inventory | First feed: {:?}",
            first_feed,
        );
        debug!(
            "Subscriptions::compare_inventory | Second feed: {:?}",
            second_feed,
        );

        match (
            &first_feed.read().unwrap().feed_type,
            &second_feed.feed_type,
        ) {
            (None, Some(_)) => Ok(Greater),
            (Some(_), None) => Ok(Less),
            (None, None) => Ok(Equal),

            (Some(first_feed_type), Some(second_feed_type)) => {
                match (first_feed_type, second_feed_type) {
                    (
                        FeedType::InfiniteSeq {
                            inventory: first_inventory,
                            ..
                        },
                        FeedType::InfiniteSeq {
                            inventory: second_inventory,
                            ..
                        },
                    ) => {
                        let inventory_compare = second_inventory.compare(first_inventory);

                        debug!(
                            "Subscriptions::compare | first: {:?} second: {:?} | inventory_compare {:?}", first_inventory, second_inventory,
                            inventory_compare
                        );

                        return inventory_compare;
                    }
                    (
                        FeedType::FiniteSeq {
                            inventory: first_inventory,
                            ..
                        },
                        FeedType::FiniteSeq {
                            inventory: second_inventory,
                            ..
                        },
                    ) => {
                        let inventory_compare = second_inventory.compare(first_inventory);
                        debug!(
                            "Subscriptions::compare | first: {:?} second: {:?} | inventory_compare {:?}", first_inventory, second_inventory,
                            inventory_compare
                        );
                        return inventory_compare;
                    }
                    _ => Err("Data Error".to_string()),
                }
            }
        }
    }

    pub fn insert(&mut self, subscription: Arc<RwLock<Feed<FI>>>) {
        debug!("Subscriptions::insert {:?}", subscription);

        let feed_id = subscription.read().unwrap().id.clone();
        self.0.insert(feed_id, subscription);
    }

    pub fn remove_feed_ids(&mut self, feed_ids: &Vec<FI>) {
        for feed_id in feed_ids {
            self.0.remove(feed_id);
        }
    }

    pub fn is_subscribed(&self, feed_id: &FI) -> bool {
        self.0.contains_key(feed_id)
    }

    pub fn into_payload<UF: UpdateFetcher<FI, UF>>(
        &self,
        feedsub: &Feedsub<FI, UF>,
    ) -> Result<Payload, String> {
        let to_send = self.get_all();

        debug!("Subscriptions::into_payload {:?}", to_send);

        let codec = FeedsubCodec::<FI>::new(
            feedsub.config.node_keys().clone(),
            feedsub.config.protocol_version(),
        );

        codec.encode_subscribe(to_send)
    }
}

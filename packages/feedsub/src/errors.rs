#[derive(Debug)]
pub enum FeedsubError {
    Generic(String),
}

impl std::error::Error for FeedsubError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            FeedsubError::Generic(_) => None,
        }
    }
}

impl std::fmt::Display for FeedsubError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            FeedsubError::Generic(msg) => write!(f, "{:?}", msg),
        }
    }
}

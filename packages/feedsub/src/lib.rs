pub use self::behavior::{Feedsub, FeedsubEvent, UpdateFetcher, UpdateItem};
pub use self::config::FeedsubConfig;
pub use self::connections::{Connection, ConnectionStatus, Connections, Direction};
pub use self::errors::FeedsubError;
pub use self::feeds::{Count, Feed, FeedIdTrait, FeedType, Update};
pub use self::range_set::RangeSet;
pub use self::task_manager::tasks::{
    Task, TaskFetchUpdatesBySeqData, TaskType::TaskFetchUpdatesBySeq,
};

mod behavior;
mod config;
mod connections;
mod errors;
mod feeds;
mod handler;
mod peers;
mod protocol;
mod range_set;
mod subscriptions;
mod task_manager;

mod proto {
    include!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/proto/generated/payload.rs"
    ));
}
